package com.orkli.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the circuito database table.
 * 
 */
@Embeddable
public class CircuitoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="cliente_id")
	private String clienteId;

	@Column(name="id_circuito")
	private String idCircuito;

	@Column(name="referencia_id")
	private String referenciaId;

	private String ubicacion;

	public CircuitoPK() {
	}
	public String getClienteId() {
		return this.clienteId;
	}
	public void setClienteId(String clienteId) {
		this.clienteId = clienteId;
	}
	public String getIdCircuito() {
		return this.idCircuito;
	}
	public void setIdCircuito(String idCircuito) {
		this.idCircuito = idCircuito;
	}
	public String getReferenciaId() {
		return this.referenciaId;
	}
	public void setReferenciaId(String referenciaId) {
		this.referenciaId = referenciaId;
	}
	public String getUbicacion() {
		return this.ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CircuitoPK)) {
			return false;
		}
		CircuitoPK castOther = (CircuitoPK)other;
		return 
			this.clienteId.equals(castOther.clienteId)
			&& this.idCircuito.equals(castOther.idCircuito)
			&& this.referenciaId.equals(castOther.referenciaId)
			&& this.ubicacion.equals(castOther.ubicacion);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.clienteId.hashCode();
		hash = hash * prime + this.idCircuito.hashCode();
		hash = hash * prime + this.referenciaId.hashCode();
		hash = hash * prime + this.ubicacion.hashCode();
		
		return hash;
	}
}