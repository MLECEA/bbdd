package com.orkli.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the ubicacion database table.
 * 
 */
@Entity
@Table(name="ubicacion")
@NamedQuery(name="Ubicacion.findAll", query="SELECT u FROM Ubicacion u")
public class Ubicacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_ubicacion")
	private String idUbicacion;

	private short activo;

	private String descripcion;

	private String localizacion;

	@Column(name="Ubi_Dataareaid")
	private String ubi_Dataareaid;

	@Column(name="ubi_des_loc")
	private String ubiDesLoc;

	@Column(name="Ubi_DesLoc")
	private String ubi_DesLoc;

	@Column(name="Ubi_Modo")
	private String ubi_Modo;

	@Column(name="Ubi_MosPenFac")
	private boolean ubi_MosPenFac;

	@Column(name="Ubi_MsgPenFac")
	private String ubi_MsgPenFac;

	@Column(name="Ubi_NotEtiVacias")
	private boolean ubi_NotEtiVacias;

	@Column(name="Ubi_RegTrazAX")
	private boolean ubi_RegTrazAX;

	//bi-directional many-to-one association to UsuarioUbicacion
	@OneToMany(mappedBy="ubicacion")
	private List<UsuarioUbicacion> usuarioUbicacions;

	//bi-directional many-to-many association to SubUbicacion
	@ManyToMany
	@JoinTable(
		name="ubicacion_sububicacion"
		, joinColumns={
			@JoinColumn(name="id_ubicacion")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_subUbicacion")
			}
		)
	private List<SubUbicacion> subUbicacions;

	public Ubicacion() {
	}

	public String getIdUbicacion() {
		return this.idUbicacion;
	}

	public void setIdUbicacion(String idUbicacion) {
		this.idUbicacion = idUbicacion;
	}

	public short getActivo() {
		return this.activo;
	}

	public void setActivo(short activo) {
		this.activo = activo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getLocalizacion() {
		return this.localizacion;
	}

	public void setLocalizacion(String localizacion) {
		this.localizacion = localizacion;
	}

	public Object getUbi_Dataareaid() {
		return this.ubi_Dataareaid;
	}

	public void setUbi_Dataareaid(String ubi_Dataareaid) {
		this.ubi_Dataareaid = ubi_Dataareaid;
	}

	public String getUbiDesLoc() {
		return this.ubiDesLoc;
	}

	public void setUbiDesLoc(String ubiDesLoc) {
		this.ubiDesLoc = ubiDesLoc;
	}

	public String getUbi_DesLoc() {
		return this.ubi_DesLoc;
	}

	public void setUbi_DesLoc(String ubi_DesLoc) {
		this.ubi_DesLoc = ubi_DesLoc;
	}

	public Object getUbi_Modo() {
		return this.ubi_Modo;
	}

	public void setUbi_Modo(String ubi_Modo) {
		this.ubi_Modo = ubi_Modo;
	}

	public boolean getUbi_MosPenFac() {
		return this.ubi_MosPenFac;
	}

	public void setUbi_MosPenFac(boolean ubi_MosPenFac) {
		this.ubi_MosPenFac = ubi_MosPenFac;
	}

	public Object getUbi_MsgPenFac() {
		return this.ubi_MsgPenFac;
	}

	public void setUbi_MsgPenFac(String ubi_MsgPenFac) {
		this.ubi_MsgPenFac = ubi_MsgPenFac;
	}

	public boolean getUbi_NotEtiVacias() {
		return this.ubi_NotEtiVacias;
	}

	public void setUbi_NotEtiVacias(boolean ubi_NotEtiVacias) {
		this.ubi_NotEtiVacias = ubi_NotEtiVacias;
	}

	public boolean getUbi_RegTrazAX() {
		return this.ubi_RegTrazAX;
	}

	public void setUbi_RegTrazAX(boolean ubi_RegTrazAX) {
		this.ubi_RegTrazAX = ubi_RegTrazAX;
	}

	public List<UsuarioUbicacion> getUsuarioUbicacions() {
		return this.usuarioUbicacions;
	}

	public void setUsuarioUbicacions(List<UsuarioUbicacion> usuarioUbicacions) {
		this.usuarioUbicacions = usuarioUbicacions;
	}

	public UsuarioUbicacion addUsuarioUbicacion(UsuarioUbicacion usuarioUbicacion) {
		getUsuarioUbicacions().add(usuarioUbicacion);
		usuarioUbicacion.setUbicacion(this);

		return usuarioUbicacion;
	}

	public UsuarioUbicacion removeUsuarioUbicacion(UsuarioUbicacion usuarioUbicacion) {
		getUsuarioUbicacions().remove(usuarioUbicacion);
		usuarioUbicacion.setUbicacion(null);

		return usuarioUbicacion;
	}

	public List<SubUbicacion> getSubUbicacions() {
		return this.subUbicacions;
	}

	public void setSubUbicacions(List<SubUbicacion> subUbicacions) {
		this.subUbicacions = subUbicacions;
	}

}