package com.orkli.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the TEmpresa database table.
 * 
 */
@Entity
@NamedQuery(name="TEmpresa.findAll", query="SELECT t FROM TEmpresa t")
public class TEmpresa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="Emp_Idd")
	private int emp_Idd;

	@Column(name="Emp_EMa")
	private String emp_EMa;

	@Lob
	@Column(name="Emp_Log")
	private byte[] emp_Log;

	@Column(name="Emp_LogURL")
	private String emp_LogURL;

	@Column(name="Emp_Nom")
	private String emp_Nom;

	@Column(name="Emp_Tel")
	private String emp_Tel;

	public TEmpresa() {
	}

	public int getEmp_Idd() {
		return this.emp_Idd;
	}

	public void setEmp_Idd(int emp_Idd) {
		this.emp_Idd = emp_Idd;
	}

	public Object getEmp_EMa() {
		return this.emp_EMa;
	}

	public void setEmp_EMa(String emp_EMa) {
		this.emp_EMa = emp_EMa;
	}

	public byte[] getEmp_Log() {
		return this.emp_Log;
	}

	public void setEmp_Log(byte[] emp_Log) {
		this.emp_Log = emp_Log;
	}

	public Object getEmp_LogURL() {
		return this.emp_LogURL;
	}

	public void setEmp_LogURL(String emp_LogURL) {
		this.emp_LogURL = emp_LogURL;
	}

	public Object getEmp_Nom() {
		return this.emp_Nom;
	}

	public void setEmp_Nom(String emp_Nom) {
		this.emp_Nom = emp_Nom;
	}

	public Object getEmp_Tel() {
		return this.emp_Tel;
	}

	public void setEmp_Tel(String emp_Tel) {
		this.emp_Tel = emp_Tel;
	}

}