package com.orkli.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the TAccionesReports database table.
 * 
 */
@Entity
@Table(name="TAccionesReports")
@NamedQuery(name="TAccionesReport.findAll", query="SELECT t FROM TAccionesReport t")
public class TAccionesReport implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="AccRep_Idd")
	private String accRep_Idd;

	@Column(name="AccRep_Act")
	private short accRep_Act;

	@Column(name="AccRep_CCir")
	private boolean accRep_CCir;

	@Column(name="AccRep_Des")
	private String accRep_Des;

	@Column(name="AccRep_LogURL")
	private String accRep_LogURL;

	@Column(name="AccRep_NVar")
	private int accRep_NVar;

	@Column(name="AccRep_QRVar")
	private int accRep_QRVar;

	@Column(name="AccRep_Rep")
	private String accRep_Rep;

	@Column(name="AccRep_Ser")
	private String accRep_Ser;

	@Column(name="AccRep_Tip")
	private String accRep_Tip;

	@Column(name="AccRep_V1Def")
	private String accRep_V1Def;

	@Column(name="AccRep_V1Txt")
	private String accRep_V1Txt;

	@Column(name="AccRep_V2Def")
	private String accRep_V2Def;

	@Column(name="AccRep_V2Txt")
	private String accRep_V2Txt;

	@Column(name="AccRep_V3Def")
	private String accRep_V3Def;

	@Column(name="AccRep_V3Txt")
	private String accRep_V3Txt;

	//bi-directional many-to-many association to SgaUsuario
	@ManyToMany
	@JoinTable(
		name="usuario_AccReports"
		, joinColumns={
			@JoinColumn(name="id_accrep")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_usuario")
			}
		)
	private List<SgaUsuario> sgaUsuarios;

	public TAccionesReport() {
	}

	public String getAccRep_Idd() {
		return this.accRep_Idd;
	}

	public void setAccRep_Idd(String accRep_Idd) {
		this.accRep_Idd = accRep_Idd;
	}

	public short getAccRep_Act() {
		return this.accRep_Act;
	}

	public void setAccRep_Act(short accRep_Act) {
		this.accRep_Act = accRep_Act;
	}

	public boolean getAccRep_CCir() {
		return this.accRep_CCir;
	}

	public void setAccRep_CCir(boolean accRep_CCir) {
		this.accRep_CCir = accRep_CCir;
	}

	public Object getAccRep_Des() {
		return this.accRep_Des;
	}

	public void setAccRep_Des(String accRep_Des) {
		this.accRep_Des = accRep_Des;
	}

	public Object getAccRep_LogURL() {
		return this.accRep_LogURL;
	}

	public void setAccRep_LogURL(String accRep_LogURL) {
		this.accRep_LogURL = accRep_LogURL;
	}

	public int getAccRep_NVar() {
		return this.accRep_NVar;
	}

	public void setAccRep_NVar(int accRep_NVar) {
		this.accRep_NVar = accRep_NVar;
	}

	public int getAccRep_QRVar() {
		return this.accRep_QRVar;
	}

	public void setAccRep_QRVar(int accRep_QRVar) {
		this.accRep_QRVar = accRep_QRVar;
	}

	public Object getAccRep_Rep() {
		return this.accRep_Rep;
	}

	public void setAccRep_Rep(String accRep_Rep) {
		this.accRep_Rep = accRep_Rep;
	}

	public Object getAccRep_Ser() {
		return this.accRep_Ser;
	}

	public void setAccRep_Ser(String accRep_Ser) {
		this.accRep_Ser = accRep_Ser;
	}

	public Object getAccRep_Tip() {
		return this.accRep_Tip;
	}

	public void setAccRep_Tip(String accRep_Tip) {
		this.accRep_Tip = accRep_Tip;
	}

	public Object getAccRep_V1Def() {
		return this.accRep_V1Def;
	}

	public void setAccRep_V1Def(String accRep_V1Def) {
		this.accRep_V1Def = accRep_V1Def;
	}

	public Object getAccRep_V1Txt() {
		return this.accRep_V1Txt;
	}

	public void setAccRep_V1Txt(String accRep_V1Txt) {
		this.accRep_V1Txt = accRep_V1Txt;
	}

	public Object getAccRep_V2Def() {
		return this.accRep_V2Def;
	}

	public void setAccRep_V2Def(String accRep_V2Def) {
		this.accRep_V2Def = accRep_V2Def;
	}

	public Object getAccRep_V2Txt() {
		return this.accRep_V2Txt;
	}

	public void setAccRep_V2Txt(String accRep_V2Txt) {
		this.accRep_V2Txt = accRep_V2Txt;
	}

	public Object getAccRep_V3Def() {
		return this.accRep_V3Def;
	}

	public void setAccRep_V3Def(String accRep_V3Def) {
		this.accRep_V3Def = accRep_V3Def;
	}

	public Object getAccRep_V3Txt() {
		return this.accRep_V3Txt;
	}

	public void setAccRep_V3Txt(String accRep_V3Txt) {
		this.accRep_V3Txt = accRep_V3Txt;
	}

	public List<SgaUsuario> getSgaUsuarios() {
		return this.sgaUsuarios;
	}

	public void setSgaUsuarios(List<SgaUsuario> sgaUsuarios) {
		this.sgaUsuarios = sgaUsuarios;
	}

}