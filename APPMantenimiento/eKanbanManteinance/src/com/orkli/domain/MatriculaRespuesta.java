package com.orkli.domain;

        import java.io.Serializable;
        import javax.persistence.*;
        import java.sql.Date;
        import java.util.List;


/**
 * The persistent class for the TFlujo database table.
 *
 */
@Entity
@Table(name="MatriculaRespuesta")
@NamedQuery(name="MatriculaRespuesta.findAll", query="SELECT m FROM MatriculaRespuesta m")
public class MatriculaRespuesta implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="IdMatricula")
    private int idMatricula;

    @Column(name="IdPregunta")
    private int idPregunta;


    @Column(name="Respuesta")
    private String respuesta;

    @Column(name="Fase")
    private int fase;

    @Column(name="RespuestaBytes")
    private byte[] respuestaBytes;

    public MatriculaRespuesta() {
    }

    public int getIdMatricula() {
        return idMatricula;
    }

    public void setIdMatricula(int idMatricula) {
        this.idMatricula = idMatricula;
    }

    public int getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(int idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public int getFase() {
        return fase;
    }

    public void setFase(int fase) {
        this.fase = fase;
    }

    public byte[] getRespuestaBytes() {
        return respuestaBytes;
    }

    public void setRespuestaBytes(byte[] respuestaBytes) {
        this.respuestaBytes = respuestaBytes;
    }
}
