package com.orkli.domain;

        import java.io.Serializable;
        import javax.persistence.*;
        import java.util.List;


/**
 * The persistent class for the TFlujo database table.
 *
 */
@Entity
@Table(name="TFlujo")
@NamedQuery(name="TFlujos.findAll", query="SELECT tf FROM TFlujo tf")
public class TFlujo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="IdFlujo")
    private String idFlujo;

    @Column(name="Descripcion")
    private String descripcion;

    @Column(name="Activo")
    private short activo;

    @Column(name="IconoURL")
    private String iconoURL;

    public TFlujo() {
    }

    public String getIdFlujo() {
        return idFlujo;
    }

    public void setIdFlujo(String idFlujo) {
        this.idFlujo = idFlujo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public short getActivo() {
        return activo;
    }

    public void setActivo(short activo) {
        this.activo = activo;
    }

    public String getIconoURL() {
        return iconoURL;
    }

    public void setIconoURL(String iconoURL) {
        this.iconoURL = iconoURL;
    }

}