package com.orkli.domain.dto;

import java.io.Serializable;
import java.sql.Date;

public class FlujoDTO implements Serializable {
    private String idFlujo;

    private String idUsuario;

    private String nombre;

    private String apellidos;

    private String descripcionUsuario;

    private int prioridad;

    private String descripcionFlujo;

    private short activo;

    private String iconoURL;

    private int idMatricula;

    private String ubicacion;

    private String sububicacion;

    private Date fechaInicio;

    private Date fechaFin;

    private short cancelado;


    public FlujoDTO(String idFlujo, String descripcionFlujo, short activo, String iconoURL, String idusuario,
                    String nombre, String apellidos, String descripcionUsuario, int prioridad,
                    int idMatricula, String ubicacion, String sububicacion, Date fechaInicio, Date fechaFin, short cancelado) {

        super();
        this.idFlujo = idFlujo;
        this.descripcionFlujo = descripcionFlujo;
        this.activo = activo;
        this.iconoURL = iconoURL;
        this.idUsuario = idusuario;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.descripcionUsuario = descripcionUsuario;
        this.prioridad = prioridad;
        this.idMatricula = idMatricula;
        this.ubicacion = ubicacion;
        this.sububicacion = sububicacion;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.cancelado = cancelado;
    }

    public String getIdFlujo() {
        return idFlujo;
    }

    public void setIdFlujo(String idFlujo) {
        this.idFlujo = idFlujo;
    }

    public void setDescripcionTFlujo(String descripcionFlujo) {
        this.descripcionFlujo = descripcionFlujo;
    }

    public String getDescripcionFlujo() {
        return descripcionFlujo;
    }

    public short getActivo() {
        return activo;
    }

    public void setActivo(short activo) {
        this.activo = activo;
    }

    public String getIconoURL() {
        return iconoURL;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getDesripcionUsuario() {
        return descripcionUsuario;
    }

    public void setDesripcionUsuario(String desripcionUsuario) {
        this.descripcionUsuario = desripcionUsuario;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    public int getIdMatricula() {
        return idMatricula;
    }

    public void setIdMatricula(int idMatricula) {
        this.idMatricula = idMatricula;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getSububicacion() {
        return sububicacion;
    }

    public void setSububicacion(String sububicacion) {
        this.sububicacion = sububicacion;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public short getCancelado() {
        return cancelado;
    }

    public void setCancelado(short cancelado) {
        this.cancelado = cancelado;
    }

    public String getTextoCancelado(){
        String respuesta;
        if (cancelado == 0) {
            respuesta = "NO";
        } else {
            respuesta = "SI";
        }
        return respuesta;
    }


}
