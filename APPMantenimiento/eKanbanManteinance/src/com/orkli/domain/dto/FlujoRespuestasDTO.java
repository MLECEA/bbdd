package com.orkli.domain.dto;

import org.glassfish.jersey.internal.util.collection.ByteBufferInputStream;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.*;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.*;

public class FlujoRespuestasDTO implements Serializable {

    public static String TIPORESPUESTA_BOOLEAN = "BOOLEAN";
    public static String TIPORESPUESTA_CLEAR_NFC = "CLEAR_NFC";
    public static String TIPORESPUESTA_DATETIME = "DATETIME";
    public static String TIPORESPUESTA_DRAW = "DRAW";
    public static String TIPORESPUESTA_DRAW_ON_PHOTO = "DRAW_ON_PHOTO";
    public static String TIPORESPUESTA_GPS = "GPS";
    public static String TIPORESPUESTA_INFO= "INFO";
    public static String TIPORESPUESTA_MULTIPLE_CHOICE = "MULTIPLE_CHOICE";
    public static String TIPORESPUESTA_NUMBER = "NUMBER";
    public static String TIPORESPUESTA_OCR = "OCR";
    public static String TIPORESPUESTA_PHOTO = "PHOTO";
    public static String TIPORESPUESTA_READ_NFC = "READ_NFC";
    public static String TIPORESPUESTA_SCAN = "SCAN";
    public static String TIPORESPUESTA_SINGLE_CHOICE = "SINGLE_CHOICE";
    public static String TIPORESPUESTA_TEXT = "TEXT";
    public static String TIPORESPUESTA_VOICE = "VOICE";

    private int idMatricula;

    private int idFlujo;

    private int idPregunta;

    private short activo;

    private int fase;

    private short obligatorio;

    private int orden;

    private String textoPregunta;

    private String respuesta;

    private int faseRespuesta;

    private String respuestaBytes;

    private byte[] respuestaBytesVoz;

    private String tipoRespuesta;

    private String origenDatos;

//Datos correspondientes a una lectura Scan
    private String scanUbicacion;

    private String scanArticulo;

    private String scanLote;

    private String scanNumeroSerie;

    private String scanCantidad;

    private String scanUsuario;

    public FlujoRespuestasDTO(int idMatricula, int idFlujo, int idPregunta, String origenDatos, short activo, int fase, short obligatorio,
                    int orden, String textoPregunta, String respuesta, int faseRespuesta, byte[] respuestaBytes, String tipoRespuesta,
                    String scanUbicacion, String scanArticulo, String scanLote, String scanNumeroSerie, String scanCantidad, String scanUsuario) {

        super();
        this.idMatricula = idMatricula;
        this.idFlujo = idFlujo;
        this.idPregunta = idPregunta;
        this.origenDatos = origenDatos;
        this.activo = activo;
        this.fase = fase;
        this.obligatorio = obligatorio;
        this.orden = orden;
        this.textoPregunta = textoPregunta;
        this.respuesta = respuesta;
        this.faseRespuesta = faseRespuesta;
        this.respuestaBytes = convertirAHexadecimal(respuestaBytes);
        this.respuestaBytesVoz = respuestaBytes;
        this.tipoRespuesta = tipoRespuesta;
        this.scanUbicacion = scanUbicacion;
        this.scanArticulo = scanArticulo;
        this.scanLote = scanLote;
        this.scanNumeroSerie = scanNumeroSerie;
        this.scanCantidad = scanCantidad;
        this.scanUsuario = scanUsuario;
    }

    private String convertirAHexadecimal(byte[] bytes){
        return javax.xml.bind.DatatypeConverter.printHexBinary(bytes);
    }

    public int getIdMatricula() {
        return idMatricula;
    }

    public void setIdMatricula(int idMatricula) {
        this.idMatricula = idMatricula;
    }

    public int getIdFlujo() {
        return idFlujo;
    }

    public void setIdFlujo(int idFlujo) {
        this.idFlujo = idFlujo;
    }

    public int getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(int idPregunta) {
        this.idPregunta = idPregunta;
    }

    public short getActivo() {
        return activo;
    }

    public void setActivo(short activo) {
        this.activo = activo;
    }

    public int getFase() {
        return fase;
    }

    public void setFase(int fase) {
        this.fase = fase;
    }

    public short getObligatorio() {
        return obligatorio;
    }

    public void setObligatorio(short obligatorio) {
        this.obligatorio = obligatorio;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public String getTextoPregunta() {
        return textoPregunta;
    }

    public void setTextoPregunta(String textoPregunta) {
        this.textoPregunta = textoPregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public int getFaseRespuesta() {
        return faseRespuesta;
    }

    public void setFaseRespuesta(int faseRespuesta) {
        this.faseRespuesta = faseRespuesta;
    }

    public String getRespuestaBytes() {
        return respuestaBytes;
    }

    public byte[] getRespuestaBytesVoz() { return respuestaBytesVoz;}

    public void setRespuestaBytes(byte[] respuestaBytes) {
        this.respuestaBytes = convertirAHexadecimal(respuestaBytes);
    }

    public String getTipoRespuesta() {
        return tipoRespuesta;
    }

    public void setTipoRespuesta(String tipoRespuesta) {
        this.tipoRespuesta = tipoRespuesta;
    }

    public String getOrigenDatos() {
        return origenDatos;
    }

    public void setOrigenDatos(String origenDatos) {
        this.origenDatos = origenDatos;
    }

    public String getScanUbicacion() {
        return scanUbicacion;
    }

    public void setScanUbicacion(String scanUbicacion) {
        this.scanUbicacion = scanUbicacion;
    }

    public String getScanArticulo() {
        return scanArticulo;
    }

    public void setScanArticulo(String scanArticulo) {
        this.scanArticulo = scanArticulo;
    }

    public String getScanLote() {
        return scanLote;
    }

    public void setScanLote(String scanLote) {
        this.scanLote = scanLote;
    }

    public String getScanNumeroSerie() {
        return scanNumeroSerie;
    }

    public void setScanNumeroSerie(String scanNumeroSerie) {
        this.scanNumeroSerie = scanNumeroSerie;
    }

    public String getScanCantidad() {
        return scanCantidad;
    }

    public void setScanCantidad(String scanCantidad) {
        this.scanCantidad = scanCantidad;
    }

    public String getScanUsuario() {
        return scanUsuario;
    }

    public void setScanUsuario(String scanUsuario) {
        this.scanUsuario = scanUsuario;
    }

    public boolean tieneImagen(){
        if (    tipoRespuesta.equals(TIPORESPUESTA_DRAW) ||
                tipoRespuesta.equals(TIPORESPUESTA_DRAW_ON_PHOTO) ||
                tipoRespuesta.equals(TIPORESPUESTA_OCR) ||
                tipoRespuesta.equals(TIPORESPUESTA_PHOTO) ||
                tipoRespuesta.equals(TIPORESPUESTA_READ_NFC)){
            if (respuestaBytes != null && respuestaBytes.length() > 0) {
                return true;
            }
        }
        return false;
    }

    private String getChoice(int numeroChoice) {
        String choice = "";

        if (origenDatos != null) {
            String[] result = origenDatos.split("\\|");
            if (result.length > 0) {
                choice = result[numeroChoice];
            }
        }
        return choice;
    }

    private String[] getValoresMultipleChoice(String respuesta) {
        String[] respuestas = null;
        if (respuesta != null) {
            respuestas = respuesta.split("\\|");
        }
        return respuestas;
    }

    private String getTextoBoolean(String booleano) {
        String respuesta;
        if (booleano.equals("0")) {
            respuesta = "NO";
        } else {
            respuesta = "SI";
        }
        return respuesta;
    }

    public boolean esScan(){
        if (tipoRespuesta.equals(TIPORESPUESTA_SCAN)){
            return true;
        }else{
            return false;
        }
    }

    public List<String> getTextoScan(){
        List<String> textoScan = new ArrayList<String>();
        textoScan.add("Lectura: " + respuesta);
        textoScan.add("Ubicaci�n: " + scanUbicacion);
        textoScan.add("Art�culo: " + scanArticulo);
        textoScan.add("Lote: " + scanLote);
        textoScan.add("N Serie: " + scanNumeroSerie);
        textoScan.add("Cantidad: " + scanCantidad);
        textoScan.add("Usuario: " + scanUsuario);
        return textoScan;
    }

    public boolean esAudio(){
        if (tipoRespuesta.equals(TIPORESPUESTA_VOICE)){
            return true;
        }else{
            return false;
        }
    }

//    private AudioFormat getOutFormat(AudioFormat inFormat) {
//        //final int ch = inFormat.getChannels();
//        //final float rate = inFormat.getSampleRate();
//        //return new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, rate, 16, ch, ch * 2, rate, false);
//        return new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,
//                8000f,
//                16,
//                1,
//                2, // frameSize
//                8000f,// frameRate
//                false);
//    }
//
    public AudioInputStream getAudioFile(){
        AudioInputStream audioInputStream = null;
//        try {
//            InputStream is = new ByteArrayInputStream(getRespuestaBytesVoz());
//            DataOutputStream dos = new DataOutputStream(new FileOutputStream(
//                    "filename.bin"));
//            dos.write(getRespuestaBytesVoz());
//            AudioFormat baseFormat = this.getOutFormat(null);
//            AudioInputStream stream = new AudioInputStream(is, baseFormat, getRespuestaBytesVoz().length);
//            File file = new File("file.wav");
//            AudioSystem.write(stream, AudioFileFormat.Type.WAVE, file);
//
//
//
//
//            /*//InputStream is = new ByteArrayInputStream(getRespuestaBytesVoz());
//            audioInputStream = AudioSystem.getAudioInputStream(is);
//            if (audioInputStream != null) {
//                final AudioFormat baseFormat = audioInputStream.getFormat();
//                final AudioFormat decodedFormat = this.getOutFormat(baseFormat);
//                // Get AudioInputStream that will be decoded by underlying VorbisSPI
//                audioInputStream = AudioSystem.getAudioInputStream(decodedFormat, audioInputStream);
//                //this.stream = in;
//                //this.streamData = StreamUtilities.getBytes(this.stream);
//                //this.format = this.stream.getFormat();
//
//            }*/
//
//
//            //Borrar
//            //AudioSystem.write(audioInputStream, AudioFileFormat.Type.AU, wavFile);
//            //Path path = Paths.get(source.getAbsolutePath());
//
//            /*try {
//                File source = new File("audio.amr");
//                FileWriter fileWriter = new FileWriter(source);
//                PrintWriter printWriter = new PrintWriter(fileWriter);
//                printWriter.print(audioInputStream);
//                fileWriter.flush();
//                fileWriter.close();
//                //Files.write(path, fileContent1);
//                System.out.println("path " + source.getAbsolutePath());
//            } catch (IOException ex) {
//                ex.printStackTrace();
//            }*/
//        }catch (Exception e) {
//            e.printStackTrace();
//        }
//
        return audioInputStream;
    }

    public String getRespuestaVisualizacion() {
        String respuesta = getRespuesta();
        if (tipoRespuesta.equals(TIPORESPUESTA_BOOLEAN)) {
            respuesta = getTextoBoolean(respuesta);
        } else if (tipoRespuesta.equals(TIPORESPUESTA_DRAW) ||
                tipoRespuesta.equals(TIPORESPUESTA_DRAW_ON_PHOTO) ||
                tipoRespuesta.equals(TIPORESPUESTA_PHOTO)){
            //Mostrar la imagen
        } else if (tipoRespuesta.equals(TIPORESPUESTA_OCR) ||
                tipoRespuesta.equals(TIPORESPUESTA_READ_NFC)){
            //Mostrar la imagen y la respuesta
        } else if (tipoRespuesta.equals(TIPORESPUESTA_SCAN)) {
            // Mostrar texto y valores

        } else if (tipoRespuesta.equals(TIPORESPUESTA_TEXT) ||
                   tipoRespuesta.equals(TIPORESPUESTA_GPS) ||
                   tipoRespuesta.equals(TIPORESPUESTA_NUMBER) ||
                   tipoRespuesta.equals(TIPORESPUESTA_SCAN) ||
                   tipoRespuesta.equals(TIPORESPUESTA_INFO) ||
                   tipoRespuesta.equals(TIPORESPUESTA_VOICE)) {
            // No requiere tratamiento
        } else if (tipoRespuesta.equals(TIPORESPUESTA_SINGLE_CHOICE) ||
                   tipoRespuesta.equals(TIPORESPUESTA_MULTIPLE_CHOICE)) {
            if (respuesta != null) {
                String[] respuestas = getValoresMultipleChoice(respuesta);
                if (respuestas != null && respuestas.length > 0){
                    try {
                        for (int i = 0; i < respuestas.length; i++) {
                            int choice = Integer.valueOf(respuestas[i]);
                            respuesta = respuesta + this.getChoice(choice);
                            if (i + 1 < respuestas.length) {
                                respuesta = respuesta + " - ";
                            }
                        }
                        respuesta = "";
                    }catch (Exception e) {
                        System.out.println("Se ha producido un error al intentar pintar las respuesta de tipo choice para " + respuesta);
                        e.printStackTrace();
                        respuesta = respuesta + "����ERROR!!!!";
                    }
                }
            }
        } else if (tipoRespuesta.equals(TIPORESPUESTA_CLEAR_NFC)) {
            respuesta =  "Se ha borrado el NFC";
        } else if (tipoRespuesta.equals(TIPORESPUESTA_READ_NFC) ) {
            respuesta = "[" + tipoRespuesta + "]";
        } else if (tipoRespuesta.equals(TIPORESPUESTA_DATETIME)) {
            //Date time
            Date fechaDate = new Date(Long.parseLong(respuesta));
            if (fechaDate != null) {
                GregorianCalendar fecha = new GregorianCalendar();
                fecha.setTime(fechaDate);
                fecha.add(Calendar.HOUR_OF_DAY, 1);
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                respuesta = format.format(fecha.getTime());
            }
        }


        if ((respuesta == null || respuesta.trim().equals(""))){
            if (!tipoRespuesta.equals(TIPORESPUESTA_DRAW) &&
                    !tipoRespuesta.equals(TIPORESPUESTA_DRAW_ON_PHOTO) &&
                    !tipoRespuesta.equals(TIPORESPUESTA_PHOTO) &&
                    !tipoRespuesta.equals(TIPORESPUESTA_INFO)) {
                respuesta = "[respuesta no rellenada]";
            }
        }
        return respuesta;
    }
}
