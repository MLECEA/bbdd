package com.orkli.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the sga_rol database table.
 * 
 */
@Entity
@Table(name="sga_rol")
@NamedQuery(name="SgaRol.findAll", query="SELECT s FROM SgaRol s")
public class SgaRol implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cod_rol")
	private long codRol;

	private short activo;

	private int anonimo;

	private String descripcion;

	private String nombre;

	@Column(name="optimistic_lock")
	private BigDecimal optimisticLock;

	private boolean rol_PedCLI;

	private boolean rol_PedPRO;

	private boolean rol_RecMaiPar;

	private short superusuario;

	@Column(name="vista_datos_reales")
	private int vistaDatosReales;

	public SgaRol() {
	}

	public long getCodRol() {
		return this.codRol;
	}

	public void setCodRol(long codRol) {
		this.codRol = codRol;
	}

	public short getActivo() {
		return this.activo;
	}

	public void setActivo(short activo) {
		this.activo = activo;
	}

	public int getAnonimo() {
		return this.anonimo;
	}

	public void setAnonimo(int anonimo) {
		this.anonimo = anonimo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getOptimisticLock() {
		return this.optimisticLock;
	}

	public void setOptimisticLock(BigDecimal optimisticLock) {
		this.optimisticLock = optimisticLock;
	}

	public boolean getRol_PedCLI() {
		return this.rol_PedCLI;
	}

	public void setRol_PedCLI(boolean rol_PedCLI) {
		this.rol_PedCLI = rol_PedCLI;
	}

	public boolean getRol_PedPRO() {
		return this.rol_PedPRO;
	}

	public void setRol_PedPRO(boolean rol_PedPRO) {
		this.rol_PedPRO = rol_PedPRO;
	}

	public boolean getRol_RecMaiPar() {
		return this.rol_RecMaiPar;
	}

	public void setRol_RecMaiPar(boolean rol_RecMaiPar) {
		this.rol_RecMaiPar = rol_RecMaiPar;
	}

	public short getSuperusuario() {
		return this.superusuario;
	}

	public void setSuperusuario(short superusuario) {
		this.superusuario = superusuario;
	}

	public int getVistaDatosReales() {
		return this.vistaDatosReales;
	}

	public void setVistaDatosReales(int vistaDatosReales) {
		this.vistaDatosReales = vistaDatosReales;
	}

}