package com.orkli.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the circuito database table.
 * 
 */
@Entity
@Table(name="circuito")
@NamedQuery(name="Circuito.findAll", query="SELECT c FROM Circuito c")
public class Circuito implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CircuitoPK id;

	private boolean activo;

	@Column(name="cantidad_piezas")
	private int cantidadPiezas;

	@Column(name="Cir_AlbaranarAut")
	private int cir_AlbaranarAut;

	@Column(name="Cir_ConFacturarAgrup")
	private boolean cir_ConFacturarAgrup;

	@Column(name="Cir_ConFacturarAut")
	private int cir_ConFacturarAut;

	@Column(name="Cir_ConInventLocationId")
	private String cir_ConInventLocationId;

	@Column(name="Cir_ConNumFactCli")
	private String cir_ConNumFactCli;

	@Column(name="Cir_ConSalesType")
	private String cir_ConSalesType;

	@Column(name="Cir_Consigna")
	private boolean cir_Consigna;

	@Column(name="Cir_DomM")
	private int cir_DomM;

	@Column(name="Cir_DomT")
	private int cir_DomT;

	@Column(name="Cir_DupConsCir")
	private String cir_DupConsCir;

	@Column(name="Cir_Empresa")
	private String cir_Empresa;

	@Column(name="Cir_HoraM")
	private String cir_HoraM;

	@Column(name="Cir_HoraT")
	private String cir_HoraT;

	@Column(name="Cir_IntDom")
	private boolean cir_IntDom;

	@Column(name="Cir_IntJue")
	private boolean cir_IntJue;

	@Column(name="Cir_IntLun")
	private boolean cir_IntLun;

	@Column(name="Cir_IntMar")
	private boolean cir_IntMar;

	@Column(name="Cir_IntMie")
	private boolean cir_IntMie;

	@Column(name="Cir_IntSab")
	private boolean cir_IntSab;

	@Column(name="Cir_IntVie")
	private boolean cir_IntVie;

	@Column(name="Cir_JueM")
	private int cir_JueM;

	@Column(name="Cir_JueT")
	private int cir_JueT;

	@Column(name="Cir_LanPed5MI")
	private boolean cir_LanPed5MI;

	@Column(name="Cir_LanPedAut")
	private boolean cir_LanPedAut;

	@Column(name="Cir_LecMinParaCons")
	private int cir_LecMinParaCons;

	@Column(name="Cir_LunM")
	private int cir_LunM;

	@Column(name="Cir_LunT")
	private int cir_LunT;

	@Column(name="Cir_MarM")
	private int cir_MarM;

	@Column(name="Cir_MarT")
	private int cir_MarT;

	@Column(name="Cir_MieM")
	private int cir_MieM;

	@Column(name="Cir_MieT")
	private int cir_MieT;

	@Column(name="Cir_NumPedCli")
	private String cir_NumPedCli;

	@Column(name="Cir_PedMarco")
	private String cir_PedMarco;

	@Column(name="Cir_RAnt")
	private int cir_RAnt;

	@Column(name="Cir_ReaprovAgrup")
	private boolean cir_ReaprovAgrup;

	@Column(name="Cir_RegistrarPedCompra")
	private boolean cir_RegistrarPedCompra;

	@Column(name="Cir_RFec")
	private Timestamp cir_RFec;

	@Column(name="Cir_SabM")
	private int cir_SabM;

	@Column(name="Cir_SabT")
	private int cir_SabT;

	@Column(name="Cir_StdInventLocationId")
	private String cir_StdInventLocationId;

	@Column(name="Cir_StdSalesType")
	private String cir_StdSalesType;

	@Column(name="Cir_UbiAnt")
	private String cir_UbiAnt;

	@Column(name="Cir_VieM")
	private int cir_VieM;

	@Column(name="Cir_VieT")
	private int cir_VieT;

	@Column(name="cliente_nombre")
	private String clienteNombre;

	private String descripcion;

	@Column(name="entrega_dias")
	private int entregaDias;

	@Column(name="entrega_horas")
	private int entregaHoras;

	private String mail;

	@Column(name="num_kanban")
	private int numKanban;

	private int num_kanbanOPT;

	@Column(name="Proveedor_id")
	private String proveedor_id;

	@Column(name="ref_cliente")
	private String refCliente;

	@Column(name="reserva_auto")
	private boolean reservaAuto;

	@Column(name="tipo_circuito")
	private BigDecimal tipoCircuito;

	@Column(name="tipo_contenedor")
	private BigDecimal tipoContenedor;

	@Column(name="TransitoActivado")
	private boolean transitoActivado;

	public Circuito() {
	}

	public CircuitoPK getId() {
		return this.id;
	}

	public void setId(CircuitoPK id) {
		this.id = id;
	}

	public boolean getActivo() {
		return this.activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public int getCantidadPiezas() {
		return this.cantidadPiezas;
	}

	public void setCantidadPiezas(int cantidadPiezas) {
		this.cantidadPiezas = cantidadPiezas;
	}

	public int getCir_AlbaranarAut() {
		return this.cir_AlbaranarAut;
	}

	public void setCir_AlbaranarAut(int cir_AlbaranarAut) {
		this.cir_AlbaranarAut = cir_AlbaranarAut;
	}

	public boolean getCir_ConFacturarAgrup() {
		return this.cir_ConFacturarAgrup;
	}

	public void setCir_ConFacturarAgrup(boolean cir_ConFacturarAgrup) {
		this.cir_ConFacturarAgrup = cir_ConFacturarAgrup;
	}

	public int getCir_ConFacturarAut() {
		return this.cir_ConFacturarAut;
	}

	public void setCir_ConFacturarAut(int cir_ConFacturarAut) {
		this.cir_ConFacturarAut = cir_ConFacturarAut;
	}

	public String getCir_ConInventLocationId() {
		return this.cir_ConInventLocationId;
	}

	public void setCir_ConInventLocationId(String cir_ConInventLocationId) {
		this.cir_ConInventLocationId = cir_ConInventLocationId;
	}

	public String getCir_ConNumFactCli() {
		return this.cir_ConNumFactCli;
	}

	public void setCir_ConNumFactCli(String cir_ConNumFactCli) {
		this.cir_ConNumFactCli = cir_ConNumFactCli;
	}

	public String getCir_ConSalesType() {
		return this.cir_ConSalesType;
	}

	public void setCir_ConSalesType(String cir_ConSalesType) {
		this.cir_ConSalesType = cir_ConSalesType;
	}

	public boolean getCir_Consigna() {
		return this.cir_Consigna;
	}

	public void setCir_Consigna(boolean cir_Consigna) {
		this.cir_Consigna = cir_Consigna;
	}

	public int getCir_DomM() {
		return this.cir_DomM;
	}

	public void setCir_DomM(int cir_DomM) {
		this.cir_DomM = cir_DomM;
	}

	public int getCir_DomT() {
		return this.cir_DomT;
	}

	public void setCir_DomT(int cir_DomT) {
		this.cir_DomT = cir_DomT;
	}

	public String getCir_DupConsCir() {
		return this.cir_DupConsCir;
	}

	public void setCir_DupConsCir(String cir_DupConsCir) {
		this.cir_DupConsCir = cir_DupConsCir;
	}

	public String getCir_Empresa() {
		return this.cir_Empresa;
	}

	public void setCir_Empresa(String cir_Empresa) {
		this.cir_Empresa = cir_Empresa;
	}

	public String getCir_HoraM() {
		return this.cir_HoraM;
	}

	public void setCir_HoraM(String cir_HoraM) {
		this.cir_HoraM = cir_HoraM;
	}

	public String getCir_HoraT() {
		return this.cir_HoraT;
	}

	public void setCir_HoraT(String cir_HoraT) {
		this.cir_HoraT = cir_HoraT;
	}

	public boolean getCir_IntDom() {
		return this.cir_IntDom;
	}

	public void setCir_IntDom(boolean cir_IntDom) {
		this.cir_IntDom = cir_IntDom;
	}

	public boolean getCir_IntJue() {
		return this.cir_IntJue;
	}

	public void setCir_IntJue(boolean cir_IntJue) {
		this.cir_IntJue = cir_IntJue;
	}

	public boolean getCir_IntLun() {
		return this.cir_IntLun;
	}

	public void setCir_IntLun(boolean cir_IntLun) {
		this.cir_IntLun = cir_IntLun;
	}

	public boolean getCir_IntMar() {
		return this.cir_IntMar;
	}

	public void setCir_IntMar(boolean cir_IntMar) {
		this.cir_IntMar = cir_IntMar;
	}

	public boolean getCir_IntMie() {
		return this.cir_IntMie;
	}

	public void setCir_IntMie(boolean cir_IntMie) {
		this.cir_IntMie = cir_IntMie;
	}

	public boolean getCir_IntSab() {
		return this.cir_IntSab;
	}

	public void setCir_IntSab(boolean cir_IntSab) {
		this.cir_IntSab = cir_IntSab;
	}

	public boolean getCir_IntVie() {
		return this.cir_IntVie;
	}

	public void setCir_IntVie(boolean cir_IntVie) {
		this.cir_IntVie = cir_IntVie;
	}

	public int getCir_JueM() {
		return this.cir_JueM;
	}

	public void setCir_JueM(int cir_JueM) {
		this.cir_JueM = cir_JueM;
	}

	public int getCir_JueT() {
		return this.cir_JueT;
	}

	public void setCir_JueT(int cir_JueT) {
		this.cir_JueT = cir_JueT;
	}

	public boolean getCir_LanPed5MI() {
		return this.cir_LanPed5MI;
	}

	public void setCir_LanPed5MI(boolean cir_LanPed5MI) {
		this.cir_LanPed5MI = cir_LanPed5MI;
	}

	public boolean getCir_LanPedAut() {
		return this.cir_LanPedAut;
	}

	public void setCir_LanPedAut(boolean cir_LanPedAut) {
		this.cir_LanPedAut = cir_LanPedAut;
	}

	public int getCir_LecMinParaCons() {
		return this.cir_LecMinParaCons;
	}

	public void setCir_LecMinParaCons(int cir_LecMinParaCons) {
		this.cir_LecMinParaCons = cir_LecMinParaCons;
	}

	public int getCir_LunM() {
		return this.cir_LunM;
	}

	public void setCir_LunM(int cir_LunM) {
		this.cir_LunM = cir_LunM;
	}

	public int getCir_LunT() {
		return this.cir_LunT;
	}

	public void setCir_LunT(int cir_LunT) {
		this.cir_LunT = cir_LunT;
	}

	public int getCir_MarM() {
		return this.cir_MarM;
	}

	public void setCir_MarM(int cir_MarM) {
		this.cir_MarM = cir_MarM;
	}

	public int getCir_MarT() {
		return this.cir_MarT;
	}

	public void setCir_MarT(int cir_MarT) {
		this.cir_MarT = cir_MarT;
	}

	public int getCir_MieM() {
		return this.cir_MieM;
	}

	public void setCir_MieM(int cir_MieM) {
		this.cir_MieM = cir_MieM;
	}

	public int getCir_MieT() {
		return this.cir_MieT;
	}

	public void setCir_MieT(int cir_MieT) {
		this.cir_MieT = cir_MieT;
	}

	public String getCir_NumPedCli() {
		return this.cir_NumPedCli;
	}

	public void setCir_NumPedCli(String cir_NumPedCli) {
		this.cir_NumPedCli = cir_NumPedCli;
	}

	public String getCir_PedMarco() {
		return this.cir_PedMarco;
	}

	public void setCir_PedMarco(String cir_PedMarco) {
		this.cir_PedMarco = cir_PedMarco;
	}

	public int getCir_RAnt() {
		return this.cir_RAnt;
	}

	public void setCir_RAnt(int cir_RAnt) {
		this.cir_RAnt = cir_RAnt;
	}

	public boolean getCir_ReaprovAgrup() {
		return this.cir_ReaprovAgrup;
	}

	public void setCir_ReaprovAgrup(boolean cir_ReaprovAgrup) {
		this.cir_ReaprovAgrup = cir_ReaprovAgrup;
	}

	public boolean getCir_RegistrarPedCompra() {
		return this.cir_RegistrarPedCompra;
	}

	public void setCir_RegistrarPedCompra(boolean cir_RegistrarPedCompra) {
		this.cir_RegistrarPedCompra = cir_RegistrarPedCompra;
	}

	public Timestamp getCir_RFec() {
		return this.cir_RFec;
	}

	public void setCir_RFec(Timestamp cir_RFec) {
		this.cir_RFec = cir_RFec;
	}

	public int getCir_SabM() {
		return this.cir_SabM;
	}

	public void setCir_SabM(int cir_SabM) {
		this.cir_SabM = cir_SabM;
	}

	public int getCir_SabT() {
		return this.cir_SabT;
	}

	public void setCir_SabT(int cir_SabT) {
		this.cir_SabT = cir_SabT;
	}

	public String getCir_StdInventLocationId() {
		return this.cir_StdInventLocationId;
	}

	public void setCir_StdInventLocationId(String cir_StdInventLocationId) {
		this.cir_StdInventLocationId = cir_StdInventLocationId;
	}

	public String getCir_StdSalesType() {
		return this.cir_StdSalesType;
	}

	public void setCir_StdSalesType(String cir_StdSalesType) {
		this.cir_StdSalesType = cir_StdSalesType;
	}

	public String getCir_UbiAnt() {
		return this.cir_UbiAnt;
	}

	public void setCir_UbiAnt(String cir_UbiAnt) {
		this.cir_UbiAnt = cir_UbiAnt;
	}

	public int getCir_VieM() {
		return this.cir_VieM;
	}

	public void setCir_VieM(int cir_VieM) {
		this.cir_VieM = cir_VieM;
	}

	public int getCir_VieT() {
		return this.cir_VieT;
	}

	public void setCir_VieT(int cir_VieT) {
		this.cir_VieT = cir_VieT;
	}

	public String getClienteNombre() {
		return this.clienteNombre;
	}

	public void setClienteNombre(String clienteNombre) {
		this.clienteNombre = clienteNombre;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getEntregaDias() {
		return this.entregaDias;
	}

	public void setEntregaDias(int entregaDias) {
		this.entregaDias = entregaDias;
	}

	public int getEntregaHoras() {
		return this.entregaHoras;
	}

	public void setEntregaHoras(int entregaHoras) {
		this.entregaHoras = entregaHoras;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public int getNumKanban() {
		return this.numKanban;
	}

	public void setNumKanban(int numKanban) {
		this.numKanban = numKanban;
	}

	public int getNum_kanbanOPT() {
		return this.num_kanbanOPT;
	}

	public void setNum_kanbanOPT(int num_kanbanOPT) {
		this.num_kanbanOPT = num_kanbanOPT;
	}

	public String getProveedor_id() {
		return this.proveedor_id;
	}

	public void setProveedor_id(String proveedor_id) {
		this.proveedor_id = proveedor_id;
	}

	public String getRefCliente() {
		return this.refCliente;
	}

	public void setRefCliente(String refCliente) {
		this.refCliente = refCliente;
	}

	public boolean getReservaAuto() {
		return this.reservaAuto;
	}

	public void setReservaAuto(boolean reservaAuto) {
		this.reservaAuto = reservaAuto;
	}

	public BigDecimal getTipoCircuito() {
		return this.tipoCircuito;
	}

	public void setTipoCircuito(BigDecimal tipoCircuito) {
		this.tipoCircuito = tipoCircuito;
	}

	public BigDecimal getTipoContenedor() {
		return this.tipoContenedor;
	}

	public void setTipoContenedor(BigDecimal tipoContenedor) {
		this.tipoContenedor = tipoContenedor;
	}

	public boolean getTransitoActivado() {
		return this.transitoActivado;
	}

	public void setTransitoActivado(boolean transitoActivado) {
		this.transitoActivado = transitoActivado;
	}

}