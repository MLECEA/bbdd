package com.orkli.domain;

        import java.io.Serializable;
        import javax.persistence.*;


/**
 * The persistent class for the TFlujoPregunta database table.
 *
 */
@Entity
@Table(name="TFlujoPregunta")

@NamedQuery(name="TFlujoPregunta.findAll", query="select fu from TFlujoPregunta fu ")

public class TFlujoPregunta implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="IdFlujo")
    private int idFlujo;

    @Id
    @Column(name="IdPregunta")
    private int idPregunta;

    @Column(name="Fase")
    private int fase;

    @Column(name="Activo")
    private short activo;

    @Column(name="Obligatorio")
    private short obligatorio;

    @Column(name="Orden")
    private int orden;

    @Column(name="TextoPregunta")
    private String textoPregunta;

    @Column(name="TipoRespuesta")
    private String tipoRespuesta;

    @Column(name="OrigenDatos")
    private String origenDatos;

    public TFlujoPregunta() {
    }

    public int getIdFlujo() {
        return idFlujo;
    }

    public void setIdFlujo(int idFlujo) {
        this.idFlujo = idFlujo;
    }

    public int getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(int idPregunta) {
        this.idPregunta = idPregunta;
    }

    public int getFase() {
        return fase;
    }

    public void setFase(int fase) {
        this.fase = fase;
    }

    public short getActivo() {
        return activo;
    }

    public void setActivo(short activo) {
        this.activo = activo;
    }

    public short getObligatorio() {
        return obligatorio;
    }

    public void setObligatorio(short obligatorio) {
        this.obligatorio = obligatorio;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public String getTextoPregunta() {
        return textoPregunta;
    }

    public void setTextoPregunta(String textoPregunta) {
        this.textoPregunta = textoPregunta;
    }

    public String getTipoRespuesta() {
        return tipoRespuesta;
    }

    public void setTipoRespuesta(String tipoRespuesta) {
        this.tipoRespuesta = tipoRespuesta;
    }

    public String getOrigenDatos() {
        return origenDatos;
    }

    public void setOrigenDatos(String origenDatos) {
        this.origenDatos = origenDatos;
    }
}
