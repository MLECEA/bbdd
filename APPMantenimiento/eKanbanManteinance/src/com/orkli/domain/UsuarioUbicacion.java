package com.orkli.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the usuario_ubicacion database table.
 * 
 */
@Entity
@Table(name="usuario_ubicacion")
@NamedQuery(name="UsuarioUbicacion.findAll", query="SELECT u FROM UsuarioUbicacion u")
public class UsuarioUbicacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long id;

	private String descripcion;

	private int prioridad;

	//bi-directional many-to-one association to SgaUsuario
	@ManyToOne
	@JoinColumn(name="id_usuario")
	private SgaUsuario sgaUsuario;

	//bi-directional many-to-one association to Ubicacion
	@ManyToOne
	@JoinColumn(name="id_ubicacion")
	private Ubicacion ubicacion;

	public UsuarioUbicacion() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getPrioridad() {
		return this.prioridad;
	}

	public void setPrioridad(int prioridad) {
		this.prioridad = prioridad;
	}

	public SgaUsuario getSgaUsuario() {
		return this.sgaUsuario;
	}

	public void setSgaUsuario(SgaUsuario sgaUsuario) {
		this.sgaUsuario = sgaUsuario;
	}

	public Ubicacion getUbicacion() {
		return this.ubicacion;
	}

	public void setUbicacion(Ubicacion ubicacion) {
		this.ubicacion = ubicacion;
	}

}