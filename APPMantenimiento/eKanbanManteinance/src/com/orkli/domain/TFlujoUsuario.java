package com.orkli.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the TFlujo_Usuario database table.
 *
 */
@Entity
@Table(name="TFlujo_Usuario")
//@SecondaryTable(name = "TFlujo", pkJoinColumns = @PrimaryKeyJoinColumn(name = "IdFlujo", referencedColumnName = "IdFlujo"))

@NamedQuery(name="TFlujoUsuario.findAll", query="select fu from TFlujoUsuario fu ")

public class TFlujoUsuario implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="IdFlujo")
    private String idFlujo;

    @Column(name="IdUsuario")
    private String idUsuario;

    @Column(name="Descripcion")
    private String descripcion;

    @Column(name="Prioridad")
    private int prioridad;

    public TFlujoUsuario() {
    }

    public String getIdFlujo() {
        return idFlujo;
    }

    public void setIdFlujo(String idFlujo) {
        this.idFlujo = idFlujo;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getDesripcionU() {
        return descripcion;
    }

    public void setDesripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

}