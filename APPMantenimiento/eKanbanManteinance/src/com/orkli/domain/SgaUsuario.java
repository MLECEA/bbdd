package com.orkli.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the sga_usuario database table.
 * 
 */
@Entity
@Table(name="sga_usuario")
@NamedQuery(name="SgaUsuario.findAll", query="SELECT s FROM SgaUsuario s")
public class SgaUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue( strategy=GenerationType.IDENTITY)
	@Column(name="cod_usuario")
	private long codUsuario;

	private short activo;

	private String apellidos;

	private short conectado;

	private Timestamp conectUltFec;

	private String FTP_Password;

	private String FTP_Server;

	private String FTP_Username;

	private String mail;

	private String nombre;

	@Column(name="optimistic_lock")
	private BigDecimal optimisticLock;

	private String password;

	@Column(name="ptr_rol")
	private BigDecimal ptrRol;

	private BigDecimal puesto;

	@Column(name="TokenAuten_CaducidadMins")
	private int tokenAuten_CaducidadMins;

	@Column(name="TokenRefresco_CaducidadMins")
	private int tokenRefresco_CaducidadMins;

	private String username;

	@Column(name="Usu_Dominio")
	private String usu_Dominio;

	@Column(name="Usu_Emp")
	private int usu_Emp;

	@Column(name="Usu_FacConsig")
	private boolean usu_FacConsig;

	@Column(name="Usu_Idi")
	private String usu_Idi;

	@Column(name="Usu_Mov")
	private String usu_Mov;

	@Column(name="Usu_MovFecDesde")
	private String usu_MovFecDesde;

	@Column(name="Usu_MovSer")
	private String usu_MovSer;

	@Column(name="Usu_Observaciones")
	private String usu_Observaciones;

	@Column(name="Usu_PasarelaActivada")
	private boolean usu_PasarelaActivada;

	@Column(name="Usu_SIM")
	private String usu_SIM;

	@Column(name="Usu_SubirDatosAFTP")
	private boolean usu_SubirDatosAFTP;

	@Column(name="Usu_TimeZone")
	private String usu_TimeZone;

	@Column(name="Usu_Token")
	private String usu_Token;

	@Column(name="isDBAdmin")
	private short isDBAdmin;

	public SgaUsuario() {
	}

	public long getCodUsuario() {
		return this.codUsuario;
	}

	public void setCodUsuario(long codUsuario) {
		this.codUsuario = codUsuario;
	}

	public short getActivo() {
		return this.activo;
	}

	public void setActivo(short activo) {
		this.activo = activo;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public short getConectado() {
		return this.conectado;
	}

	public void setConectado(short conectado) {
		this.conectado = conectado;
	}

	public Timestamp getConectUltFec() {
		return this.conectUltFec;
	}

	public void setConectUltFec(Timestamp conectUltFec) {
		this.conectUltFec = conectUltFec;
	}

	public String getFTP_Password() {
		return this.FTP_Password;
	}

	public void setFTP_Password(String FTP_Password) {
		this.FTP_Password = FTP_Password;
	}

	public String getFTP_Server() {
		return this.FTP_Server;
	}

	public void setFTP_Server(String FTP_Server) {
		this.FTP_Server = FTP_Server;
	}

	public String getFTP_Username() {
		return this.FTP_Username;
	}

	public void setFTP_Username(String FTP_Username) {
		this.FTP_Username = FTP_Username;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getNombre() {
		return this.nombre;
	}

	public String getNombreYApellidos() {
		return this.nombre + " " + this.apellidos;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigDecimal getOptimisticLock() {
		return this.optimisticLock;
	}

	public void setOptimisticLock(BigDecimal optimisticLock) {
		this.optimisticLock = optimisticLock;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public BigDecimal getPtrRol() {
		return this.ptrRol;
	}

	public void setPtrRol(BigDecimal ptrRol) {
		this.ptrRol = ptrRol;
	}

	public BigDecimal getPuesto() {
		return this.puesto;
	}

	public void setPuesto(BigDecimal puesto) {
		this.puesto = puesto;
	}

	public int getTokenAuten_CaducidadMins() {
		return this.tokenAuten_CaducidadMins;
	}

	public void setTokenAuten_CaducidadMins(int tokenAuten_CaducidadMins) {
		this.tokenAuten_CaducidadMins = tokenAuten_CaducidadMins;
	}

	public int getTokenRefresco_CaducidadMins() {
		return this.tokenRefresco_CaducidadMins;
	}

	public void setTokenRefresco_CaducidadMins(int tokenRefresco_CaducidadMins) {
		this.tokenRefresco_CaducidadMins = tokenRefresco_CaducidadMins;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsu_Dominio() {
		return this.usu_Dominio;
	}

	public void setUsu_Dominio(String usu_Dominio) {
		this.usu_Dominio = usu_Dominio;
	}

	public int getUsu_Emp() {
		return this.usu_Emp;
	}

	public void setUsu_Emp(int usu_Emp) {
		this.usu_Emp = usu_Emp;
	}

	public boolean getUsu_FacConsig() {
		return this.usu_FacConsig;
	}

	public void setUsu_FacConsig(boolean usu_FacConsig) {
		this.usu_FacConsig = usu_FacConsig;
	}

	public String getUsu_Idi() {
		return this.usu_Idi;
	}

	public void setUsu_Idi(String usu_Idi) {
		this.usu_Idi = usu_Idi;
	}

	public String getUsu_Mov() {
		return this.usu_Mov;
	}

	public void setUsu_Mov(String usu_Mov) {
		this.usu_Mov = usu_Mov;
	}

	public String getUsu_MovFecDesde() {
		return this.usu_MovFecDesde;
	}

	public void setUsu_MovFecDesde(String usu_MovFecDesde) {
		this.usu_MovFecDesde = usu_MovFecDesde;
	}

	public String getUsu_MovSer() {
		return this.usu_MovSer;
	}

	public void setUsu_MovSer(String usu_MovSer) {
		this.usu_MovSer = usu_MovSer;
	}

	public String getUsu_Observaciones() {
		return this.usu_Observaciones;
	}

	public void setUsu_Observaciones(String usu_Observaciones) {
		this.usu_Observaciones = usu_Observaciones;
	}

	public boolean getUsu_PasarelaActivada() {
		return this.usu_PasarelaActivada;
	}

	public void setUsu_PasarelaActivada(boolean usu_PasarelaActivada) {
		this.usu_PasarelaActivada = usu_PasarelaActivada;
	}

	public String getUsu_SIM() {
		return this.usu_SIM;
	}

	public void setUsu_SIM(String usu_SIM) {
		this.usu_SIM = usu_SIM;
	}

	public boolean getUsu_SubirDatosAFTP() {
		return this.usu_SubirDatosAFTP;
	}

	public void setUsu_SubirDatosAFTP(boolean usu_SubirDatosAFTP) {
		this.usu_SubirDatosAFTP = usu_SubirDatosAFTP;
	}

	public String getUsu_TimeZone() {
		return this.usu_TimeZone;
	}

	public void setUsu_TimeZone(String usu_TimeZone) {
		this.usu_TimeZone = usu_TimeZone;
	}

	public String getUsu_Token() {
		return this.usu_Token;
	}

	public void setUsu_Token(String usu_Token) {
		this.usu_Token = usu_Token;
	}

	public short getIsDBAdmin() {
		return isDBAdmin;
	}

	public void setIsDBAdmin(short isDBAdmin) {
		this.isDBAdmin = isDBAdmin;
	}

}