package com.orkli.domain;

        import org.hibernate.annotations.Immutable;

        import java.io.Serializable;
        import javax.persistence.*;


/**
 * The persistent class for the public view TTraduccionLecturas
 *
 */
@Entity
@Immutable
@Table(name="TTraduccionLecturas")
@NamedQuery(name="TTraduccionLecturas.findAll", query="SELECT s FROM TTraduccionLecturas s")
public class TTraduccionLecturas implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private String lectura;

    private String ubicacion;

    private String articulo;

    private String lote;

    private String numserie;

    private String cantidad;

    private String usuario;


    public TTraduccionLecturas() {
    }

    public String getLectura() {
        return lectura;
    }

    public void setLectura(String lectura) {
        this.lectura = lectura;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getArticulo() {
        return articulo;
    }

    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getNumserie() {
        return numserie;
    }

    public void setNumserie(String numserie) {
        this.numserie = numserie;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
