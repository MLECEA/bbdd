package com.orkli.domain;

        import java.io.Serializable;
        import javax.persistence.*;
        import java.sql.Date;
        import java.util.List;


/**
 * The persistent class for the TFlujo database table.
 *
 */
@Entity
@Table(name="Matricula")
@NamedQuery(name="Matricula.findAll", query="SELECT mr FROM Matricula mr")
public class Matricula implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="IdMatricula")
    private int idMatricula;

    @Column(name="IdFlujo")
    private String idFlujo;


    @Column(name="IdUsuario")
    private String idUsuario;

    @Column(name="FechaInicio")
    private Date fechaInicio;

    @Column(name="FechaFin")
    private Date fechaFin;

    @Column(name="Cancelado")
    private short cancelado;

    @Column(name="Ubicacion")
    private String ubicacion;

    @Column(name="SubUbicacion")
    private String subUbicacion;


    public Matricula() {
    }

    public int getIdMatricula() {
        return idMatricula;
    }

    public void setIdMatricula(int idMatricula) {
        this.idMatricula = idMatricula;
    }

    public String getIdFlujo() {
        return idFlujo;
    }

    public void setIdFlujo(String idFlujo) {
        this.idFlujo = idFlujo;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public short getCancelado() {
        return cancelado;
    }

    public void setCancelado(short cancelado) {
        this.cancelado = cancelado;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getSubUbicacion() {
        return subUbicacion;
    }

    public void setSubUbicacion(String subUbicacion) {
        this.subUbicacion = subUbicacion;
    }



}