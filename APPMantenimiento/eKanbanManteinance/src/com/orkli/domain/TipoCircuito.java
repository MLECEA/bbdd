package com.orkli.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tipo_circuito database table.
 * 
 */
@Entity
@Table(name="tipo_circuito")
@NamedQuery(name="TipoCircuito.findAll", query="SELECT t FROM TipoCircuito t")
public class TipoCircuito implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long id;

	private String descripcion;

	private String nombre;

	public TipoCircuito() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}