package com.orkli.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the usuario_proveedor database table.
 * 
 */
@Entity
@Table(name="usuario_proveedor")
@NamedQuery(name="UsuarioProveedor.findAll", query="SELECT u FROM UsuarioProveedor u")
public class UsuarioProveedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue( strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="id_proveedor")
	private String idProveedor;

	private int prioridad;

	//bi-directional many-to-one association to SgaUsuario
	@ManyToOne
	@JoinColumn(name="id_usuario")
	private SgaUsuario sgaUsuario;

	public UsuarioProveedor() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIdProveedor() {
		return this.idProveedor;
	}

	public void setIdProveedor(String idProveedor) {
		this.idProveedor = idProveedor;
	}

	public int getPrioridad() {
		return this.prioridad;
	}

	public void setPrioridad(int prioridad) {
		this.prioridad = prioridad;
	}

	public SgaUsuario getSgaUsuario() {
		return this.sgaUsuario;
	}

	public void setSgaUsuario(SgaUsuario sgaUsuario) {
		this.sgaUsuario = sgaUsuario;
	}

}