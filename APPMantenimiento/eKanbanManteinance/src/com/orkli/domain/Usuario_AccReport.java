package com.orkli.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the usuario_AccReports database table.
 * 
 */
@Entity
@Table(name="usuario_AccReports")
@NamedQuery(name="Usuario_AccReport.findAll", query="SELECT u FROM Usuario_AccReport u")
public class Usuario_AccReport implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue( strategy=GenerationType.IDENTITY)
	private BigDecimal id;

	@Column(name="id_accrep")
	private String idAccrep;

	@Column(name="id_usuario")
	private BigDecimal idUsuario;

	private int prioridad;

	public Usuario_AccReport() {
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public Object getIdAccrep() {
		return this.idAccrep;
	}

	public void setIdAccrep(String idAccrep) {
		this.idAccrep = idAccrep;
	}

	public BigDecimal getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(BigDecimal idUsuario) {
		this.idUsuario = idUsuario;
	}

	public int getPrioridad() {
		return this.prioridad;
	}

	public void setPrioridad(int prioridad) {
		this.prioridad = prioridad;
	}

}