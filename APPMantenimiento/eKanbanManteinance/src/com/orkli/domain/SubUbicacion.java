package com.orkli.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the subUbicacion database table.
 * 
 */
@Entity
@Table(name="subUbicacion")
@NamedQuery(name="SubUbicacion.findAll", query="SELECT s FROM SubUbicacion s")
public class SubUbicacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id_SubUbicacion;

	private short activo;

	private String descripcion;

	//bi-directional many-to-many association to Ubicacion
	@ManyToMany(mappedBy="subUbicacions")
	private List<Ubicacion> ubicacions;

	public SubUbicacion() {
	}

	public String getId_SubUbicacion() {
		return this.id_SubUbicacion;
	}

	public void setId_SubUbicacion(String id_SubUbicacion) {
		this.id_SubUbicacion = id_SubUbicacion;
	}

	public short getActivo() {
		return this.activo;
	}

	public void setActivo(short activo) {
		this.activo = activo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Ubicacion> getUbicacions() {
		return this.ubicacions;
	}

	public void setUbicacions(List<Ubicacion> ubicacions) {
		this.ubicacions = ubicacions;
	}

}