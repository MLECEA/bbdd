package com.orkli.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tipo_contenedor database table.
 * 
 */
@Entity
@Table(name="tipo_contenedor")
@NamedQuery(name="TipoContenedor.findAll", query="SELECT t FROM TipoContenedor t")
public class TipoContenedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private long id;

	private String descripcion;

	private String nombre;

	public TipoContenedor() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}