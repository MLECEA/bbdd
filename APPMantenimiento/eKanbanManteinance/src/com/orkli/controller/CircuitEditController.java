package com.orkli.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orkli.domain.Circuito;
import com.orkli.domain.CircuitoPK;
import com.orkli.domain.SgaUsuario;
import com.orkli.domain.TipoCircuito;
import com.orkli.domain.TipoContenedor;
import com.orkli.domain.Ubicacion;
import com.orkli.domain.UsuarioProveedor;
import com.orkli.domain.UsuarioUbicacion;

/**
 * Servlet implementation class CircuitEditController
 */
@WebServlet("/CircuitEditController")
public class CircuitEditController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;
	private Circuito circuitoEdicion = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CircuitEditController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String idCircuito = request.getParameter("circuitID");
		idCircuito = URLDecoder.decode(idCircuito, "UTF-8");
		request.setAttribute("idCircuito", idCircuito);
		factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
		entityManager = factory.createEntityManager();
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PCtaGetCircuitoInfo", Circuito.class);
		query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		query.setParameter(1, idCircuito);
		List<Circuito> resultado = query.getResultList();
		if (resultado != null) {
			circuitoEdicion = resultado.get(0);
			request.setAttribute("circuito", circuitoEdicion);
			StoredProcedureQuery query2 = entityManager.createStoredProcedureQuery("PCtaGetTiposCircuito", TipoCircuito.class);
			List<TipoCircuito> listaTiposCircuito = query2.getResultList();
			request.setAttribute("LTipoCircuito", listaTiposCircuito);
			StoredProcedureQuery query3 = entityManager.createStoredProcedureQuery("PCtaGetUbicaciones", Ubicacion.class);
			query3.registerStoredProcedureParameter("codUser", String.class, ParameterMode.IN);
			query3.registerStoredProcedureParameter("mode", Integer.class, ParameterMode.IN);
			query3.setParameter("codUser", "-1");
			query3.setParameter("mode", 2);
			List<Ubicacion> listaUbicaciones = query3.getResultList();
			request.setAttribute("LUbicaciones", listaUbicaciones);
			query3 = entityManager.createStoredProcedureQuery("PCtaGetTiposContenedor", TipoContenedor.class);
			List<TipoContenedor> listaContenedores = query3.getResultList();
			
			request.setAttribute("LTipoContenedor", listaContenedores);
		}
		getServletContext().getRequestDispatcher("/editCircuit.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("formSubmit").equals("Guardar")) {
			CircuitoPK key = new CircuitoPK() ;
			key.setReferenciaId(request.getParameter("refID"));
			key.setUbicacion(request.getParameter("cUbicacion"));
			key.setClienteId(request.getParameter("clienteID"));
			key.setIdCircuito(circuitoEdicion.getId().getIdCircuito());
			if (!key.equals(circuitoEdicion.getId())){
				Circuito circuitoNuevo = new Circuito();
				circuitoNuevo.setId(key);
				circuitoNuevo.setDescripcion(request.getParameter("refDesc"));
				circuitoNuevo.setNumKanban(Integer.parseInt(request.getParameter("ctdNumKanban")));
				circuitoNuevo.setCantidadPiezas(Integer.parseInt(request.getParameter("ctdPiezas")));
				circuitoNuevo.setNum_kanbanOPT(Integer.parseInt(request.getParameter("ctdOpt")));
				
				circuitoNuevo.setTipoCircuito(BigDecimal.valueOf(Long.parseLong(request.getParameter("tipoCircuito"))));
				circuitoNuevo.setTipoContenedor(BigDecimal.valueOf(Long.parseLong(request.getParameter("tipoContenedor"))));
				//circuitoNuevo.setTipoContenedor(BigDecimal.valueOf(2));
				circuitoNuevo.setClienteNombre(request.getParameter("nombreCliente"));
				circuitoNuevo.setRefCliente(request.getParameter("refCliente"));
				circuitoNuevo.setProveedor_id(request.getParameter("proveedorID"));
				if (request.getParameter("circuitoActivo") != null && request.getParameter("circuitoActivo").trim() != "") {
					circuitoNuevo.setActivo(true);
				}else {
					circuitoNuevo.setActivo(false);
				}
				circuitoNuevo.setMail(request.getParameter("mailNotificaciones"));
				circuitoNuevo.setCir_LecMinParaCons(Integer.parseInt(request.getParameter("lecMinCons")));
				if (request.getParameter("transitoActivado") != null && request.getParameter("transitoActivado").trim() != "") {
					circuitoNuevo.setTransitoActivado(true);
				}else {
					circuitoNuevo.setTransitoActivado(false);
				}
				if (request.getParameter("lanPedAut") != null && request.getParameter("lanPedAut").trim() != "") {
					circuitoNuevo.setCir_LanPedAut(true);
				}else {
					circuitoNuevo.setCir_LanPedAut(false);
				}
				if (request.getParameter("pedCompraAut") != null && request.getParameter("pedCompraAut").trim() != "") {
					circuitoNuevo.setCir_RegistrarPedCompra(true);
				}else {
					circuitoNuevo.setCir_RegistrarPedCompra(false);
				}
				if (request.getParameter("reaproAgrup") != null && request.getParameter("reaproAgrup").trim() != "") {
					circuitoNuevo.setCir_ReaprovAgrup(true);
				}else {
					circuitoNuevo.setCir_ReaprovAgrup(false);
				}
				if (request.getParameter("cirConsigna") != null && request.getParameter("cirConsigna").trim() != "") {
					circuitoNuevo.setCir_Consigna(true);
				}else {
					circuitoNuevo.setCir_Consigna(false);
				}
				circuitoNuevo.setCir_ConSalesType(request.getParameter("salesType"));
				circuitoNuevo.setCir_ConInventLocationId(request.getParameter("inventLocationID"));
				circuitoNuevo.setCir_PedMarco(request.getParameter("pedMarco"));
				circuitoNuevo.setCir_AlbaranarAut(Integer.parseInt(request.getParameter("albAut")));
				if (request.getParameter("factAgrupada") != null && request.getParameter("factAgrupada").trim() != "") {
					circuitoNuevo.setCir_ConFacturarAgrup(true);
				}else {
					circuitoNuevo.setCir_ConFacturarAgrup(false);
				}
				circuitoNuevo.setCir_Empresa(request.getParameter("empresa"));
				circuitoNuevo.setCir_NumPedCli(request.getParameter("numPedCli"));
				circuitoNuevo.setEntregaDias(Integer.parseInt(request.getParameter("ctdEntregaDias")));
				circuitoNuevo.setEntregaHoras(Integer.parseInt(request.getParameter("ctdEntregaHoras")));
				circuitoNuevo.setCir_HoraM(request.getParameter("horaM"));
				circuitoNuevo.setCir_HoraT(request.getParameter("horaT"));
				if (request.getParameter("intLun") != null && request.getParameter("intLun").trim() != "") {
					circuitoNuevo.setCir_IntLun(true);
				}else {
					circuitoNuevo.setCir_IntLun(false);
				}
				circuitoNuevo.setCir_LunM(Integer.parseInt(request.getParameter("ctdLunM")));
				circuitoNuevo.setCir_LunT(Integer.parseInt(request.getParameter("ctdLunT")));
				if (request.getParameter("intMar") != null && request.getParameter("intMar").trim() != "") {
					circuitoNuevo.setCir_IntMar(true);
				}else {
					circuitoNuevo.setCir_IntMar(false);
				}
				circuitoNuevo.setCir_MarM(Integer.parseInt(request.getParameter("ctdMarM")));
				circuitoNuevo.setCir_MarT(Integer.parseInt(request.getParameter("ctdMarT")));
				if (request.getParameter("intMie") != null && request.getParameter("intMie").trim() != "") {
					circuitoNuevo.setCir_IntMie(true);
				}else {
					circuitoNuevo.setCir_IntMie(false);
				}
				circuitoNuevo.setCir_MieM(Integer.parseInt(request.getParameter("ctdMieM")));
				circuitoNuevo.setCir_MieT(Integer.parseInt(request.getParameter("ctdMieT")));
				if (request.getParameter("intJue") != null && request.getParameter("intJue").trim() != "") {
					circuitoNuevo.setCir_IntJue(true);
				}else {
					circuitoNuevo.setCir_IntJue(false);
				}
				circuitoNuevo.setCir_JueM(Integer.parseInt(request.getParameter("ctdJueM")));
				circuitoNuevo.setCir_JueT(Integer.parseInt(request.getParameter("ctdJueT")));
				if (request.getParameter("intVie") != null && request.getParameter("intVie").trim() != "") {
					circuitoNuevo.setCir_IntVie(true);
				}else {
					circuitoNuevo.setCir_IntVie(false);
				}
				circuitoNuevo.setCir_VieM(Integer.parseInt(request.getParameter("ctdVieM")));
				circuitoNuevo.setCir_VieT(Integer.parseInt(request.getParameter("ctdVieT")));
				if (request.getParameter("intSab") != null && request.getParameter("intSab").trim() != "") {
					circuitoNuevo.setCir_IntSab(true);
				}else {
					circuitoNuevo.setCir_IntSab(false);
				}
				circuitoNuevo.setCir_SabT(Integer.parseInt(request.getParameter("ctdSabM")));
				circuitoNuevo.setCir_VieT(Integer.parseInt(request.getParameter("ctdSabT")));
				if (request.getParameter("intDom") != null && request.getParameter("intDom").trim() != "") {
					circuitoNuevo.setCir_IntDom(true);
				}else {
					circuitoNuevo.setCir_IntDom(false);
				}
				circuitoNuevo.setCir_DomM(Integer.parseInt(request.getParameter("ctdDomM")));
				circuitoNuevo.setCir_DomT(Integer.parseInt(request.getParameter("ctdDomT")));
//				System.out.println(request.getParameter("usrUbicaciones"));
				try {
					entityManager.getTransaction().begin();
					entityManager.remove(circuitoEdicion);
					entityManager.getTransaction().commit();
					
					entityManager.getTransaction().begin();
					entityManager.persist(circuitoNuevo);
					
					entityManager.getTransaction().commit();
				}catch (Exception e1) {
					
				}
				
			}else {
				circuitoEdicion.setDescripcion(request.getParameter("refDesc"));
				circuitoEdicion.setNumKanban(Integer.parseInt(request.getParameter("ctdNumKanban")));
				circuitoEdicion.setCantidadPiezas(Integer.parseInt(request.getParameter("ctdPiezas")));
				circuitoEdicion.setNum_kanbanOPT(Integer.parseInt(request.getParameter("ctdOpt")));
				
				circuitoEdicion.setTipoCircuito(BigDecimal.valueOf(Long.parseLong(request.getParameter("tipoCircuito"))));
				circuitoEdicion.setTipoContenedor(BigDecimal.valueOf(Long.parseLong(request.getParameter("tipoContenedor"))));
				//circuitoNuevo.setTipoContenedor(BigDecimal.valueOf(2));
				circuitoEdicion.setClienteNombre(request.getParameter("nombreCliente"));
				circuitoEdicion.setRefCliente(request.getParameter("refCliente"));
				circuitoEdicion.setProveedor_id(request.getParameter("proveedorID"));
				if (request.getParameter("circuitoActivo") != null && request.getParameter("circuitoActivo").trim() != "") {
					circuitoEdicion.setActivo(true);
				}else {
					circuitoEdicion.setActivo(false);
				}
				circuitoEdicion.setMail(request.getParameter("mailNotificaciones"));
				circuitoEdicion.setCir_LecMinParaCons(Integer.parseInt(request.getParameter("lecMinCons")));
				if (request.getParameter("transitoActivado") != null && request.getParameter("transitoActivado").trim() != "") {
					circuitoEdicion.setTransitoActivado(true);
				}else {
					circuitoEdicion.setTransitoActivado(false);
				}
				if (request.getParameter("lanPedAut") != null && request.getParameter("lanPedAut").trim() != "") {
					circuitoEdicion.setCir_LanPedAut(true);
				}else {
					circuitoEdicion.setCir_LanPedAut(false);
				}
				if (request.getParameter("pedCompraAut") != null && request.getParameter("pedCompraAut").trim() != "") {
					circuitoEdicion.setCir_RegistrarPedCompra(true);
				}else {
					circuitoEdicion.setCir_RegistrarPedCompra(false);
				}
				if (request.getParameter("reaproAgrup") != null && request.getParameter("reaproAgrup").trim() != "") {
					circuitoEdicion.setCir_ReaprovAgrup(true);
				}else {
					circuitoEdicion.setCir_ReaprovAgrup(false);
				}
				if (request.getParameter("cirConsigna") != null && request.getParameter("cirConsigna").trim() != "") {
					circuitoEdicion.setCir_Consigna(true);
				}else {
					circuitoEdicion.setCir_Consigna(false);
				}
				circuitoEdicion.setCir_ConSalesType(request.getParameter("salesType"));
				circuitoEdicion.setCir_ConInventLocationId(request.getParameter("inventLocationID"));
				circuitoEdicion.setCir_PedMarco(request.getParameter("pedMarco"));
				circuitoEdicion.setCir_AlbaranarAut(Integer.parseInt(request.getParameter("albAut")));
				if (request.getParameter("factAgrupada") != null && request.getParameter("factAgrupada").trim() != "") {
					circuitoEdicion.setCir_ConFacturarAgrup(true);
				}else {
					circuitoEdicion.setCir_ConFacturarAgrup(false);
				}
				circuitoEdicion.setCir_Empresa(request.getParameter("empresa"));
				circuitoEdicion.setCir_NumPedCli(request.getParameter("numPedCli"));
				circuitoEdicion.setEntregaDias(Integer.parseInt(request.getParameter("ctdEntregaDias")));
				circuitoEdicion.setEntregaHoras(Integer.parseInt(request.getParameter("ctdEntregaHoras")));
				circuitoEdicion.setCir_HoraM(request.getParameter("horaM"));
				circuitoEdicion.setCir_HoraT(request.getParameter("horaT"));
				if (request.getParameter("intLun") != null && request.getParameter("intLun").trim() != "") {
					circuitoEdicion.setCir_IntLun(true);
				}else {
					circuitoEdicion.setCir_IntLun(false);
				}
				circuitoEdicion.setCir_LunM(Integer.parseInt(request.getParameter("ctdLunM")));
				circuitoEdicion.setCir_LunT(Integer.parseInt(request.getParameter("ctdLunT")));
				if (request.getParameter("intMar") != null && request.getParameter("intMar").trim() != "") {
					circuitoEdicion.setCir_IntMar(true);
				}else {
					circuitoEdicion.setCir_IntMar(false);
				}
				circuitoEdicion.setCir_MarM(Integer.parseInt(request.getParameter("ctdMarM")));
				circuitoEdicion.setCir_MarT(Integer.parseInt(request.getParameter("ctdMarT")));
				if (request.getParameter("intMie") != null && request.getParameter("intMie").trim() != "") {
					circuitoEdicion.setCir_IntMie(true);
				}else {
					circuitoEdicion.setCir_IntMie(false);
				}
				circuitoEdicion.setCir_MieM(Integer.parseInt(request.getParameter("ctdMieM")));
				circuitoEdicion.setCir_MieT(Integer.parseInt(request.getParameter("ctdMieT")));
				if (request.getParameter("intJue") != null && request.getParameter("intJue").trim() != "") {
					circuitoEdicion.setCir_IntJue(true);
				}else {
					circuitoEdicion.setCir_IntJue(false);
				}
				circuitoEdicion.setCir_JueM(Integer.parseInt(request.getParameter("ctdJueM")));
				circuitoEdicion.setCir_JueT(Integer.parseInt(request.getParameter("ctdJueT")));
				if (request.getParameter("intVie") != null && request.getParameter("intVie").trim() != "") {
					circuitoEdicion.setCir_IntVie(true);
				}else {
					circuitoEdicion.setCir_IntVie(false);
				}
				circuitoEdicion.setCir_VieM(Integer.parseInt(request.getParameter("ctdVieM")));
				circuitoEdicion.setCir_VieT(Integer.parseInt(request.getParameter("ctdVieT")));
				if (request.getParameter("intSab") != null && request.getParameter("intSab").trim() != "") {
					circuitoEdicion.setCir_IntSab(true);
				}else {
					circuitoEdicion.setCir_IntSab(false);
				}
				circuitoEdicion.setCir_SabT(Integer.parseInt(request.getParameter("ctdSabM")));
				circuitoEdicion.setCir_VieT(Integer.parseInt(request.getParameter("ctdSabT")));
				if (request.getParameter("intDom") != null && request.getParameter("intDom").trim() != "") {
					circuitoEdicion.setCir_IntDom(true);
				}else {
					circuitoEdicion.setCir_IntDom(false);
				}
				circuitoEdicion.setCir_DomM(Integer.parseInt(request.getParameter("ctdDomM")));
				circuitoEdicion.setCir_DomT(Integer.parseInt(request.getParameter("ctdDomT")));
				try {
					entityManager.getTransaction().begin();
					entityManager.persist(circuitoEdicion);
					entityManager.getTransaction().commit();
				}catch (Exception e2) {
					
				}
				
			}
		}
		circuitoEdicion = null;
		response.sendRedirect("./circuits");
		this.destroy();
		if (entityManager.isOpen()) {
			entityManager.close();
			factory.close();
		}
//		doGet(request, response);
	}

}
