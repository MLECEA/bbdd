package com.orkli.controller;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.orkli.domain.Circuito;
import com.orkli.domain.SgaUsuario;
import com.orkli.domain.Ubicacion;

/**
 * Servlet implementation class UbicacionController
 */
@WebServlet("/UbicacionController")
public class UbicacionController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UbicacionController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sesion = request.getSession();
		SgaUsuario usuario = (SgaUsuario) sesion.getAttribute("usuario");
		if (usuario == null) {
			response.sendRedirect("./login");
		}else {
			String esAdministrador = (String) sesion.getAttribute("esAdministrador");
			if (esAdministrador != null && esAdministrador.equals("true")) {
				factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
				entityManager = factory.createEntityManager();
				StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PCtaGetUbicaciones", Ubicacion.class);
				query.registerStoredProcedureParameter("codUser", String.class, ParameterMode.IN);
				query.registerStoredProcedureParameter("mode", Integer.class, ParameterMode.IN);
				query.setParameter("codUser", "-1");
				query.setParameter("mode", 2);
				List<Ubicacion> listaUbicaciones = query.getResultList();
				request.setAttribute("LUbicaciones", listaUbicaciones);
				getServletContext().getRequestDispatcher("/ubicaciones.jsp").forward(request, response);
			} else {
				response.sendError(401);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
	}

}
