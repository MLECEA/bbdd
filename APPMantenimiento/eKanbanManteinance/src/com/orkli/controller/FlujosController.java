package com.orkli.controller;

        import java.io.IOException;
        import java.util.List;

        import javax.persistence.EntityManager;
        import javax.persistence.EntityManagerFactory;
        import javax.persistence.Persistence;
        import javax.persistence.Query;
        import javax.servlet.ServletException;
        import javax.servlet.annotation.WebServlet;
        import javax.servlet.http.HttpServlet;
        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpServletResponse;

        import com.orkli.domain.SgaUsuario;
        import com.orkli.domain.dto.FlujoDTO;

/**
 * Servlet implementation class AccionesController
 */
@WebServlet("/FlujosController")
public class FlujosController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static EntityManagerFactory factory = null;
    private static EntityManager entityManager = null;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FlujosController() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		response.getWriter().append("Served at: ").append(request.getContextPath());
        factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
        entityManager = factory.createEntityManager();
        SgaUsuario usuario = (SgaUsuario) request.getSession().getAttribute("usuario");
        if (usuario == null) {
            response.sendRedirect("./login");
            return;
        }
        //Query query = entityManager.createNativeQuery("Select * from TFlujo", TFlujo.class);
        Query query = entityManager.createQuery(
                                "select new com.orkli.domain.dto.FlujoDTO(f.idFlujo, f.descripcion, f.activo, f.iconoURL, " +
                                        "fu.idUsuario, " +
                                        " u.nombre , u.apellidos, " +
                                        " fu.descripcion, fu.prioridad, " +
                                        " m.idMatricula, m.ubicacion, m.subUbicacion, " +
                                        " m.fechaInicio, m.fechaFin, m.cancelado) " +
                                        " from TFlujoUsuario fu " +
                                        " join TFlujo f on fu.idFlujo = f.idFlujo" +
                                        " join SgaUsuario u on u.codUsuario = fu.idUsuario " +
                                        " join Matricula m on m.idUsuario = u.username and m.idFlujo = fu.idFlujo", FlujoDTO.class);
        List<FlujoDTO> listaFlujos = query.getResultList();
        request.setAttribute("LFlujos", listaFlujos);
        getServletContext().getRequestDispatcher("/flujosManagement.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        doGet(request, response);
    }

}

