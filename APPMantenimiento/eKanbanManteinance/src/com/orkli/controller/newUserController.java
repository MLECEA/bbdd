package com.orkli.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orkli.domain.SgaRol;
import com.orkli.domain.SgaUsuario;
import com.orkli.domain.TEmpresa;
import com.orkli.domain.Ubicacion;

/**
 * Servlet implementation class newUserController
 */
@WebServlet("/newUserController")
public class newUserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public newUserController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
		entityManager = factory.createEntityManager();
		StoredProcedureQuery query2 = entityManager.createStoredProcedureQuery("PCtaGetUbicaciones", Ubicacion.class);
		query2.registerStoredProcedureParameter("codUser", String.class, ParameterMode.IN);
		query2.registerStoredProcedureParameter("mode", Integer.class, ParameterMode.IN);
		query2.setParameter("codUser","-1");
		query2.setParameter("mode", 2);
		List<Ubicacion> listaUbicaciones = query2.getResultList();
		request.setAttribute("LUbicacionesNoUsuario", listaUbicaciones);
		List<TEmpresa> listaEmpresas = getListaEmpresas();
		request.setAttribute("LEmpresas", listaEmpresas);
		List<SgaRol> listaRoles = getListaRoles();
		request.setAttribute("LRoles", listaRoles);
		getServletContext().getRequestDispatcher("/newUser.jsp").forward(request, response);
	}
	
	private List<TEmpresa> getListaEmpresas () {
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PCtaGetEmpresas", TEmpresa.class);
		List<TEmpresa> listaUbicaciones = query.getResultList();
		return listaUbicaciones;
	}
	
	private List<SgaRol> getListaRoles () {
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PCtaGetAllRoles", SgaRol.class);
		List<SgaRol> listaRoles = query.getResultList();
		return listaRoles;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
		String msgTexto = "";
		if(request.getParameter("formSubmit").equals("Guardar")) {
			SgaUsuario usuarioEdicion = new SgaUsuario();
			usuarioEdicion.setNombre(request.getParameter("nombre"));
			usuarioEdicion.setApellidos(request.getParameter("apellidos"));
			usuarioEdicion.setMail(request.getParameter("email"));
			usuarioEdicion.setApellidos(request.getParameter("apellidos"));
			usuarioEdicion.setUsu_Idi(request.getParameter("idioma"));
			if (request.getParameter("usrEmpresa") != null) {
				usuarioEdicion.setUsu_Emp(Integer.parseInt(request.getParameter("usrEmpresa")));
			}
			if (request.getParameter("usrRol") != null) {
				usuarioEdicion.setPtrRol(BigDecimal.valueOf(Long.valueOf((request.getParameter("usrRol").toString()))));
			}
			
			//usuarioEdicion.setPtrRol(request.getParameter("usrRol"));
			usuarioEdicion.setUsername(request.getParameter("username"));
			usuarioEdicion.setPassword(request.getParameter("pwd"));
			if (request.getParameter("activo") != null && request.getParameter("activo").trim() != "") {
				usuarioEdicion.setActivo(Short.parseShort("1"));
			}else {
				usuarioEdicion.setActivo(Short.parseShort("0"));
			}
			try {
				usuarioEdicion.setTokenAuten_CaducidadMins(Integer.parseInt(request.getParameter("tokenAuten")));
			}catch (Exception ex) {
				usuarioEdicion.setTokenAuten_CaducidadMins(0);
			}
			try {
				usuarioEdicion.setTokenRefresco_CaducidadMins(Integer.parseInt(request.getParameter("tokenRefresh")));
			}catch (Exception ex1) {
				usuarioEdicion.setTokenRefresco_CaducidadMins(0);
			}
			
			if (request.getParameter("pasarelaActivada") != null && request.getParameter("pasarelaActivada").trim() != "") {
				usuarioEdicion.setUsu_PasarelaActivada(true);
			}else {
				usuarioEdicion.setUsu_PasarelaActivada(false);
			}
			usuarioEdicion.setFTP_Server(request.getParameter("srvFTP"));
			usuarioEdicion.setFTP_Username(request.getParameter("userFTP"));
			usuarioEdicion.setFTP_Password(request.getParameter("pwdFTP"));
			try {
				usuarioEdicion.setOptimisticLock(BigDecimal.valueOf(Long.valueOf(request.getParameter("optimisticLock"))));
			}catch(Exception ex2) {
				usuarioEdicion.setOptimisticLock(BigDecimal.valueOf(0));
			}
			
			if (request.getParameter("subirFTP") != null && request.getParameter("subirFTP").trim() != "") {
				usuarioEdicion.setUsu_SubirDatosAFTP(true);
			}else {
				usuarioEdicion.setUsu_SubirDatosAFTP(false);
			}
			String ubicacionesStr = "";
			if (request.getParameterValues("usrUbicaciones") != null) {
				String ubicaciones[] = request.getParameterValues("usrUbicaciones");
				System.out.println(ubicaciones.toString());
				for (int i = 0; i < ubicaciones.length; i++) {
					if (ubicacionesStr.isEmpty()) {
						ubicacionesStr += ubicaciones[i];
					}else {
						ubicacionesStr += "|" + ubicaciones[i];
					}
				}
			}
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(usuarioEdicion);
				entityManager.getTransaction().commit();
				StoredProcedureQuery query2 = entityManager.createStoredProcedureQuery("PAX_ActualizarUbicacionesUsuario");
				query2.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
				query2.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
				query2.setParameter(1, String.valueOf(usuarioEdicion.getCodUsuario()));
				query2.setParameter(2, ubicacionesStr);
				query2.execute();
				msgTexto = "El usuario se ha creado correctamente.";
			}catch (EntityExistsException e1) {
				msgTexto = "Error al crear usuario, clave primaria duplicada";
			}catch (Exception e2) {
				msgTexto = "Error creando usuario";
			}
		}
		
//		usuarioEdicion = null;
//		response.sendRedirect("./users");
		if(request.getParameter("formSubmit").equals("Guardar")) {
			msgTexto = URLEncoder.encode(msgTexto, "UTF-8");
			response.sendRedirect("./message.jsp?msgTexto=" + msgTexto + "&redirectOpt=1");
		}else {
			response.sendRedirect("./users");
		}
		
		this.destroy();
		if (entityManager.isOpen()) {
			entityManager.close();
			factory.close();
		}
	}

}
