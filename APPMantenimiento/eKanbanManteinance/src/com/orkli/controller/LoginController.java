package com.orkli.controller;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.orkli.domain.SgaUsuario;

/**
 * Servlet implementation class LoginController
 */
//@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;
	      
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
	public void init() throws ServletException {
		System.out.println("Servlet " + this.getServletName() + " has started");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		//PrintWriter out = response.getWriter();
		//out.println("prueba");
//		request.setAttribute("message", "kkkkkkkkkk");
		factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
		entityManager = factory.createEntityManager();
		request.setAttribute("msgError", "none");
		getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
		System.out.println("test");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		factory = Persistence.createEntityManagerFactory("eKanbanManteinance"); //imr
		entityManager = factory.createEntityManager(); //imr
        request.removeAttribute("msgError");
		String username = request.getParameter("username").toString();
		String pwd = request.getParameter("password").toString();
		System.out.println(username);
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("login_isUser", SgaUsuario.class);
		query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
		query.setParameter(1, username);
		query.setParameter(2, pwd);
		List<SgaUsuario> resultado = query.getResultList();

		if (resultado == null || resultado.size() == 0) {
			request.setAttribute("msgError", "inline");
			getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
			return;
		}else {
			SgaUsuario usuario = resultado.get(0);
			request.getSession().setAttribute("usuario", usuario);
			HttpSession sesion = request.getSession();
			if (usuario != null && usuario.getIsDBAdmin() == 1) {
				sesion.setAttribute("esAdministrador", "true");
				response.sendRedirect("./users");
			}else {
				sesion.setAttribute("esAdministrador", "false");
				response.sendRedirect("./flujos");
			}
		}
		entityManager.close();
		factory.close();
		
	}

}
