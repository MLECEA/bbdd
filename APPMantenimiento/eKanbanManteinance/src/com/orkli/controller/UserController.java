package com.orkli.controller;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.orkli.domain.SgaUsuario;

public class UserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserController() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
	public void init() throws ServletException {
		System.out.println("Servlet " + this.getServletName() + " has started");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sesion = request.getSession();
		SgaUsuario usuario = (SgaUsuario) sesion.getAttribute("usuario");
		if (usuario == null) {
			response.sendRedirect("./login");
		}else {
			String esAdministrador = (String) sesion.getAttribute("esAdministrador");
			if (esAdministrador != null && esAdministrador.equals("true")) {
				factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
				entityManager = factory.createEntityManager();
				StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PAccGetAllUsers", SgaUsuario.class);
				List<SgaUsuario> resultado = query.getResultList();
				request.setAttribute("LUsuarios", resultado);
				getServletContext().getRequestDispatcher("/UserManagement.jsp").forward(request, response);
			} else {
				response.sendError(401);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		String username = request.getParameter("username").toString();
		String pwd = request.getParameter("password").toString();
		System.out.println(username);
//		entityManager.close();
//		factory.close();
		this.destroy();
		entityManager.close();
		factory.close();
//		response.sendRedirect("./UserManagement.jsp");
	}
}
