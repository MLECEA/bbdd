package com.orkli.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orkli.domain.Circuito;
import com.orkli.domain.CircuitoPK;
import com.orkli.domain.TipoCircuito;
import com.orkli.domain.TipoContenedor;
import com.orkli.domain.Ubicacion;

/**
 * Servlet implementation class CopyCircuit
 */
@WebServlet("/CopyCircuit")
public class CopyCircuit extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;
	private Circuito circuitoNuevo = new Circuito();   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CopyCircuit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String idCircuito = request.getParameter("circuitID");
		idCircuito = URLDecoder.decode(idCircuito, "UTF-8");
		request.setAttribute("idCircuito", idCircuito);
		factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
		entityManager = factory.createEntityManager();
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PCtaGetCircuitoInfo", Circuito.class);
		query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		query.setParameter(1, idCircuito);
		List<Circuito> resultado = query.getResultList();
		if (resultado != null) {
			circuitoNuevo = resultado.get(0);
//			circuitoNuevo.getId().setIdCircuito(circuitoNuevo.getId().getIdCircuito() + "_copia");
			request.setAttribute("circuito", circuitoNuevo);
			StoredProcedureQuery query2 = entityManager.createStoredProcedureQuery("PCtaGetTiposCircuito", TipoCircuito.class);
			List<TipoCircuito> listaTiposCircuito = query2.getResultList();
			request.setAttribute("LTipoCircuito", listaTiposCircuito);
			StoredProcedureQuery query3 = entityManager.createStoredProcedureQuery("PCtaGetUbicaciones", Ubicacion.class);
			query3.registerStoredProcedureParameter("codUser", String.class, ParameterMode.IN);
			query3.registerStoredProcedureParameter("mode", Integer.class, ParameterMode.IN);
			query3.setParameter("codUser", "-1");
			query3.setParameter("mode", 2);
			List<Ubicacion> listaUbicaciones = query3.getResultList();
			request.setAttribute("LUbicaciones", listaUbicaciones);
			query3 = entityManager.createStoredProcedureQuery("PCtaGetTiposContenedor", TipoContenedor.class);
			List<TipoContenedor> listaContenedores = query3.getResultList();
			request.setAttribute("LTipoContenedor", listaContenedores);
			request.setAttribute("msgError", "hidden");
		}
		getServletContext().getRequestDispatcher("/copyCircuit.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
		
		Circuito circuito = new Circuito();
		if(request.getParameter("formSubmit").equals("Guardar")) {
			CircuitoPK key = new CircuitoPK();
			key.setIdCircuito(request.getParameter("idCircuito"));
			key.setClienteId(request.getParameter("clienteID"));
			key.setReferenciaId(request.getParameter("refID"));
			key.setUbicacion(request.getParameter("cUbicacion"));
			circuito.setId(key);
			circuito.setDescripcion(request.getParameter("refDesc"));
			circuito.setNumKanban(Integer.parseInt(request.getParameter("ctdNumKanban")));
			circuito.setCantidadPiezas(Integer.parseInt(request.getParameter("ctdPiezas")));
			circuito.setNum_kanbanOPT(Integer.parseInt(request.getParameter("ctdOpt")));
			circuito.setTipoCircuito(BigDecimal.valueOf(Long.parseLong(request.getParameter("tipoCircuito"))));
			circuito.setTipoContenedor(BigDecimal.valueOf(Long.parseLong(request.getParameter("tipoContenedor"))));
			circuito.setClienteNombre(request.getParameter("nombreCliente"));
			circuito.setRefCliente(request.getParameter("refCliente"));
			circuito.setProveedor_id(request.getParameter("proveedorID"));
			if (request.getParameter("circuitoActivo") != null && request.getParameter("circuitoActivo").trim() != "") {
				circuito.setActivo(true);
			}else {
				circuito.setActivo(false);
			}
			circuito.setMail(request.getParameter("mailNotificaciones"));
			circuito.setCir_LecMinParaCons(Integer.parseInt(request.getParameter("lecMinCons")));
			if (request.getParameter("transitoActivado") != null && request.getParameter("transitoActivado").trim() != "") {
				circuito.setTransitoActivado(true);
			}else {
				circuito.setTransitoActivado(false);
			}
			if (request.getParameter("lanPedAut") != null && request.getParameter("lanPedAut").trim() != "") {
				circuito.setCir_LanPedAut(true);
			}else {
				circuito.setCir_LanPedAut(false);
			}
			if (request.getParameter("pedCompraAut") != null && request.getParameter("pedCompraAut").trim() != "") {
				circuito.setCir_RegistrarPedCompra(true);
			}else {
				circuito.setCir_RegistrarPedCompra(false);
			}
			if (request.getParameter("reaproAgrup") != null && request.getParameter("reaproAgrup").trim() != "") {
				circuito.setCir_ReaprovAgrup(true);
			}else {
				circuito.setCir_ReaprovAgrup(false);
			}
			if (request.getParameter("cirConsigna") != null && request.getParameter("cirConsigna").trim() != "") {
				circuito.setCir_Consigna(true);
			}else {
				circuito.setCir_Consigna(false);
			}
			circuito.setCir_ConSalesType(request.getParameter("salesType"));
			circuito.setCir_ConInventLocationId(request.getParameter("inventLocationID"));
			circuito.setCir_PedMarco(request.getParameter("pedMarco"));
			circuito.setCir_AlbaranarAut(Integer.parseInt(request.getParameter("albAut")));
			if (request.getParameter("factAgrupada") != null && request.getParameter("factAgrupada").trim() != "") {
				circuito.setCir_ConFacturarAgrup(true);
			}else {
				circuito.setCir_ConFacturarAgrup(false);
			}
			circuito.setCir_Empresa(request.getParameter("empresa"));
			circuito.setCir_NumPedCli(request.getParameter("numPedCli"));
			circuito.setEntregaDias(Integer.parseInt(request.getParameter("ctdEntregaDias")));
			circuito.setEntregaHoras(Integer.parseInt(request.getParameter("ctdEntregaHoras")));
			circuito.setCir_HoraM(request.getParameter("horaM"));
			circuito.setCir_HoraT(request.getParameter("horaT"));
			if (request.getParameter("intLun") != null && request.getParameter("intLun").trim() != "") {
				circuito.setCir_IntLun(true);
			}else {
				circuito.setCir_IntLun(false);
			}
			circuito.setCir_LunM(Integer.parseInt(request.getParameter("ctdLunM")));
			circuito.setCir_LunT(Integer.parseInt(request.getParameter("ctdLunT")));
			if (request.getParameter("intMar") != null && request.getParameter("intMar").trim() != "") {
				circuito.setCir_IntMar(true);
			}else {
				circuito.setCir_IntMar(false);
			}
			circuito.setCir_MarM(Integer.parseInt(request.getParameter("ctdMarM")));
			circuito.setCir_MarT(Integer.parseInt(request.getParameter("ctdMarT")));
			if (request.getParameter("intMie") != null && request.getParameter("intMie").trim() != "") {
				circuito.setCir_IntMie(true);
			}else {
				circuito.setCir_IntMie(false);
			}
			circuito.setCir_MieM(Integer.parseInt(request.getParameter("ctdMieM")));
			circuito.setCir_MieT(Integer.parseInt(request.getParameter("ctdMieT")));
			if (request.getParameter("intJue") != null && request.getParameter("intJue").trim() != "") {
				circuito.setCir_IntJue(true);
			}else {
				circuito.setCir_IntJue(false);
			}
			circuito.setCir_JueM(Integer.parseInt(request.getParameter("ctdJueM")));
			circuito.setCir_JueT(Integer.parseInt(request.getParameter("ctdJueT")));
			if (request.getParameter("intVie") != null && request.getParameter("intVie").trim() != "") {
				circuito.setCir_IntVie(true);
			}else {
				circuito.setCir_IntVie(false);
			}
			circuito.setCir_VieM(Integer.parseInt(request.getParameter("ctdVieM")));
			circuito.setCir_VieT(Integer.parseInt(request.getParameter("ctdVieT")));
			if (request.getParameter("intSab") != null && request.getParameter("intSab").trim() != "") {
				circuito.setCir_IntSab(true);
			}else {
				circuito.setCir_IntSab(false);
			}
			circuito.setCir_SabT(Integer.parseInt(request.getParameter("ctdSabM")));
			circuito.setCir_VieT(Integer.parseInt(request.getParameter("ctdSabT")));
			if (request.getParameter("intDom") != null && request.getParameter("intDom").trim() != "") {
				circuito.setCir_IntDom(true);
			}else {
				circuito.setCir_IntDom(false);
			}
			circuito.setCir_DomM(Integer.parseInt(request.getParameter("ctdDomM")));
			circuito.setCir_DomT(Integer.parseInt(request.getParameter("ctdDomT")));
//			System.out.println(request.getParameter("usrUbicaciones"));
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(circuito);
				entityManager.getTransaction().commit();
				circuitoNuevo = null;
				response.sendRedirect("./circuits");
				this.destroy();
				if (entityManager.isOpen()) {
					entityManager.close();
					factory.close();
				}
			}catch (Exception ex) {
				return;
				
//				return;
			}
		}
		circuitoNuevo = null;
		response.sendRedirect("./circuits");
		this.destroy();
		if (entityManager.isOpen()) {
			entityManager.close();
			factory.close();
		}
	}

}
