package com.orkli.controller;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orkli.domain.SubUbicacion;
import com.orkli.domain.Ubicacion;

/**
 * Servlet implementation class SubUbicacionEditController
 */
@WebServlet("/SubUbicacionEditController")
public class SubUbicacionEditController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;
	private SubUbicacion sububicacionEdicion = null;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubUbicacionEditController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String idSububicacion = request.getParameter("sububicacionID");
		idSububicacion = URLDecoder.decode(idSububicacion, "UTF-8");
		int mode = Integer.parseInt( request.getParameter("mode").toString().trim());
		request.setAttribute("idSububicacion", idSububicacion);
		factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
		entityManager = factory.createEntityManager();
		Query consulta = entityManager.createNativeQuery("Select * from subUbicacion where id_SubUbicacion='" + idSububicacion + "'", SubUbicacion.class);
		List<SubUbicacion> lSubUbicaciones = consulta.getResultList();
		if (lSubUbicaciones != null && lSubUbicaciones.size() > 0) {
			sububicacionEdicion = lSubUbicaciones.get(0);
			request.setAttribute("sububicacion", sububicacionEdicion);
			if (mode == 1) {
				getServletContext().getRequestDispatcher("/editSubUbicacion.jsp").forward(request, response);
			}
			if (mode == 2){
				getServletContext().getRequestDispatcher("/copySubUbicacion.jsp").forward(request, response);
			}
			if (mode == 3){
				getServletContext().getRequestDispatcher("/deleteSububicacion.jsp").forward(request, response);
			}
			
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
		String msgTexto = "";
		if(request.getParameter("formSubmit").equals("Guardar")) {
			sububicacionEdicion.setDescripcion(request.getParameter("descripcion"));
			if (request.getParameter("activo") != null && request.getParameter("activo").trim() != "") {
				sububicacionEdicion.setActivo(Short.parseShort("1"));
			}else {
				sububicacionEdicion.setActivo(Short.parseShort("0"));
			}
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(sububicacionEdicion);
				entityManager.getTransaction().commit();
				msgTexto = "Sububicacion actualizada correctamente";
			}catch(Exception e) {
				msgTexto = "Error al actualizar sububicacion";
			}
		}
		if (request.getParameter("formSubmit").equals("Desactivar")) {
			sububicacionEdicion.setActivo(Short.parseShort("0"));
			entityManager.getTransaction().begin();
			entityManager.persist(sububicacionEdicion);
			entityManager.getTransaction().commit();
			msgTexto = "La sububicación se ha marcado como inactiva";
		}
		if(request.getParameter("formSubmit").equals("Copiar")) {
			if (entityManager.find(SubUbicacion.class, request.getParameter("idSubUbic")) == null) {
				SubUbicacion copia = new SubUbicacion();
				copia.setId_SubUbicacion(request.getParameter("idSubUbic"));
				copia.setDescripcion(request.getParameter("descripcion"));
				if (request.getParameter("activo") != null && request.getParameter("activo").trim() != "") {
					copia.setActivo(Short.parseShort("1"));
				}else {
					copia.setActivo(Short.parseShort("0"));
				}
				try {
					entityManager.getTransaction().begin();
					entityManager.persist(copia);
					entityManager.getTransaction().commit();
					msgTexto = "Sububicacion copiada correctamente";
				}catch(Exception e) {
					msgTexto = "Error al copiar sububicacion";
				}
			}else {
				msgTexto="Ya existe una sububicacion con el ID introducido";
			}
			
		}
		sububicacionEdicion = null;
		if(request.getParameter("formSubmit").equals("Guardar") || request.getParameter("formSubmit").equals("Copiar")) {
			msgTexto = URLEncoder.encode(msgTexto, "UTF-8");
			response.sendRedirect("./message.jsp?msgTexto=" + msgTexto + "&redirectOpt=5");
		}else {
			response.sendRedirect("./subUbicaciones");
		}
	}

}
