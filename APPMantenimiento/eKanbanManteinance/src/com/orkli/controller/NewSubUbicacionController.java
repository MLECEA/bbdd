package com.orkli.controller;

import java.io.IOException;
import java.net.URLEncoder;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orkli.domain.SubUbicacion;

/**
 * Servlet implementation class NewSubUbicacionController
 */
@WebServlet("/NewSubUbicacionController")
public class NewSubUbicacionController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewSubUbicacionController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
		entityManager = factory.createEntityManager();
		getServletContext().getRequestDispatcher("/newSubUbicacion.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
		String msgTexto = "";
		if(request.getParameter("formSubmit").equals("Guardar")) {
			SubUbicacion newSubUbi = new SubUbicacion();
			newSubUbi.setId_SubUbicacion(request.getParameter("idSubUbi"));
			newSubUbi.setDescripcion(request.getParameter("desc"));
			try {
				if (entityManager.find(SubUbicacion.class, newSubUbi.getId_SubUbicacion())== null) {
					entityManager.getTransaction().begin();
					entityManager.persist(newSubUbi);
					entityManager.getTransaction().commit();
					msgTexto = "SubUbicacion creada correctamente.";
				}
				else {
					msgTexto = "Error al crear sububicacion, ya existe una sububicacion con la ID introducida";
				}
			}
			catch (EntityExistsException e1) {
				msgTexto = "Error al crear sububicacion, revise lo datos, puede haber una sububicacion con la ID introducida";
			}catch (Exception e2) {
				
				msgTexto = "Error creando sububicacion.";
			}
			
		}
		if(request.getParameter("formSubmit").equals("Guardar")) {
			msgTexto = URLEncoder.encode(msgTexto, "UTF-8");
			response.sendRedirect("./message.jsp?msgTexto=" + msgTexto + "&redirectOpt=3");
		}else {
			response.sendRedirect("./ubicaciones");
		}
		
		this.destroy();
		if (entityManager.isOpen()) {
			entityManager.close();
			factory.close();
		}
	}

}
