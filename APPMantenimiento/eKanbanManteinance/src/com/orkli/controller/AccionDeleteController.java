package com.orkli.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orkli.domain.SgaUsuario;
import com.orkli.domain.TAccionesReport;

/**
 * Servlet implementation class AccionDeleteController
 */
@WebServlet("/AccionDeleteController")
public class AccionDeleteController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;
	private TAccionesReport accionEdicion = null; 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AccionDeleteController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String accionID = request.getParameter("accionID");
		request.setAttribute("accionID", accionID);
		factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
		entityManager = factory.createEntityManager();
		Query query = entityManager.createNativeQuery("Select * from TAccionesReports where AccRep_Idd ='" + accionID + "'", TAccionesReport.class);
		List<TAccionesReport> resultado = query.getResultList();
		if (resultado != null) {
			accionEdicion = resultado.get(0);
			getServletContext().getRequestDispatcher("/deleteAccion.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
		if(request.getParameter("formSubmit").equals("Confirmar")) {
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(accionEdicion);
				entityManager.getTransaction().commit();
				String msgTexto = URLEncoder.encode("Accion eliminada con �xito", "UTF-8");
				response.sendRedirect("./message.jsp?msgTexto=" + msgTexto + "&redirectOpt=4");
			}catch (Exception e) {
				String msgTexto = URLEncoder.encode("No se puede eliminar la acci�n, datos registrados", "UTF-8");
				response.sendRedirect("./message.jsp?msgTexto=" + msgTexto + "&redirectOpt=4");
			}
		}else {
			response.sendRedirect("./acciones");
		}
		this.destroy();
		if (entityManager.isOpen()) {
			entityManager.close();
			factory.close();
		}
	}

}
