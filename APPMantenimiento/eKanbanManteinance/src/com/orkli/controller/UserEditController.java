package com.orkli.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//imrimport org.apache.tomcat.util.codec.binary.StringUtils;

import com.orkli.domain.Circuito;
import com.orkli.domain.SgaRol;
import com.orkli.domain.SgaUsuario;
import com.orkli.domain.TEmpresa;
import com.orkli.domain.Ubicacion;
import com.orkli.domain.UsuarioProveedor;
import com.orkli.domain.UsuarioUbicacion;

/**
 * Servlet implementation class UserEditController
 */
@WebServlet("/UserEditController")
public class UserEditController extends HttpServlet implements Servlet {
	private static final long serialVersionUID = 1L;
	
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;
    private SgaUsuario usuarioEdicion = null;
    private String strPasaleraActiva = "";
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserEditController() {
        super();
        // TODO Auto-generated constructor stub
    }
    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String codUser = request.getParameter("codUser");
		request.setAttribute("codUser", codUser);
		factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
		entityManager = factory.createEntityManager();
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PAccGetAllUsers", SgaUsuario.class);
		query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		query.setParameter(1, codUser);
		//query.getResultList();
		@SuppressWarnings("unchecked")
		List<SgaUsuario> resultado = query.getResultList();
		if (resultado != null) {
			usuarioEdicion = resultado.get(0);
			request.setAttribute("UserData", resultado.get(0));
			StoredProcedureQuery query2 = entityManager.createStoredProcedureQuery("PCtaGetUbicaciones", Ubicacion.class);
			query2.registerStoredProcedureParameter("codUser", String.class, ParameterMode.IN);
			query2.registerStoredProcedureParameter("mode", Integer.class, ParameterMode.IN);
			query2.setParameter("codUser", codUser);
			query2.setParameter("mode", 1);
			List<Ubicacion> listaUbicaciones = query2.getResultList();
			request.setAttribute("LUbicacionesNoUsuario", listaUbicaciones);
			StoredProcedureQuery query3 = entityManager.createStoredProcedureQuery("PCtaGetUbicaciones", UsuarioUbicacion.class);
			query3.registerStoredProcedureParameter("codUser", String.class, ParameterMode.IN);
			query3.registerStoredProcedureParameter("mode", Integer.class, ParameterMode.IN);
			query3.setParameter("codUser", codUser);
			query3.setParameter("mode", 0);
			List<UsuarioUbicacion> listaUsuarioUbicaciones = query3.getResultList();
			request.setAttribute("LUbicaciones", listaUsuarioUbicaciones);
			List<TEmpresa> listaEmpresas = getListaEmpresas();
			request.setAttribute("LEmpresas", listaEmpresas);
			List<SgaRol> listaRoles = getListaRoles();
			request.setAttribute("LRoles", listaRoles);
			Query consulta = entityManager.createNativeQuery("Select * from Usuario_Proveedor where id_usuario=" + codUser, UsuarioProveedor.class);
			List<UsuarioProveedor> listaProveedores = consulta.getResultList();
			request.setAttribute("LProveedores", listaProveedores);
			getServletContext().getRequestDispatcher("/EditUser.jsp").forward(request, response);
		}
		
	}
	
	private List<TEmpresa> getListaEmpresas () {
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PCtaGetEmpresas", TEmpresa.class);
		List<TEmpresa> listaUbicaciones = query.getResultList();
		return listaUbicaciones;
	}
	
	private List<SgaRol> getListaRoles () {
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PCtaGetAllRoles", SgaRol.class);
		List<SgaRol> listaRoles = query.getResultList();
		return listaRoles;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String msgTexto = "";
		if(request.getParameter("formSubmit").equals("Guardar")) {
			usuarioEdicion.setNombre(request.getParameter("nombre"));
			usuarioEdicion.setApellidos(request.getParameter("apellidos"));
			usuarioEdicion.setMail(request.getParameter("email"));
			usuarioEdicion.setApellidos(request.getParameter("apellidos"));
			usuarioEdicion.setUsu_Idi(request.getParameter("idioma"));
			usuarioEdicion.setUsu_Emp(Integer.parseInt(request.getParameter("usrEmpresa")));
			usuarioEdicion.setPtrRol(BigDecimal.valueOf(Long.valueOf((request.getParameter("usrRol").toString()))));
			//usuarioEdicion.setPtrRol(request.getParameter("usrRol"));
			usuarioEdicion.setUsername(request.getParameter("username"));
			usuarioEdicion.setPassword(request.getParameter("pwd"));
			if (request.getParameter("activo") != null && request.getParameter("activo").trim() != "") {
				usuarioEdicion.setActivo(Short.parseShort("1"));
			}else {
				usuarioEdicion.setActivo(Short.parseShort("0"));
			}
			try {
				usuarioEdicion.setOptimisticLock(BigDecimal.valueOf(Long.valueOf(request.getParameter("optimisticLock"))));
			}catch(Exception ex2) {
				usuarioEdicion.setOptimisticLock(BigDecimal.valueOf(0));
			}
			usuarioEdicion.setTokenAuten_CaducidadMins(Integer.parseInt(request.getParameter("tokenAuten")));
			usuarioEdicion.setTokenRefresco_CaducidadMins(Integer.parseInt(request.getParameter("tokenRefresh")));
			if (request.getParameter("pasarelaActivada") != null && request.getParameter("pasarelaActivada").trim() != "") {
				usuarioEdicion.setUsu_PasarelaActivada(true);
			}else {
				usuarioEdicion.setUsu_PasarelaActivada(false);
			}
			usuarioEdicion.setFTP_Server(request.getParameter("srvFTP"));
			usuarioEdicion.setFTP_Username(request.getParameter("userFTP"));
			usuarioEdicion.setFTP_Password(request.getParameter("pwdFTP"));
			if (request.getParameter("subirFTP") != null && request.getParameter("subirFTP").trim() != "") {
				usuarioEdicion.setUsu_SubirDatosAFTP(true);
			}else {
				usuarioEdicion.setUsu_SubirDatosAFTP(false);
			}
			String ubicaciones[] = request.getParameterValues("usrUbicaciones");
//			System.out.println(ubicaciones.toString());
			String ubicacionesStr = "";
			if (ubicaciones != null) {
				for (int i = 0; i < ubicaciones.length; i++) {
					if (ubicacionesStr.isEmpty()) {
						ubicacionesStr += ubicaciones[i];
					}else {
						ubicacionesStr += "|" + ubicaciones[i];
					}
				}
			}
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(usuarioEdicion);
//				entityManager.getTransaction().commit();
				StoredProcedureQuery query2 = entityManager.createStoredProcedureQuery("PAX_ActualizarUbicacionesUsuario");
				query2.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
				query2.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
				query2.setParameter(1, String.valueOf(usuarioEdicion.getCodUsuario()));
				query2.setParameter(2, ubicacionesStr);
				query2.execute();
				msgTexto = "Usuario actualizado correctamente";
				
//				entityManager.getTransaction().begin();
				String[] listaProveedores = request.getParameterValues("proveedoresList");
				if (listaProveedores != null) {
					for (String proveedor : listaProveedores) {
						Query consulta = entityManager.createNativeQuery("Select * from usuario_proveedor where id_usuario=" + usuarioEdicion.getCodUsuario() + 
								" and id_proveedor='" + proveedor + "'", UsuarioProveedor.class);
						List<UsuarioProveedor> resultado = consulta.getResultList();
						if (resultado == null || resultado.size() <= 0) {
							UsuarioProveedor newUserProv = new UsuarioProveedor();
							newUserProv.setIdProveedor(proveedor);
							newUserProv.setSgaUsuario(usuarioEdicion);
							entityManager.persist(newUserProv);
						}
					}
				}
				
				entityManager.getTransaction().commit();
				
			}catch (EntityExistsException e1) {
				msgTexto = "Error actualizando datos del usuario, clave primaria duplicada.";
			}catch (Exception e2) {
				msgTexto = "Error actualizando datos del usuario. Intentelo de nuevo";
			}
			
		}
		
		usuarioEdicion = null;
//		response.sendRedirect("./users");
		if(request.getParameter("formSubmit").equals("Guardar")) {
			msgTexto = URLEncoder.encode(msgTexto, "UTF-8");
			response.sendRedirect("./message.jsp?msgTexto=" + msgTexto + "&redirectOpt=1");
		}else {
			response.sendRedirect("./users");
		}
		
		this.destroy();
		if (entityManager.isOpen()) {
			entityManager.close();
			factory.close();
		}
		
	}

}
