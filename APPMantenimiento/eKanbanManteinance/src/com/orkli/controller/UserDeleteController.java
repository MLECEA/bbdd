package com.orkli.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orkli.domain.SgaUsuario;

/**
 * Servlet implementation class UserDeleteController
 */
@WebServlet("/UserDeleteController")
public class UserDeleteController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;
    private SgaUsuario usuarioEdicion = null;   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String codUser = request.getParameter("codUser");
		request.setAttribute("codUser", codUser);
		factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
		entityManager = factory.createEntityManager();
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PAccGetAllUsers", SgaUsuario.class);
		query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		query.setParameter(1, codUser);
		//query.getResultList();
		@SuppressWarnings("unchecked")
		List<SgaUsuario> resultado = query.getResultList();
		if (resultado != null) {
			usuarioEdicion = resultado.get(0);
			getServletContext().getRequestDispatcher("/deleteUser.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("formSubmit").equals("Confirmar")) {
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(usuarioEdicion);
				entityManager.getTransaction().commit();
				String msgTexto = URLEncoder.encode("Usuario eliminado con �xito", "UTF-8");
				response.sendRedirect("./message.jsp?msgTexto=" + msgTexto + "&redirectOpt=1");
			}catch (Exception e) {
				String msgTexto = URLEncoder.encode("No se puede eliminar el usuario, datos registrados", "UTF-8");
				response.sendRedirect("./message.jsp?msgTexto=" + msgTexto + "&redirectOpt=1");
			}
		}else {
			response.sendRedirect("./users");
		}
		this.destroy();
		if (entityManager.isOpen()) {
			entityManager.close();
			factory.close();
		}
	}

}
