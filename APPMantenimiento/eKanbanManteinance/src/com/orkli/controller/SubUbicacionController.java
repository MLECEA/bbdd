package com.orkli.controller;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.orkli.domain.SgaUsuario;
import com.orkli.domain.SubUbicacion;

/**
 * Servlet implementation class SubUbicacionController
 */
@WebServlet("/SubUbicacionController")
public class SubUbicacionController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;     
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubUbicacionController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sesion = request.getSession();
		SgaUsuario usuario = (SgaUsuario) sesion.getAttribute("usuario");
		if (usuario == null) {
			response.sendRedirect("./login");
		}else {
			String esAdministrador = (String) sesion.getAttribute("esAdministrador");
			if (esAdministrador != null && esAdministrador.equals("true")) {
				factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
				entityManager = factory.createEntityManager();
				Query consulta = entityManager.createNativeQuery("Select * from subUbicacion", SubUbicacion.class);
				List<SubUbicacion> subUbicaciones = consulta.getResultList();
				request.setAttribute("LSubUbicaciones", subUbicaciones);
				getServletContext().getRequestDispatcher("/subUbicacionManagement.jsp").forward(request, response);
			} else {
				response.sendError(401);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
	}

}
