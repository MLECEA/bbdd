package com.orkli.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orkli.domain.SgaRol;
import com.orkli.domain.SgaUsuario;
import com.orkli.domain.TAccionesReport;
import com.orkli.domain.TEmpresa;
import com.orkli.domain.Ubicacion;
import com.orkli.domain.UsuarioUbicacion;

/**
 * Servlet implementation class CopyAccionController
 */
@WebServlet("/CopyAccionController")
public class CopyAccionController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;
	private TAccionesReport nuevaAccion = new TAccionesReport();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CopyAccionController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String accionID = request.getParameter("accionID");
		request.setAttribute("accionID", accionID);
		factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
		entityManager = factory.createEntityManager();
		Query query = entityManager.createNativeQuery("select * from TAccionesReports where AccRep_Idd='" + accionID + "'", TAccionesReport.class);
		List<TAccionesReport> resultado = query.getResultList();
		if (resultado != null) {
			nuevaAccion = resultado.get(0);
			request.setAttribute("accionData", nuevaAccion);
			Query query2 = entityManager.createNativeQuery("Select t2.* from usuario_AccReports as t1 inner join sga_usuario t2 on t1.id_usuario = t2.cod_usuario  where id_accrep='" + accionID + "'", SgaUsuario.class );
			List<SgaUsuario> resultado2 = query2.getResultList();
			request.setAttribute("LUsuariosConPermiso", resultado2);
			query2 = entityManager.createNativeQuery("Select t2.* from sga_usuario t2 where t2.cod_usuario not in (select id_usuario from usuario_AccReports where id_accrep='" + accionID + "')", SgaUsuario.class );
			List<SgaUsuario> resultado3 = query2.getResultList();
			request.setAttribute("LUsuariosSinPermiso", resultado3);
			getServletContext().getRequestDispatcher("/copyAccion.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
		String msgTexto = "";
		if(request.getParameter("formSubmit").equals("Guardar")) {
			
			TAccionesReport nuevaAccion = new TAccionesReport();
			nuevaAccion.setAccRep_Idd(request.getParameter("idRep"));
			nuevaAccion.setAccRep_Des(request.getParameter("descripcion"));
			nuevaAccion.setAccRep_Tip(request.getParameter("tipo"));
			nuevaAccion.setAccRep_LogURL(request.getParameter("logURL"));
			nuevaAccion.setAccRep_V1Txt(request.getParameter("accV1"));
			nuevaAccion.setAccRep_V1Def(request.getParameter("accV1Def"));
			nuevaAccion.setAccRep_V2Txt(request.getParameter("accV2"));
			nuevaAccion.setAccRep_V2Def(request.getParameter("accV2Def"));
			nuevaAccion.setAccRep_V3Txt(request.getParameter("accV3"));
			nuevaAccion.setAccRep_V3Def(request.getParameter("accV3Def"));
			nuevaAccion.setAccRep_Ser(request.getParameter("servidor"));
			nuevaAccion.setAccRep_Rep(request.getParameter("informe"));
			if (request.getParameter("activo") != null && request.getParameter("activo").trim() != "") {
				nuevaAccion.setAccRep_Act(Short.parseShort("1"));
			}else {
				nuevaAccion.setAccRep_Act(Short.parseShort("0"));
			}
			try{
				entityManager.getTransaction().begin();
				entityManager.persist(nuevaAccion);
				entityManager.getTransaction().commit();
				String usuariosAutorizados[] = request.getParameterValues("usuariosConPermisos");
				String arrUsuariosAutorizados = "";
				if (usuariosAutorizados != null) {
					for (int i = 0; i < usuariosAutorizados.length; i++) {
						if (arrUsuariosAutorizados.isEmpty()) {
							arrUsuariosAutorizados += usuariosAutorizados[i];
						}else {
							arrUsuariosAutorizados += "|" + usuariosAutorizados[i];
						}
					}
				}
				StoredProcedureQuery query2 = entityManager.createStoredProcedureQuery("PAX_ActualizarPermisosReport");
				query2.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
				query2.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
				query2.setParameter(1, nuevaAccion.getAccRep_Idd());
				query2.setParameter(2, arrUsuariosAutorizados);
				query2.execute();
				msgTexto = "Accion creada correctamente";
			} catch (EntityExistsException e1) {
				msgTexto = "Error creando accion, clave primaria duplicada.";
			}catch (Exception e2) {
				msgTexto = "Error creando acci�n. Intentelo de nuevo";
			}
		}
		nuevaAccion = null;
//		response.sendRedirect("./users");
		if(request.getParameter("formSubmit").equals("Guardar")) {
			msgTexto = URLEncoder.encode(msgTexto, "UTF-8");
			response.sendRedirect("./message.jsp?msgTexto=" + msgTexto + "&redirectOpt=3");
		}else {
			response.sendRedirect("./acciones");
		}
		
		this.destroy();
		if (entityManager.isOpen()) {
			entityManager.close();
			factory.close();
		}
	}

}
