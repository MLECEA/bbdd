package com.orkli.controller;

        import java.io.IOException;
        import java.util.List;

        import javax.persistence.EntityManager;
        import javax.persistence.EntityManagerFactory;
        import javax.persistence.Persistence;
        import javax.persistence.Query;
        import javax.servlet.ServletException;
        import javax.servlet.annotation.WebServlet;
        import javax.servlet.http.HttpServlet;
        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpServletResponse;

        import com.orkli.domain.dto.FlujoDTO;
        import com.orkli.domain.dto.FlujoRespuestasDTO;

/**
 * Servlet implementation class FlujoEditController
 */
@WebServlet("/FlujoEditController")
public class FlujoEditController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static EntityManagerFactory factory = null;
    private static EntityManager entityManager = null;
    private FlujoDTO accionEdicion = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FlujoEditController() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String idMatricula = request.getParameter("idMatricula");
            request.setAttribute("matriculaID", idMatricula);
            factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
            entityManager = factory.createEntityManager();
            Query query = entityManager.createQuery(
                    "select new com.orkli.domain.dto.FlujoDTO(f.idFlujo, f.descripcion, f.activo, f.iconoURL, " +
                            "fu.idUsuario, " +
                            " u.nombre , u.apellidos, " +
                            " fu.descripcion, fu.prioridad, " +
                            " m.idMatricula, m.ubicacion, m.subUbicacion, " +
                            " m.fechaInicio, m.fechaFin, m.cancelado) " +
                            " from TFlujoUsuario fu " +
                            " join TFlujo f on fu.idFlujo = f.idFlujo" +
                            " join SgaUsuario u on u.codUsuario = fu.idUsuario " +
                            " join Matricula m on m.idUsuario = u.username and m.idFlujo = fu.idFlujo " +
                            " where m.idMatricula = :idMatricula", FlujoDTO.class);
            int idMatriculaInt = 0;
            if (idMatricula != null && !idMatricula.trim().equals("")) {
                idMatriculaInt = Integer.valueOf(idMatricula);
            }
            query.setParameter("idMatricula", idMatriculaInt);
            List<FlujoDTO> resultado = query.getResultList();

            if (resultado != null) {
                accionEdicion = resultado.get(0);
                request.setAttribute("accionData", accionEdicion);

                Query query2 = entityManager.createQuery(
                        "select new com.orkli.domain.dto.FlujoRespuestasDTO(m.idMatricula, p.idFlujo, r.idPregunta, p.origenDatos,  p.activo, p.fase, p.obligatorio, p.orden, p.textoPregunta, " +
                                "r.respuesta, r.fase, r.respuestaBytes, p.tipoRespuesta, " +
                                "l.ubicacion, l.articulo, l.lote, l.numserie, l.cantidad, l.usuario) " +
                                "from MatriculaRespuesta r " +
                                "join Matricula m on r.idMatricula = m.idMatricula " +
                                "join TFlujoPregunta p on r.idPregunta =  p.idPregunta and " +
                                                            "p.idFlujo = m.idFlujo " +
                                "left join TTraduccionLecturas l on r.respuesta = l.lectura " +
                                "where r.idMatricula = :idMatricula " +
                                "order by r.idMatricula, p.orden ");
                //System.out.println ("Query2 " + query2.toString());
                //System.out.println("Parametro " + idMatriculaInt);
                query2.setParameter("idMatricula", idMatriculaInt);
                List<FlujoRespuestasDTO> resultado2 = query2.getResultList();
                request.setAttribute("LRespuestasFlujo", resultado2);

                //System.out.println ("Numero de preguntas " + resultado2.size());
                getServletContext().getRequestDispatcher("/editFlujo.jsp").forward(request, response);

            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
		doGet(request, response);
    }

}

