package com.orkli.controller;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orkli.domain.SubUbicacion;
import com.orkli.domain.Ubicacion;

/**
 * Servlet implementation class UbicacionCopyConroller
 */
@WebServlet("/UbicacionCopyConroller")
public class UbicacionCopyConroller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;
	private Ubicacion ubicacionEdicion = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UbicacionCopyConroller() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String idUbicacion = request.getParameter("ubicacionID");
		idUbicacion = URLDecoder.decode(idUbicacion, "UTF-8");
		request.setAttribute("idUbicacion", idUbicacion);
		factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
		entityManager = factory.createEntityManager();
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PCtaGetUbicaciones", Ubicacion.class);
		query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter(2, Integer.class, ParameterMode.IN);
		query.registerStoredProcedureParameter(3, String.class, ParameterMode.IN);
		query.setParameter(1, "-1");
		query.setParameter(2, 3);
		query.setParameter(3, idUbicacion);
		List<Ubicacion> listaUbicaciones = query.getResultList();
		ubicacionEdicion = listaUbicaciones.get(0);
		request.setAttribute("Ubicacion", ubicacionEdicion);
		query = entityManager.createStoredProcedureQuery("PCtaGetSububicaciones", SubUbicacion.class);
		query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		query.setParameter(1, idUbicacion);
		List<SubUbicacion> subUbicacionesAsignadas = query.getResultList();
		request.setAttribute("subUbicAsignadas", subUbicacionesAsignadas);
		query = entityManager.createStoredProcedureQuery("PCtaGetSububicaciones", SubUbicacion.class);
		query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		query.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
		query.setParameter(1, idUbicacion);
		query.setParameter(2, "2");
		List<SubUbicacion> subUbicacionesRestantes = query.getResultList();
		request.setAttribute("subUbicLibres", subUbicacionesRestantes);
		getServletContext().getRequestDispatcher("/copyUbicacion.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String msgTexto = "";
		if(request.getParameter("formSubmit").equals("Guardar")) {
			Ubicacion nuevaUbic = new Ubicacion();
			nuevaUbic.setIdUbicacion(request.getParameter("idUbicacion"));
			if (entityManager.find(Ubicacion.class, nuevaUbic.getIdUbicacion()) == null) {
				nuevaUbic.setDescripcion(request.getParameter("descripcion"));
				nuevaUbic.setLocalizacion(request.getParameter("localizacion"));
				nuevaUbic.setUbi_DesLoc(request.getParameter("ubiDescLoc"));
				if (request.getParameter("activo") != null && request.getParameter("activo").trim() != "") {
					nuevaUbic.setActivo(Short.parseShort("1"));
				}else {
					nuevaUbic.setActivo(Short.parseShort("0"));
				}
				try {
					entityManager.getTransaction().begin();
					entityManager.persist(nuevaUbic);
					entityManager.getTransaction().commit();
					msgTexto = "Ubicación copiada correctamente";
					entityManager.getTransaction().begin();
					String subUbicacionesSeleccionadas[] = request.getParameterValues("ubicSububicaciones");
					if (subUbicacionesSeleccionadas != null) {
						for (String subUbi : subUbicacionesSeleccionadas) {
							Query consulta = entityManager.createNativeQuery("Select * from ubicacion_sububicacion where id_ubicacion='" + nuevaUbic.getIdUbicacion() + "' and id_subUbicacion='" 
									+ subUbi + "'");
							List<Object> resultado = consulta.getResultList();
							if (resultado == null || resultado.size() <= 0) {
								//insert
								SubUbicacion subUbicacion =  entityManager.find(SubUbicacion.class, subUbi);
								nuevaUbic.getSubUbicacions().add(subUbicacion);
							}else {
								//activo = 1
								Query update = entityManager.createNativeQuery("Update ubicacion_sububicacion set Activo = 1 where id_ubicacion='" + nuevaUbic.getIdUbicacion() 
										+ "' and id_subUbicacion='" +  subUbi + "'");
								update.executeUpdate();
							}
						}
					}
					String subUbicacionesNOSeleccionadas[] = request.getParameterValues("noSelected");
					if (subUbicacionesNOSeleccionadas != null) {
						for (String subUbiNoSelec : subUbicacionesNOSeleccionadas) {
							Query update = entityManager.createNativeQuery("Update ubicacion_sububicacion set Activo = 0 where id_ubicacion='" + nuevaUbic.getIdUbicacion() 
							+ "' and id_subUbicacion='" +  subUbiNoSelec + "'");
							update.executeUpdate();
						}
					}
					entityManager.getTransaction().commit();
				}catch (Exception e) {
					msgTexto = "Error actualizando datos de la ubicación";
				}
				
			}else {
				msgTexto = "Ya existe una ubicacion con el ID introducido";
			}
			
		}
		ubicacionEdicion = null;
		if(request.getParameter("formSubmit").equals("Guardar")) {
			msgTexto = URLEncoder.encode(msgTexto, "UTF-8");
			response.sendRedirect("./message.jsp?msgTexto=" + msgTexto + "&redirectOpt=3");
		}else {
			response.sendRedirect("./ubicaciones");
		}
		
		this.destroy();
		if (entityManager.isOpen()) {
			entityManager.close();
			factory.close();
		}
	}

}
