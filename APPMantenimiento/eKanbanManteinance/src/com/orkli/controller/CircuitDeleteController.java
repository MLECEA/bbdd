package com.orkli.controller;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orkli.domain.Circuito;

/**
 * Servlet implementation class CircuitDeleteController
 */
@WebServlet("/CircuitDeleteController")
public class CircuitDeleteController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;   
	private Circuito circuitoEdicion = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CircuitDeleteController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String idCircuito = request.getParameter("circuitID");
		idCircuito = URLDecoder.decode(idCircuito, "UTF-8");
		request.setAttribute("idCircuito", idCircuito);
		factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
		entityManager = factory.createEntityManager();
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PCtaGetCircuitoInfo", Circuito.class);
		query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		query.setParameter(1, idCircuito);
		List<Circuito> resultado = query.getResultList();
		if (resultado != null) {
			circuitoEdicion = resultado.get(0);
			getServletContext().getRequestDispatcher("/deleteCircuit.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("formSubmit").equals("Confirmar")) {
			try {
				entityManager.getTransaction().begin();
				entityManager.remove(circuitoEdicion);
				entityManager.getTransaction().commit();
				String msgTexto = URLEncoder.encode("Circuito eliminado con �xito", "UTF-8");
				response.sendRedirect("./message.jsp?msgTexto=" + msgTexto + "&redirectOpt=2");
			}catch (Exception e) {
				String msgTexto = URLEncoder.encode("No se puede eliminar el circuito, datos registrados", "UTF-8");
				response.sendRedirect("./message.jsp?msgTexto=" + msgTexto + "&redirectOpt=2");
			}
		}else {
			response.sendRedirect("./circuits");
		}
		this.destroy();
		if (entityManager.isOpen()) {
			entityManager.close();
			factory.close();
		}
	}

}
