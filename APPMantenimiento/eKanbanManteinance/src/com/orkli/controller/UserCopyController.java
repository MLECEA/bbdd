package com.orkli.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orkli.domain.SgaRol;
import com.orkli.domain.SgaUsuario;
import com.orkli.domain.TEmpresa;
import com.orkli.domain.Ubicacion;
import com.orkli.domain.UsuarioUbicacion;

/**
 * Servlet implementation class UserCopyController
 */
@WebServlet("/UserCopyController")
public class UserCopyController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;
	private SgaUsuario nuevoUsuario = new SgaUsuario();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserCopyController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String codUser = request.getParameter("codUser");
		request.setAttribute("codUser", codUser);
		factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
		entityManager = factory.createEntityManager();
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PAccGetAllUsers", SgaUsuario.class);
		query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
		query.setParameter(1, codUser);
		//query.getResultList();
		@SuppressWarnings("unchecked")
		List<SgaUsuario> resultado = query.getResultList();
		Long nuevoCod = nuevoUsuario.getCodUsuario();
		if (resultado != null) {
			nuevoUsuario = resultado.get(0);
//			nuevoUsuario.setCodUsuario(0);
			request.setAttribute("UserData", nuevoUsuario);
			StoredProcedureQuery query2 = entityManager.createStoredProcedureQuery("PCtaGetUbicaciones", Ubicacion.class);
			query2.registerStoredProcedureParameter("codUser", String.class, ParameterMode.IN);
			query2.registerStoredProcedureParameter("mode", Integer.class, ParameterMode.IN);
			query2.setParameter("codUser", codUser);
			query2.setParameter("mode", 1);
			List<Ubicacion> listaUbicaciones = query2.getResultList();
			request.setAttribute("LUbicacionesNoUsuario", listaUbicaciones);
			StoredProcedureQuery query3 = entityManager.createStoredProcedureQuery("PCtaGetUbicaciones", UsuarioUbicacion.class);
			query3.registerStoredProcedureParameter("codUser", String.class, ParameterMode.IN);
			query3.registerStoredProcedureParameter("mode", Integer.class, ParameterMode.IN);
			query3.setParameter("codUser", codUser);
			query3.setParameter("mode", 0);
			List<UsuarioUbicacion> listaUsuarioUbicaciones = query3.getResultList();
			request.setAttribute("LUbicaciones", listaUsuarioUbicaciones);
			List<TEmpresa> listaEmpresas = getListaEmpresas();
			request.setAttribute("LEmpresas", listaEmpresas);
			List<SgaRol> listaRoles = getListaRoles();
			request.setAttribute("LRoles", listaRoles);
			getServletContext().getRequestDispatcher("/copyUser.jsp").forward(request, response);
		}
	}
	
	private List<TEmpresa> getListaEmpresas () {
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PCtaGetEmpresas", TEmpresa.class);
		List<TEmpresa> listaUbicaciones = query.getResultList();
		return listaUbicaciones;
	}
	
	private List<SgaRol> getListaRoles () {
		StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PCtaGetAllRoles", SgaRol.class);
		List<SgaRol> listaRoles = query.getResultList();
		return listaRoles;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
		String msgTexto = "";
		if(request.getParameter("formSubmit").equals("Guardar")) {
			SgaUsuario newUser = new SgaUsuario();
			newUser.setNombre(request.getParameter("nombre"));
			newUser.setApellidos(request.getParameter("apellidos"));
			newUser.setMail(request.getParameter("email"));
			newUser.setApellidos(request.getParameter("apellidos"));
			newUser.setUsu_Idi(request.getParameter("idioma"));
			newUser.setUsu_Emp(Integer.parseInt(request.getParameter("usrEmpresa")));
			newUser.setPtrRol(BigDecimal.valueOf(Long.valueOf((request.getParameter("usrRol").toString()))));
			//usuarioEdicion.setPtrRol(request.getParameter("usrRol"));
			newUser.setUsername(request.getParameter("username"));
			newUser.setPassword(request.getParameter("pwd"));
			if (request.getParameter("activo") != null && request.getParameter("activo").trim() != "") {
				newUser.setActivo(Short.parseShort("1"));
			}else {
				newUser.setActivo(Short.parseShort("0"));
			}
			newUser.setTokenAuten_CaducidadMins(Integer.parseInt(request.getParameter("tokenAuten")));
			newUser.setTokenRefresco_CaducidadMins(Integer.parseInt(request.getParameter("tokenRefresh")));
			if (request.getParameter("pasarelaActivada") != null && request.getParameter("pasarelaActivada").trim() != "") {
				newUser.setUsu_PasarelaActivada(true);
			}else {
				newUser.setUsu_PasarelaActivada(false);
			}
			try {
				newUser.setOptimisticLock(BigDecimal.valueOf(Long.valueOf(request.getParameter("optimisticLock"))));
			}catch(Exception ex2) {
				newUser.setOptimisticLock(BigDecimal.valueOf(0));
			}
			newUser.setFTP_Server(request.getParameter("srvFTP"));
			newUser.setFTP_Username(request.getParameter("userFTP"));
			newUser.setFTP_Password(request.getParameter("pwdFTP"));
			if (request.getParameter("subirFTP") != null && request.getParameter("subirFTP").trim() != "") {
				newUser.setUsu_SubirDatosAFTP(true);
			}else {
				newUser.setUsu_SubirDatosAFTP(false);
			}
			String ubicaciones[] = request.getParameterValues("usrUbicaciones");
			System.out.println(ubicaciones.toString());
			String ubicacionesStr = "";
			for (int i = 0; i < ubicaciones.length; i++) {
				if (ubicacionesStr.isEmpty()) {
					ubicacionesStr += ubicaciones[i];
				}else {
					ubicacionesStr += "|" + ubicaciones[i];
				}
			}
			try {
				entityManager.getTransaction().begin();
				entityManager.persist(newUser);
				entityManager.getTransaction().commit();
				StoredProcedureQuery query2 = entityManager.createStoredProcedureQuery("PAX_ActualizarUbicacionesUsuario");
				query2.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
				query2.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
				query2.setParameter(1, String.valueOf(newUser.getCodUsuario()));
				query2.setParameter(2, ubicacionesStr);
				query2.execute();
				msgTexto = "Usuario copiado correctamente";
			}catch (EntityExistsException e) {
				msgTexto = "Error copiando usuario, clave primaria duplicada";
			}catch (Exception e) {
				msgTexto = "Error copiando usuario";
			}
		}
		
//		newUser = null;
//		response.sendRedirect("./users");
		if(request.getParameter("formSubmit").equals("Guardar")) {
			msgTexto = URLEncoder.encode(msgTexto, "UTF-8");
			response.sendRedirect("./message.jsp?msgTexto=" + msgTexto + "&redirectOpt=1");
		}else {
			response.sendRedirect("./users");
		}
		
		this.destroy();
		if (entityManager.isOpen()) {
			entityManager.close();
			factory.close();
		}
	}

}
