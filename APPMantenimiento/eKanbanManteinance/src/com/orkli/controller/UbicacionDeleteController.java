package com.orkli.controller;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.orkli.domain.Ubicacion;

/**
 * Servlet implementation class UbicacionDeleteController
 */
@WebServlet("/UbicacionDeleteController")
public class UbicacionDeleteController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static EntityManagerFactory factory = null;
	private static EntityManager entityManager = null;
	private Ubicacion ubicacion = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UbicacionDeleteController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		String ubicacionID = request.getParameter("ubicacionID");
		ubicacionID = URLDecoder.decode(ubicacionID, "UTF-8");
		factory = Persistence.createEntityManagerFactory("eKanbanManteinance");
		entityManager = factory.createEntityManager();
		ubicacion = entityManager.find(Ubicacion.class, ubicacionID);
		if (ubicacion != null) {
			getServletContext().getRequestDispatcher("/deleteUbicacion.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
		if(request.getParameter("formSubmit").equals("Confirmar")) {
			try {
				
				ubicacion.setActivo(new Integer(0).shortValue());
				entityManager.getTransaction().begin();
				entityManager.persist(ubicacion);
				entityManager.getTransaction().commit();
				String msgTexto = URLEncoder.encode("La ubicación se ha marcado como inactiva", "UTF-8");
				response.sendRedirect("./message.jsp?msgTexto=" + msgTexto + "&redirectOpt=3");
			}catch (Exception e) {
				String msgTexto = URLEncoder.encode("Error al marcar la ubicación como inactiva", "UTF-8");
				response.sendRedirect("./message.jsp?msgTexto=" + msgTexto + "&redirectOpt=3");
			}
		}else {
			response.sendRedirect("./ubicaciones");
		}
		this.destroy();
		if (entityManager.isOpen()) {
			entityManager.close();
			factory.close();
		}
	}

}
