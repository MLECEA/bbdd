<%-- <%@ include file="/WEB-INF/templates/header.jsp"%>  --%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="resources/css/stylesheet.css"></link>
<link rel="stylesheet" type="text/css" href="resources/css/_style.css"></link>
<link rel="stylesheet" type="text/css" href="resources/css/csswizardry-grids.scss"></link>
<script type="text/javascript" src="resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="resources/js/modernizr.min.js"></script>
<script type="text/javascript" src="resources/js/ekanban-core.js"></script>
<script type="text/javascript" src="resources/js/ekanban-vendor.js"></script>
</head>
<body>
<c:out value="${message}"></c:out>
<div class="main--container">
<div class="login-boxes grid">
	<div class="login-boxes-column login-boxes-column--half">
	      <div class="login-box--content">
	        <img alt="Ekanban circle" src="resources/img/ekanbanCirculo.png">
	      </div>
	    </div>
	<div class="login-boxes-column login-boxes-column--half">
	      <div class="login-box">
	        <div class="login-head">
	            Access
	        </div>
	        <div class="message message--error" style="display:${msgError}">
                <span class="icon-check--unchecked"></span>
                <span class="title">Error.</span>
                <span>Invalid username or password.</span>
            </div>
	        <form name="loginForm" action="login" method="post">
	        	<div class="login-field">
	        		<label for="username">Username</label>
	          		<input id="username" name="username" type="text" autofocus="autofocus"> 
	        	</div>
	          	<div class="login-field">
					<label for="password">Password</label>
	          		<input id="password" name="password" type="password">
				</div>
				<button type="submit" class="spin-loader"><span class="icon-checkmark"></span>Log in</button>
	        </form>
	        
	      </div>
	    </div>
	   </div>
	  </div>
</body>
</html>