<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<jsp:include page="/WEB-INF/templates/header.jsp"/> 
 
	<link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
	<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.mobile.min.css" />
    <link rel="stylesheet" href="resources/css/Orkli.css"></link>
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/jquery.min.js"></script>
    <script src="resources/js/jquery-ui.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
</head>
<body>
	<div style="display:inline-table">
		<div align="center" style="float:left; width:18%">
			<jsp:include page="/WEB-INF/templates/Lateralmenu.jsp"/> 
		</div>
		<div align="center" style="width:80%; float:right">
	    	<div style="margin-bottom: 45px;">
	    		<div style="float:right">
	    			<input type="radio" id="chkTodos" name="chkFiltro"  onchange="filtrarUbicacionesActivos()" checked>Mostrar todas las ubicaciones	
		    		<input type="radio" id="chkActivos" name="chkFiltro"  onchange="filtrarUbicacionesActivos()">Solo las activas
		    		<input type="radio" id="chkInactivos" name="chkFiltro" onchange="filtrarUbicacionesActivos()">Solo las inactivas
	    		</div>
    		</div>
	        <table id="ubicacionesGrid" cellpadding="5">
	            <tr>
	                <th class="k-header k-sorted">Ubicacion</th>
	                <th class="k-header k-sorted">Descripcion</th>
	                <th class="k-header k-sorted" style="display:none">Activo</th>
	            </tr>
	            <c:forEach var="ubicacion" items="${LUbicaciones}" varStatus="theCount">
	            	<c:choose>
	            		<c:when test="${ubicacion.activo eq 1 }">
	            			<tr>
				                    <td id="divIDNo${theCount.index}">
				                    	<a href="javascript:void(0)" onclick="deleteUbicacion(this);"><img src="resources/img/delete.png" style="width: 15px; margin-right:10px" /></a>
				                    	<a href="javascript:void(0)" onclick="copyUbicacion(this);"><img src="resources/img/copy.png" style="width: 15px; margin-right:10px" /></a>
				                    	<c:out value="${ubicacion.idUbicacion}" />
			                    	</td>
				                    <td><c:out value="${ubicacion.descripcion}" /></td>
			                       <td style="display:none"><c:out value="${ubicacion.activo}" /></td>
			                </tr>
	            		</c:when>
	            		<c:otherwise>
		            		<tr>
			                    <td id="divIDNo${theCount.index}">
				                    	<a href="javascript:void(0)" onclick="deleteUbicacion(this);"><img src="resources/img/delete.png" style="width: 15px; margin-right:10px" /></a>
				                    	<a href="javascript:void(0)" onclick="copyUbicacion(this);"><img src="resources/img/copy.png" style="width: 15px; margin-right:10px" /></a>
				                    	<c:out value="${ubicacion.idUbicacion}" />
			                    	</td>
				                    <td><span class="inactivo"><c:out value="${ubicacion.descripcion}" /></span></td>
			                       <td style="display:none"><c:out value="${ubicacion.activo}" /></td>
			                </tr>
	            		</c:otherwise>
	            	</c:choose>
            		 
	            </c:forEach>
	        </table>
	      	<div id="targetDiv">
	      	
	      	</div>
	    </div>
	</div>
    
    <script>
    $(document).ready(function() {
    	$("#targetDiv").dialog({  //create dialog, but keep it closed
    		   autoOpen: false,
    		   height: 530,
    		   width: 900,
    		   modal: true,
    		   open: function(event, ui) {
      				$(".ui-dialog-titlebar-close").hide();
      		    },
      		    title: "Editar información de la ubicación"
   		});
        $("#ubicacionesGrid").kendoGrid({
        	schema: {  
                model: {  
                    id: "Ubicaciones",  
                    fields: {  
                        Codigo: { type: "string" },  
                        Descripcion: { type: "string" },
                        Activo: {type: "number"}
                    }  
                }  
            }, 
            columns: [
                { field: "Codigo"},
                { field: "Descripcion" },
                { field: "activo", hidden:true }
              ],
            sortable: true,
            filterable:{
            	extra: false,
            	mode: "row",
                operators: {
                	string: {
                		contains: "Contains"
                	}
                }
            },
            pageable: {
                pageSize: 20
            },
            selectable: "row",
            dataBound: ondataBound
        });
        $("#ubicacionesGrid").on("dblclick", "tr.k-state-selected", function (e) {
        	var idUbic = $("#ubicacionesGrid").find('tr.k-state-selected').find("td:first")[0].innerText.trim();
        	idUbic = encodeURI(idUbic);
        	$('#targetDiv').dialog('option','title','Editar ubicacion seleccionada: ' + idUbic);
            // insert code here
            //window.location.href = "./editUser?codUser=" + codUser;
        	$("#targetDiv").load("./editUbicacion?ubicacionID=" + idUbic);
            $("#targetDiv").dialog("open"); 
        });
    });    
    function deleteUbicacion(event){
    	var ubicID = event.parentNode.innerText.toString().trim();
    	$('#targetDiv').dialog('option','width',500);
    	$('#targetDiv').dialog('option','height',120);
    	$('#targetDiv').dialog('option','title','Eliminar ubicacion seleccionada: ' + ubicID);
    	ubicID = encodeURI(ubicID);
    	$("#targetDiv").load("./deleteUbicacion?ubicacionID=" + ubicID);
        $("#targetDiv").dialog("open");
    }
    
    function copyUbicacion(event){
    	var ubicID = event.parentNode.innerText.toString().trim();
    	ubicID = encodeURI(ubicID);
    	$('#targetDiv').dialog('option','title','Copiar ubicacion seleccionada: ' + ubicID);
    	$("#targetDiv").load("./copyUbicacion?ubicacionID=" + ubicID);
        $("#targetDiv").dialog("open");
    }
    function filtrarUbicacionesActivos(){
    	var grid = $("#ubicacionesGrid");
    	if ($("#chkActivos")[0].checked){
    		$("#ubicacionesGrid").data("kendoGrid").dataSource.filter({ field: "activo", operator: "eq", value: 1 }); 
    	}
    	if ($("#chkInactivos")[0].checked){
    		$("#ubicacionesGrid").data("kendoGrid").dataSource.filter({ field: "activo", operator: "eq", value: 0 }); 
    	}
    	if ($("#chkTodos")[0].checked){
    		$("#ubicacionesGrid").data("kendoGrid").dataSource.filter({}); 
    	}
    	
    }
    
    function ondataBound(){
    	$(".inactivo").each(function() {
            var trElemn = $(this)[0].parentNode.parentNode;
            trElemn.className += " RowInactivo";
         });
    }
    
    </script>
</body>
</html>