<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<jsp:include page="/WEB-INF/templates/header.jsp"/> 
 	<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.mobile.min.css" />
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/jquery.min.js"></script>
    <script src="resources/js/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="resources/css/Orkli.css"></link>
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
</head>
<body>
<div style="display:inline-table">
	<div align="center"  style="float:left; width:18%">
		<jsp:include page="/WEB-INF/templates/Lateralmenu.jsp"/> 
	</div>
 	<div align="center" style="width:80%; float:right">
 		<div style="margin-bottom: 35px;">
    		<div style="float:right">
    			<input type="radio" id="chkTodos" name="chkFiltro"  onchange="filtrarCircuitosActivos()" checked>Mostrar todos los circuitos	
	    		<input type="radio" id="chkActivos" name="chkFiltro"  onchange="filtrarCircuitosActivos()">Solo los circuitos activos
	    		<input type="radio" id="chkInactivos" name="chkFiltro" onchange="filtrarCircuitosActivos()">Solo los circuitos inactivos
    		</div>
   		</div>
        <table id="circuitGrid" cellpadding="5">
            <tr>
                <th class="k-header k-sorted">Id Circuito</th>
                <th class="k-header k-sorted">Referencia ID</th>
                <th class="k-header k-sorted">Ubicacion</th>
                <th class="k-header k-sorted">Descripción</th>
                <th class="k-header k-sorted" style="display:none">Activo</th>
            </tr>
            <c:forEach var="circuito" items="${LCircuitos}" varStatus="theCount">
            	<c:choose>
            		<c:when test="${circuito.activo eq true }">
	            		<tr>
							 <td id="idCircuito_${theCount.count}" class="idCircuito">
		                    <td id="idCircuito_${theCount.count}" class="idCircuito">
		                    	<a href="javascript:void(0)" onclick="deleteCircuit(this);"><img src="resources/img/delete.png" style="width: 15px; margin-right:10px" /></a>
		                    	<a href="javascript:void(0)" onclick="copyCircuit(this);"><img src="resources/img/copy.png" style="width: 15px; margin-right:10px" /></a>
		                    	 <c:out value="${circuito.id.idCircuito}" />
                    	 	</td>
		                    <td><c:out value="${circuito.id.referenciaId}" /></td>
		                    <td><c:out value="${circuito.id.ubicacion}" /></td>
		                    <td><c:out value="${circuito.descripcion}" /></td>
		                    <td style="display:none"><c:out value="${circuito.activo}" /></td>
		                </tr>
            		</c:when>
            		<c:otherwise>
            			<tr>
		                    <td id="idCircuito_${theCount.count}" class="idCircuito">
		                    	<a href="javascript:void(0)" onclick="deleteCircuit(this);"><img src="resources/img/delete.png" style="width: 15px; margin-right:10px" /></a>
		                    	<a href="javascript:void(0)" onclick="copyCircuit(this);"><img src="resources/img/copy.png" style="width: 15px" /></a> 
		                    	<c:out value="${circuito.id.idCircuito}" />
	                    	</td>
		                    <td><c:out value="${circuito.id.referenciaId}" /></td>
		                    <td><c:out value="${circuito.id.ubicacion}" /></td>
		                    <td><span class="inactivo"><c:out value="${circuito.descripcion}" /></span></td>
		                    <td style="display:none"><c:out value="${circuito.activo}" /></td>
		                </tr>
            		</c:otherwise>
            	</c:choose>
                
            </c:forEach>
        </table>
      	<div id="targetDiv">
      	
      	</div>
    </div>
</div>
     <script>
                $(document).ready(function() {
                	$("#targetDiv").dialog({  //create dialog, but keep it closed
                		   autoOpen: false,
                		   height: 630,
                		   width: 900,
                		   modal: true,
                		   open: function(event, ui) {
	              				$(".ui-dialog-titlebar-close").hide();
	              		    },
	              		    title: "Editar información del circuito" 
               		});
                    $("#circuitGrid").kendoGrid({
                    	schema: {  
                            model: {  
                                id: "Circuitos",  
                                fields: {  
                                    IdCircuito: { type: "string" },  
                                    ReferenciaID: { type: "string" } ,
                                    Ubicacion: { type: "string" },
                                    Descripcion: { type: "string" },
                                    Activo: { type: "string" }
                                }  
                            }  
                        }, columns: [
                            { field: "IdCircuito"},
                            { field: "ReferenciaID" },
                            { field: "Ubicacion" },
                            { field: "Descripcion" },
                            { field: "Activo", hidden: true}
                          ],
                    	//height: 550,
                        sortable: true,
                        filterable:{
                        	extra: false,
                        	mode: "row",
                            operators: {
                            	string: {
                            		contains: "Contains"
                            	}
                            }
                        },
                        pageable: {
                            pageSize: 20
                        },
                        selectable: "row",
                        dataBound: ondataBound
                    });
                    $("#circuitGrid").on("dblclick", "tr.k-state-selected", function (e) {
                        // insert code here
                    	var idCircuitoVal = $("#circuitGrid").find('tr.k-state-selected').find("td:first")[0].innerText.trim();
                        //window.location.href = "./editUser?codUser=" + codUser;
                         idCircuitoVal = encodeURI(idCircuitoVal);
                    	$("#targetDiv").load("./editCircuit?circuitID=" + idCircuitoVal);
                        $("#targetDiv").dialog("open"); 
                    });
                });
                
                function ondataBound(){
                	$(".inactivo").each(function() {
                        var trElemn = $(this)[0].parentNode.parentNode;
                        trElemn.className += " RowInactivo";
                     });
                }
                function deleteCircuit(event){
                	$('#targetDiv').dialog('option','width',500);
                	$('#targetDiv').dialog('option','height',120);
                	$('#targetDiv').dialog('option','title','Eliminar circuito seleccionado');
                	var circuitID = event.parentNode.innerText.toString().trim();
                	$("#targetDiv").load("./deleteCircuit?circuitID=" + circuitID);
                    $("#targetDiv").dialog("open");
                }
                
                function copyCircuit(event){
                	var idCircuitoVal = event.parentNode.innerText.toString().trim();
                    idCircuitoVal = encodeURI(idCircuitoVal);
                	$("#targetDiv").load("./copyCircuit?circuitID=" + idCircuitoVal);
                    $("#targetDiv").dialog("open");
                }
                
                function filtrarCircuitosActivos(){
                	var grid = $("#circuitGrid");
                	if ($("#chkActivos")[0].checked){
                		$("#circuitGrid").data("kendoGrid").dataSource.filter({ field: "Activo", operator: "eq", value: "true" }); 
                	}
                	if ($("#chkInactivos")[0].checked){
                		$("#circuitGrid").data("kendoGrid").dataSource.filter({ field: "Activo", operator: "eq", value: "false" }); 
                	}
                	if ($("#chkTodos")[0].checked){
                		$("#circuitGrid").data("kendoGrid").dataSource.filter({}); 
                	}
                	
                }
            </script>
</body>
</html>