<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
	<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.mobile.min.css" />
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/jquery.min.js"></script>
    <script src="resources/js/jquery-ui.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
    <script>
	$(function() {
	    $("#tabs").tabs();
	   
	    $("#usuariosSinPermiso").kendoListBox({
            connectWith: "usuariosConPermisos",
            draggable: true,
            dropSources: ["usuariosConPermisos"],
            toolbar: {
              position: "right",
              tools: ["transferTo", "transferFrom", "transferAllTo", "transferAllFrom"]
            },
            selectable: "multiple",
            dataTextField: "Nombre",
            dataValueField: "codUsuario"
          });
	    $("#usuariosConPermisos").kendoListBox({
            connectWith: "usuariosSinPermiso",
            draggable: true,
            dropSources: ["usuariosSinPermiso"],
            selectable: "multiple",
            dataTextField: "Nombre",
            dataValueField: "codUsuario"
          });
  } );
  function goBack(){
	  window.location.href = "./acciones";
  }
  
  
  
</script>
</head>
<body>
	<form action="editAccion" method="post">
		<div id="tabs">
			<ul>
				<li><a href="#accionData">Datos generales</a></li>
				<li><a href="#Acciones">Acciones</a></li>
				<li><a href="#Permisos">Permiso</a></li>
			</ul>
			<div id="accionData" style="width:100%;height:100%">
				<table style="width:100%; height:100%" >
					<tr>
						<TD>ID Acci�n</TD>
						<TD><c:out value="${accionData.accRep_Idd}" /></TD>
					</tr>
					<tr>
						<TD>Descripci�n</TD>
						<TD><input type="text" name="descripcion" value="${accionData.accRep_Des }" ></TD>
					</tr>
					<tr>
						<TD>Tipo</TD>
						<TD><input name="tipo" type="text" value="${accionData.accRep_Tip}" ></TD>
					</tr>
					<tr>
						<TD>Log URL</TD>
						<TD><input name="logURL" type="text" value="${accionData.accRep_LogURL}" ></TD>
					</tr>
					<tr>
						<TD>Servidor</TD>
						<TD><input name="servidor" type="text" value="${accionData.accRep_Ser}" ></TD>
					</tr>
					<tr>
						<TD>Informe</TD>
						<TD><input name="informe" type="text" value="${accionData.accRep_Rep}" ></TD>
					</tr>
					<tr>
						<TD>Activo</TD>
						<TD>
							<c:choose>
								<c:when test="${accionData.accRep_Act eq 1}">
									<input name="activo" type="checkbox" checked />
								</c:when>
								<c:otherwise>
									<input name="activo" type="checkbox" />
								</c:otherwise>
							</c:choose>
						</TD>
					</tr>
				</table>
			</div>
			<div id="Acciones">
				<table style="width:100%; height:100%" >	
					<thead>
						<th>Pregunta</th>
						<th>Texto acci�n</th>
						<th>Valor por defecto</th>
					</thead>
					<tr>
						<TD>Accion 1</TD>
						<TD>
							<input style="width:100%;" name="accV1" type="text" value="${accionData.accRep_V1Txt}" >
						</TD>
						<TD>
							<input style="width:100%;" name="accV1Def" type="text" value="${accionData.accRep_V1Def}" >
						</TD>
					</tr>
					<tr>
						<TD>Accion 2</TD>
						<TD>
							<input style="width:100%;" name="accV2" type="text" value="${accionData.accRep_V2Txt}" >
						</TD>
						<TD>
							<input style="width:100%;" name="accV2Def" type="text" value="${accionData.accRep_V2Def}" >
						</TD>
					</tr>
					<tr>
						<TD>Accion 3</TD>
						<TD>
							<input style="width:100%;" name="accV3" type="text" value="${accionData.accRep_V3Txt}" >
						</TD>
						<TD>
							<input style="width:100%;" name="accV3Def" type="text" value="${accionData.accRep_V3Def}" >
						</TD>
					</tr>
				</table>
			</div>
			<div id="Permisos">
				<div style="width:100%; height:100%;display:flex">
					<div>
						<span>Usuarios sin autorizaci�n:</span>
				    	</br>
						<select multiple="multiple" id="usuariosSinPermiso" style="height:300px; margin-right:10px">
							 <c:forEach var="usuario2" items="${LUsuariosSinPermiso}" varStatus="theCount2">
				               		<option value="${usuario2.codUsuario}"><c:out value="${usuario2.nombre}" /></option>
				            </c:forEach>
			            </select>
		            </div>
<!-- 		            <div style="margin-right: 10px;margin-top: 150px;"> -->
<!-- 			            <input type="button" id="left" value="&lt;" /> -->
<!-- 					    <input type="button" id="right" value="&gt;" /> -->
<!-- 				    </div> -->
				    <div>
				    	<span>Usuarios autorizados:</span>
				    	</br>
						<select multiple="multiple" name="usuariosConPermisos" id="usuariosConPermisos" style="height:300px">
							 <c:forEach var="usuario" items="${LUsuariosConPermiso}" varStatus="theCount">
				               		<option value="${usuario.codUsuario}"><c:out value="${usuario.nombre}" /></option>
				            </c:forEach>
			            </select>
		            </div>
		            <br />
			    </div>
			</div>
		</div>
		<div style="margin-top:15px">
			<input type="submit" name="formSubmit" value="Guardar" onclick="seleccionarTodos();" style="float:right" />
			<input type="submit" name="formSubmit" value="Cancelar" style="float:left"/>
		</div>
	</form>
</body>
</html>