<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 	<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.mobile.min.css" />
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/jquery.min.js"></script>
    <script src="resources/js/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="resources/css/Orkli.css"></link>
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
    
</head>
<body>
	<div id="msgDiv">
		<form action="deleteUser" method="post">
			<span>Si no hay datos registrados, se eliminar� el usuario seleccionado. �Est�s seguro?</span>
			<div style="margin-top:15px">
				<input type="submit" name="formSubmit" value="Confirmar" style="float:right" />
				<input type="submit" name="formSubmit" value="Cancelar" style="float:left"/>
			</div> 
		</form>
	</div>
</body>
</html>