<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
	<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.mobile.min.css" />
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/jquery.min.js"></script>
    <script src="resources/js/jquery-ui.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
    <script>
    var formResp = "";
    function cancelarSubmit(){
  	  formResp = "KO";
    }
    
    function sendForm(){
    	var valid = true;
  	  if (formResp == "KO") 
  		  return true; 
  	  else 
  	  	{
	  		var idSubUbi = $("#idSubUbi").val().trim();
			if (idSubUbi.length == 0 ){
				$("#IDObli").show();
				valid = false;
			}else{
				$("#IDObli").hide;
			}
  	  	}
  	  return valid;
    }
    </script>
</head>
<body>
	<form action="editSubUbicacion" method="post" onsubmit="return sendForm();">
			<table style="width:100%; height:100%">
				<tr>
						<TD>Sububicacion ID<span style="color:red"> *</span></TD>
						<TD><input type="text" id="idSubUbi" name="idSubUbic" value="${sububicacion.id_SubUbicacion }" >
						</br><span id="IDObli" style="display:none; color:red; font-size:11px">Campo obligatorio</span>
						</TD>
					</tr>
					<tr>
						<TD>Descripción</TD>
						<TD><input type="text" name="descripcion" value="${sububicacion.descripcion }" ></TD>
					</tr>
					<tr>
						<TD>Activo</TD>
						<TD>
							<c:choose>
								<c:when test="${sububicacion.activo eq 1}">
									<input name="activo" type="checkbox" checked />
								</c:when>
								<c:otherwise>
									<input name="activo" type="checkbox" />
								</c:otherwise>
							</c:choose>
							
						</TD>
					</tr>
			</table>
			<div style="margin-top:15px">
				<input type="submit" name="formSubmit" value="Copiar" style="float:right" />
				<input type="submit" name="formSubmit" value="Cancelar" style="float:left" onclick="cancelarSubmit();"/>
			</div> 
		</form>
</body>
</html>