<div class="ext-box">
	<div class="int-box">
		<% //Solo se muestran estas opciones para los usuarios con el permiso de administrador
			Object objetoPermiso = (String)session.getAttribute("esAdministrador");

			if (objetoPermiso != null && objetoPermiso.equals("true")){
		%>
		<input type="button" class="btn" name="btnMgtUsuarios" value="Gesti�n usuarios" onclick="gestionUsuarios()"></input>
		<input type="button" class="btn" name="btnSububicaciones" value="Gesti�n sububicaciones" onclick="gestionSububicaciones()"></input>
		<input type="button" class="btn" name="btnUbicaciones" value="Gesti�n ubicaciones" onclick="gestionUbicaciones()"></input>
		<input type="button" class="btn" name="btnMgtCircuitos" value="Gesti�n circuitos" onclick="gestionCircuitos()"></input>
		<input type="button" class="btn" name="btnAcciones" value="Gesti�n acciones" onclick="gestionAcciones()"></input>
		<% } %>

		<!-- Para todos los usuarios se muestra la opci�n de flujos -->
		<input type="button" class="btn" name="btnFlujos" value="Gesti�n flujos" onclick="gestionFlujos()"></input>

	</div>
</div>
<style>
div.ext-box { display: table;  float:left}
div.int-box { display: table-cell; vertical-align: middle; }
input.btn {height:60px; width:100%; margin-top:15px !important;  }

</style>
<script>
	function gestionUsuarios(){
		window.location.href = "./users";
	}
	function gestionCircuitos(){
		window.location.href = "./circuits";
	}
	function gestionUbicaciones(){
		window.location.href = "./ubicaciones";
	}
	function gestionAcciones(){
		window.location.href = "./acciones";
	}
	function gestionSububicaciones(){
		window.location.href = "./subUbicaciones";
	}
    function gestionFlujos(){
        window.location.href = "./flujos";
    }
</script>