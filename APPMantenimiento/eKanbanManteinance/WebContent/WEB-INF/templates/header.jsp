<header class="header">
	<link rel="stylesheet" type="text/css" href="resources/css/_header.scss"></link>
    <div class="header--main">
        <div class="header--main--container" style="width:100%;display: flex;">
	            <div class="user-logo" style="width: 35%;">
	            	<img src="resources/img/logo-ekanban.png" alt="Logo">
            	</div>
            	<div style="width: 35%;text-align: center;">
	            		<h1>App Mantenimiento</h1>
	            	</div>
	            <div class="user-logo" style="align-items: right;width: 35%;text-align: right;">
	            	<img src="resources/img/LogoOrkli.png" alt="Logo" style="width:150px">
            	</div>
        </div>
    </div>
</header>