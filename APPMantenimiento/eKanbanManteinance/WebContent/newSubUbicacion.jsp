<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
	<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.mobile.min.css" />
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/jquery.min.js"></script>
    <script src="resources/js/jquery-ui.min.js"></script>
    
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
    <script>
    function sendForm(){
    	var valid = true;
    	var idSubUbi = $("#subUbicacionID").val().trim();
		if (idSubUbi.length == 0 ){
			$("#IDObli").show();
			valid = false;
		}else{
			$("#IDObli").hide;
		}
		return valid;
    }
    </script>
</head>
<body>
	<form action="newSubUbicacion" method="post" id="newSubUbiFrm" onsubmit="return sendForm();">
		<div>
			<table style="width:100%; height:100%" >
				<tr>
					<TD>Id SubUbicacion<span style="color:red"> *</span></TD>
					<TD><input type="text" id="subUbicacionID" name="idSubUbi">
					</br><span id="IDObli" style="display:none; color:red; font-size:11px">Campo obligatorio</span>
					</TD>
				</tr>
				<tr>
					<TD>Descripción</TD>
					<TD><input type="text" name="desc"></TD>
				</tr>
			</table>
		</div>
		<div style="margin-top:15px">
			<input type="submit" name="formSubmit" value="Guardar"  style="float:right" />
			<input type="submit" name="formSubmit" value="Cancelar" style="float:left" />
		</div>
	</form>
	
	
</body>
</html>