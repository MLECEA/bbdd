<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
	<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.mobile.min.css" />
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/jquery.min.js"></script>
    <script src="resources/js/jquery-ui.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
    <script>
$(function() {
    $("#tabs").tabs();
    function moveItems(origin, dest) {
        $(origin).find(':selected').appendTo(dest);
    }      
    $('#left').click(function () {
        moveItems('#ubicacionesUsuario', '#ubicacionesNoUsuario');
    });
     
    $('#right').on('click', function () {
        moveItems('#ubicacionesNoUsuario', '#ubicacionesUsuario');
    });
  } );
  
  
</script>
</head>
<body>
	<form action="copyCircuit" method="post">
		<div id="tabs">
			<ul>
				<li><a href="#circuitData">Datos generales</a></li>
				<li><a href="#integracion">Parametros AX</a></li>
				<li><a href="#programacion">Programaci�n de la integraci�n</a></li>
			</ul>
			<div id="circuitData" style="width:100%;height:100%">
				<table style="width:100%; height:100%">
					<tr>
						<td>ID Circuito</td>
						<td><input type="textbox" name="idCircuito" value="${circuito.id.idCircuito}_copia" /></td>
					</tr>
					<tr>
						<td>Referencia ID</td>
						<td><input type="textbox" name="refID" value="${circuito.id.referenciaId}"/></td>
					</tr>
					<tr>
						<td>Descripci�n</td>
						<td><input type="textbox" name="refDesc" value="${circuito.descripcion}"/></td>
					</tr>
					<tr>
						<td>Cantidad de tarjetas </td>
						<td><input type="textbox" name="ctdNumKanban" value="${circuito.numKanban}"/></td>
					</tr>
					<tr>
						<td>Cantidad piezas por tarjeta</td>
						<td><input type="textbox" name="ctdPiezas" value="${circuito.cantidadPiezas}"/></td>
					</tr>
					<tr>
						<td>Cantidad optima</td>
						<td><input type="textbox" name="ctdOpt" value="${circuito.num_kanbanOPT}"/></td>
					</tr>
					<tr>
						<td>Ubicaci�n</td>
						<td>
							<select name="cUbicacion"  id="ubicaciones">
								 <c:forEach var="ubic" items="${LUbicaciones}" varStatus="countUbi">
								 	<c:choose>
								 		<c:when test="${circuito.id.ubicacion == ubic.idUbicacion}">
								 			<option value="${ubic.idUbicacion}" selected><c:out value="${ubic.descripcion}" /></option>
								 		</c:when>
								 		<c:otherwise>
								 			<option value="${ubic.idUbicacion}"><c:out value="${ubic.descripcion}" /></option>
								 		</c:otherwise>
							 		</c:choose>
					            </c:forEach>
				            </select>
						</td>
					</tr>
					<tr>
						<td>Tipo circuito</td>
						<td>
							<select name="tipoCircuito"  id="tipoCircuito">
								 <c:forEach var="tipoCircuito" items="${LTipoCircuito}" varStatus="countRol">
								 	<c:choose>
								 		<c:when test="${circuito.tipoCircuito == tipoCircuito.id}">
								 			<option value="${tipoCircuito.id}" selected><c:out value="${tipoCircuito.descripcion}" /></option>
								 		</c:when>
								 		<c:otherwise>
								 			<option value="${tipoCircuito.id}"><c:out value="${tipoCircuito.descripcion}" /></option>
								 		</c:otherwise>
							 		</c:choose>
					            </c:forEach>
				            </select>
						</td>
					</tr>
					<tr>
						<td>Tipo contenedor</td>
						<td>
							<select name="tipoContenedor"  id="tipoContenedor">
								 <c:forEach var="contenedor" items="${LTipoContenedor}" varStatus="countRol">
								 	<c:choose>
								 		<c:when test="${circuito.tipoContenedor == contenedor.id}">
								 			<option value="${contenedor.id}" selected><c:out value="${contenedor.descripcion}" /></option>
								 		</c:when>
								 		<c:otherwise>
								 			<option value="${contenedor.id}"><c:out value="${contenedor.descripcion}" /></option>
								 		</c:otherwise>
							 		</c:choose>
					            </c:forEach>
				            </select>
						</td>
					</tr>
					<tr>
						<td>Cliente ID</td>
						<td><input type="textbox" name="clienteID" value="${circuito.id.clienteId}"/></td>
					</tr>
					<tr>
						<td>Nombre cliente</td>
						<td><input type="textbox" name="nombreCliente" value="${circuito.clienteNombre}"/></td>
					</tr>
					<tr>
						<td>Referencia cliente</td>
						<td><input type="textbox" name="refCliente" value="${circuito.refCliente}"/></td>
					</tr>
					<tr>
						<td>Proveedor ID</td>
						<td><input type="textbox" name="proveedorID" value="${circuito.proveedor_id}"/></td>
					</tr>
					<tr>
						<td>Circuito activo</td>
						<td>
							<c:choose>
				            	<c:when test="${circuito.activo eq true }">
				            		<input type="checkbox" name="circuitoActivo" checked />
				            	
				            	</c:when>
				            	<c:otherwise>
				            		<input type="checkbox" name="circuitoActivo"/>
				            	</c:otherwise>
			            	</c:choose>
						</td>
					</tr>
					<tr>
						<td>Mail notificaciones</td>
						<td><input type="textbox" name="mailNotificaciones" value="${circuito.mail}"/></td>
					</tr>
					<tr>
						<td>Lecturas minimas</td>
						<td>
							<input type="textbox" name="lecMinCons" value="${circuito.cir_LecMinParaCons}"/>
						</td>
					</tr>
				</table>
			</div>
			<div id="integracion">
				<table style="width:100%; height:100%">
					<tr>
						<td>Lanzar pedido automatico</td>
						<td>
							<c:choose>
				            	<c:when test="${circuito.cir_LanPedAut eq true }">
				            		<input type="checkbox" name="lanPedAut" checked />
				            	</c:when>
				            	<c:otherwise>
				            		<input type="checkbox" name="lanPedAut"/>
				            	</c:otherwise>
			            	</c:choose>
						</td>
					</tr>
					<tr>
						<td>Registrar pedido de compra</td>
						<td>
							<c:choose>
				            	<c:when test="${circuito.cir_RegistrarPedCompra eq true }">
				            		<input type="checkbox" name="pedCompraAut" checked />
				            	
				            	</c:when>
				            	<c:otherwise>
				            		<input type="checkbox" name="pedCompraAut"/>
				            	</c:otherwise>
			            	</c:choose>
						</td>
					</tr>
					<tr>
						<td>Reaprovisinamiento agrupado</td>
						<td>
							<c:choose>
				            	<c:when test="${circuito.cir_ReaprovAgrup eq true }">
				            		<input type="checkbox" name="reaproAgrup" checked />
				            	
				            	</c:when>
				            	<c:otherwise>
				            		<input type="checkbox" name="reaproAgrup"/>
				            	</c:otherwise>
			            	</c:choose>
						</td>
					</tr>
					<tr>
						<td>Circuito de consigna</td>
						<td>
							<c:choose>
				            	<c:when test="${circuito.cir_Consigna eq true }">
				            		<input type="checkbox" name="cirConsigna" checked />
				            	
				            	</c:when>
				            	<c:otherwise>
				            		<input type="checkbox" name="cirConsigna"/>
				            	</c:otherwise>
			            	</c:choose>
						</td>
					</tr>
					
					<tr>
						<td>ConSalesType</td>
						<td>
							<input type="textbox" name="salesType" value="${circuito.cir_ConSalesType}"/>
						</td>
					</tr>
					<tr>
						<td>InventLocationID</td>
						<td>
							<input type="textbox" name="inventLocationID" value="${circuito.cir_ConInventLocationId}"/>
						</td>
					</tr>
					<tr>
						<td>Pedido marco</td>
						<td>
							<input type="textbox" name="pedMarco" value="${circuito.cir_PedMarco}"/>
						</td>
					</tr>
					<tr>
						<td>Albaran automatico</td>
						<td>
							<input type="textbox" name="albAut" value="${circuito.cir_AlbaranarAut}"/>
						</td>
					</tr>
					<tr>
						<td>Factura agrupada</td>
						<td>
							<c:choose>
				            	<c:when test="${circuito.cir_ConFacturarAgrup eq true }">
				            		<input type="checkbox" name="factAgrupada" checked />
				            	
				            	</c:when>
				            	<c:otherwise>
				            		<input type="checkbox" name="factAgrupada"/>
				            	</c:otherwise>
			            	</c:choose>
						</td>
					</tr>
					
				</table>
			</div>
			<div id="programacion">
				<table style="width:100%; height:100%">
					<tr>
						<td>Entrega dias</td>
						<td><input type="textbox" name="ctdEntregaDias" value="${circuito.entregaDias}"/></td>
					</tr>
					<tr>
						<td>Entrega horas</td>
						<td><input type="textbox" name="ctdEntregaHoras" value="${circuito.entregaHoras}"/></td>
					</tr>
					<tr>
						<td>Hora de integraci�n (ma�ana)</td>
						<td><input type="textbox" name="horaM" value="${circuito.cir_HoraM}"/></td>
					</tr>
					<tr>
						<td>Hora de integraci�n (tarde)</td>
						<td><input type="textbox" name="horaM" value="${circuito.cir_HoraT}"/></td>
					</tr>
				</table>
						<div>
							<fieldset>
					      		<legend style="color:#e2001a;font-weight:bold;">Dias </legend>
								<table>
									<thead>
										<th></th>
										<th>Integrar pedidos?</th>
										<th>Ma�ana</th>
										<th>Tarte</th>
									</thead>
									<tr>
										<td>Lunes</td>
										<td>
											<c:choose>
								            	<c:when test="${circuito.cir_IntLun eq true }">
								            		<input type="checkbox" name="intLun" checked />
								            	
								            	</c:when>
								            	<c:otherwise>
								            		<input type="checkbox" name="intLun"/>
								            	</c:otherwise>
							            	</c:choose>
										</td>
										<td><input type="textbox" name="ctdLunM" value="${circuito.cir_LunM}"/></td>
										<td><input type="textbox" name="ctdLunT" value="${circuito.cir_LunT}"/></td>
									</tr>
									<tr>
										<td>Martes</td>
										<td>
											<c:choose>
								            	<c:when test="${circuito.cir_IntMar eq true }">
								            		<input type="checkbox" name="intMar" checked />
								            	
								            	</c:when>
								            	<c:otherwise>
								            		<input type="checkbox" name="intMar"/>
								            	</c:otherwise>
							            	</c:choose>
										</td>
										<td><input type="textbox" name="ctdMarM" value="${circuito.cir_MarM}"/></td>
										<td><input type="textbox" name="ctdMarT" value="${circuito.cir_MarT}"/></td>
									</tr>
									<tr>
										<td>Miercoles</td>
										<td>
											<c:choose>
								            	<c:when test="${circuito.cir_IntMie eq true }">
								            		<input type="checkbox" name="intMie" checked />
								            	
								            	</c:when>
								            	<c:otherwise>
								            		<input type="checkbox" name="intMie"/>
								            	</c:otherwise>
							            	</c:choose>
										</td>
										<td><input type="textbox" name="ctdMieM" value="${circuito.cir_MieM}"/></td>
										<td><input type="textbox" name="ctdMieT" value="${circuito.cir_MieT}"/></td>
									</tr>
									<tr>
										<td>Jueves</td>
										<td>
											<c:choose>
								            	<c:when test="${circuito.cir_IntJue eq true }">
								            		<input type="checkbox" name="intJue" checked />
								            	
								            	</c:when>
								            	<c:otherwise>
								            		<input type="checkbox" name="intJue"/>
								            	</c:otherwise>
							            	</c:choose>
										</td>
										<td><input type="textbox" name="ctdJueM" value="${circuito.cir_JueM}"/></td>
										<td><input type="textbox" name="ctdJueT" value="${circuito.cir_JueT}"/></td>
									</tr>
									<tr>
										<td>Viernes</td>
										<td>
											<c:choose>
								            	<c:when test="${circuito.cir_IntVie eq true }">
								            		<input type="checkbox" name="intVie" checked />
								            	
								            	</c:when>
								            	<c:otherwise>
								            		<input type="checkbox" name="intVie"/>
								            	</c:otherwise>
							            	</c:choose>
										</td>
										<td><input type="textbox" name="ctdVieM" value="${circuito.cir_VieM}"/></td>
										<td><input type="textbox" name="ctdVieT" value="${circuito.cir_VieT}"/></td>
									</tr>
									<tr>
										<td>Sabado</td>
										<td>
											<c:choose>
								            	<c:when test="${circuito.cir_IntSab eq true }">
								            		<input type="checkbox" name="intSab" checked />
								            	
								            	</c:when>
								            	<c:otherwise>
								            		<input type="checkbox" name="intSav"/>
								            	</c:otherwise>
							            	</c:choose>
										</td>
										<td><input type="textbox" name="ctdSabM" value="${circuito.cir_SabM}"/></td>
										<td><input type="textbox" name="ctdSabT" value="${circuito.cir_SabT}"/></td>
									</tr>
									<tr>
										<td>Domingo</td>
										<td>
											<c:choose>
								            	<c:when test="${circuito.cir_IntDom eq true }">
								            		<input type="checkbox" name="intDom" checked />
								            	
								            	</c:when>
								            	<c:otherwise>
								            		<input type="checkbox" name="intDom"/>
								            	</c:otherwise>
							            	</c:choose>
										</td>
										<td><input type="textbox" name="ctdDomM" value="${circuito.cir_DomM}"/></td>
										<td><input type="textbox" name="ctdDomT" value="${circuito.cir_DomT}"/></td>
									</tr>
									
								</table>
							</fieldset>
						</div>
					
			</div>
		</div>
		<div>
			<span><c:out value="${msgError }"></c:out></span>
		</div>
		<div style="margin-top:15px">
			<input type="submit" name="formSubmit" value="Guardar"  style="float:right" />
			<input type="submit" name="formSubmit" value="Cancelar" style="float:left"/>
		</div>
	</form>
</body>
</html>