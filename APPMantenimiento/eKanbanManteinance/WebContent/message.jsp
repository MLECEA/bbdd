<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
	<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.mobile.min.css" />
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/jquery.min.js"></script>
    <script src="resources/js/jquery-ui.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
    <script>
    $(function() {
    	var texto = getUrlParameter("msgTexto");
    	$("#msgTexto").text(texto);
    	$("#msgDiv").dialog({  //create dialog, but keep it closed
 		   autoOpen: true,
 		   height: 120,
 		   width: 500,
 		   modal: true,
 		   open: function(event, ui) {
   				$(".ui-dialog-titlebar-close").hide();
   		    },
   		    title: "Aviso"
		});		    	
    });
    
    function getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };
    
    function redirect(){
    	var opt = getUrlParameter("redirectOpt");
    	if (opt == "1"){
    		window.location.href = "./users";
    	}
    	if (opt == "2"){
    		window.location.href = "./circuits";
    	}
    	if (opt == "3"){
    		window.location.href = "./ubicaciones";
    	}
    	if (opt == "4"){
    		window.location.href = "./acciones";
    	}
    	if (opt == "5"){
    		window.location.href = "./subUbicaciones";
    	}
    	
    }
    </script>
</head>
<body>
	<div id="msgDiv">
		<span id="msgTexto"></span>
		<br/>
		<div style="text-align: center;margin-top: 15px">
			<input type="button" value="OK" onclick="redirect();"></input>
		</div>
	</div>
</body>
</html>