<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
	<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.mobile.min.css" />
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/jquery.min.js"></script>
    <script src="resources/js/jquery-ui.min.js"></script>
    <script src="resources/js/usuarios.js"></script>
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
<script>
var formResp = "";
$(function() {
    $("#tabs").tabs();
    function moveItems(origin, dest) {
        $(origin).find(':selected').appendTo(dest);
    }      
    $('#left').click(function () {
        moveItems('#ubicacionesUsuario', '#ubicacionesNoUsuario');
    });
     
    $('#right').on('click', function () {
        moveItems('#ubicacionesNoUsuario', '#ubicacionesUsuario');
    });
    $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
        $(this).val($(this).val().replace(/[^\d].+/, ""));
         if ((event.which < 48 || event.which > 57)) {
             event.preventDefault();
         }
     });
  } );
  function goBack(){
	  window.location.href = "./users";
  }
  
  function seleccionarTodos(){
	  $('#ubicacionesUsuario option').prop('selected', true);
  }
  
  function cancelarSubmit(){
	  formResp = "KO";
  }
  
  function sendForm(){
	  if (formResp == "KO") 
		  return true; 
	  else 
	  		return validarDatos();
  }
  
</script>
</head>
<body>
<form action="copyUser" method="post" onsubmit="return sendForm();">
		<div id="tabs">
			<ul>
				<li><a href="#userData">Datos generales</a></li>
				<li><a href="#Accesos">Acceso aplicaci�n</a></li>
				<li><a href="#Ubicaciones">Ubicaciones</a></li>
				<li><a href="#Otros">Otros datos</a></li>
			</ul>
			<div id="userData" style="width:100%;height:100%">
				<table style="width:100%; height:100%" >
					<tr>
						<TD>C�digo usuario</TD>
						<TD><c:out value="${UserData.codUsuario }" /></TD>
					</tr>
					<tr>
						<TD>Nombre</TD>
						<TD><input type="text" name="nombre" value="${UserData.nombre }" ></TD>
					</tr>
					<tr>
						<TD>Apellidos</TD>
						<TD><input name="apellidos" type="text" value="${UserData.apellidos}" ></TD>
					</tr>
					<tr>
						<TD>Mail</TD>
						<TD><input name="email" type="text" value="${UserData.mail}" ></TD>
					</tr>
					<tr>
						<TD>Idioma</TD>
						<TD><input name="idioma" type="text" value="${UserData.usu_Idi}" ></TD>
					</tr>
					<tr>
						<TD>Empresa</TD>
						<TD>
							<select name="usrEmpresa" id="empresaUsuario">
								 <c:forEach var="empresa" items="${LEmpresas}" varStatus="countEmpresa">
					               		<c:choose>
								 		<c:when test="${UserData.usu_Emp == empresa.emp_Idd}">
								 			<option value="${empresa.emp_Idd}" selected><c:out value="${empresa.emp_Nom}" /></option>
								 		</c:when>
								 		<c:otherwise>
								 			<option value="${empresa.emp_Idd}"><c:out value="${empresa.emp_Nom}" /></option>
								 		</c:otherwise>
							 		</c:choose>
					            </c:forEach>
				            </select>
						</TD>
					</tr>
					<tr>
						<TD>Rol<span style="color:red"> *</span></TD>
						<TD>
							<select name="usrRol"  id="rolUsuario">
								 <c:forEach var="rol" items="${LRoles}" varStatus="countRol">
								 	<c:choose>
								 		<c:when test="${UserData.ptrRol == rol.codRol}">
								 			<option value="${rol.codRol}" selected><c:out value="${rol.descripcion}" /></option>
								 		</c:when>
								 		<c:otherwise>
								 			<option value="${rol.codRol}"><c:out value="${rol.descripcion}" /></option>
								 		</c:otherwise>
							 		</c:choose>
					            </c:forEach>
				            </select>
				            </br><span id="rolObli" style="display:none; color:red; font-size:11px">Campo obligatorio</span>
						</TD>
					</tr>
				</table>
			</div>
			<div id="Accesos">
				<table style="width:100%; height:100%" >	
					<tr>
						<TD>Username<span style="color:red"> *</span></TD>
						<TD><input name="username" id="txtUsername" type="text" value="${UserData.username}" >
						</br><span id="usernameObli" style="display:none; color:red; font-size:11px">Campo obligatorio</span>
						</TD>
					</tr>
					<tr>
						<TD>Password</TD>
						<TD><input name="pwd" type="text" value="${UserData.password}" ></TD>
					</tr>
					<tr>
						<TD>Activo</TD>
						<TD>
							<c:choose>
								<c:when test="${UserData.activo eq 1}">
									<input name="activo" type="checkbox" checked />
								</c:when>
								<c:otherwise>
									<input name="activo" type="checkbox" />
								</c:otherwise>
							</c:choose>
							
						</TD>
					</tr>
					<tr>
						<TD>Caducidad token autenticaci�n (min)</TD>
						<TD><input name="tokenAuten" type="text" value="${UserData.tokenAuten_CaducidadMins}" ></TD>
					</tr>
					<tr>
						<TD>Caducidad token refresco (min)</TD>
						<TD><input name="tokenRefresh" type="text" value="${UserData.tokenRefresco_CaducidadMins}" ></TD>
					</tr>
					<tr>
						<TD>Optimistic Lock</TD>
						<TD><input class="allownumericwithoutdecimal" id="optLock" name="optimisticLock" type="text" value="${UserData.optimisticLock}" >
						</br><span id="optObli" style="display:none; color:red; font-size:11px">Campo obligatorio</span>
						</TD>
					</tr>
				</table>
				<div id="AccesoPasarela" style="width:100%;">
				   <fieldset>
				      <legend style="color:#e2001a;font-weight:bold;">Acceso pasaleras eKanban-ERP</legend>
				      <table>
				         <tr>
				            <td>Pasarela activada</td>
				            <td>
				            	<c:choose>
				            	<c:when test="${UserData.usu_PasarelaActivada eq true }">
				            		<input type="checkbox" name="pasarelaActivada" checked />
				            	
				            	</c:when>
				            	<c:otherwise>
				            		<input type="checkbox" name="pasarelaActivada"/>
				            	</c:otherwise>
				            	</c:choose>
			            	</td>
				         </tr>
				         <tr>
				            <td>Servidor FTP</td>
				            <td><input type="text" name="srvFTP" value="${UserData.FTP_Server}"/></td>
				         </tr>
				         <tr>
				            <td>Username FTP</td>
				            <td><input type="text" name="userFTP" value="${UserData.FTP_Username}"/></td>
				         </tr>
				         <tr>
				            <td>Password FTP</td>
				            <td><input type="text" name="pwdFTP" value="${UserData.FTP_Password}"/></td>
				         </tr>
				         <tr>
				            <td>Subir datos a FTP</td>
				            <td>
				            	<c:choose>
					            	<c:when test="${UserData.usu_SubirDatosAFTP eq true }">
					            		<input type="checkbox" name="subirFTP" checked />
					            	
					            	</c:when>
					            	<c:otherwise>
					            		<input type="checkbox" name="subirFTP"/>
					            	</c:otherwise>
				            	</c:choose>
				            </td>
				         </tr>
				      </table>
				   </fieldset>
				</div>
			</div>
			<div id="Ubicaciones">
				<div style="width:100%; height:100%;display:flex">
					<div>
						<span>Ubicaciones sin asignadar:</span>
				    	</br>
						<select multiple="multiple" id="ubicacionesNoUsuario" style="height:300px; margin-right:10px">
							 <c:forEach var="ubicacion2" items="${LUbicacionesNoUsuario}" varStatus="theCount2">
				               		<option value="${ubicacion2.idUbicacion}"><c:out value="${ubicacion2.idUbicacion}" />-<c:out value="${ubicacion2.descripcion}" /></option>
				            </c:forEach>
			            </select>
		            </div>
		            <div style="margin-right: 10px;margin-top: 150px;">
			            <input type="button" id="left" value="&lt;" />
					    <input type="button" id="right" value="&gt;" />
				    </div>
				    <div>
				    	<span>Ubicaciones asignadas:</span>
				    	</br>
						<select multiple="multiple" name="usrUbicaciones" id="ubicacionesUsuario" style="height:300px">
							 <c:forEach var="ubicacion" items="${LUbicaciones}" varStatus="theCount">
				               		<option value="${ubicacion.ubicacion.idUbicacion}"><c:out value="${ubicacion.ubicacion.idUbicacion}" />-<c:out value="${ubicacion.ubicacion.descripcion}" /></option>
				            </c:forEach>
			            </select>
		            </div>
		            <br />
			    </div>
			</div>
			<div id="Otros">
				<table style="width:100%; height:100%" >	
					<tr>
						<TD>Ultima conexi�n</TD>
						<TD><span><c:out value="${UserData.conectUltFec}"></c:out></span></TD>
					</tr>
					<tr>
						<TD>M�vil</TD>
						<TD><span><c:out value="${UserData.usu_Mov}"></c:out></span></TD>
					</tr>
					<tr>
						<TD>Observaciones</TD>
						<TD><span><c:out value="${UserData.usu_Observaciones}"></c:out></span></TD>
					</tr>
				</table>					    
			</div>
		</div>
		<div style="margin-top:15px">
			<input type="submit" name="formSubmit" value="Guardar" onclick="seleccionarTodos();" style="float:right" />
			<input type="submit" name="formSubmit" value="Cancelar" style="float:left" onclick="cancelarSubmit();"/>
		</div>
	</form>
</body>
</html>