<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<jsp:include page="/WEB-INF/templates/header.jsp"/> 
 	<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.mobile.min.css" />
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/jquery.min.js"></script>
    <script src="resources/js/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="resources/css/Orkli.css"></link>
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
</head>
<body>
	<div style="display:inline-table">
		<div align="center" style="float:left; width:18%">
			<jsp:include page="/WEB-INF/templates/Lateralmenu.jsp"/> 
		</div>
		<div align="center" style="width:80%; float:right">
	    	<div style="margin-bottom: 45px;">
<!-- 	    		<div style="float:left"> -->
<!-- 	    			<input type="button" value="Nueva accion" onclick="newAction();" style="height: 30px;"/> -->
<!-- 	    		</div> -->
	    		<div style="float:right">
	    			<input type="radio" id="chkTodos" name="chkFiltro"  onchange="filtrarAccionesActivas()" checked>Mostrar todas las acciones	
		    		<input type="radio" id="chkActivos" name="chkFiltro"  onchange="filtrarAccionesActivas()">Solo las acciones activas
		    		<input type="radio" id="chkInactivos" name="chkFiltro" onchange="filtrarAccionesActivas()">Solo las acciones inactivas
	    		</div>
    		</div>
	        <table id="accionesGrid" cellpadding="5">
	            <tr>
	                <th class="k-header k-sorted">Codigo</th>
	                <th class="k-header k-sorted">Descripci�n</th>
	                <th class="k-header k-sorted">Tipo</th>
	                <th class="k-header k-sorted" style="display:none">Activo</th>
	                
	            </tr>
	            <c:forEach var="accion" items="${LAcciones}" varStatus="theCount">
	            	<c:choose>
	            		<c:when test="${accion.accRep_Act eq 1 }">
	            			<tr >
			                    <td id="divIDNo${theCount.index}">
			                    	<a href="javascript:void(0)" onclick="deleteAccion(this);"><img src="resources/img/delete.png" style="width: 15px; margin-right:10px" /></a> 
			                    	<a href="javascript:void(0)" onclick="copyAction(this);"><img src="resources/img/copy.png" style="width: 15px; ; margin-right:10px" /></a> 
			                    	<c:out value="${accion.accRep_Idd}" />
		                    	</td>
			                    <td><c:out value="${accion.accRep_Des}" /></td>
			                    <td><c:out value="${accion.accRep_Tip}" /></td>
			                    <td style="display:none"><c:out value="${accion.accRep_Act}" /></td>
		                	</tr>
	            		</c:when>
	            		<c:otherwise>
	            			<tr>
			                    <td id="divIDNo${theCount.index}">
			                    	<a href="javascript:void(0)" onclick="deleteAccion(this);"><img src="resources/img/delete.png" style="width: 15px; margin-right:10px" /></a> 
			                    	<a href="javascript:void(0)" onclick="copyAction(this);"><img src="resources/img/copy.png" style="width: 15px; margin-right:10px" /></a> 
		                    		<c:out value="${accion.accRep_Idd}" />
	                    		</td>
			                    <td><c:out value="${accion.accRep_Des}" /></span></td>
			                    <td><span class="inactivo"><c:out value="${accion.accRep_Tip}" /></span></td>
			                    <td style="display:none"><c:out value="${accion.accRep_Act}" /></td>
		                	</tr>
	            		</c:otherwise>
	            	</c:choose>
		                
		              
	            </c:forEach>
	        </table>
	      	<div id="targetDiv">
	      	
	      	</div>
	    </div>
	</div>
	<script>
                $(document).ready(function() {
                	$("#targetDiv").dialog({  //create dialog, but keep it closed
                		   autoOpen: false,
                		   height: 530,
                		   width: 900,
                		   modal: true,
                		   open: function(event, ui) {
	              				$(".ui-dialog-titlebar-close").hide();
	              		    },
	              		    title: "Editar informaci�n de la acci�n"
               		});
                	
                    $("#accionesGrid").kendoGrid({
                    	schema: {  
                            model: {  
                                id: "Acciones",  
                                fields: {  
                                    Codigo: { type: "string" },  
                                    Descripcion: { type: "string" } ,
                                    Tipo: { type: "string" },
                                    Activo: {type: "number"}
                                }  
                            }  
                        }, 
                        columns: [
                            { field: "Codigo"},
                            { field: "Descripcion" },
                            { field: "Tipo" },
                            { field: "Activo", hidden:true }
                          ],
                        sortable: true,
                        filterable:{
                        	extra: false,
                        	mode: "row",
                            operators: {
                            	string: {
                            		contains: "Contains"
                            	}
                            }
                        },
                        pageable: {
                            pageSize: 20
                        },
                        selectable: "row",
                        dataBound: ondataBound
                    });
                    $("#accionesGrid").on("dblclick", "tr.k-state-selected", function (e) {
                        // insert code here
                    	var accID = $("#accionesGrid").find('tr.k-state-selected').find("td:first")[0].innerText.trim(); //html();
                        //window.location.href = "./editUser?codUser=" + codUser;
                    	$("#targetDiv").load("./editAccion?accionID=" + accID);
                        $("#targetDiv").dialog("open"); 
                    });
                });
                
                function ondataBound(){
                	$(".inactivo").each(function() {
                        var trElemn = $(this)[0].parentNode.parentNode;
                        trElemn.className += " RowInactivo";
                     });
                }
                
                function copyAction(event){
                	$('#targetDiv').dialog('option','width',900);
                	$('#targetDiv').dialog('option','height',530);
                	var accionID = event.parentNode.innerText.toString().trim();
                	$("#targetDiv").load("./copyAccion?accionID=" + accionID);
                    $("#targetDiv").dialog("open");
                }
                
                function deleteAccion(event){
                	$('#targetDiv').dialog('option','width',500);
                	$('#targetDiv').dialog('option','height',120);
                	$('#targetDiv').dialog('option','title','Eliminar usuario seleccionado');
                	var accionID = event.parentNode.innerText.toString().trim();
                	$("#targetDiv").load("./deleteAccion?accionID=" + accionID);
                    $("#targetDiv").dialog("open");
                }
                function newUser(){
                	
                	$("#targetDiv").load("./newUser");
                    $("#targetDiv").dialog("open"); 
                }
                
                function filtrarAccionesActivas(){
                	var grid = $("#accionesGrid");
                	if ($("#chkActivos")[0].checked){
                		$("#accionesGrid").data("kendoGrid").dataSource.filter({ field: "Activo", operator: "eq", value: 1 }); 
                	}
                	if ($("#chkInactivos")[0].checked){
                		$("#accionesGrid").data("kendoGrid").dataSource.filter({ field: "Activo", operator: "eq", value: 0 }); 
                	}
                	if ($("#chkTodos")[0].checked){
                		$("#accionesGrid").data("kendoGrid").dataSource.filter({}); 
                	}
                	
                }
            </script>
</body>
</html>