<%-- <%@ include file="/WEB-INF/templates/header.jsp"%> --%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<jsp:include page="/WEB-INF/templates/header.jsp"/> 
 
	<link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
	<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.mobile.min.css" />
    <link rel="stylesheet" href="resources/css/Orkli.css"></link>
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/jquery.min.js"></script>
    <script src="resources/js/jquery-ui.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
    <style>
    
    </style>
</head>
<body>   
	<div style="display:inline-table">
		<div align="center" style="float:left; width:18%">
			<jsp:include page="/WEB-INF/templates/Lateralmenu.jsp"/> 
		</div>
		<div align="center" style="width:80%; float:right">
	    	<div style="margin-bottom: 45px;">
	    		<div style="float:left">
	    			<input type="button" value="Nuevo usuario" onclick="newUser();" style="height: 30px;"/>
	    		</div>
	    		<div style="float:right">
	    			<input type="radio" id="chkTodos" name="chkFiltro"  onchange="filtrarUsuariosActivos()" checked>Mostrar todos los usuarios	
		    		<input type="radio" id="chkActivos" name="chkFiltro"  onchange="filtrarUsuariosActivos()">Solo los usuarios activos
		    		<input type="radio" id="chkInactivos" name="chkFiltro" onchange="filtrarUsuariosActivos()">Solo los usuarios inactivos
	    		</div>
    		</div>
	        <table id="userGrid" cellpadding="5">
	            <tr>
	            	<th class="k-header k-sorted"></th>
	                <th class="k-header k-sorted">Codigo</th>
	                <th class="k-header k-sorted">Nombre usuario</th>
	                <th class="k-header k-sorted">Email</th>
	                <th class="k-header k-sorted" style="display:none">Activo</th>
	                
	            </tr>
	            <c:forEach var="user" items="${LUsuarios}" varStatus="theCount">
	            	<c:choose>
	            		<c:when test="${user.activo eq 1 }">
	            			<tr >
	            				<td>
	            					<a href="javascript:void(0)" onclick="deleteUser(this);"><img src="resources/img/delete.png" style="width: 15px; margin-right:10px" /></a> 
			                    	<a href="javascript:void(0)" onclick="copyUser(this);"><img src="resources/img/copy.png" style="width: 15px; ; margin-right:10px" /></a>
	            				</td>
			                    <td id="divIDNo${theCount.index}">
			                    	<c:out value="${user.codUsuario}" />
		                    	</td>
			                    <td><c:out value="${user.nombre}" /></td>
			                    <td><c:out value="${user.mail}" /></td>
			                    <td style="display:none"><c:out value="${user.activo}" /></td>
		                	</tr>
	            		</c:when>
	            		<c:otherwise>
	            			<tr>
	            				<td>
	            					<a href="javascript:void(0)" onclick="deleteUser(this);"><img src="resources/img/delete.png" style="width: 15px; margin-right:10px" /></a> 
			                    	<a href="javascript:void(0)" onclick="copyUser(this);"><img src="resources/img/copy.png" style="width: 15px; ; margin-right:10px" /></a>
	            				</td>
			                    <td id="divIDNo${theCount.index}">
		                    		<c:out value="${user.codUsuario}" />
	                    		</td>
			                    <td><c:out value="${user.nombre}" /></span></td>
			                    <td><span class="inactivo"><c:out value="${user.mail}" /></span></td>
			                    <td style="display:none"><c:out value="${user.activo}" /></td>
		                	</tr>
	            		</c:otherwise>
	            	</c:choose>
		                
		              
	            </c:forEach>
	        </table>
	      	<div id="targetDiv">
	      	
	      	</div>
	    </div>
	</div>
    
    <script>
                $(document).ready(function() {
                	$("#targetDiv").dialog({  //create dialog, but keep it closed
                		   autoOpen: false,
                		   height: 530,
                		   width: 900,
                		   modal: true,
                		   open: function(event, ui) {
	              				$(".ui-dialog-titlebar-close").hide();
	              		    },
	              		    title: "Editar información del usuario"
               		});
                	
                    $("#userGrid").kendoGrid({
                    	schema: {  
                            model: {  
                                id: "Users",  
                                fields: {  
                                	Enlaces: { type: "string" },
                                    Codigo: { type: "string" },  
                                    Nombre: { type: "string" } ,
                                    Email: { type: "string" },
                                    Activo: {type: "number"}
                                }  
                            }  
                        }, 
                        columns: [
                        	{ field: "Enlaces", filterable: false, width:100 },
                            { field: "Codigo"},
                            { field: "Nombre" },
                            { field: "Email" },
                            { field: "activo", hidden:true }
                          ],
                        sortable: true,
                        filterable: {
                        	extra: false,
                        	mode: "row",
                            operators: {
                            	string: {
                            		contains: "Contains"
                            	}
                            }
                        },
                        pageable: {
                            pageSize: 20
                        },
                        selectable: "row",
                        dataBound: ondataBound
                    });
                    $("#userGrid").on("dblclick", "tr.k-state-selected", function (e) {
                    	$('#targetDiv').dialog('option','title','Editar usuario seleccionado');
                        // insert code here
                    	var codUser = $("#userGrid").find('tr.k-state-selected')[0].childNodes[1].innerText.trim(); //html();
                        //window.location.href = "./editUser?codUser=" + codUser;
                    	$("#targetDiv").load("./editUser?codUser=" + codUser);
                        $("#targetDiv").dialog("open"); 
                    });
                });
                
                function ondataBound(){
                	$(".inactivo").each(function() {
                        var trElemn = $(this)[0].parentNode.parentNode;
                        trElemn.className += " RowInactivo";
                     });
                }
                
                function copyUser(event){
                	$('#targetDiv').dialog('option','width',900);
                	$('#targetDiv').dialog('option','height',530);
                	$('#targetDiv').dialog('option','title','Copiar usuario seleccionado');
                	var codUser = event.parentNode.innerText.toString().trim();
                	$("#targetDiv").load("./copyUser?codUser=" + codUser);
                    $("#targetDiv").dialog("open");
                }
                
                function deleteUser(event){
                	$('#targetDiv').dialog('option','width',500);
                	$('#targetDiv').dialog('option','height',120);
                	$('#targetDiv').dialog('option','title','Eliminar usuario seleccionado');
                	var codUser = event.parentNode.innerText.toString().trim();
                	$("#targetDiv").load("./deleteUser?codUser=" + codUser);
                    $("#targetDiv").dialog("open");
                }
                function newUser(){
                	$('#targetDiv').dialog('option','title','Crear nuevo usuario');
                	$("#targetDiv").load("./newUser");
                    $("#targetDiv").dialog("open"); 
                }
                
                function filtrarUsuariosActivos(){
                	var grid = $("#userGrid");
                	if ($("#chkActivos")[0].checked){
                		$("#userGrid").data("kendoGrid").dataSource.filter({ field: "activo", operator: "eq", value: 1 }); 
                	}
                	if ($("#chkInactivos")[0].checked){
                		$("#userGrid").data("kendoGrid").dataSource.filter({ field: "activo", operator: "eq", value: 0 }); 
                	}
                	if ($("#chkTodos")[0].checked){
                		$("#userGrid").data("kendoGrid").dataSource.filter({}); 
                	}
                	
                }
            </script>
</body>
</html>