<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.mobile.min.css" />
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/jquery.min.js"></script>
    <script src="resources/js/jquery-ui.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.3/FileSaver.min.js"></script>
    <script src="resources/js/BinaryToImage.js"></script>
    <script>
        var formResp = "";
        $(function() {
            $("#tabs").tabs();
            function moveItems(origin, dest) {
                $(origin).find(':selected').appendTo(dest);
            }
            $('#left').click(function () {
                moveItems('#ubicacionesUsuario', '#ubicacionesNoUsuario');
            });

            $('#right').on('click', function () {
                moveItems('#ubicacionesNoUsuario', '#ubicacionesUsuario');
            });
            $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {
                $(this).val($(this).val().replace(/[^\d].+/, ""));
                if ((event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });
        } );

        function cancelarSubmit(){
            formResp = "KO";
        }

        function sendForm(){

            if (formResp == "KO")
                return true;
            else
                return validarDatos();
        }

        function mostrarTabla(){
            //abre
            var seleccionado = $("#flujosGrid").find('tr.k-state-selected').find("td:first")[0].innerText.trim();
            window.open("./tablaFlujo?idMatricula=" + seleccionado, '_blank', 'location=no,height=570,width=1024,scrollbars=yes,status=no,toolbar=no,menubar=no');
            //fin abre
            //var tempElemento = document.createElement('a');
            //var data_type = 'data:application/vnd.ms-excel';

        }
    </script>
</head>
<body>
<form action="flujos" method="post" onsubmit="return sendForm();">
    <div id="tabs">
        <ul>
            <li><a href="#flujoData">Datos generales</a></li>
            <li><a href="#preguntas">Preguntas</a></li>
        </ul>
        <div id="flujoData" style="width:100%;height:100%">
            <table style="width:100%; height:100%">
                <tr>
                    <td>ID Flujo</td>
                    <td><c:out value="${accionData.idFlujo}" /></td>
                </tr>
                <tr>
                    <td>Descripción</td>
                    <td><c:out  value="${accionData.descripcionFlujo}"/></td>
                </tr>
                <tr>
                    <td>Icono URL</td>
                    <td><c:out  value="${accionData.iconoURL}"/></td>
                </tr>
                <tr>
                    <td>Activo</td>
                    <td><c:out  value="${accionData.activo}"/></td>
                </tr>
                <tr/>
                <tr>
                    <td>Matricula</td>
                    <input type="hidden" name="idMatricula" value="${accionData.idMatricula}">
                    <td><c:out  value="${accionData.idMatricula}"/></td>
                </tr>
                <tr>
                    <td>Usuario</td>
                    <td><c:out value="${accionData.nombre}${' '}${accionData.apellidos}" /></td>
                </tr>
                <tr>
                    <td>Prioridad</td>
                    <td><c:out  value="${accionData.prioridad}"/></td>
                </tr>
                <tr>
                    <td>Ubicación</td>
                    <td><c:out value="${accionData.ubicacion}" /></td>
                </tr>
                <tr>
                    <td>Sububicación</td>
                    <td><c:out value="${accionData.sububicacion}" /></td>
                </tr>
                <tr>
                    <td>Fecha Inicio</td>
                    <td><c:out value="${accionData.fechaInicio}" /></td>
                </tr>
                <tr>
                    <td>Fecha Fin</td>
                    <td><c:out value="${accionData.fechaFin}" /></td>
                </tr>
                <tr>
                    <td>Cancelado</td>
                    <td><c:out value="${accionData.getTextoCancelado()}" /></td>
                </tr>
            </table>

        </div>
        <div id="preguntas">
           <table style="width:100%; height:100%">
                <tr>
                    <c:forEach var="pregunta" items="${LRespuestasFlujo}" varStatus="theCount">
                        <tr><td style="font-weight:bold;" ><c:out value="${pregunta.idPregunta}${'.- '}${pregunta.textoPregunta}${' ['}${pregunta.tipoRespuesta}${']'}" /> </td></tr>
                        <tr><td><c:out value="${'    '}${pregunta.getRespuestaVisualizacion()}" /> </td></tr>
                        <c:if test="${pregunta.esScan()}">
                            <c:forEach var="texto" items="${pregunta.getTextoScan()}" varStatus="theCountTexto">
                                <tr><td><c:out value="${'        '}${texto}" /> </td></tr>
                            </c:forEach>
                        </c:if>
                        <c:if test="${pregunta.tieneImagen()}">
                            <tr>
                                <td>
                                     <div id="imagen" >
                                         <img  id="img${pregunta.idPregunta}" width="300" border="1" >
                                         <script>
                                             ponerImagen('img${pregunta.idPregunta}' , '${pregunta.respuestaBytes}');
                                         </script>
                                     </div>
                                </td>
                            </tr>
                        </c:if>
                        <c:if test="${pregunta.esAudio()}">
                            <!-- <tr>
                                <td>
                                    <audio controls>
                                        <source src="${pregunta.getAudioFile()}" type="audio/mpeg">
                                    </audio>
                            </tr> -->
                        </c:if>
                        <tr><td></td></tr>
                    </c:forEach>
               <!--<video controls="" autoplay="" name="media1"><source src="http://techslides.com/demos/samples/sample.m4a" type="audio/mpeg"></video>-->
                </tr>
            </table>
        </div>
    </div>
    <div style="margin-top:15px">
        <input type="submit" name="formSubmit" value="Cancelar" style="float:left" onclick="cancelarSubmit();"/>
        <input type="submit" value="Mostrar Tabla" style="float:left" onclick="mostrarTabla();" style="float:right"/>
    </div>
</form>
</body>
</html>
