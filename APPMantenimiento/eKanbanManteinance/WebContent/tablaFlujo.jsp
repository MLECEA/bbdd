<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <title>App Mantenimiento - Flujo</title>
    <meta http-equiv="Content-Type" content="text/plain; charset=UTF-8">
    <link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.mobile.min.css" />
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/jquery.min.js"></script>
    <script src="resources/js/jquery-ui.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.3/FileSaver.min.js"></script>
    <script src="resources/js/BinaryToImage.js"></script>
    <script>
        var formResp = "";
        $(function() {
            $("#tabs").tabs();
            function moveItems(origin, dest) {
                $(origin).find(':selected').appendTo(dest);
            }
            $('#left').click(function () {
                moveItems('#ubicacionesUsuario', '#ubicacionesNoUsuario');
            });

            $('#right').on('click', function () {
                moveItems('#ubicacionesNoUsuario', '#ubicacionesUsuario');
            });
            $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {
                $(this).val($(this).val().replace(/[^\d].+/, ""));
                if ((event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });
        } );

        function cancelarSubmit(){
            formResp = "KO";
        }

        function sendForm(){

            if (formResp == "KO")
                return true;
            else
                return validarDatos();
        }

        function cerrar(){
            this.close();
        }

        function abrirExcel() {
            var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
            tab_text = tab_text + '<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">';
            tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';
            tab_text = tab_text + '<x:Name>Flujo</x:Name>';
            tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
            tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';
            tab_text = tab_text + "<table border='1px'>";
            var exportTable = $('#' + 'flujosData').clone(); //new
            exportTable.find('input').each(function (index, elem) { $(elem).remove(); }); //new
            tab_text = tab_text + exportTable.html();

            tab_text = tab_text + '</table>';

            $('.c_tbl').each(function( index ) {
                tab_text = tab_text + "<table>";
                tab_text = tab_text + "<tr><td></td></tr><tr><td></td></tr>";
                tab_text = tab_text + '</table>';
                tab_text = tab_text + "<table>";
                var exportTable = $(this).clone();
                tab_text = tab_text + exportTable.html();
                tab_text = tab_text + '</table>';
            });

            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // borrar imagenes
            tab_text = tab_text + '</body></html>';


             var blob = new Blob([tab_text], { type: "application/vnd.ms-excel;charset=ISO-8859-1" });
             saveAs(blob, "flujo.xls");

            alert("Vamos a descargar el fichero Excel con los datos del flujo");
        }

    </script>
</head>
<body >

        <div align="center" style="width:100%; float:right">
            <div style="margin-bottom: 45px;">
                <div style="float:left">
                    <input type="submit" value="Excel"  onclick="abrirExcel()" style="float:left" />
                    <input type="submit" value="Cerrar"  onclick="cerrar()" style="float:left" />
                </div>
            </div>
            <table id="flujosData" cellpadding="5" style="border: 1px solid #000000;background-color: #ffffff;width: 100%;border-collapse: collapse;font-size: 13px;font-family: Roboto,sans-serif;">
                    <tr >
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">ID Flujo</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Descripción</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Icono URL</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Activo</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Matricula</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Usuario</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Prioridad</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Ubicación</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Sububicación</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Fecha Inicio</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Fecha Fin</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Cancelado</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Pregunta</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Respuesta</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Lectura/Foto</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Ubicación</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Artículo</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Lote</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">N Serie</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Cantidad</td>
                        <th class="k-header k-sorted" style="border: 1px solid #000000;">Usuario</td>
                    </tr>

                    <input type="hidden" name="idMatricula" value="${matriculaID}">
                    <c:forEach var="pregunta" items="${LRespuestasFlujo}" varStatus="theCount">
                    <tr>
                        <td style="border: 1px solid #000000;"><c:out value="${accionData.idFlujo}" /></td>
                        <td style="border: 1px solid #000000;"><c:out value="${accionData.descripcionFlujo}"/></td>
                        <td style="border: 1px solid #000000;"><c:out value="${accionData.iconoURL}"/></td>
                        <td style="border: 1px solid #000000;"><c:out value="${accionData.activo}"/></td>
                        <td style="border: 1px solid #000000;"><c:out value="${accionData.idMatricula}"/></td>
                        <td style="border: 1px solid #000000;"><c:out value="${accionData.nombre}${' '}${accionData.apellidos}" /></td>
                        <td style="border: 1px solid #000000;"><c:out value="${accionData.prioridad}"/></td>
                        <td style="border: 1px solid #000000;"><c:out value="${accionData.ubicacion}" /></td>
                        <td style="border: 1px solid #000000;"><c:out value="${accionData.sububicacion}" /></td>
                        <td style="border: 1px solid #000000;"><c:out value="${accionData.fechaInicio}" /></td>
                        <td style="border: 1px solid #000000;"><c:out value="${accionData.fechaFin}" /></td>
                        <td style="border: 1px solid #000000;"><c:out value="${accionData.getTextoCancelado()}" /></td>

                        <td style="border: 1px solid #000000;"><c:out value="${pregunta.idPregunta}${'.- '}${pregunta.textoPregunta}${' ['}${pregunta.tipoRespuesta}${']'}" /> </td>
                        <td style="border: 1px solid #000000;"><c:out value="${'    '}${pregunta.getRespuestaVisualizacion()}" /> </td>
                        <c:if test="${pregunta.esScan()}">
                            <c:forEach var="texto" items="${pregunta.getTextoScan()}" varStatus="theCountTexto">
                                <td style="border: 1px solid #000000;"><c:out value="${texto}" /> </td>
                            </c:forEach>
                        </c:if>
                        <c:if test="${pregunta.tieneImagen()}">
                            <td style="border: 1px solid #000000;">
                                <div id="imagen" >
                                    <img  id="img${pregunta.idPregunta}" width="300"  >
                                    <script>
                                        ponerImagen('img${pregunta.idPregunta}' , '${pregunta.respuestaBytes}');
                                    </script>
                                </div>
                            </td>
                        </c:if>

                        </c:forEach>
                    </tr>
            </table>
            <div id="targetDiv">

            </div>
        </div> 
    </div>
</body>
</html>

