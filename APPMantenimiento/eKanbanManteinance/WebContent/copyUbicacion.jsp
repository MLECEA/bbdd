<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
	<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.mobile.min.css" />
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/jquery.min.js"></script>
    <script src="resources/js/jquery-ui.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
    <script>
    var formResp = "";
	$(function() {
	    $("#tabs").tabs();
	    function moveItems(origin, dest) {
	        $(origin).find(':selected').appendTo(dest);
	    }      
	    $('#left').click(function () {
	        moveItems('#Lsububicaciones', '#subUbicacionesNoSelec');
	    });
	     
	    $('#right').on('click', function () {
	        moveItems('#subUbicacionesNoSelec', '#Lsububicaciones');
	    });
	  } );
	 function cancelarSubmit(){
		  formResp = "KO";
	  }
	function seleccionarTodos(){
		  $('#LsubUbicacionesNoSelec option').prop('selected', true);
		  $('#Lsububicaciones option').prop('selected', true);
		  var valid=true;
		  var idUbic = $("#idUbic").val().trim();
			if (idUbic.length == 0 ){
				$("#idUbicObli").show();
				valid = false;
			}else{
				$("#idUbicObli").hide;
			}
			if (formResp == "KO") 
				  return true; 
			  else
				  return valid;
	  }
</script>
</head>
<body>
<form action="copyUbicacion" method="post" onsubmit="return seleccionarTodos();">
		<div id="tabs">
			<ul>
				<li><a href="#ubicData">Datos generales</a></li>
				<li><a href="#sububicaciones">Sububicaciones</a></li>
			</ul>
			<div id="ubicData" style="width:100%;height:100%">
				<table style="width:100%; height:100%" >
					<tr>
						<TD>Ubicación ID<span style="color:red"> *</span></TD>
						<TD><input type="text" name="idUbicacion" id="idUbic" value="${Ubicacion.idUbicacion }" >
						</br><span id="idUbicObli" style="display:none; color:red; font-size:11px">Campo obligatorio</span>
						</TD>
					</tr>
					<tr>
						<TD>Descripción</TD>
						<TD><input type="text" name="descripcion" value="${Ubicacion.descripcion }" ></TD>
					</tr>
					<tr>
						<TD>Localización</TD>
						<TD><input name="localizacion" type="text" value="${Ubicacion.localizacion}" ></TD>
					</tr>
					<tr>
						<TD>Descripción general</TD>
						<TD><input name="ubiDescLoc" type="text" value="${Ubicacion.ubi_DesLoc}" ></TD>
					</tr>
					<tr>
						<TD>Activo</TD>
						<TD>
							<c:choose>
								<c:when test="${Ubicacion.activo eq 1}">
									<input name="activo" type="checkbox" checked />
								</c:when>
								<c:otherwise>
									<input name="activo" type="checkbox" />
								</c:otherwise>
							</c:choose>
							
						</TD>
					</tr>
				</table>
			</div>
			<div id="sububicaciones">
				<div style="width:100%; height:100%;display:flex">
					<select multiple="multiple" name="noSelected" id="LsubUbicacionesNoSelec" style="height:300px; margin-right:10px">
						 <c:forEach var="subUbicacion2" items="${subUbicLibres}" varStatus="theCount2">
			               		<option value="${subUbicacion2.id_SubUbicacion}"><c:out value="${subUbicacion2.descripcion}" /></option>
			            </c:forEach>
		            </select>
		            <div style="margin-right: 10px;margin-top: 150px;">
			            <input type="button" id="left" value="&lt;" />
					    <input type="button" id="right" value="&gt;" />
				    </div>
					<select multiple="multiple" name="ubicSububicaciones" id="Lsububicaciones" style="height:300px">
						<c:choose>
							<c:when test="${not empty subUbicAsignadas}">
								<c:forEach var="subUbicacion1" items="${subUbicAsignadas}" varStatus="theCount">
				               		<option value="${subUbicacion1.id_SubUbicacion}"><c:out value="${subUbicacion1.descripcion}" /></option>
				            	</c:forEach>
							</c:when>
							<c:otherwise>
								
							</c:otherwise>
						</c:choose>
<%-- 						 <c:if test="${empty subUbicAsignadas}"> --%>
<%-- 							 <c:forEach var="subUbicacion1" items="${subUbicAsignadas}" varStatus="theCount"> --%>
<%-- 				               		<option value="${subUbicacion1.id_SubUbicacion}"><c:out value="${subUbicacion1.descripcion}" /></option> --%>
<%-- 				            </c:forEach> --%>
<%-- 			            </c:if> --%>
		            </select>
		            <br />
			    </div>	
			</div>
		</div>
		<div style="margin-top:15px">
			<input type="submit" name="formSubmit" value="Guardar" onclick="seleccionarTodos();" style="float:right" />
			<input type="submit" name="formSubmit" value="Cancelar" style="float:left" onclick="cancelarSubmit();"/>
		</div>
		
	</form>
</body>
</html>