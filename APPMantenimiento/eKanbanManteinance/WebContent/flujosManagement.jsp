<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <jsp:include page="/WEB-INF/templates/header.jsp"/>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.common-material.min.css" />
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.min.css" />
    <link rel="stylesheet" href="resources/css/jquery-ui.min.css"></link>
    <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2018.1.221/styles/kendo.material.mobile.min.css" />
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/jquery.min.js"></script>
    <script src="resources/js/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="resources/css/Orkli.css"></link>
    <script src="https://kendo.cdn.telerik.com/2018.1.221/js/kendo.all.min.js"></script>
</head>
<body>
<div style="display:inline-table">
    <div align="center" style="float:left; width:18%">
        <jsp:include page="/WEB-INF/templates/Lateralmenu.jsp"/>
    </div>
    <div align="center" style="width:80%; float:right">
        <div style="margin-bottom: 45px;">
            <div style="float:right">
                <input type="radio" id="chkTodos" name="chkFiltro"  onchange="filtrarFlujosActivos()" checked>Mostrar todos los flujos
                <input type="radio" id="chkActivos" name="chkFiltro"  onchange="filtrarFlujosActivos()">Solo los flujos activos
                <input type="radio" id="chkInactivos" name="chkFiltro" onchange="filtrarFlujosActivos()">Solo los flujos inactivos
            </div>
        </div>
        <table id="flujosGrid" cellpadding="5">
            <tr>
                <th class="k-header k-sorted">Codigo</th>
                <th class="k-header k-sorted">Descripción</th>
                <th class="k-header k-sorted">Usuario</th>
                <th class="k-header k-sorted">Ubicación</th>
                <th class="k-header k-sorted">Fecha Inicio</th>
                <th class="k-header k-sorted">Cancelado</th>
                <th class="k-header k-sorted" style="display:none">Activo</th>

            </tr>
            <c:forEach var="flujo" items="${LFlujos}" varStatus="theCount">
                <c:choose>
                    <c:when test="${flujo.activo eq 1 }">
                        <tr >
                            <td id="divIDNo${theCount.index}">
                                <!-- <a href="javascript:void(0)" onclick="deleteFlujo(this);"><img src="resources/img/delete.png" style="width: 15px; margin-right:10px" /></a>
                                <a href="javascript:void(0)" onclick="copyFlujo(this);"><img src="resources/img/copy.png" style="width: 15px; ; margin-right:10px" /></a> -->
                                <c:out value="${flujo.idMatricula}" />
                            </td>
                            <td><c:out value="${flujo.descripcionFlujo}" /></td>
                            <td><c:out value="${flujo.nombre}${' '}${flujo.apellidos}" /></td>
                            <td><c:out value="${flujo.ubicacion}" /></td>
                            <td><c:out value="${flujo.fechaInicio}" /></td>
                            <td><c:out value="${flujo.cancelado}" /></td>
                            <td style="display:none"><c:out value="${flujo.activo}" /></td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <tr>
                            <td id="divIDNo${theCount.index}">
                                <!-- <a href="javascript:void(0)" onclick="deleteFlujo(this);"><img src="resources/img/delete.png" style="width: 15px; margin-right:10px" /></a>
                                <a href="javascript:void(0)" onclick="copyFlujo(this);"><img src="resources/img/copy.png" style="width: 15px; margin-right:10px" /></a> -->
                                <c:out value="${flujo.idFlujo}" />
                            </td>
                            <td><c:out value="${flujo.descripcionFlujo}" /></td>
                            <td><c:out value="${flujo.nombre}${' '}${flujo.apellidos}" /></td>
                            <td><c:out value="${flujo.ubicacion}" /></td>
                            <td><c:out value="${flujo.cancelado}" /></td>
                            <td><span class="inactivo"><c:out value="${flujo.activo}" /></span></td>
                            <td style="display:none"><c:out value="${flujo.activo}" /></td>
                        </tr>
                    </c:otherwise>
                </c:choose>


            </c:forEach>
        </table>
        <div id="targetDiv">

        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#targetDiv").dialog({  //create dialog, but keep it closed
            autoOpen: false,
            height: 530,
            width: 900,
            modal: true,
            open: function(event, ui) {
                $(".ui-dialog-titlebar-close").hide();
            },
            title: "Información del flujo"
        });

        $("#flujosGrid").kendoGrid({
            schema: {
                model: {
                    id: "flujos",
                    fields: {
                        Codigo: { type: "string" },
                        Descripcion: { type: "string" } ,
                        Usuario: { type: "string" },
                        Ubicacion: { type: "string" },
                        FechaInicio: { type: "string" },
                        Cancelado: { type: "string" },
                        Activo: {type: "number"}
                    }
                }
            },
            columns: [
                { field: "Codigo"},
                { field: "Descripcion" },
                { field: "Usuario" },
                { field: "Ubicacion" },
                { field: "FechaInicio" },
                { field: "Cancelado" },
                { field: "Activo", hidden:true }
            ],
            sortable: true,
            filterable:{
                extra: false,
                mode: "row",
                operators: {
                    string: {
                        contains: "Contains"
                    }
                }
            },
            pageable: {
                pageSize: 20
            },
            selectable: "row",
            dataBound: ondataBound
        });
        $("#flujosGrid").on("dblclick", "tr.k-state-selected", function (e) {
            // insert code here
            var seleccionado = $("#flujosGrid").find('tr.k-state-selected').find("td:first")[0].innerText.trim(); //html();
            //window.location.href = "./editUser?codUser=" + codUser;
            $("#targetDiv").load("./editFlujo?idMatricula=" + seleccionado);
            $("#targetDiv").dialog("open");
        });
    });

    function ondataBound(){
        $(".inactivo").each(function() {
            var trElemn = $(this)[0].parentNode.parentNode;
            trElemn.className += " RowInactivo";
        });
    }

/*    function copyFlujo(event){
        $('#targetDiv').dialog('option','width',900);
        $('#targetDiv').dialog('option','height',530);
        var flujoID = event.parentNode.innerText.toString().trim();
        $("#targetDiv").load("./copyFlujo?flujoID=" + flujoID);
        $("#targetDiv").dialog("open");
    }

    function deleteFlujo(event){
        $('#targetDiv').dialog('option','width',500);
        $('#targetDiv').dialog('option','height',120);
        $('#targetDiv').dialog('option','title','Eliminar flujo seleccionado');
        var flujoID = event.parentNode.innerText.toString().trim();
        $("#targetDiv").load("./deleteFlujo?flujoID=" + flujoID);
        $("#targetDiv").dialog("open");
    }*/
    /*function newUser(){

        $("#targetDiv").load("./newUser");
        $("#targetDiv").dialog("open");
    }*/

    function filtrarFlujosActivos(){
        var grid = $("#flujosGrid");
        if ($("#chkActivos")[0].checked){
            $("#flujosGrid").data("kendoGrid").dataSource.filter({ field: "Activo", operator: "eq", value: 1 });
        }
        if ($("#chkInactivos")[0].checked){
            $("#flujosGrid").data("kendoGrid").dataSource.filter({ field: "Activo", operator: "eq", value: 0 });
        }
        if ($("#chkTodos")[0].checked){
            $("#flujosGrid").data("kendoGrid").dataSource.filter({});
        }

    }
</script>
</body>
</html>
