﻿CREATE FUNCTION [dbo].[alertasEstado_estado00000000]
(@Circuito as nvarchar(50) )
RETURNS int
AS
BEGIN
	DECLARE @T AS INT
	DECLARE @V AS int 
	declare @AE as int
	declare @A as int
	declare @R as int
	DECLARE @SS as int
	DECLARE @Stat as int

	declare @Tipo as int
	
	SET @T=DBO.T(@Circuito)
	SET @R=DBO.R(@Circuito)
	SET @AE=DBO.AE(@Circuito)
	SET @A=DBO.A(@Circuito)
	SET @SS=DBO.SS(@Circuito)
	
	SET @V=@T-@AE-@A-@R+@SS

	set @Stat=1 --verde por defecto
	
	--if ((@R+@A > @T/2)  OR @V=0 )
	if (( @R>0)  OR @V=0 )
		begin
			set @Stat=2 --amarillo
			if @R > 20 --20
				BEGIN
					set @Stat=3 -- ROJO
				END
		end

	RETURN @Stat

END