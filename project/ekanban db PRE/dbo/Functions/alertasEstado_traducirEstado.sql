﻿CREATE FUNCTION alertasEstado_traducirEstado
(@estado as INT )
RETURNS VARCHAR(10)
AS
BEGIN
	DECLARE @texto AS VARCHAR(10)

	if (@estado is not null)
		begin
		if (@estado = 1)
			SET @texto = 'Verde'	
		else if (@estado = 2)
				SET @texto = 'Amarillo'	
			 else if (@estado = 3)
				SET @texto = 'Rojo'	
		end

	RETURN @texto
END