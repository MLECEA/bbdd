﻿
CREATE FUNCTION [dbo].[Estado_pruebas]
(@Circuito as nvarchar(50) )
RETURNS int
AS
BEGIN
	--DECLARE @Stat as int
	DECLARE @estadoCircuito as INT
	DECLARE @estadoActualCircuito as INT
	DECLARE @Tipo as int
	DECLARE @enviado as INT

	--1 proveedor, 3 cliente
	begin
		SELECT @Tipo=tipo_circuito
		FROM dbo.circuito
		WHERE id_circuito = @Circuito
	end
	set @Tipo=isnull(@Tipo, 1) -- si no estuviera definido, lo consideramos como proveedor

	--!!! forzamos a tipocliente !!!!! esto habrá que borrarlo
	set @Tipo=3

	--set @Stat=1 --verde por defecto

	if @tipo=3 --cliente
		begin
			-- obtenemos el estado actual del circuito
			EXEC @estadoCircuito = alertasEstado_calcularEstadoCircuito @Circuito
						
			-- obtenemos el estado anterior del circuito
			EXEC @estadoActualCircuito = alertasEstado_getEstadoCircuito @Circuito
			
			if (@estadoActualCircuito is null or @estadoCircuito <> @estadoActualCircuito)
				begin
					-- ha cambiado de estado
			
					if (@estadoCircuito = 3)
						-- rojo
						begin
							-- ha pasado a estado rojo enviar mail alerta
							--insert into _pruebas_mensajes (mensaje) values ('' + @Circuito + ' es rojo')
							EXEC @enviado = alertasEstado_sendMailAlertaEstadoRojo @Circuito = @Circuito
							--insert into _pruebas_mensajes (mensaje) values ('' + @Circuito + ' mail enviado')
							
						end
					else 
						begin
							if (@estadoCircuito = 2)
								-- amarillo
								begin
									-- ha pasado a estado amarillo enviar mail alerta
									EXEC @enviado = alertasEstado_sendMailAlertaEstadoAmarillo @Circuito = @Circuito
								end
							else 
								begin
									if (@estadoCircuito = 1)
										-- verde
										begin
											-- ha pasado a estado verde enviar mail alerta
											EXEC @enviado = alertasEstado_sendMailAlertaEstadoVerde @Circuito = @Circuito
										end
								end
						end

					-- actualizamos el estado
					EXEC dbo.alertasEstado_setEstadoActualCircuito @Circuito = @Circuito, @estadoCircuito = @estadoCircuito
				end
		end

	RETURN @estadoCircuito

END