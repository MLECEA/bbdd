﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FAX_SitTarjetas]
(@Circuito as nvarchar(50))
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	SELECT DBO.ART(@Circuito) AS ART, DBO.ARTCLI(@Circuito) AS ARTCLI, DBO.T(@Circuito) AS T, DBO.V(@Circuito) AS V, DBO.AE(@Circuito) AS AE, DBO.A(@Circuito) AS A, DBO.R(@Circuito) AS R
)
