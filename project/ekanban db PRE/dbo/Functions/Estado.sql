﻿USE [EKANBAN]
GO
/****** Object:  UserDefinedFunction [dbo].[Estado]    Script Date: 11/13/2018 12:37:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[Estado]
(@Circuito as nvarchar(50) )
RETURNS int
AS
BEGIN
	--DECLARE @Stat as int
	DECLARE @estadoCircuito as INT
	DECLARE @estadoActualCircuito as INT
	DECLARE @Tipo as int
	
	DECLARE @T AS INT
	DECLARE @V AS int 
	DECLARE @AE as int
	DECLARE @A as int
	DECLARE @R as int
	DECLARE @SS as int

	--1 proveedor, 3 cliente
	begin
		SELECT @Tipo=tipo_circuito
		FROM dbo.circuito
		WHERE id_circuito = @Circuito
	end
	set @Tipo=isnull(@Tipo, 1) -- si no estuviera definido, lo consideramos como proveedor

	set @estadoCircuito=1 --verde por defecto
	
	set @Tipo = 3 --borrar

	if @tipo=3 --cliente
		begin
			-- obtenemos el estado actual del circuito
			EXEC @estadoCircuito = alertasEstado_calcularEstadoCircuito @Circuito
		end
	else
		begin
			SET @V=@T-@AE-@A-@R+@SS
			
			--no existe AE por ser proveedor, externos, etc...
			if @V < @T/2
				begin
					set @estadoCircuito=2 --amarillo
					if @A=0 
					BEGIN
						set @estadoCircuito=3 -- ROJO
					END
				end
		end

	RETURN @estadoCircuito

END