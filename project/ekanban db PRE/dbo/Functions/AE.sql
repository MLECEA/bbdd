﻿
CREATE FUNCTION [dbo].[AE]
(@Circuito as nvarchar(50))
RETURNS int
AS
BEGIN
	DECLARE @AE as int
	declare @Tipo as int

	SET @AE=0

	begin
		SELECT        @Tipo=tipo_circuito
		FROM            dbo.circuito
		WHERE        id_circuito = @Circuito
	end

	set @Tipo=isnull(@Tipo,1)

	--EN PROVEEDORES NO GESTIONAMOS LA CTD EN TRANSITO, O CONSUMIDO, O CONFIRMADO O EN ALMACÉN
	if @Tipo=1
	begin
		SET @AE=0
		--SIEMPRE 0
	end

	--TRANSITO EN CLIENTES
	if @Tipo=3
	begin
		SELECT        @AE=  SUM(dbo.VAX_EKAN_ALBA.QTY / dbo.circuito.cantidad_piezas)
		FROM            dbo.circuito INNER JOIN
						dbo.VAX_EKAN_ALBA ON dbo.circuito.cliente_id = dbo.VAX_EKAN_ALBA.ORDERACCOUNT AND dbo.circuito.referencia_id = dbo.VAX_EKAN_ALBA.ITEMID AND 
						(dbo.circuito.ubicacion = SUBSTRING(dbo.VAX_EKAN_ALBA.PURCHASEORDER, 1, 3) OR
						SUBSTRING(dbo.VAX_EKAN_ALBA.PURCHASEORDER, 1, 2) = dbo.circuito.Cir_UbiAnt OR
						 dbo.VAX_EKAN_ALBA.PURCHASEORDER LIKE LTRIM(RTRIM(dbo.circuito.Cir_UbiAnt)) + N'%')
		WHERE        
		  (dbo.VAX_EKAN_ALBA.QTY > 0) AND 
		--(DATEADD(day, dbo.circuito.entrega_dias, DATEADD(hour, dbo.circuito.entrega_horas, dbo.VAX_EKAN_ALBA.CREATEDDATETIME)) > GETDATE())
		(dbo.ufn_ADD_WORKING_DAYS(dbo.VAX_EKAN_ALBA.CREATEDDATETIME, dbo.circuito.entrega_dias, dbo.circuito.entrega_horas) > GETDATE())

		GROUP BY dbo.circuito.id_circuito
		HAVING        (dbo.circuito.id_circuito = @Circuito)

		SET @AE=ISNULL(@AE,0)

	END
	
	RETURN @AE

END

