﻿



CREATE FUNCTION [dbo].[GS1Data]
(@Txt as nvarchar(500)='', @ID as nvarchar(3) )
RETURNS nvarchar(50)
AS
BEGIN
	DECLARE @Dato nvarchar (50)
	declare @position int
	declare @position2 int

	set	@position=CHARINDEX(CHAR(29)+@ID, @txt, 1)+1+LEN(@ID)
	set	@position2=CHARINDEX(CHAR(29), @txt, @position)

	IF @position2<@position  SET @position2=LEN(@TXT)+10 --Para garantizar coger toda la longitud, algunos car no cuentan en len. HE VUELTO A ACTIVARLO, DEBIDO A LA ELIMINACIÓN DEL RS, EOT, . IKER 8/9/2016
	set @Dato=SUBSTRING(@txt, @position, @position2-@position)
	
	IF @position=2 SET @Dato=''
	
	SET @Dato=REPLACE(@Dato,CHAR(29),'') --GS
	SET @Dato=REPLACE(@Dato,CHAR(30),'') --RS
	SET @Dato=REPLACE(@Dato,CHAR(4),'') --EOT

	if @id='Q'
		BEGIN
			--EN CASO DE QUE QUEDE ALGÚN CAMPO DE SEPARACIÓN, LO QUITAMOS
			if RIGHT(@Dato,2)='.0' SET @Dato=substring(@dato, 1, len(@dato)-2)--si utiliza decimales. como xxx.0
			if RIGHT(@Dato,3)='.00' SET @Dato=substring(@dato, 1, len(@dato)-3) --si utiliza decimales. como xxx.00

			SET @Dato=REPLACE(@Dato,'.','')
			SET @Dato=REPLACE(@Dato,',','.')
		END
	RETURN @Dato

END





