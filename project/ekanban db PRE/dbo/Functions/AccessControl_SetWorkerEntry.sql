﻿CREATE FUNCTION [dbo].[AccessControl_SetWorkerEntry]
(@wsURL NVARCHAR (4000), @uidWorker INT, @oidOrder INT)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Unifikas_WSPublic].[AccessControl].[AccessControl_SetWorkerEntry]

