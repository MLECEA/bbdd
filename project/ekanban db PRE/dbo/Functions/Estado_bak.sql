﻿
CREATE FUNCTION [dbo].[Estado]
(@Circuito as nvarchar(50) )
RETURNS int
AS
BEGIN
	DECLARE @estadoCircuito as INT
	
	EXEC @estadoCircuito = alertasEstado_calcularEstadoCircuito @Circuito

	RETURN @estadoCircuito
END




