﻿/*****************************************************************************************
 *
 * alertasEstado_calcularEstadoCircuito
 *
 * Devuelve el estado de un circuito teniendo en cuenta la configuración del cliente (o bien en número o en porcentaje)
 *
 * Posibles valores: 1, 2, 3
 *
 ******************************************************************************************/

CREATE FUNCTION [dbo].[alertasEstado_calcularEstadoCircuito]
(@Circuito as nvarchar(50) )
RETURNS int
AS
BEGIN
	DECLARE @estadoCircuito AS INT
	DECLARE @funcionAlertas AS VARCHAR(255)

	SET @estadoCircuito = 1 --verde por defecto
	
	select @funcionAlertas = funcionAlertasCambioEstado 
	from alertasEstado_funcionEstado
	where clienteId + '||' + STR(tipoCircuito) = 
		(select cliente_id + '||' + STR(tipo_circuito) from circuito where id_circuito = @Circuito)

	-- si no existe configuracion especifica para el cliente, coger la configuración por defecto
	if @@ROWCOUNT = 0 or @funcionAlertas is null or @funcionAlertas = ''
		begin
			SET @funcionAlertas = 'alertasEstado_estadoDefecto'
			SET @estadoCircuito = 99 --verde por defecto
		end

	EXEC @estadoCircuito = dbo.alertasEstado_functionDispatcher @funcion = @funcionAlertas, @parametro = @Circuito
	
	RETURN @estadoCircuito
END