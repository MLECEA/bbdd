﻿CREATE FUNCTION functionDispatcher(@name SYSNAME, @Circuito)
RETURNS VARCHAR(100)
AS
BEGIN
  DECLARE @r VARCHAR(100);
  IF OBJECT_ID(@name, N'FN') IS NULL  --handling non-existing function
    RETURN NULL;
  EXEC @r = @name; 
  RETURN @r;
END;