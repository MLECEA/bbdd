﻿

CREATE FUNCTION [dbo].[V]
(@Circuito as nvarchar(50) )
RETURNS int
AS
BEGIN
	DECLARE @T AS INT
	DECLARE @V AS int 
	declare @AE as int
	declare @A as int
	declare @R as int
	DECLARE @SS as int

	SET @T=DBO.T(@Circuito)
	SET @R=DBO.R(@Circuito)
	SET @AE=DBO.AE(@Circuito)
	SET @A=DBO.A(@Circuito)
	SET @SS=DBO.SS(@Circuito)

	SET @V=@T-@AE-@A-@R


	--todo sobrestock se suma como si fuese verde.
	SET @V=@V+@SS

	IF @V<0 SET @V=0

	RETURN @V

END



