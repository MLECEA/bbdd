﻿/*****************************************************************************************
 *
 * getEstadoCircuito
 *
 * Devuelve el estado de un circuito teniendo en cuenta la configuración del cliente (o bien en número o en porcentaje)
 *
 * Posibles valores: 'A' , 'R', 'V'
 *
 ******************************************************************************************/

CREATE FUNCTION [dbo].[getEstadoCircuito]
(@Circuito as nvarchar(50) )
RETURNS VARCHAR(2)
AS
BEGIN
	DECLARE @T AS INT
	DECLARE @V AS int 
	--declare @AE as int
	DECLARE @A as INT
	DECLARE @R as INT
	--DECLARE @SS as int
	DECLARE @estadoCircuito as VARCHAR(2)
	DECLARE @tipoConfig as VARCHAR(255)
	DECLARE @desdeAmarillo as INT
	DECLARE @desdeRojo as INT

	DECLARE @porcentajeAmarillo as INT
	DECLARE @porcentajeRojo as INT

	SET @T=DBO.T(@Circuito)
	SET @R=DBO.R(@Circuito)
	--SET @AE=DBO.AE(@Circuito)
	SET @A=DBO.A(@Circuito)
	--SET @SS=DBO.SS(@Circuito)
	
	--SET @V=@T-@AE-@A-@R+@SS

	SET @estadoCircuito = 'V' --verde por defecto

	select @tipoConfig = tipoConfig, @desdeAmarillo = rangoAmarillo, @desdeRojo = rangoRojo
	from clienteConfigEstado
	where cliente_id = (select cliente_id from circuito where id_circuito = @Circuito)

	if (@tipoConfig = 'desdeNumero')
		begin
			if (@R >= @desdeRojo)
				begin
					SET @estadoCircuito = 'R'
				end
			else if (@A >= @desdeAmarillo)
				begin
					SET @estadoCircuito = 'A'
				end
		end
	else if (@tipoConfig = 'desdePorcentaje')
		begin
			SET @porcentajeAmarillo = @A / @T * 100
			SET @porcentajeRojo = @R / @T * 100

			if (@porcentajeRojo >= @desdeRojo)
				begin
					SET @estadoCircuito = 'R'
				end
			else if (@porcentajeAmarillo >= @desdeAmarillo)
				begin
					SET @estadoCircuito = 'A'
				end
		end
	
	RETURN @estadoCircuito

END