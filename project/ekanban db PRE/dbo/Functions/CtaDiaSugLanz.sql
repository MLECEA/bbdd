﻿


CREATE FUNCTION [dbo].[CtaDiaSugLanz]
(@Cir as nvarchar(50) )
RETURNS DATETIME
AS
BEGIN


	DECLARE @DiasM int=0
	DECLARE @DiasT int=0
	DECLARE @DiasCons int=0
	DECLARE @FechaPed date=getdate()


	--SET DATEFIRST 1
 
	--COGEMOS LOS DÍAS A AÑADIR A LA ENTREGA, SEGÚN EL DÍA DE HOY
	SELECT     @DiasM = CASE DATEPART(dw, GETDATE()) -1 
	WHEN 1 THEN cir_lunM WHEN 2 THEN cir_marM WHEN 3 THEN cir_mieM WHEN 4 THEN cir_jueM WHEN 5 THEN cir_vieM WHEN 6 THEN cir_sabM WHEN 7 THEN cir_DomM END , 
	@DiasT = CASE DATEPART(dw, GETDATE())-1
	WHEN 1 THEN cir_lunT WHEN 2 THEN cir_marT WHEN 3 THEN cir_mieT WHEN 4 THEN cir_jueT WHEN 5 THEN cir_vieT WHEN 6 THEN cir_SabT WHEN 7 THEN cir_DomT END
	FROM         dbo.circuito
	WHERE     (id_circuito = @Cir)
	
	--CONSIDERAMOS QUE ANTES DE LAS 12 ES MAÑANA Y DESPUÉS DE LAS 12 ES TARDE. 
	if DATEPART(hh, GETDATE())<12 
	begin
		set @DiasCons=isnull(@DiasM,0)
		set @FechaPed = DATEADD(dd, @DiasCons, CONVERT(datetime, CONVERT(int, GETDATE()) - 0))
	end
	else
	begin
		set @DiasCons=isnull(@DiasT, 0)
		set @FechaPed = DATEADD(dd, @DiasCons, CONVERT(datetime, CONVERT(int, GETDATE()) - 1))
	end

	RETURN @FechaPed

END




