﻿

-- =============================================
-- Author:		IKER
-- Create date: 13/5/2014
-- Description:	Procedimiento para conseguir datos, según patrón de lectura del código de barras

-- Este procedimiento se ejecuta siempre que hay una lectura de un codigo de barras en los dispositivos ANDROID (Consumo y Consulta)
-- y cuando hay una lectura del portico. 
-- El objetivo es conseguir la OF y la SERIE de la información de la etiqueta/QR/RFID, etc. 
-- El resultado da, OF, SERIE y GRAB. El campo grab es la que se utiliza para garantizar que no se repite la lectura de la misma etiqueta. 
-- Si el resultado tiene OF no válido (Que no sea null, vacio o 000000) el proceso siguiente, coge el Articulo y cliente desde el resultado del procedimiento. 
-- Si el resultado tiene OF válido, va a VAX_EKAN_OF y coge como resultado el artículo y cliente. 

-- una vez que tiene, ARTICULO, CLIENTE Y USUARIO (el de la aplicación), consulta en la tabla de circuitos y obtiene el código circuito para seguir el proceso. 
-- =============================================
CREATE PROCEDURE [dbo].[PAX_PatronLec_BAK]
(
@Txt varchar(255)='', @Usu AS varchar(255)='', @Ubi AS varchar(255)='', @Pant AS int=0 , @Disp as varchar(255)='', @Ant as varchar(50)='', @Sen as varchar(5)=''
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @TP AS INT=0  --TIPO DE PATRON APLICADO. SI ES 0 EN RESULTADO, NO HA ENTRADO EN NINGUNO DEFINIDO. 
	DECLARE @L AS VARCHAR(25)=''
	DECLARE @L1 AS INT
	DECLARE @L2 AS INT
	DECLARE @S AS VARCHAR(100)=''
	DECLARE @S1 AS INT
	DECLARE @S2 AS INT
	DECLARE @A AS VARCHAR(25)=''
	DECLARE @A1 AS INT
	DECLARE @A2 AS INT
	DECLARE @AR AS VARCHAR(25)=''
	DECLARE @Cli AS VARCHAR(25)=''
	DECLARE @UbiTip AS VARCHAR(25)=''
	DECLARE @V AS VARCHAR(25)=''
	DECLARE @CliOF AS VARCHAR(25)=''
	DECLARE @CliUBI AS VARCHAR(25)=''
	DECLARE @I AS INT
	DECLARE @CIR AS VARCHAR(100)=''
	DECLARE @CTD AS VARCHAR(100)=''
	DECLARE @PED AS VARCHAR(100)=''
	DECLARE @NIVACC AS INT
	DECLARE @ACCION AS VARCHAR(10)=''
	DECLARE @CTDACCCION AS INT
	DECLARE @TipCont AS INT
	DECLARE @position int
	DECLARE @string char(500)
	DECLARE @1P AS VARCHAR(100)=''
	DECLARE @16K AS VARCHAR(100)=''
	DECLARE @2L AS VARCHAR(100)=''
	DECLARE @1J AS VARCHAR(100)=''
	DECLARE @5J AS VARCHAR(100)=''
	DECLARE @6J AS VARCHAR(100)=''
	DECLARE @7Q58 AS VARCHAR(100)=''
	DECLARE @7QGT AS VARCHAR(100)=''
	DECLARE @16D AS VARCHAR(100)=''
	DECLARE @2P AS VARCHAR(100)=''
	DECLARE @ALB AS VARCHAR(100)=''
	DECLARE @ALBLIN AS VARCHAR(100)=''
	DECLARE @UbiCap AS VARCHAR(25)=''
	DECLARE @LotCap AS VARCHAR(50)=''
	DECLARE @Emp AS VARCHAR(3)=''
	DECLARE @URLPlano AS VARCHAR(50)=''
	--SI LA UBICACIÓN ACTUAL, TIENE CHECK DE REGISTRO DE TRAZABILIDAD. 
	DECLARE @RegTraz bit=0
	DECLARE @EMPR NCHAR(3)=''
	declare @cade varchar(200)
	declare @TIPOETIQUETA VARCHAR(50) = NULL;
	DECLARE @TIPO AS VARCHAR(50) = NULL;
	DECLARE @INDICE AS INT = 0;
	DECLARE @QRDATA VARCHAR(255) = NULL;

	SET NOCOUNT ON;
	SET XACT_ABORT ON;  --- iker. 2016-05-09 crear pedidos-en sortu ahal izateko. iker.
	--SET TRANSACTION ISOLATION LEVEL SNAPSHOT
	begin transaction
	--*************************************************************************************************************************************************
	--Si la ubicación tiene predefinida un tipo concreto de etiqueta tratamos esa
	 SELECT @TIPOETIQUETA = TipoEtiqueta FROM ubicacion WHERE id_ubicacion = @UBI;
	 PRINT @TIPOETIQUETA;
	 IF @TIPOETIQUETA <> ''
	 BEGIN
		PRINT 'TIPO ETIQUETA PERSONALIZADA';
		DECLARE TIPO CURSOR FOR select * from dbo.fnSplitString (@TIPOETIQUETA,':');
		OPEN TIPO
		FETCH NEXT FROM TIPO INTO @TIPO
		WHILE @@fetch_status = 0
		BEGIN
			IF @INDICE = 0 SET @TP = @TIPO;
			else set @qrData = @tipo;
			SET @INDICE = @INDICE + 1;
			FETCH NEXT FROM TIPO INTO @TIPO
		END
		print @tp;
		DECLARE @VARIABLE VARCHAR(255);
		DECLARE @VALOR VARCHAR(255);
		declare @var varchar(255);
		declare @posVariable int = -1;
		DECLARE DATA CURSOR FOR select * from dbo.fnSplitString (@QRDATA,'|');
		OPEN DATA
		FETCH NEXT FROM DATA INTO @var
		WHILE @@fetch_status = 0
		BEGIN
			select @posVariable=  CHARINDEX('=', @var); 
			set @VARIABLE = SUBSTRING(@var, 0, @posVariable);
			set @VALOR = SUBSTRING(@var, @posVariable + 1, len(@var));
			print @variable +  @valor;
			IF @VARIABLE='CTD'  BEGIN PRINT @VALOR;SET @CTD = DBO.GS1Data(@TXT, @VALOR); END
			IF @VARIABLE='LOTE' SET @L = DBO.GS1Data(@TXT, @VALOR);
			IF @VARIABLE='REF' SET @A = DBO.GS1Data(@TXT, @VALOR);
			IF @VARIABLE='PROV' SET @V = DBO.GS1Data(@TXT, @VALOR);
			IF @VARIABLE='BULTO' SET @S = DBO.GS1Data(@TXT, @VALOR);
			FETCH NEXT FROM DATA INTO @VAR
		END
		print 'lote';
		print @L;
		PRINT 'ERREFERENTZIA=';
		PRINT @A;
		deallocate  tipo;
		deallocate  data;
		
		set @CIR=''

		--SELECT @S=DBO.GS1Data(@txt,'S')
		set @L=isnull(@L,'')
		IF @L='' SET @L='0' 
		PRINT @CTD;
		SET @CTD= CONVERT(NUMERIC(9,1),@CTD)
		PRINT 'CANTIDAD OK';
		--revisamos a qué empresa corresponde la ubicación
		SELECT       @RegTraz=Ubi_RegTrazAX, @EMPR=Ubi_Dataareaid
		FROM            dbo.ubicacion
		WHERE        (id_ubicacion = @Ubi)

		--PARA QUE EL DISPOSITIVO MOVIL NO DE ERROR, ASIGNAMOS CIRCUITOS, LOTES ETC.
		--Cogemos el cod cliente de la ubiación
		SET @A=ISNULL(@A,'')
		SET @CIR=''
		--En caso de que ese artículo no tenga circuito, cogemos el de trazabilidad, para que podamos dar consumos. 
		PRINT 'REFERENCIA_ID=';
		PRINT @A;
		SELECT        @CIR=id_circuito, @Cli=cliente_id
		FROM            dbo.circuito
		WHERE        (referencia_id = @A) AND (ubicacion =  @Ubi) AND activo=1

		set @ant=@V

		SET @CIR=ISNULL(@CIR,'')

		--SI NO TIENE CIRCUITO EL ARTÍCULO, COGEMOS EL DE TRAZABILIDAD, SI EXISTE. SINO ERROR.
		IF @CIR='' or @A in ('20100034','20100060','L-11158','L-11180','L-11170','20100551','20100017','20100013') 
		--hasta el 2/9/2017 20100463 ere sartuta. hori kendu. iker.

		BEGIN
			SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
			WHERE        (ubicacion = @Ubi) AND (activo = 1) and referencia_id='TRAZABILITATEA'
		END


		SET @S=RTRIM(@S) + '_' + @L
		SET @L='000000'
		
		if (not exists(select * from tarjetaMetadata where id_lectura=@L + '_' + @S and sentido=@Sen))
			insert into tarjetaMetadata (id_lectura, sentido, txtLectura, tipo,fechaPrimeraLectura ,fechaUltimaLectura, PatronLectura) values (@L + '_' + @S, @Sen, @Txt, @tp, GETDATE(), GETDATE(), @TIPOETIQUETA);
		else update tarjetaMetadata set fechaUltimaLectura = GETDATE(), PatronLectura=@TIPOETIQUETA where id_lectura = @L + '_' + @S and sentido = @Sen
		GOTO REGISTROLECTURAS;
	 END
	
	
	--*************************************************************************************************************************************************
	-- Cuando no se inserta ningún dato manualmente en la referencia
	if @txt='P'
	BEGIN
		SET @TP=1
		SET @L='000000'
		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación y que sean activos
		SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi AND (activo = 1) ORDER BY num_kanban
	END

	--*************************************************************************************************************************************************
	--Si es un artículo el que se lee. Automaticamente le añade una P al inicio
	if @txt<>'P' AND left(@txt,1)='P' AND left(@txt,2)<>'P*' AND left(@txt,4)<>'PROV' and  (CHARINDEX('_', @txt, 1) = 0)
	begin
		--P08296603
		SET @TP=2
		SET @L ='000000'
		SET @Cli ='00000000'

		SET @AR =SUBSTRING(@txt, 2, LEN(@TXT))

		SELECT      @Cli= localizacion
		FROM        dbo.ubicacion
		WHERE       id_ubicacion = @Ubi

		--en vez de esta consulta, mejor la de lista de precios, o ref cruzadas, para garantizar que es una del cliente.
		SELECT   @A = ITEMID
		FROM     PREAXSQL.SQLAXBAK.dbo.INVENTTABLE AS INVENTTABLE
		WHERE    (DATAAREAID = N'ORK') AND (ITEMID = @AR)

		IF @A = ''
		BEGIN
			SELECT     @A= CUSTVENDEXTERNALITEM.ITEMID
			FROM       PREAXSQL.SQLAXBAK.dbo.CUSTVENDEXTERNALITEM AS CUSTVENDEXTERNALITEM INNER JOIN
					   PREAXSQL.SQLAXBAK.dbo.INVENTTABLE AS INVENTTABLE ON CUSTVENDEXTERNALITEM.DATAAREAID = INVENTTABLE.DATAAREAID AND CUSTVENDEXTERNALITEM.ITEMID = INVENTTABLE.ITEMID
			WHERE        (CUSTVENDEXTERNALITEM.DATAAREAID = N'ork') AND (CUSTVENDEXTERNALITEM.EXTERNALITEMID = @AR) AND (CUSTVENDEXTERNALITEM.CUSTVENDRELATION = @Cli)
			ORDER BY CUSTVENDEXTERNALITEM.EXTERNALITEMID
		END
	end 



	--*************************************************************************************************************************************************
	-- cuando se lee una consulta con * para realizar operaciones. 
	--  *A2-609:T12
	--  *A2-609:N5
	if left(@txt,2)='P*' and left(@txt,3)<>'P*U'
	begin
		SET @TP=3
		IF CHARINDEX(':', @TXT,1) > 0
			BEGIN
				SET @L='000000'	
				SET @A1 = 3
				SET @A2 =CHARINDEX(':', @TXT,1)
				IF @A2>0 
				begin
					SET @A =SUBSTRING(@txt, @A1, @A2-@A1)
					sET @ACCION =SUBSTRING(@txt, @A2+1, LEN(@TXT)-(@A2))
					set @CTDACCCION= SUBSTRING(@ACCION, 2, len(@ACCION)-1)
					set @ACCION=SUBSTRING(@ACCION, 1, 1)
				end
		
				--CIRCUITO? SI EXISTE
				SELECT       @CIR= id_circuito, @Cli=cliente_id
				FROM            dbo.circuito
				WHERE        ubicacion = @Ubi AND referencia_id = @A
		
				--si no existe, cogemos el cliente de la ubicación
				IF @CIR=''
					BEGIN
						SELECT        @Cli=localizacion
						FROM            dbo.ubicacion
						WHERE        id_ubicacion = @Ubi
					END
		
				--Cogemos el rol del usuario
				SELECT        @NIVACC= ptr_rol
				FROM            dbo.sga_usuario
				WHERE        USERNAME = @Usu
		
				-- Con permiso, para crear y no hay circuito
				IF @NIVACC=7 AND @ACCION='N' AND @CIR=''
				BEGIN
		
					-- CREAR CIRCUITO
					INSERT INTO [dbo].[circuito]
					   ([cliente_id],[id_circuito],[referencia_id],[activo],[cantidad_piezas],[cliente_nombre],[descripcion],[entrega_dias]
					   ,[entrega_horas],[mail],[num_kanban],[ref_cliente],[reserva_auto],[ubicacion],[tipo_circuito],[tipo_contenedor])
					VALUES
					   (@Cli, @Ubi+@A, @A, 1, 1, '', '', 0,0,'', @CTDACCCION, '', 1, @Ubi, 1,1)

					   SET @S='NuevoCir'
				END
	
				-- Con permiso, para modif y  hay circuito
				IF @NIVACC=7 AND @ACCION='T' AND @CIR<>''
				BEGIN
					SET @S='ActNumKan'
					UPDATE [dbo].[circuito]
					SET [num_kanban] = @CTDACCCION
					WHERE [id_circuito]= @CIR
				END

				IF @NIVACC=7 AND @ACCION='Q' AND @CIR<>''
				BEGIN
					SET @S='ActQPzas'
					UPDATE [dbo].[circuito]
					SET [cantidad_piezas] = @CTDACCCION
					WHERE [id_circuito]= @CIR
				END
			END
			else
			begin
				DECLARE	@return_value int
				if left(@txt,4)='P*er' 
				BEGIN
					EXECUTE	@return_value = EKANBAN.[dbo].[PEnvUsers]
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END

				if left(@txt,4)='P*rf' 
				BEGIN
					EXECUTE	@return_value = EKANBAN.[dbo].[PEnvSitArcRFID]
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END

				if left(@txt,4)='P*eg' 
				BEGIN
					EXECUTE	@return_value = EKANBAN.[dbo].[PEnvSitPanel]
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END
				
				if left(@txt,4)='P*01' 
				BEGIN
					UPDATE [dbo].[sga_parametros]
					SET [descripcion] = SUBSTRING(@txt,5, LEN(@TXT))
					WHERE [nombre]='RFIDTEST0001'
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END
				if left(@txt,4)='P*02' 
				BEGIN
					UPDATE [dbo].[sga_parametros]
					SET [descripcion] = SUBSTRING(@txt,5, LEN(@TXT))
					WHERE [nombre]='RFIDTEST0002'
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END

				if left(@txt,4)='P*03' 
				BEGIN
					UPDATE [dbo].[sga_parametros]
					SET [descripcion] = SUBSTRING(@txt,5, LEN(@TXT))
					WHERE [nombre]='RFIDTEST0003'
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END
				
			end

	end


	----*************************************************************************************************************************************************
	----PARA PROBAR EL EKANBAN, BORRANDO LAS ENTRADAS AL CIRCUITO
	----/**
	--if left(@txt,2)='P*' and (left(@txt,5)='P*U00') --OR left(@txt,5)='P*U07'
	--begin
	--	begin
	--		DELETE FROM [dbo].[tarjeta]
	--		WHERE        (ubicacion = SUBSTRING(@TXT,3,3))

	--	end
	--	SET @L='000000'	
	--	--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
	--	SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
	--end
	----**/
	----*************************************************************************************************************************************************



	-- si es tipo de SMSTO
	if left(@txt,5)='SMSTO'
	begin
		SET @TP=4
		--LOS MENSAJES NORMALES:     (QUE NO SEAN DE FORMACIÓN NI DE TIPO ORK. QUE NO EXISTA UORK O FORMACIÓN)
		IF CHARINDEX('UORK_', @TXT,1)=0 and CHARINDEX('FORMACION', @TXT,1)=0 
		BEGIN
			--SMSTO:+34696469617:H123456_179993_20900-31
			SET @L1 =CHARINDEX('H', @TXT,15)+1
			SET @L2 =CHARINDEX('_', @TXT,@L1)
			SET @L =SUBSTRING(@txt, @L1, @L2-@L1)

			SET @S1 =CHARINDEX('_', @TXT,@L2)+1
			SET @S2 =CHARINDEX('_', @TXT,@S1)
			SET @S =SUBSTRING(@txt, @S1, @S2-@S1)

			SET @A1 =CHARINDEX('_', @TXT,@S2)+1
			SET @A2 =LEN(@TXT)
			SET @A =SUBSTRING(@txt, @A1, @A2-@A1+1)

			IF @L ='000000' SET @Cli ='00000000'

 
			-- para el error de reetiquetados de COPRECI
			--SI MIRAMOS 

			
			SELECT       @CliOF=CUSTACCOUNT
			FROM            dbo.VAX_EKAN_OF
			WHERE        (PRODID = @L)


			SELECT      @CliUBI= localizacion
			FROM        dbo.ubicacion
			WHERE       id_ubicacion = @Ubi

			IF @CliUBI<>@CliOF 
				BEGIN
					SET @Cli =@CliUBI
					SET @L ='000000'
				END

		END
		
		--IF CHARINDEX('FORMACION', @TXT,1)>0 
		--BEGIN
		----SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
		----SMSTO:+34696469617:U0PFORMACION4K00
		--	SET @A ='20900-1'
		--	SET @L ='000000'
		--	SET @Cli ='00000000'
		--	SET @S = CONVERT(varchar(100), GETDATE(), 121) 
		--END

			
		IF CHARINDEX('UORK_', @TXT,1)>0 
		BEGIN
			--Etiquetas internas de SKINTER
			--SMSTO:+34629552542:UORK_A2-609_S69901
			SET @A1 =CHARINDEX('UORK_', @TXT,15)+5
			SET @A2 =CHARINDEX('_', @TXT,@A1)
			SET @A =SUBSTRING(@txt, @A1, @A2-@A1)

			SET @S1 =CHARINDEX('_S', @TXT,@A2)+2
			SET @S2 =LEN(@TXT)+1
			SET @S =SUBSTRING(@txt, @S1, @S2-@S1)
			SET @L ='000000'
			SET @Cli ='00000000'
		END
	
	END

--*************************************************************************************************************************************************

-- si es tipo de http://www.ekanban.es/qr/157238252841
	-- si es tipo de http		
	if left(@txt,5)='http:' --and @Ubi<>'U41'
	begin
		SET @TP=5
		SET @Emp='ORK'
		--http://www.orkli.com/qr/123456088082
		SET @L1 =CHARINDEX('QR/', @TXT,15)+3
		SET @L =SUBSTRING(@txt, @L1, 6)
		SET @S =SUBSTRING(@txt, @L1+6, 6)
		SET @Emp=SUBSTRING(@txt, @L1+12, 3)-- no siempre lo tiene. 

		--Si no dispone de dato en empresa, cogemos ORK por defecto.
		IF @Emp<>'BRA' SET @Emp='ORK'
		
		--SET @Emp='BRA'


		SELECT       @CliOF=CUSTACCOUNT, @A=ITEMID
		FROM            dbo.VAX_EKAN_OF
		WHERE        (PRODID = @L) AND (DATAAREAID = @Emp)

		SELECT      @CliUBI= localizacion
		FROM        dbo.ubicacion
		WHERE       id_ubicacion = @Ubi

		IF (@CliUBI<>@CliOF) or (@Emp='BRA')   -- EN CASO DE QUE SEA LA EMPRESA BRASIL, COMO EL APP, NO REVISA LAS OF, SEGÚN EMPRESA, LE PASAMOS EL CLIENTE CORRESPONDIENTE Y LOTE 000000, PARA QUE NO COJA EN BASE 
			BEGIN
				SET @Cli =@CliUBI
				SET @S= @L + @S + @Emp  -- @Emp jarria el 25/5/2017, para evitar de que consumos de defendi, diga que existen por haber consumido apis
				SET @L ='000000'

				
				SET @AR=''
				--PARA RECUPERAR LOS MAL ETIQUETADSO   http://www.ekanban.es/ekanban/qr/1287251230/1851-1200
				--1230/1851-1200
				SELECT        @AR=referencia_id
				FROM            dbo.circuito
				WHERE        (referencia_id = @A) AND (ubicacion = @Ubi)

				SET @AR=ISNULL(@AR,'')

				IF @AR=''
				BEGIN
					set @A=SUBSTRING (@A, LEN(@A)-3, 4)
					IF SUBSTRING (@A, LEN(@A)-3, 4)='1000' SET @A='12301851K1000'
					IF SUBSTRING (@A, LEN(@A)-3, 4)='1001' SET @A='12301851K1000' --berria 2/8/2016, IES
					IF SUBSTRING (@A, LEN(@A)-3, 4)='1200' SET @A='12301851K1200'
					IF SUBSTRING (@A, LEN(@A)-3, 4)='1201' SET @A='12301851K1200'--berria 2/8/2016, IES
				END

			END
	END


	
-- si es tipo de http://www.ekanban.es/qr/157238252841   para mueller
	-- si es tipo de http		, long 37
	if left(@txt,5)='http:'  and @Ubi='U41' and CHARINDEX('BRA', @TXT,1)=0 
	begin
		SET @TP=55
		--http://www.orkli.com/qr/123456088082
		--http://www.ekanban.es/ekanban/qr/196020BR123018511201 
		SET @L1 =CHARINDEX('QR/', @TXT,15)+3
		SET @S =SUBSTRING(@txt, @L1, 6)
		SET @A =SUBSTRING(@txt, @L1+6, 50)
		set @L='000000'


			--SELECT       @CliOF=CUSTACCOUNT, @A=ITEMID
			--FROM            dbo.VAX_EKAN_OF
			--WHERE        (PRODID = @L)


			SET @AR=''
			--PARA RECUPERAR LOS MAL ETIQUETADSO   http://www.ekanban.es/ekanban/qr/1287251230/1851-1200
			--1230/1851-1200
			SELECT        @AR=referencia_id
			FROM            dbo.circuito
			WHERE        (referencia_id = @A) AND (ubicacion = @Ubi)

			SET @AR=ISNULL(@AR,'')

			IF @AR=''
			BEGIN
				set @A=SUBSTRING (@A, LEN(@A)-3, 4)
				IF SUBSTRING (@A, LEN(@A)-3, 4)='1000' SET @A='12301851K1000'
				IF SUBSTRING (@A, LEN(@A)-3, 4)='1001' SET @A='12301851K1000' --berria 2/8/2016, IES
				IF SUBSTRING (@A, LEN(@A)-3, 4)='1200' SET @A='12301851K1200'
				IF SUBSTRING (@A, LEN(@A)-3, 4)='1201' SET @A='12301851K1200'--berria 2/8/2016, IES

			END




			SELECT      @CliUBI= localizacion
			FROM        dbo.ubicacion
			WHERE       id_ubicacion = @Ubi

			IF @CliUBI<>@CliOF 
				BEGIN
					SET @Cli =@CliUBI
					SET @L ='000000'
				END
	end 


-------------------------------------------------------------------------------------------
	
	--borrado el 30/11/2015. quitar el 31/12/2015 del todo
	-------------------------------------------------------------------------------------------
	
	--ETIQUETA "ESTAMPA" CON SOLO PROVEEDOR Y ARTÍCULO (opcional la cantidad)
		--PROV_000388_20100374_151.1

		--SI ES EL TIPO VIEJO DE ETIQUETAS DE ESTAMPA. A BORRAR ESTA PARTE.
	if (SUBSTRING(@txt, 1, 4)='PROV' AND CHARINDEX('PROVV', @txt, 1) = 0)				--PROV_V
	begin
		SET @TP=6
		IF CHARINDEX('_', @txt, 13)>0
			BEGIN
			SET @A=SUBSTRING(@txt, 13, CHARINDEX('_', @txt, 13)-13)
			SET @CTD=SUBSTRING(@txt, CHARINDEX('_', @txt, 13)+1, LEN(@TXT)-CHARINDEX('_', @txt, 13))
			END
		ELSE
			BEGIN
			SET @A=SUBSTRING(@txt, 13, LEN(@TXT))
			SET @CTD='0'
			END

		SET @L ='000000'
		SET @Cli ='00000000'
		
		--SET @S = right(@A,3) + '_' + @CTD + '_' + CONVERT(varchar(100), GETDATE(), 103) +' ' + left(CONVERT(varchar(100), GETDATE(), 114) ,5) 
		SET @S = SUBSTRING(@txt, 6, LEN(@TXT))+ CONVERT(varchar(100), GETDATE(), 103) +' ' + left(CONVERT(varchar(100), GETDATE(), 114) ,5) 
		
	end 
	--	A BORRAR HASTA ESTA PARTE

	
--*************************************************************************************************************************************************

	-- ETIQUETAS DE TRAZABILIDAD INTERNA, SIN CONSIDERAR EL FORMATO PROV_ QUE ES EL TIPO 6 E INICIO CON INTE O PROV Y SEPARADOR ;
	if ((SUBSTRING(@txt, 1, 4)='PROV' or SUBSTRING(@txt, 1, 4)='INTE') AND 
		SUBSTRING(@txt, 1, 5)<>'PROV_' AND CHARINDEX(';', @txt, 1) > 0 )
	
		begin
		SET @TP=7
		
		--PROV;V000388;P20100301;Q1010;TT032580351;S14288      se utiliza en estampa
		--INTE;SORKA77013  se utiliza para etiquetado interior decoletajes, lapeados, etc. 

		DECLARE @N AS INT=0 
		DECLARE @N1 AS INT=0
		DECLARE @C AS char(40)=''
		DECLARE @ID AS Char(30)=''
		DECLARE @P AS Char(30)=''
		DECLARE @VCAP AS Char(30)=''

		SET @CTD='0'
		SET @L ='000000'
		SET @Cli ='00000000'
		
		SET @N=CHARINDEX(';', @txt, 1)+1
		
		SET @TXT=REPLACE(@TXT,CHAR(4),'') --EOT


		SET @EMPR='ORK'
		
		--QUE EMPRESA ES ESTA UBICACIÓN?
		SELECT    @EMPR=Ubi_Dataareaid 
		FROM     dbo.ubicacion
		WHERE   (id_ubicacion = @Ubi)

		--PROV;V000388;P20100301;Q1010;TT032580351;S14288   
		WHILE @N<LEN(@TXT)
			BEGIN
				SET @ID=''
				SET @N1=CHARINDEX(';', @txt, @N)
				IF @N>0 AND @N1=0 SET @N1=LEN(@TXT)+1
				SET @C =SUBSTRING(@TXT, @N, @N1-@N)

				SET @ID=SUBSTRING(@C, 1, 1)
			
				if @ID='P' set @A=SUBSTRING(@C, 2, len(@c)) 
				if @ID='Q' set @CTD=SUBSTRING(@C, 2, len(@c)) 
				if @ID='H' OR @ID='T' set @L=SUBSTRING(@C, 2, len(@c)) 
				if @ID='S' set @S=SUBSTRING(@C, 2, len(@c))						-- PARA LAS ETIQUETAS INTERNAS, COGEMOS LA SERIE
				if @ID='V' set @V=SUBSTRING(@C, 2, len(@c)) 

				SET @N=@N1+1
			END

		--INTE;SORKA77013  ETIQUETADO INTERIOR DE DECOLETAJE, LAPEADO
		IF SUBSTRING(@txt, 1, 4)='INTE' 
			BEGIN
				--COGEMOS LOS DATOS REGISTRADOS AL IMPRIMIR LA ETIQUETA
				SELECT TOP 1 @A=Referencia, @L=NLote, @CTD=Cantidad, @V=Proveedor         --CAMPOS DISPONIBLES Referencia, NSerie, NLote, Cantidad, Proveedor, Fecha, Tipo
				from [SRVSPS2007\SQL2005x32].[ORKLB000].dbo.VEtiquetas 
				where nserie = @S
				ORDER BY FECHA DESC
			END
		
		
		set @LotCap=@S
		-- SI ES DE CAPTOR
		IF SUBSTRING(@txt, 1, 9)='INTE;SCAP' 
		BEGIN
				SELECT   @Usu=SUBSTRING(descripcion,1,10)--limitamos a 10 la descripcion, bestela error
				FROM     dbo.ubicacion
				WHERE   (id_ubicacion = @Ubi)

				--EL USUARIO DEBE SER EL NOMBRE DEL RECURSO DEL AX
				-- 9/3/2017 no estamos insertando THI1 como recurso, pq el apartado anterior estaba anulado. lo activo otra vez. iker.  INTE;SCAP115049  
				--iñigo y blanca han venido. hay foto etiquta en mov ies



				set @S=SUBSTRING(@S, 4, LEN(@S))
				
				SELECT        @L=Lote, @A=Referencia, @CTD=Cantidad--, Compañia
				FROM            SRVCAPTORDB.Captor3.dbo.VCAPTOR_Lotes AS A
				WHERE        ( A.Compañia = '01' AND A.Lote=@S)

				set @LotCap=@S
				set @S=@ubi

				SET @V='000000' -- CONSIDERAMOS QUE SI ES INTERNA Y DE CAPTOR EL PROVEEDOR ES ORKLI
		END

		

		set @A=isnull(@A,'')
		SET @CTD= CONVERT(NUMERIC(9,1),@CTD)
		set @Cli='00000000'
		SET @V=ISNULL(@V,'000000')
		IF @V='' SET @V='000000'

		EXEC PREAXSQL.ORKLB000.[dbo].[PAUX_EtiTrazabValores]  --este procemiento se ha creado por no poder grabar directamente en AX, por tema permisos. iker. 15/4/2016
				@empresa = @EMPR,
				@linea = @Usu, -- @Ubi, 
				@P = @A,
				@Q = @CTD ,
				@V = @V,
				@serie = @S,
				@H = @L
		
		
			SET @VCAP='PROV:' + @V -- + ';TRAT:16' + ';POS:8'    --CUIDADO EN APLICAR ESTO, YA QUE LUEGO NO ENCUENTRA EL CIRCUITO DE ESTE PROVEEDOR. CAMBIAR NOMBRE DE VARIALBE.


		EXEC	@return_value = PREAXSQL.[ORKLB000].[dbo].[Captor_StartConsumption]
			@Empresa = N'01',
			@Maquina = @Usu,
			@NLote =@LotCap,
			@Referencia = @A,
			@Cantidad = @CTD,
			@DatosAdicionales = N''		--DEBERÍA DE SER EL SIGUIENTE PARA AÑADIR EL PROVEEDOR. DE MOMENTO NADA. @DatosAdicionales = @VCAP

		
		
		--MIRAMOS SI LA REF Y PROVEEDOR DISPONE DE CIRCUITO O NO 
		SET @CIR=''
		
		SELECT @CIR= id_circuito
		FROM  dbo.circuito
		WHERE (referencia_id = @A) AND (Proveedor_id = @V) AND (ubicacion = @Ubi) AND    (activo = 1)

		SET @CIR=isnull(@CIR,'')	--SI NO EXISTE MANTENEMOS EN ''

		IF @CIR=''
		BEGIN
			SET @A=''  --ANULAMOS LA REFERENCIA PARA EVITAR QUE SE PUEDA DAR CONSUMO DE UNA REFERENCIA QUE SEA DE OTRO PROVEEDOR QUE TENGA KANBAN
			SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
			WHERE        (ubicacion = @Ubi) AND (activo = 1) and referencia_id='TRAZABILITATEA'
			set @A=isnull(@A,'')
		END


		SET @S=@L + '/' + @S +@CIR
		SET @L ='000000'

	end

	
--*************************************************************************************************************************************************

	if left(@txt,5)='[)>{R'
	begin
		SET @TP=8
		SET @txt='[)>{RS}06{GS}2L1{GS}V001700{GS}1PXXX{GS}16K004330{GS}Q1{GS}PF-4002-4{GS}1JUNXXXXXXXXXXXX{GS}7Q1058{GS}7Q16GT{GS}T147/99{GS}6DD10/9/99{GS}S2{GS}2P4{RS}{EOT}'



		SET @L ='000000'
		SET @Cli ='00000000'
		SET @S = CONVERT(varchar(100), GETDATE(), 121) 
		SET @A='Prueba GTL'
	end


--*************************************************************************************************************************************************


	--ETIQUETA "MALA" CON SOLO ARTÍCULO DE SKINTER
	--A2-609
	if SUBSTRING(@txt, 1, 1)='A' AND CHARINDEX('-', @txt, 1) =3
	begin
		SET @TP=9
		SET @L ='000000'
		SET @Cli ='00000000'
		SET @S = CONVERT(varchar(100), GETDATE(), 121) 
		SET @A=@Txt
	end 

	
--*************************************************************************************************************************************************


	--ETIQUETA EAN 13
	--8477637477
	if LEN(@TXT)=13 AND (SUBSTRING(@txt, 1, 2)='84' OR SUBSTRING(@txt, 1, 2)='44')
	begin
		SET @TP=10
		SET @L ='000000'
		SET @Cli ='00000000'
		SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
		SET @A=@Txt
	end 

	
--*************************************************************************************************************************************************

	--LECTURAS RFID DE LOS PORTICOS
	--114208092289
	if LEN(@TXT)=12 AND left(@txt,1)<>'P' AND LEFT(@TXT,2)<>'P*' AND LEFT(@TXT,3)<>'CAE' AND LEFT(@TXT,2)<>'AL' AND LEFT(@TXT,4)<>'RFID'
	begin
		SET @TP=11

		SET @L =SUBSTRING(@txt, 1, 6)
		SET @S =SUBSTRING(@txt, 7, 6)


		SELECT        @A= ITEMID, @CLI=CUSTACCOUNT
		FROM            dbo.VAX_EKAN_OF
		WHERE        (PRODID = @L)
	
			
		SELECT       @CIR= id_circuito,  @TipCont = tipo_contenedor,  @CLI=cliente_id
		FROM            dbo.circuito
		WHERE        (referencia_id = @A) AND (ubicacion = @Ubi) --AND (cliente_id = @CLI)   --al haber cambiado al hijo de SABAF, 01, 


		set @TipCont=isnull(@TipCont,0)
		--QUE OCURRE SI EL CLI ES EL HIJO, Y ES A NIVEL DE PALET?, QUE AL NO EXISTIR EL CIRCUITO DE ESE CLIENTE, NO GENERAR CONSUMO
		--XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

		-- EN CASO DE QUE SEA A NIVEL DE PALET
		-- DESCARTAMOS NUM SERIE Y LE PASAMOS 000000 
		IF @TipCont=2 AND @L<>'000000'
		BEGIN 
			IF @Ubi='UWOR'
				BEGIN
				--si es en la ubicación BOSCH, LE AÑADIMOS EL NÚMERO DE PALET AL SERIE
			
				set @S=@L + @S --SUBSTRING(@S,1,2) + '0000'--MACHACAMOS EL NÚMERO DE SERIE CON EL LOTE
				set @L='000000' --PARA QUE COJA LOS ARTÍCULOS QUE SE INDICAN EN ESTE RESULTADO

				END
			ELSE
				BEGIN 
				--LECTURAS DE SABAF
			
				set @S=@L --MACHACAMOS EL NÚMERO DE SERIE CON EL LOTE
				set @L='000000' --PARA QUE COJA LOS ARTÍCULOS QUE SE INDICAN EN ESTE RESULTADO


				END
		
		END
		ELSE
		BEGIN
		IF @TipCont=1 
			begin

				set @S=@L + @S--MACHACAMOS EL NÚMERO DE SERIE CON EL LOTE
					set @L='000000' --PARA QUE COJA LOS ARTÍCULOS QUE SE INDICAN EN ESTE RESULTADO

			end


		END
				
			--*****************************
			--PARA COGER SÓLO LAS ANTENAS QUE QUEREMOS
			DECLARE @ActAnt0 bit
			DECLARE @ActAnt1 bit

			SELECT        @ActAnt0=dbo.dispositivo.Dis_ActAnt0, @ActAnt1=dbo.dispositivo.Dis_ActAnt1
			FROM            dbo.dispositivo INNER JOIN
			dbo.dispositivo_ubicacion ON dbo.dispositivo.id_dispositivo = dbo.dispositivo_ubicacion.id_dispositivo
			WHERE        (dbo.dispositivo_ubicacion.id_ubicacion = @Ubi)

			if (@Ant='Ant0' and @ActAnt0=0) or (@Ant='Ant1' and @ActAnt1=0)--SI ES LA ANTENA X Y NO HAY QUE CONSIDERARLO, BORRA EL ART PARA NO CONSIDERARLO
			begin
				set @A='' --lo borramos para que no habiendo lote y sin referencia no considere nada
				set @S='000000'
				set @L='000000'
			end
			--*****************************
			
			if @Ubi='BIDI' set @S=@S + 'DI'

			if @Ubi='BIDI' AND @Sen='S'
			BEGIN
			
				DELETE FROM [dbo].[tarjeta]
				WHERE (id_lectura = @L + '_' + @S) AND (ubicacion = 'BIDI')	AND (sentido = 'E')	
			END
			
			if @Ubi='BIDI' AND @Sen='E'
			BEGIN
			
				DELETE FROM [dbo].[tarjeta]
				WHERE (id_lectura = @L + '_' + @S) AND (ubicacion = 'BIDI')	AND (sentido = 'S')	
			END

			

	end

	
	--LECTURAS RFID DE LOS PORTICOS CON CÓDIGO HEXADECIMAL
	--114208092289
	if (LEN(@TXT)=22 OR LEN(@TXT)=24 ) AND left(@txt,1)<>'P' AND LEFT(@TXT,2)<>'P*' AND LEFT(@TXT,3)<>'CAE' AND LEFT(@TXT,2)<>'AL' AND LEFT(@TXT,4)<>'RFID' AND LEFT(@TXT,4)<>'PROV'
	begin
		SET @TP=110



		declare @sql nvarchar(MAX), @ch varchar(200)
		--select @v = '313931323434323637393338'
		select @sql = 'SELECT @ch = convert(varchar, 0x' + @TXT + ')'
		EXEC sp_executesql @sql, N'@ch varchar(30) OUTPUT', @ch OUTPUT
		--SELECT @ch AS ConvertedToASCII
		SET @txt=@ch
		
		SET @L =SUBSTRING(@txt, 1, 6)
		SET @S =SUBSTRING(@txt, 7, 6)

		SELECT        @A= ITEMID, @CLI=CUSTACCOUNT
		FROM            dbo.VAX_EKAN_OF
		WHERE        (PRODID = @L)
	
			
		SELECT       @CIR= id_circuito,  @TipCont = tipo_contenedor,  @CLI=cliente_id
		FROM            dbo.circuito
		WHERE        (referencia_id = @A) AND (ubicacion = @Ubi) --AND (cliente_id = @CLI)   --al haber cambiado al hijo de SABAF, 01, 



		--QUE OCURRE SI EL CLI ES EL HIJO, Y ES A NIVEL DE PALET?, QUE AL NO EXISTIR EL CIRCUITO DE ESE CLIENTE, NO GENERAR CONSUMO
		--XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

		-- EN CASO DE QUE SEA A NIVEL DE PALET
		-- DESCARTAMOS NUM SERIE Y LE PASAMOS 000000 
		IF @TipCont=2 AND @L<>'000000'
		BEGIN 

			--SET @S ='000000'

			set @S=@L
			--al haber cambiado en sabaf a hijos
			set @L='000000'
			


		END
		IF @TipCont<>2 AND @L<>'000000'
		BEGIN 

			--SET @S ='000000'

			--set @S=@L
			--al haber cambiado en sabaf a hijos
			set @L='000000'
			


		END

					
			if @Ubi='BIDI' set @S=@S + 'DI'

			if @Ubi='BIDI' AND @Sen='S'
			BEGIN
			
				DELETE FROM [dbo].[tarjeta]
				WHERE (id_lectura = @L + '_' + @S) AND (ubicacion = 'BIDI')	AND (sentido = 'E')	
			END
			
			if @Ubi='BIDI' AND @Sen='E'
			BEGIN
			
				DELETE FROM [dbo].[tarjeta]
				WHERE (id_lectura = @L + '_' + @S) AND (ubicacion = 'BIDI')	AND (sentido = 'S')	
			END


	end

	
--*************************************************************************************************************************************************
	
	--control de stocks DISPONIBILIDAD
	if LEFT(@TXT,3)='CAE' --AND LEN(@TXT)=12 left(@txt,1)<>'P' AND LEFT(@TXT,2)<>'P*' AND 
	BEGIN
		SET @TP=12
		begin 
			set @A=substring(@TXT,1,6)
			
			
			SELECT      @UbiTip=Ubi_Modo   --EVEN  DISP
			FROM        dbo.ubicacion
			WHERE       id_ubicacion = @Ubi


			SET @L='000000'
			SET @Cli ='00000000'
			SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
			SET @S = @Ubi + '_' + @TXT 


			IF @Sen='E' AND @UbiTip='DISP'
			BEGIN
				DELETE FROM [dbo].[tarjeta]
				WHERE
				id_lectura=@L + '_' + @S AND SENTIDO='S'
			END





		end
	END

	
--*************************************************************************************************************************************************

	--LECTURA DE ALBARANES (CONSIGNA)
	--AL00109472_1_134340    
	if  LEFT(@TXT,2)='AL'
	begin
			DECLARE @CLI1 AS VARCHAR(25)=''
			

			SET @TP=13
			
			SET @ALB = SUBSTRING(@txt, 1, CHARINDEX('_', @txt, 1)-1)
			SET @ALBLIN = SUBSTRING(@txt,  CHARINDEX('_', @txt, 1)+1,1)
			SET @L = SUBSTRING(@txt,  CHARINDEX('_', @txt,  CHARINDEX('_', @txt, 1)+1)+1, len(@txt))
			
			--000500 ES UN LOTE FICTICIO DEL AX
			SET @L=ISNULL(@L,'000500')
			IF @L='' SET @L='000500'


			SET @S = @txt 

			--Cogemos el artículo según el lote de fabricación. 
			SELECT        @A= ITEMID, @CLI=CUSTACCOUNT
			FROM            dbo.VAX_EKAN_OF
			WHERE        (PRODID = @L)


			--CANTIDADES SEGÚN EL ALBARAN??????????????????????????????????????????????????????????


			--Cogemos el cliente, según el artículo y ubicación donde estamos leyendo.
			SELECT       @CIR= id_circuito,  @TipCont = tipo_contenedor, @CLI1=cliente_id 
			FROM            dbo.circuito
			WHERE        (referencia_id = @A) AND (ubicacion = @Ubi) AND   (activo = 1)

			IF @CLI<>@CLI1
			begin
				set @CLI=@CLI1
				SET @L='000000'
			end
			



	end

	--Si es sólo lote de la etiqueta, cogemos el lote y apartir de este dato, consultará en vista de OFs
	if left(@txt,1)='H' AND LEN(@TXT)=7
	begin
		--H113688
		SET @TP=15
		SET @L =SUBSTRING(@txt, 2, 6)
		SET @S ='000000'
	end 
	
--*************************************************************************************************************************************************

	IF  LEFT(@TXT,4)='RFID'
	BEGIN
		SET @TP=16
		SET @L ='000000'--SUBSTRING(@txt, 1, 8)
		SET @S =SUBSTRING(@txt, 1, 8)
		set @cade = @TXT + ' en Ubi: ' + @Ubi + ' ' --    + CONVERT(CHAR(3),@NMinLecPor)  + '. '
		declare @cade1 as varchar(50)

		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
		SELECT @A=REFERENCIA_ID, @Cli=cliente_id FROM dbo.circuito WHERE id_circuito = @TXT
		SET @L='000000'
		SET @S = CONVERT(varchar(100), GETDATE(), 121) 



		
			--*****************************
			--PARA COGER SÓLO LAS ANTENAS QUE QUEREMOS
			--DECLARE @ActAnt0 bit
			--DECLARE @ActAnt1 bit

			SELECT        @ActAnt0=dbo.dispositivo.Dis_ActAnt0, @ActAnt1=dbo.dispositivo.Dis_ActAnt1
			FROM            dbo.dispositivo INNER JOIN
			dbo.dispositivo_ubicacion ON dbo.dispositivo.id_dispositivo = dbo.dispositivo_ubicacion.id_dispositivo
			WHERE        (dbo.dispositivo_ubicacion.id_ubicacion = @Ubi)

			if (@Ant='Ant0' and @ActAnt0=0) or (@Ant='Ant1' and @ActAnt1=0)--SI ES LA ANTENA X Y NO HAY QUE CONSIDERARLO, BORRA EL ART PARA NO CONSIDERARLO
			begin
				set @A='' --lo borramos para que no habiendo lote y sin referencia no considere nada
			end
			--*****************************
			




		SELECT        @cade1=mail
		FROM            dbo.circuito
		WHERE        (id_circuito = @TXT)


		set @cade1=isnull(@cade1,'')

		--SELECT @cade1= [descripcion] + '; iker@orkli.es'
		--FROM [dbo].[sga_parametros]
		--where [nombre]=@TXT
		IF @cade1<>''
		BEGIN
			EXEC	[dbo].[PEnvCorreo]
			@Des = @cade1,
			@Tem = N'Lectura de Etiqueta TEST RFID',
			@Cue =@cade
		END

	END

	
--*************************************************************************************************************************************************

	--ELABORACIÓN, SEGUIMINETO TRAZABILIDAD THIELENHAUS....
	
	IF  LEFT(@TXT,2)='SO' AND @TP=0 --COINCIDE CON AL OPCIÓN Y TODAVÍA NO HA ENTRADO EN NINGUNA OPCIÓN ANTERIOR. 
	BEGIN
		SET @TP=17

			SET @S=SUBSTRING(@TXT, 2, LEN(@TXT)+1)
		EXEC	PREAXSQL.ORKLB000.[dbo].[PAUX_EtiTrazab]
		@serie =@S,--'ORKA50136',--
		@linea =@Ubi


		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
		SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
		WHERE        (ubicacion = @Ubi) AND (activo = 1)
		ORDER BY num_kanban


		SET @S=@L + '/' + @S
		SET @L ='000000'
	END


	--V001857PE-20262Q4093H126511K154572   la etiqueta de vilardell
	--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

	--PTE


	--ETIQUETA GS1, SEGÚN NUEVA NORMA. ENERO 2016
	--[)>06V000782PA5-437Q450H2/5357S
	
	if left(@txt,3)='[)>'
		BEGIN
		SET @TP=18
			print @txt;
			SET @L=''
			SET @A=''
			set @CIR=''

			SELECT @A=DBO.GS1Data(@txt,'P')
			SELECT @CTD=DBO.GS1Data(@txt,'Q')
			SELECT @L=DBO.GS1Data(@txt,'H')
			IF @L='' SELECT @L=DBO.GS1Data(@txt,'T')
			SELECT @S=DBO.GS1Data(@txt,'S')
			SELECT @V=DBO.GS1Data(@txt,'V')

			--SELECT @16K=DBO.GS1Data(@txt,'16K')
			--SELECT @2L=DBO.GS1Data(@txt,'2L')
			--SELECT @1J=DBO.GS1Data(@txt,'1J')
			--SELECT @5J=DBO.GS1Data(@txt,'5J')
			--SELECT @6J=DBO.GS1Data(@txt,'6J')
			--SELECT @7Q58=DBO.GS1Data(@txt,'7Q')
			--SELECT @16D=DBO.GS1Data(@txt,'16D')
			--SELECT @2P=DBO.GS1Data(@txt,'2P')
			print @L;
			set @L=isnull(@L,'')
			IF @L='' SET @L='0' 
			


			--SET @CTD=REPLACE(@CTD,'.','') gs-n jarriak
			--SET @CTD=REPLACE(@CTD,',','.')gs-n jarriak
			SET @CTD= CONVERT(NUMERIC(9,1),@CTD)

			--revisamos a qué empresa corresponde la ubicación
			SELECT       @RegTraz=Ubi_RegTrazAX, @EMPR=Ubi_Dataareaid
			FROM            dbo.ubicacion
			WHERE        (id_ubicacion = @Ubi)
			
			--if @RegTraz=1
			--begin

			--	--REGISTRAMOS DATOS EN EL AX A TRAVÉS DEL PROCEDMINETO DEL SERVIDOR DEL AX.
			--	EXEC PREAXSQL.ORKLB000.[dbo].[PAUX_EtiTrazabValores]  --este procemiento se ha creado por no poder grabar directamente en AX, por tema permisos. iker. 15/4/2016
			--		@empresa = @EMPR,
			--		@linea = @Usu, -- @Ubi,
			--		@P = @A,
			--		@Q = @CTD ,
			--		@V = @V,
			--		@serie = @S,
			--		@H = @L
		


			--	----REGISTRAMOS LOS DATOS EN CAPTOR
			--	--como da error si el lote no existe...


			--	SET @V='PROV:' + @V
			--	set @LotCap=@L + '_' + @S

			--	EXEC	@return_value =PREAXSQL.[ORKLB000].[dbo].[Captor_StartConsumption]
			--			@Empresa = N'01',
			--			@Maquina = @Usu,
			--			@NLote =@LotCap,
			--			@Referencia = @A,
			--			@Cantidad =@CTD,
			--			@DatosAdicionales = @V

			--END

			--PARA QUE EL DISPOSITIVO MOVIL NO DE ERROR, ASIGNAMOS CIRCUITOS, LOTES ETC.
			--Cogemos el cod cliente de la ubiación
			SET @A=ISNULL(@A,'')
			SET @CIR=''
			--En caso de que ese artículo no tenga circuito, cogemos el de trazabilidad, para que podamos dar consumos. 


			if  @ubi='U45' OR @ubi='U49' OR @ubi='U51' OR @ubi='U52' OR @ubi='U53' 

				BEGIN 
				SELECT        @CIR=id_circuito, @Cli=cliente_id
				FROM            dbo.circuito
				WHERE        (referencia_id = @A) AND (ubicacion =  @Ubi)  AND activo=1 and Proveedor_id=@V
								
				END
			ELSE
				BEGIN
				
				SELECT        @CIR=id_circuito, @Cli=cliente_id
				FROM            dbo.circuito
				WHERE        (referencia_id = @A) AND (ubicacion =  @Ubi) AND activo=1
				END
			--PRINT 'CLIENTE = ' + @CLI;
			--PRINT 'CIRCUITO =' + @CIR;

			set @ant=@V




			SET @CIR=ISNULL(@CIR,'')

			--SI NO TIENE CIRCUITO EL ARTÍCULO, COGEMOS EL DE TRAZABILIDAD, SI EXISTE. SINO ERROR.
			IF @CIR='' or @A in ('20100034','20100060','L-11158','L-11180','L-11170','20100551','20100017','20100013') 
			--hasta el 2/9/2017 20100463 ere sartuta. hori kendu. iker.

			BEGIN
				SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
				WHERE        (ubicacion = @Ubi) AND (activo = 1) and referencia_id='TRAZABILITATEA'
			END


			SET @S=RTRIM(@S) + '_' + @L
			SET @L='000000'

		END
		---------------*******************************************************************

	IF  LEFT(@TXT,3)='UTI' AND @TP=0 and (@Ubi='UUTI' or @Ubi='UUTIDEK') --COINCIDE CON AL OPCIÓN Y TODAVÍA NO HA ENTRADO EN NINGUNA OPCIÓN ANTERIOR. 
	--UTI_P1210-1115-1-10_Q4_V000535
	BEGIN
		SET @TP=19
		
		DECLARE @Usuario nvarchar(50),
		@Referencia nvarchar(20),
		@ReferenciaDescripcion nvarchar(60),
		@Proveedor nvarchar(20),
		@Cantidad int,
		@Ubicacion nvarchar(600),
		@FechaSolicitud nvarchar(10),
		@FechaPrevista nvarchar(10),
		@Precio decimal(14,6),
		@Url nvarchar(1),
		@Resultado int
		
		DECLARE @SelectedValue int=0
		DECLARE @SelectedValueTXT VARCHAR(50)



		SET @A = SUBSTRING(@txt, CHARINDEX('_P', @txt, 1)+2,CHARINDEX('_Q', @txt, 1)-CHARINDEX('_P', @txt, 1)-2)
		SET @CTD = SUBSTRING(@txt, CHARINDEX('_Q', @txt, 1)+2,CHARINDEX('_V', @txt, 1)-CHARINDEX('_Q', @txt, 1)-2)
		SET @Cli = SUBSTRING(@txt, CHARINDEX('_V', @txt, 1)+2,CHARINDEX('_U', @txt, 1)-CHARINDEX('_V', @txt, 1)-2)
		SET @Ubicacion = SUBSTRING(@txt, CHARINDEX('_U', @txt, 1)+2,LEN(@TXT))

		SET @Cli =isnull(@Cli,'')
		SET @Ubicacion =isnull(@Ubicacion,'')

		SET @Cli = SUBSTRIng(@Cli ,1,6)


		set @URLPlano='1'--'http://srvsps2007/Procesos/UtilesPDF/' + @A + '.pdf'

		SET XACT_ABORT ON  
	   
		SELECT @Usuario=[Usu_Dominio]
		FROM [dbo].[sga_usuario]
		where [username]=@Usu

		SET @Usuario =isnull(@Usuario,'orkliord\gm')
		
		DECLARE @UbicacionDes nchar(100)
		
		SELECT @UbicacionDes=[descripcion]
		FROM [EKANBAN].[dbo].[ubicacion]
		WHERE   [id_ubicacion]=@Ubi
		
		SET @UbicacionDes =isnull(@UbicacionDes,'')


		SET @Referencia = @A  --N'1331-0111-0-11'
		SET @ReferenciaDescripcion = ''
		SET @Proveedor = @Cli
		SET @Cantidad = @CTD
		SET @Ubicacion = 'Sekzioa: ' + rtrim(LTRIM( @UbicacionDes)) + ' (' + @Ubicacion + ') Erreferentziak LASER bidez markatu.' 
		SET @FechaSolicitud = null --para que coja el de hoy
		SET @FechaPrevista = null --si es nulo, añade 21 días. 
		SET @Precio = 1 --no se utiliza para nada
		SET @Url = 1 --1 edo 0





		SELECT        @Referencia= ITEMID
		FROM             [PREaxsql].[SQLAXBAK].dbo.INVENTTABLE
		WHERE        (DATAAREAID = N'ork') AND (ITEMID = @A)

		set @Referencia=isnull(@Referencia,'') 

		if @Referencia=''
		begin
			SELECT     @Referencia=   ITEMID
			FROM             [PREaxsql].[SQLAXBAK].dbo.CUSTVENDEXTERNALITEM
			WHERE        (DATAAREAID = N'ORK') AND (EXTERNALITEMID = @A) AND (CUSTVENDRELATION = @Proveedor)
		end

		set @Referencia=isnull(@Referencia,'') 


		


		--EXEC [PREaxsql].[ORKLB000].[dbo].[TSolicitudesCompra_WS_SEL]
		--	'orkliord\aauzmendi',
		--	'1331-0111-0-11',
		--	'',
		--	'000535 MEKALAN',
		--	1,
		--	'UUTI',
		--	null,
		--	null,
		--	1,
		--	1,
		--   @Resultado OUTPUT

		EXEC [PREaxsql].[ORKLB000].[dbo].[TSolicitudesCompra_WS_SEL]
			@Usuario,
			@Referencia,
			@ReferenciaDescripcion,
			@Proveedor,
			@Cantidad,
			@Ubicacion,
			@FechaSolicitud,
			@FechaPrevista,
			@Precio,
			@Url,
		   @Resultado OUTPUT

		 SET @SelectedValueTXT=CASE @Resultado 
					WHEN 0 THEN 'Error dentro del servicio. Tendréis que comunicármelo para que revise que a poder pasar.'
					WHEN 1 THEN 'El pedido se ha generado. Otra cosa que se haya quedado parado, se haya un producido un problema al volcar al AX o haya todo correctamente. Estas situaciones ya se revisaran dentro de la aplicación.'
					WHEN 2 THEN 'Fecha solicitud incorrecta.  La fecha tiene un formato incorrecto o es anterior al día de hoy.'
					WHEN 3 THEN 'Fecha prevista incorrecta.  La fecha tiene un formato incorrecto'
					WHEN 4 THEN 'Fecha Prevista menor que la Fecha Solicitud.'
					WHEN 5 THEN 'Usuario incorrecto. El usuario que se pasa (usuario de red) es incorrecto o no tiene permisos para Compras.'
					WHEN 6 THEN 'Empresa incorrecta. La empresa del usuario no es correcta (No debería pasar que se valida por si acaso).'
					WHEN 7 THEN 'Sección incorrecta. Se valida que la sección del usuario es correcta (No debería pasar que se valida por si acaso).'
					WHEN 8 THEN 'Almacén incorrecto. Se valida que el almacén que se obtiene en base a los datos de la sección del usuario es correcto en base a la empresa en que estamos tratando (No debería pasar que se valida por si acaso).'
					WHEN 9 THEN 'Referencia incorrecta. El código de la referencia no existe en la empresa que estamos tratando.'
					WHEN 10 THEN 'Proveedor incorrecto. El código de proveedor introducido no existe para la empresa que estamos tratando.'
					WHEN 11 THEN 'Precio no encontrado. No hay en el AX un precio definido para ese artículo y proveedor.'
				END
		


		--REGISTRAMOS EL PEDIDO EN CREAR PEDIDOS, PERO YA TRATADO CON ESTADO 99. PARA SEGUIMIENTO
		INSERT INTO [PREaxsql].[ORKLB000].[dbo].[TAUX_CREAR_PEDIDO]
		(Status, Journalname, Name, CUSTACCOUNT, PURCHORDERFORMNUM, ITEMID, SALESQTY,  SHIPPINGDATEREQUESTED, uservolc, fechaVolc, Reservaauto, type, SalesType, InventLocationId, FacturarAut, PedMarco, NumFactCli, AlbaranarAut, RegistrarPedCompra, DocumentoAdjunto,  Observaciones)
		VALUES
		('99',    @Referencia,        'UTILES', substring(@Cli,1,6),  @Usu, @A,   @CTD,      GETDATE()+7,           @Usu,   GETDATE(),   '0',    'PRO', '3',                       '', 0, '','', 0,0,@SelectedValue,  @Ubicacion + @SelectedValueTXT)
		
		IF @Resultado=1--1
		BEGIN 
		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
			SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
			WHERE        (ubicacion = @Ubi) AND (activo = 1)
			ORDER BY num_kanban
		END

		SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
		SET @L ='000000'

	END

	---------------*******************************************************************

	IF   @Ubi='URFIdD' --COINCIDE CON AL OPCIÓN Y TODAVÍA NO HA ENTRADO EN NINGUNA OPCIÓN ANTERIOR. 
	--pruebas de EMBEBLUE
	BEGIN
		SET @TP=20

		--SET @A = SUBSTRING(@txt, CHARINDEX('_P', @txt, 1)+2,CHARINDEX('_Q', @txt, 1)-CHARINDEX('_P', @txt, 1)-2)
		--SET @CTD = SUBSTRING(@txt, CHARINDEX('_Q', @txt, 1)+2,CHARINDEX('_V', @txt, 1)-CHARINDEX('_Q', @txt, 1)-2)
		--SET @Cli = SUBSTRING(@txt, CHARINDEX('_V', @txt, 1)+2,LEN(@TXT))
			
		
		SET @cade=@Usu + ': ' + @Txt
		--SELECT @cade1= [descripcion] + '; iker@orkli.es'
		--FROM [dbo].[sga_parametros]
		--where [nombre]=@TXT
		--IF @cade1<>''
		--BEGIN
		--	EXEC	[dbo].[PEnvCorreo]
		--	@Des = 'test_910@embeblue.com; auxiliar@orkli.es; iker@orkli.es',
		--	@Tem = N'Lectura de Etiqueta RFID EMBEBLUE',
		--	@Cue =@cade
		--END

		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
		SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
		WHERE        (ubicacion = @Ubi) AND (activo = 1)
		ORDER BY num_kanban


		SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
		SET @L ='000000'

	END
	
		
--*************************************************************************************************************************************************


	if @txt like '%@%' 
	begin
		SET @TP=14
		DECLARE @Notif bit=0

		SELECT       @Notif= Ubi_NotEtiVacias
		FROM            dbo.ubicacion
		WHERE        (id_ubicacion = @Ubi)



		set @cade = 'Ubi: ' + @Ubi + ', Texto: ' + @txt +' ' --    + CONVERT(CHAR(3),@NMinLecPor)  + '. '

		if @Notif=1 --@Ubi<>'UCAE'
			begin
			EXEC	[dbo].[PEnvCorreo]
					@Des = N'alert@ekanban.es;',
					@Tem = N'Lectura de Etiquetas Vacías por RFID',
					@Cue = @cade
			end
	end



	
--*************************************************************************************************************************************************
REGISTROLECTURAS:

print 'tregistroLecturas';

INSERT INTO [dbo].[TRegistroLecturas] 
           ([Reg_Fec]
           ,[Reg_Dis]
           ,[Reg_Usu]
           ,[Reg_Ubi]
           ,[Reg_Pan]
           ,[Reg_Txt]
           ,[Reg_Cir]
           ,[Reg_Cli]
           ,[Reg_Art]
           ,[Reg_Lot]
           ,[Reg_Ser]
		   ,[Reg_Ant]
		   ,[Reg_Sen]
		   ,[Reg_Tip]
		   ,[Reg_Ctd]) 
     VALUES
           (getdate()
           ,@Disp
           ,@Usu
		   ,@Ubi
           ,@Pant
           ,@Txt
           ,@CIR
           ,@Cli
           ,@A
           ,@L
           ,@s
		   ,@Ant
		   ,@Sen
		   ,@TP
		   ,@CTD)


	
	--select @L1,  @L2, @L,  @S1,  @S2, @S, @A1,  @A2, @A
	select @L AS LOT,  @S AS SER, @A AS ART, 0 as CTD, '000000' AS PRO, @Cli as CLI,  @L + '_' + @S AS GRAB, @CTD AS CTDLEIDA, @PED AS IDPED

	commit;
	print getDate();
	SET XACT_ABORT OFF;  --- iker. 2016-05-09 crear pedidos-en sortu ahal izateko. iker.
		



END



