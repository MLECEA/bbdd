﻿/*******************************************************************************************
 *
 * sendMailAlertaEstadoRojo
 *
 * Envía un mail de alerta indicando que el estado del circuito a pasado a rojo
 *
 * @Des es el destinatario/s , si son más de uno deberá ir separado por ";" (TO)
 * @Cc son las direcciones de correo a las que se envía copia, si son varios separados por ";" (CC)
 * @Cco son las direcciones de correo a las que se envía copia oculta, si son varias separadas por ";" (CCO)
 * @Tem es el título o asunto del correo electrónico
 * @Cue es el contenido del mensaje
 *
 ********************************************************************************************/
CREATE PROCEDURE [dbo].[alertasEstado_sendMailAlertaEstado]
	( @Circuito as nvarchar(50),
	 @estadoAnteriorCircuito as INT,
	 @estadoNuevoCircuito as INT,
	 @idReferencia as VARCHAR(255),
	 @ubicacion as VARCHAR(255),
	 @descripcion as VARCHAR(255),
	 @numeroTarjetas as INT,
	 @transitoActivado as BIT,
	 @CtdVerdes as INT, 
	 @CtdAmarillas as INT, 
	 @CtdRojas as INT, 
	 @CtdAmarillasVerdes as INT,
	 @CtdSobreStock as INT)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		DECLARE @subject as VARCHAR(255) = null
		DECLARE @to as VARCHAR(1000)
		DECLARE @cc as VARCHAR(1000) = null
		DECLARE @body as VARCHAR(8000)
		DECLARE @mensaje as VARCHAR(100) = ''
		DECLARE @estadoNuevo as VARCHAR(10)
		DECLARE @estadoAnterior as VARCHAR(10) = null
		
		if @estadoNuevoCircuito is not null
			begin
				print 'tenemos estado nuevo'
				if @estadoNuevoCircuito = 3
					begin
						--si el estado es rojo
						select @subject = mailRojoAsunto, @to = mailRojoPara, @cc = mailRojoCc from alertasEstado_configuracionMail
						where cliente_id = (select cliente_id from circuito where id_circuito = @Circuito)
						print 'titulo 3 ' + @subject
					end
				else if @estadoNuevoCircuito = 2
						begin
							--si el estado es amarillo
							select @subject = mailAmarilloAsunto, @to = mailAmarilloPara, @cc = mailAmarilloCc from alertasEstado_configuracionMail
							where cliente_id = (select cliente_id from circuito where id_circuito = @Circuito)
							print 'titulo 2 ' + @subject	
						end
					else if @estadoNuevoCircuito = 1
						begin
							--si el estado es verde
							select @subject = mailVerdeAsunto, @to = mailVerdePara, @cc = mailVerdeCc from alertasEstado_configuracionMail
							where cliente_id = (select cliente_id from circuito where id_circuito = @Circuito)
							print 'titulo 1 ' + @subject
						end
			end
		else
			begin				
				SET @mensaje = 'No se conoce estado del circuito ' + @Circuito
				print 'alertasEstado_sendMailAlertaEstado - ' + @mensaje
				RAISERROR(@mensaje, 19, 1) WITH LOG
			end
			
			print 'titulo !!! ' + @subject

		if (@to is not null or @cc is not null)
			begin
				print 'alertasEstado_sendMailAlertaEstado - Se va a enviar un mail '
				select @estadoNuevo = dbo.alertasEstado_traducirEstado (@estadoNuevoCircuito)
				select @estadoAnterior = dbo.alertasEstado_traducirEstado (@estadoAnteriorCircuito)
				
				if (@subject is null or @subject = '')
					begin
						if @estadoNuevoCircuito = 3
							begin
								-- si es rojo ponermos alerta
								SET @subject = 'Alerta notificación de cambio a estado ' + @estadoNuevo
							end
						else
							begin
								-- sino ponemos aviso
								SET @subject = 'Aviso notificación de cambio a estado ' + @estadoNuevo
							end
					end
				
				SET @body = '<html>' +
					'<div><table style="width: 100%;"><tbody>' +
					'<tr>' +
					'<td>' +
					'<p>eKanban notifica que la siguiente referencia ha pasado del estado ' + @estadoAnterior +' a ' + @estadoNuevo + 
					'</p></td>' +
					'<td align="right" width="10%">' +
					'<img src="https://image.freepik.com/iconos-gratis/jpg-variante-de-formato-de-archivo_318-45505.jpg" width="100%" />' +
					--'<img src="http://predmz:9090/images/ekanban/LogoEkanban1.jpg" width="100%" />' +
					'</p></td>' +
					'</tr></tbody></table><div>' +
					'<p>Referencia: ' + CONVERT(varchar, @idReferencia) + '</p>' +
					'<p>Ubicaci&oacute;n: ' + @ubicacion + '</p>' + 
					'<p>Descripci&oacute;n: ' + @descripcion + '</p>' + 
					'<p>Cantidad de tarjetas eKanban: ' + CONVERT(varchar, @numeroTarjetas) + '</p>' +
					'<p>&nbsp;</p>' +
					'<h2><span style="color: #ff0000;">Estado actual</span></h2>' +
					'<p>Cantidad de rojas: ' + CONVERT(varchar, @CtdRojas) + '</p>' +
					'<p>Cantidad de amarillas: ' + CONVERT(varchar, @CtdAmarillas) + '</p>'
					
				if (@transitoActivado = 1)
					BEGIN
						SET @body = @body + '<p>Cantidad de amarillas-verdes: ' + CONVERT(varchar, @CtdAmarillasVerdes) + '</p>'
					END

				SET @body = @body +	'<p>Cantidad de verdes: ' + CONVERT(varchar, @CtdVerdes) + '</p>' +
					'<p>Cantidad de sobre stock: ' + CONVERT(varchar, @CtdSobreStock) + '</p>'+
					'<html>'
				
				print N'Body del mail ' + @body

				SET @to = RTRIM(@to)

				EXEC dbo.common_sendMail  @subject = @subject, @to = @to, @cc = @cc, @body = @body,@format='html'	  
			end		 
		else
			begin				
				SET @mensaje = 'No se ha especificado ni to ni cc, o no existe configuracion de envío de la alerta para el circuito ' + @Circuito
				print 'alertasEstado_sendMailAlertaEstado - ' + @mensaje
				RAISERROR(@mensaje, 19, 1) WITH LOG
			end
	END TRY
	BEGIN CATCH
		EXEC dbo.common_sendMailError 
	END CATCH 
END