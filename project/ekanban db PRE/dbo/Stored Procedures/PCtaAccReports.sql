﻿




-- =============================================
-- Author:		IKER
-- Create date: 8-5-2014
-- Description:	Procedimiento para obtener numero de tarjetas
-- =============================================
CREATE PROCEDURE [dbo].[PCtaAccReports]
(
@Usuario AS varchar(255)='', @Cliente varchar(255)='',@Ubicacion varchar(255)='', @Circuito varchar(255)=''
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	-- SET ANSI_PADDING OFF

	SET NOCOUNT ON;
	
		SELECT     dbo.usuario_AccReports.id_accrep, dbo.usuario_AccReports.prioridad, dbo.TAccionesReports.AccRep_Tip, RTRIM(dbo.TAccionesReports.AccRep_Des) AS AccRep_Des, dbo.TAccionesReports.AccRep_NVar, 
		isnull(dbo.TAccionesReports.AccRep_V1Txt,'') as AccRep_V1Txt,
		dbo.ARValDef(dbo.usuario_AccReports.id_accrep,'1', @Usuario, @Cliente, @Ubicacion, @Circuito) AS AccRep_V1Def,
		isnull(dbo.TAccionesReports.AccRep_V2Txt,'') as AccRep_V2Txt,
    	dbo.ARValDef(dbo.usuario_AccReports.id_accrep,'2', @Usuario, @Cliente, @Ubicacion, @Circuito) AS AccRep_V2Def,
		isnull(dbo.TAccionesReports.AccRep_V3Txt,'') as AccRep_V3Txt, 
		dbo.ARValDef(dbo.usuario_AccReports.id_accrep,'3', @Usuario, @Cliente, @Ubicacion, @Circuito) AS AccRep_V3Def,
        dbo.TAccionesReports.AccRep_CCir, dbo.TAccionesReports.AccRep_QRVar
		FROM            dbo.TAccionesReports INNER JOIN
		dbo.usuario_AccReports ON dbo.TAccionesReports.AccRep_Idd = dbo.usuario_AccReports.id_accrep INNER JOIN
		dbo.sga_usuario ON dbo.usuario_AccReports.id_usuario = dbo.sga_usuario.cod_usuario
		WHERE        (dbo.sga_usuario.username = @Usuario) AND (dbo.TAccionesReports.AccRep_Act = 1)
		ORDER BY dbo.usuario_AccReports.prioridad


	EXEC	[dbo].[PRegistro]
		@Reg_Usu = @Usuario,
		@Reg_Ubi = '',
		@Reg_opt = N'[PCtaAccReports]',
		@Reg_Art = '',
		@Reg_cli = '',
		@Reg_Txt = @Usuario
		
END







