﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PEnvSitArcRFID] 
	-- Add the parameters for the stored procedure here
	--@Des as varchar(40)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	 DECLARE @tableHTML  NVARCHAR(MAX) ;

	SET @tableHTML =
    N'<H1>eKanban RFID Arkuak</H1>' +
    N'<table border="1">' +
    N'<tr><th>Arkua</th>' +
	N'<th>Izena</th>' +
    N'<th>Egoera</th>' +
	N'<th>Azken Konexioa</th>' +
	N'<th>IP</th>' +
    N'<th>Bertxioa</th>' +
	N'<th>Azken piztuera</th></tr>' +
    CAST ( ( SELECT "td/@align" =  'center', td = [id_dispositivo],   ''
      ,td = [descripcion],   ''
      ,"td/@align" =  'center',  "td/@bgcolor" =  case when [Dis_EnError]=0 then 'green' ELSE 'red' END, td = case when [Dis_EnError]=0 then 'OK' ELSE 'ERR' END,  ''
      ,td = convert(varchar(50), [ultima_conexion], 120),   ''
	  ,td = 'https://' + [ip] + ':10000',   ''
      ,"td/@align" =  'center', td = [version], '',td=convert(varchar(50), [fecha_inicio], 120)
	  FROM [dbo].[dispositivo] where activo=1
	  FOR XML PATH('tr'), TYPE 
		) AS NVARCHAR(MAX) ) +
		N'</table>' ;


	EXEC msdb.dbo.sp_send_dbmail 
	  @profile_name='posta',
	  @recipients='jsolis@orkli.es; eunanue@orkli.es; iker@orkli.es',
	  @subject='eKanban RFID Arkuak',
	  @body = @tableHTML,
	  @body_format = 'HTML'
	  --@body='Datos de los porticos RFID:',
	  --@query = 'SELECT id_dispositivo, descripcion, ultima_conexion, Dis_EnError, ip, version FROM  ekanban.dbo.dispositivo' ,
	  --@query_result_header=0,
	  --@attach_query_result_as_file = 0
	  --@file_attachments='C:\temp\ik.txt'





	  /**



	  para ver la última lectura de cada portico. iker.
	  (SELECT        MAX(fecha_lectura) AS Expr1
                               FROM            dbo.tarjeta
                               GROUP BY ubicacion, tipo
                               HAVING         (ubicacion = dbo.dispositivo_ubicacion.id_ubicacion) AND (tipo = 'P')) AS UltLec




	  DECLARE @tableHTML  NVARCHAR(MAX) ;

SET @tableHTML =
    N'<H1>Work Order Report</H1>' +
    N'<table border="1">' +
    N'<tr><th>Work Order ID</th><th>Product ID</th>' +
    N'<th>Name</th><th>Order Qty</th><th>Due Date</th>' +
    N'<th>Expected Revenue</th></tr>' +
    CAST ( ( SELECT td = [id_dispositivo],   ''
      ,td = [activo],   ''
      ,td = [imei],   ''
      ,td = [num_serie],   ''
      ,td = [descripcion],   ''
      ,td = [Dis_UltAccFec]
  FROM [dbo].[dispositivo]
  FOR XML PATH('tr'), TYPE 
    ) AS NVARCHAR(MAX) ) +
    N'</table>' ;

EXEC msdb.dbo.sp_send_dbmail 
	@profile_name='posta',
	@recipients='iker@orkli.es',
    @subject = 'Work Order List',
    @body = @tableHTML,
    @body_format = 'HTML'











	  SELECT        TOP (100) PERCENT dbo.dispositivo.id_dispositivo, dbo.dispositivo.descripcion, dbo.dispositivo.ultima_conexion, dbo.dispositivo.Dis_EnError, dbo.dispositivo.ip, dbo.dispositivo.version, 
                         dbo.TRegistroEstPort.RegPor_Des, dbo.TRegistroEstPort.RegPor_Has, dbo.TRegistroEstPort.RegPor_Err, DATEDIFF(mi, dbo.TRegistroEstPort.RegPor_Des, dbo.TRegistroEstPort.RegPor_Has) AS Expr1
FROM            dbo.dispositivo INNER JOIN
                         dbo.TRegistroEstPort ON dbo.dispositivo.id_dispositivo = dbo.TRegistroEstPort.RegPor_Dis
WHERE        (dbo.dispositivo.id_dispositivo = '4') AND (dbo.TRegistroEstPort.RegPor_Err = 1) AND (DATEDIFF(mi, dbo.TRegistroEstPort.RegPor_Des, dbo.TRegistroEstPort.RegPor_Has) > 10)
ORDER BY dbo.TRegistroEstPort.RegPor_Has DESC
	  **/



END

