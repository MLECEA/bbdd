﻿/*******************************************************************************************
 *
 * log
 *
 * Envía un mail 
 *
 * @to es el destinatario/s , si son más de uno deberá ir separado por ";" (TO)
 * @cc son las direcciones de correo a las que se envía copia, si son varios separados por ";" (CC)
 * @cco son las direcciones de correo a las que se envía copia oculta, si son varias separadas por ";" (CCO)
 * @subject es el título o asunto del correo electrónico
 * @body es el contenido del mensaje
 *
 ********************************************************************************************/
CREATE PROCEDURE [dbo].[common_log]
	( 
		@mensaje as VARCHAR(255)
	)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY

		insert into _pruebas_mensajes (fecha, mensaje) values (CURRENT_TIMESTAMP, @mensaje)
				
	END TRY
	BEGIN CATCH
		select '' as ERROR_USER,
			 ERROR_NUMBER() as ERROR_NUMBER,
			 ERROR_STATE() as ERROR_STATE,
			 ERROR_SEVERITY() as ERROR_SEVERITY,
			 ERROR_LINE() as ERROR_LINE,
			 ERROR_PROCEDURE() as ERROR_PROCEDURE,
			 ERROR_MESSAGE() as ERROR_MESSAGE,
			 GETDATE() as date
	END CATCH
END