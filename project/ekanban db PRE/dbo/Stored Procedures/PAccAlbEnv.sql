﻿

CREATE PROCEDURE [dbo].[PAccAlbEnv]
(
@Usuario AS varchar(255)='', @Cliente varchar(255)='', @Recid varchar(50)='', 
@Alb varchar(255)='', @Lin varchar(5)='', @Art varchar(100)=''
)
AS

BEGIN
	DECLARE @Rep varchar(250)
	DECLARE @Ser varchar(25)=''
	DECLARE @Resul INT=1
	DECLARE @Msg varchar(250)=''
	DECLARE @Url varchar(250)=''
	SET NOCOUNT ON;
	
	SET XACT_ABORT ON;


	SET @Resul=0  --1 es malo, 0 es ok
	set @Msg='Error'


	--ACCIONES
	if @Recid<>'' and @Cliente='01900200'
	begin

		--LANZAMOS EL PEDIDO PARA FACTURACIÓN Y REPOSICIÓN, DANDO CONSUMO AL ALBARÁN, LINEA Y ARTÍCULO Y MARCAMOS COMO FACTURADO. 
		--************************* PENDIENTE
		--
		begin distributed transaction
		
		
		UPDATE  PREAXSQL.SQLAXBAK.dbo.CUSTPACKINGSLIPTRANS
		SET INK_FACTURADOCONSIGNA = 1
		WHERE RECID=@Recid
	
		SET @Resul=0
		set @Msg='OK'
		
		commit transaction;
	end




	--RESULTADOS para Acciones, como para Reporting
	SELECT  'PAccAlbEnv' as Tipo, 
			@Resul as Resul, 
			@Msg as Msg  
			

	
		--EXEC	[dbo].[PRegistro]
		--@Reg_Usu = @Usuario,
		--@Reg_Ubi = @Recid,
		--@Reg_opt = N'[PCtaAccReports]',
		--@Reg_Art = @Art,
		--@Reg_cli = @Cliente,
		--@Reg_Txt = @Usuario
		

			

END


