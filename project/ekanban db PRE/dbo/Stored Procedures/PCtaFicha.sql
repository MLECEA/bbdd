﻿




CREATE PROCEDURE [dbo].[PCtaFicha] 
(
@Circuito varchar(255)='', @Usu AS varchar(255)=''
)
AS
BEGIN
	SET NOCOUNT ON;

SELECT        referencia_id, cliente_nombre, ISNULL(ref_cliente, '') AS ref_cliente, tipo_contenedor, cantidad_piezas, isnull(Cir_LanPedAut,0) AS Cir_LanPedAut, reserva_auto, isnull(Cir_IntLun,0) as Cir_IntLun,  isnull(Cir_IntMar,0) as Cir_IntMar,  isnull(Cir_IntMie,0) as Cir_IntMie,  isnull(Cir_IntJue,0) as Cir_IntJue, isnull(Cir_IntVie,0) as Cir_IntVie, isnull(Cir_IntSab,0) as Cir_IntSab,isnull(Cir_IntDom,0) as Cir_IntDom, ISNULL(Cir_HoraM, CAST('00 AM' AS time(7))) AS Cir_HoraM, ISNULL(Cir_HoraT,  CAST('00 AM' AS time(7))) AS Cir_HoraT, 
                        isnull(Cir_LunM,0) as Cir_LunM,  isnull(Cir_LunT,0) as Cir_LunT,  isnull(Cir_MarM,0) as Cir_MarM, isnull(Cir_MarT,0) as Cir_MarT, isnull(Cir_MieM,0) as Cir_MieM, isnull(Cir_MieT,0) as Cir_MieT,
						 isnull(Cir_JueM,0) as Cir_JueM,  isnull(Cir_JueT,0) as Cir_JueT, isnull(Cir_VieM,0) as Cir_VieM,  isnull(Cir_VieT,0) as Cir_VieT, isnull(Cir_SabM,0) as Cir_SabM, isnull(Cir_SabT,0) as  Cir_SabT,
						  isnull(Cir_DomM,0) as Cir_DomM,   isnull(Cir_DomT,0) as Cir_DomT
						  , tipo_circuito AS CIRTIP, ISNULL(entrega_dias, 0) AS EntDia, ISNULL(entrega_horas, 0) AS EntHor, ISNULL(Cir_Consigna, 0) AS CONS

FROM            dbo.circuito
WHERE        (id_circuito = @Circuito)
END




