﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PCtaTarOptimoConConsumosHistoricos] 
	-- Add the parameters for the stored procedure here
	@circuito varchar(255) = '',
	@Usu AS varchar(255)='',
	@cantOptimas int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
    
    --Con la ubiacion y referencia conseguimos el IdCircuito.
        
	select @cantOptimas = num_kanbanOPT from circuito where id_circuito= @circuito;
	--seleccionar solo las 10 primeras.
	
	--exec PAX_ConsumosHtoricos @circuito, @Usu;
		
	declare @HISTORICO table (CLI varchar(255), UBI varchar(255), ART varchar(255), 
	AÑO varchar(255), SEM varchar(255), ETIQ varchar(255), CTDSEM INT, COLOR INT, EMP VARCHAR(255));
			
	insert into @HISTORICO exec PAX_ConsumosHtoricos @circuito, @Usu;
	
	SELECT *
	FROM (SELECT TOP(10) * FROM @HISTORICO ORDER BY CONVERT(int, AÑO) DESC, CONVERT(int, SEM) DESC) AS T
	ORDER BY CONVERT(int, AÑO) ASC, CONVERT(int, SEM) ASC;
	
END
