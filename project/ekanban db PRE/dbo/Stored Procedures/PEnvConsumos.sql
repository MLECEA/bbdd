﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PEnvConsumos]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @Cir AS varchar(50)=''
	DECLARE @Usu AS varchar(500)=''
	DECLARE @NBase_Cursor varchar(500)

	DECLARE  CDatos CURSOR FOR
		SELECT        mail  
		FROM            dbo.circuito
		--WHERE        (dbo.R(id_circuito) <> Cir_RAnt) and (activo =1) --Es la condición inicial. Pero como notificaba en Sabaf, las lecturas, que por no llegar a mínimas, luego se anulan, 
		--dejamos un tiempo de garantía de que hayan pasado 2 minutos. Y qué pasa si ya se integran? sería igual al anterior, por lo que no avisa. 


		WHERE        
		--Garantizamos que han pasado 2 minutos, para evitar notificar. 
		(
		-- que pasen 2 minutos.
		(DATEADD(mi, 2, dbo.UltLec(id_circuito)) < GETDATE()) AND (year(dbo.UltLec(id_circuito)) > 2000) 
		or 		
		--para los casos como facturaciones automáticas, lo dejamos en automático. sin añadir 2 minutos. 
		Cir_LanPed5MI=1	)
		--y que haya más tarjetas ahora y circuito activo
		AND (dbo.R(id_circuito) > Cir_RAnt) and (activo =1)

        GROUP BY   mail  
		HAVING        (mail > '')


	OPEN CDatos

	FETCH NEXT FROM CDatos
	INTO @NBase_Cursor

	WHILE @@fetch_status = 0 
	BEGIN
			SET @Usu = @NBase_Cursor

	
			DECLARE @tableHTML  NVARCHAR(MAX) ;

			SET @tableHTML =
			N'<H1>eKanban Consumptions</H1>' +
			N'<table border="1">' +
			N'<tr><th>Customer</th>' +
			N'<th>PartNumber</th>' +
			N'<th>CustomerRef</th>' +
			N'<th>PreviousRedCards</th>' +
			N'<th>CurrentRedCards</th>' +
			N'<th>QtyPerCard</th>' +
			N'<th></th>' +
			N'<th></th>' +
			N'<th></th></tr>' +
			CAST ( ( SELECT "td/@align" =  'center', td = cliente_nombre,   ''
			  ,td = referencia_id,   ''
			    ,td = ref_cliente,   ''
			  ,"td/@align" =  'center',  td = Cir_RAnt,  ''  -- "td/@bgcolor" =  case when [Dis_EnError]=0 then 'green' ELSE 'red' END,
			  ,"td/@align" =  'center',  td = convert(varchar(50), dbo.R(id_circuito), 120),   ''
			  
			   ,"td/@align" =  'center',  td = convert(varchar(50), cantidad_piezas, 120),   ''

			  ,td = '' + '',   ''
			  ,"td/@align" =  'center', td = '', ''
			  ,td=''--convert(varchar(50), [fecha_inicio], 120)
			  FROM [dbo].circuito
			  WHERE      
			    		(
				-- que pasen 2 minutos.
				(DATEADD(mi, 2, dbo.UltLec(id_circuito)) < GETDATE()) AND (year(dbo.UltLec(id_circuito)) > 2000) 
				or 		
				--para los casos como facturaciones automáticas, lo dejamos en automático. sin añadir 2 minutos. 
				Cir_LanPed5MI=1	)
				--y que haya más tarjetas ahora y circuito activo
				AND (dbo.R(id_circuito) > Cir_RAnt) and (activo =1)

				 and mail=@Usu
			  order by cliente_id, referencia_id
			  FOR XML PATH('tr'), TYPE 
				) AS NVARCHAR(MAX) ) +
				N'</table>' ;


			EXEC msdb.dbo.sp_send_dbmail 
			  @profile_name='posta',
			  @recipients=@Usu,  
			  @subject='eKanban Consumptions',
			  @body = @tableHTML,
			  @body_format = 'HTML'


			  select 'Enviando msg' + @usu as accion

		
		FETCH NEXT FROM CDatos
		INTO @NBase_Cursor
	END

	close CDatos
	deallocate CDatos

	
	UPDATE [dbo].[circuito]
	SET 
	[Cir_RAnt] = dbo.R(id_circuito)
	,[Cir_RFec] = getdate()
	--PARA QUE LA CONSIDERACIÓN DE ROJAS SEA DESPUÉS DE LA GESTIÓN DE MÍNIMOS QUE ESTÁ POR PARÁMETRO A 60SG
	WHERE DATEADD(mi, 2, dbo.UltLec(id_circuito)) < GETDATE() --AND (year(dbo.UltLec(id_circuito)) > 2000)

END
