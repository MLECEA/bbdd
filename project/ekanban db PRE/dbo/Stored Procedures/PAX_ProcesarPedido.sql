﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PAX_ProcesarPedido]
	-- Add the parameters for the stored procedure here
	@circuito varchar(255), 
	@referencia varchar(255),
	@cantidadPedido numeric(18,5),
	@cantidadPiezasPorTarjeta numeric(18,5), 
	@cantidadTarjetasRojas int, 
	@usuario varchar(255), 
	@FechaEntregaConfirmada datetime,
	@pedido varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT <@Param1, sysname, @p1>, <@Param2, sysname, @p2>
	declare @cantidadTarjetas numeric(18,5);
	declare @CampGest varchar(255);
	declare @StatusTrat varchar(1);
	DECLARE @IdTar varchar(50);
	DECLARE @FeTar datetime;
	declare @cantTarjetasInt int;
	
	set @cantidadTarjetas = @cantidadPedido / @cantidadPiezasPorTarjeta;
	SELECT @cantidadTarjetas = CONVERT(INT, ROUND(@cantidadTarjetas, 0, 0), 0);
	set @cantTarjetasInt = CAST(@cantidadTarjetas as int);
	set @CampGest='AutWS' + '_' + CONVERT(nvarchar(100), @FechaEntregaConfirmada, 112) + '_' + CONVERT(nvarchar(100), getdate(), 113);
	SET @StatusTrat='G';
		
	if @cantidadTarjetas > @cantidadTarjetasRojas
	--actualizar todas las tarjetas
	begin
		print 'actualizar todas + generar nuevas tarjetas ';
		UPDATE [EKANBAN].[dbo].[tarjeta]
				SET gestionado =  @CampGest
				, Estado=@StatusTrat, Tar_GestionadoUSU = @Usuario
				, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechaEntregaConfirmada,
				Tar_IdPedido = @pedido
				WHERE (gestionado IS NULL OR gestionado = '') AND (id_circuito =  @circuito) and (estado='P' OR estado='A') AND sentido = 'S'
		
		INSERT INTO dbo.tarjeta (id_lectura,antena,estado,fecha_lectura,gestionado,movil,tipo,cliente_id,id_circuito,referencia_id,ubicacion,sentido, kill_tag, Tar_FecEntSol, Tar_IdPedido)
					SELECT        TOP (convert(int, @cantidadTarjetas-@cantidadTarjetasRojas)) 'AJUSTE' + RIGHT(dbo.sga_usuario.cod_usuario, 2) + '_' + CONVERT(nvarchar(30), GETDATE(), 131) AS Expr1, NULL AS ANTENA, 'A_MODIF' AS ES, GETDATE() AS Expr2, NULL AS GEST, 'M' AS MOV, 'M' AS tipo, 
							 dbo.circuito.cliente_id, dbo.circuito.id_circuito, dbo.circuito.referencia_id, dbo.circuito.ubicacion, 'S' AS Expr3, 0, @FechaEntregaConfirmada, @pedido
					FROM            dbo.circuito CROSS JOIN dbo.sga_usuario
					WHERE        (dbo.circuito.id_circuito = @circuito)
	end
	else
	begin
		print 'Actualizar algunas tarjetas:' + cast (@cantTarjetasInt as varchar(255));
		--hay que actualizar parte de las tarjetas
		SELECT    TOP (@cantTarjetasInt)  @FeTar=fecha_lectura,   @IdTar =id_lectura
					FROM            dbo.tarjeta
					WHERE        (gestionado IS NULL OR
									gestionado = '') AND (id_circuito = @circuito)
					ORDER BY fecha_lectura, id_lectura
		print @IdTar;
		UPDATE [EKANBAN].[dbo].[tarjeta]
						SET gestionado =  @CampGest
						, Estado=@StatusTrat, Tar_GestionadoUSU = @Usuario
						, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechaEntregaConfirmada,
						Tar_IdPedido = @pedido
						WHERE     (gestionado IS NULL OR gestionado = '') AND (id_circuito =  @circuito)
						 and
						 (fecha_lectura < @FeTar or  --todas las tarjetas anteriores a la última seleccionada.
						 fecha_lectura = @FeTar) and id_lectura<=@IdTar --+ las que siendo la misma fecha, (si ha habido ajustes), la id lectura sea igual o menor.  
	end
END
