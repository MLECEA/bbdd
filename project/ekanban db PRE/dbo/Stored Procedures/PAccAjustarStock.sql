﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PAccAjustarStock]
	-- Add the parameters for the stored procedure here
	@CodCircuito varchar(20) = '',
	@NumTarjVerdes integer = 0,
	@Usu varchar(255) = '',
	@Dis varchar(4)=''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	BEGIN TRY
	
		declare @codRespuesta int; declare @resultado varchar(255);
		
		declare @Tresultado table (Resul  varchar(255), MsgTxt  varchar(255));
		
		declare @result table (art varchar(255), artcli varchar(255), t int, r  int, a  int, ae  int, v  int, ss  int,
					 estado int, circuito varchar(255), ultLec datetime, Cont  int, Circ  int, opt int, transito_Activado bit, ctd_Kan int);	
		
		-- Insert statements for procedure here
		INSERT INTO @Tresultado EXEC PAccAjustar @CodCircuito, @NumTarjVerdes, @Usu, @Dis;
		
		select @codRespuesta= Resul, @resultado = MsgTxt from @Tresultado;
		--if @codRespuesta = 0
		--begin
		insert into @result exec PAX_SitTarjetas @codCircuito, @Usu;
		select *, @codRespuesta as Result, @resultado as Msg from @result;
		--end	
		--else
		--begin
			--select * from @Tresultado;
		--end
		
		-----20181008 INCLUSION DE TABLA HISTORICOVERDES	
		exec dbo.PAX_HistoricoVerdes @Circuito = @CodCircuito, @Origen = 'AjusteStock', @Usuario = @Usu
		-----20181008 FIN INCLUSION DE TABLA HISTORICOVERDES
		
	END TRY
	BEGIN CATCH
		EXEC dbo.common_sendMailError @usuario = @Usu
	END CATCH
	
END
