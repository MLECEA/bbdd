﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE PCtaGetCustomProperties 
	-- Add the parameters for the stored procedure here
	@Usuario varchar(255) = NULL,
	@idCircuito varchar(255) = NULL,
	@Ubicacion varchar(255) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	SELECT 1 as IDPREGUNTA, convert(varchar(255), GETDATE()) as ValorDefecto, 'INTRODUZCA LA FECHA DE CADUCIDAD' as label
	union
	SELECT 2 as IDPREGUNTA, 'L10265' ValorDefecto, 'INTRODUZCA EL LOTE' as label
END