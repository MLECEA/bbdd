﻿-- ========================================================================================
-- Author:		CyC
-- Create date: 2015-05-15
-- Description: Procedimiento de ejemplo creado para Orkli, con ejemplos de llamada los 
--				4 metodos solicitados. No eliminar ninguna funcion de la BBDD EKANBAN cuyos 
--				nombres contengan AccessControl.
-- ========================================================================================

CREATE PROCEDURE [dbo].[AccessControl_PetitionExample]
	AS
BEGIN
	declare @UIDWorker int = 999
	declare @OIDOrder int = 1
	declare @returnValue nvarchar(4000)
	
	--comprobamos estado del usuario en el pedido
	print 'Comprobamos estado del usuario en el pedido'
	set @returnValue = (SELECT dbo.fAccessControl_CheckWorkerAccess(@UIDWorker, @OIDOrder)) 
	print @returnValue
	print '---------------------'
	
	--comprobamos si el usuario esta dentro
	print 'Comprobamos si el usuario ha accedido con dicho pedido'
	set @returnValue = (SELECT dbo.fAccessControl_IsWorkerInside(@UIDWorker, @OIDOrder))  
	if(@returnValue = '0')
		begin
			print 'El usuario esta fuera'
			print '---------------------'
		end
	else 
		begin
			print 'El usuario esta dentro'
			print '----------------------'
		end
	
	--registramos la entrada
	print 'Registramos entrada'
	set @returnValue = (SELECT dbo.fAccessControl_SetWorkerEntry(@UIDWorker, @OIDOrder))
	if(@returnValue = '0')
		print 'Entrada no registrada'
	else 
		print 'Entrada registrada'
	
	set @returnValue = (SELECT dbo.fAccessControl_IsWorkerInside(@UIDWorker, @OIDOrder))  
	if(@returnValue = '0')
		begin
			print 'El usuario esta fuera'
			print '---------------------'
		end
	else 
		begin
			print 'El usuario esta dentro'
			print '----------------------'
		end
	
	--registramos la salida
	print 'Registramos salida'	
	set @returnValue = (SELECT dbo.fAccessControl_SetWorkerExit(@UIDWorker, @OIDOrder))
	if(@returnValue = '0')
		print 'Salida no registrada'
	else 
		print 'Salida registrada'
		
	set @returnValue = (SELECT dbo.fAccessControl_IsWorkerInside(@UIDWorker, @OIDOrder))  
	if(@returnValue = '0')
		begin
			print 'El usuario esta fuera'
			print '---------------------'
		end
	else 
		begin
			print 'El usuario esta dentro'
			print '----------------------'
		end
END
