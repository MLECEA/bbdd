﻿/*******************************************************************************************
 *
 * sendMailAlertaEstadoVerde
 *
 * Envía un mail de alerta indicando que el estado del circuito a pasado a verde
 *
 * @Des es el destinatario/s , si son más de uno deberá ir separado por ";" (TO)
 * @Cc son las direcciones de correo a las que se envía copia, si son varios separados por ";" (CC)
 * @Cco son las direcciones de correo a las que se envía copia oculta, si son varias separadas por ";" (CCO)
 * @Tem es el título o asunto del correo electrónico
 * @Cue es el contenido del mensaje
 *
 ********************************************************************************************/
CREATE PROCEDURE [dbo].[alertasEstado_sendMailAlertaEstadoVerde]
	( @Circuito as nvarchar(50))
AS
BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRY
		DECLARE @subject as VARCHAR(255)
		DECLARE @to as VARCHAR(1000)
		DECLARE @cc as VARCHAR(1000) = null
		DECLARE @body as VARCHAR(5000)
		DECLARE @mensaje as VARCHAR(100) = ''

		select @subject = mailVerdeAsunto, @to = mailVerdePara, @cc = mailVerdeCc from alertasEstado_configuracionMail
		where cliente_id = (select cliente_id from circuito where id_circuito = @Circuito)
		
		if @@ROWCOUNT = 0 
			begin
				SET @mensaje = 'No existe configuracion de envío de la alerta para el circuito ' + @Circuito
				print 'alertasEstado_sendMailAlertaEstadoVerde - ' + @mensaje
				RAISERROR(@mensaje, 19, 1) WITH LOG
			end
		else
			begin
				if (@to is null and @cc is null)
					begin
						SET @mensaje = 'No se ha especificado ni to ni cc para el circuito ' + @Circuito
						print 'alertasEstado_sendMailAlertaEstadoVerde - ' + @mensaje
						RAISERROR(@mensaje, 19, 1) WITH LOG
					end	
				else
					begin
						SET @body = 'Se ha pasado al estado verde...'
						SET @to = RTRIM(@to)

						EXEC dbo.common_sendMail  @subject = @subject, @to = @to, @cc = @cc, @body = body	  
					end
			end		 
 
	END TRY
	BEGIN CATCH
		EXEC dbo.common_sendMailError 
	END CATCH 		
		
END