﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE PAX_UserIsDBAdmin
	-- Add the parameters for the stored procedure here
	@username varchar(255),
	@pwd varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select * from sga_usuario where username = @username and password = @pwd and isDBAdmin = 1
END
