﻿USE [EKANBAN]
GO
/****** Object:  StoredProcedure [dbo].[alertasEstado_estado]    Script Date: 01/16/2019 09:12:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[alertasEstado_estado]
(@Circuito as nvarchar(50) )
AS
BEGIN
	--DECLARE @Stat as int
	DECLARE @estadoCircuito as INT
	DECLARE @estadoActualCircuito as INT
	DECLARE @Tipo as int
	DECLARE @referencia_id as VARCHAR(255)
	DECLARE @cliente_id as VARCHAR(255)
	DECLARE @proveedor_id as VARCHAR(255)
	DECLARE @ubicacion as VARCHAR(255)
	DECLARE @descripcion as VARCHAR(255)
	DECLARE @num_kanban as INT
	DECLARE @transitoActivado as BIT
	DECLARE @CtdVerdes as INT 
	DECLARE @CtdAmarillas as INT 
	DECLARE @CtdRojas as INT 
	DECLARE @CtdAmarillasVerdes as INT
	DECLARE @CtdSobreStock as INT

	print 'alertasEstado_estado - ' + @Circuito + ' Inicio'

	--1 proveedor, 3 cliente
	begin
		SELECT @Tipo=tipo_circuito, @referencia_id=referencia_id, @ubicacion=ubicacion, @descripcion=descripcion, 
				@num_kanban=num_kanban, @transitoActivado=transitoActivado, @proveedor_id = Proveedor_id, @cliente_id = cliente_id
		FROM dbo.circuito
		WHERE id_circuito = @Circuito
	end
	set @Tipo=isnull(@Tipo, 1) -- si no estuviera definido, lo consideramos como proveedor

	--!!! forzamos a tipocliente !!!!! esto habrá que borrarlo
	set @Tipo=3

	--set @Stat=1 --verde por defecto

	if @tipo=3 --cliente
		begin
			print 'alertasEstado_estado - ' + @Circuito + ' es cliente'

			-- obtenemos el estado actual del circuito
			EXEC @estadoCircuito = alertasEstado_calcularEstadoCircuito @Circuito
			Print N'alertasEstado_estado - Estado CALCULADO ' + CONVERT(varchar, @estadoCircuito)
			
			-- obtenemos el estado anterior del circuito
			EXEC @estadoActualCircuito = alertasEstado_getEstadoCircuito @Circuito
			print N'alertasEstado_estado - Estado ANTERIOR ' + CONVERT(varchar, @estadoActualCircuito)

			if (@estadoActualCircuito is null or @estadoCircuito <> @estadoActualCircuito)
				begin
					-- ha cambiado de estado
					print N'alertasEstado_estado - El estado ha cambiado'

					SELECT fecha as fecha, IdCircuito, CtdVerdes, CtdAmarillas, CtdRojas, CtdAmarillasVerdes,
		                      CtdSobreStock
		               FROM THistoricoVerdes
		               WHERE IdCircuito = @Circuito
		               AND 	fecha in (select max(fecha) as fecha from THistoricoVerdes where IdCircuito = @Circuito)
		            
		            
		            
		            print 'alertasEstado_estado - Comprobamos si ha cambiado el estado'
		            
		            if (@estadoCircuito = 1 or @estadoCircuito = 2 or @estadoCircuito = 1)   
						begin
							print 'alertasEstado_estado - Cambio a estado ' + CONVERT(varchar, @estadoCircuito)
							
							--vamos a registrar el envío de la alerta en la tabla en la tabla
							insert into dbo.alertasEstado_alertasEnviadas
							(circuitoId, referenciaId, estadoAnterior, estadoNuevo, ubicacion, proveedorId, clienteId, fecha)
							values
							(@Circuito, @referencia_id, @estadoCircuito, @estadoActualCircuito, @ubicacion, @proveedor_id, @cliente_id, SYSDATETIME())
				            
							EXEC dbo.alertasEstado_sendMailAlertaEstado @Circuito = @Circuito, @estadoAnteriorCircuito = @estadoActualCircuito,
															 @estadoNuevoCircuito = @estadoCircuito, @idReferencia = @referencia_id , 
															 @ubicacion = @ubicacion, @descripcion = @descripcion,
															 @numeroTarjetas = @num_kanban , @transitoActivado = @transitoActivado,
															 @CtdVerdes = @CtdVerdes, @CtdAmarillas = @CtdAmarillas, 
															 @CtdRojas = @CtdRojas, @CtdAmarillasVerdes = @CtdAmarillasVerdes, 
															 @CtdSobreStock = @CtdSobreStock					 
						end

					-- actualizamos el estado
					print 'alertasEstado_estado - ' + @Circuito + ' vamos a actualizar el estado'
					EXEC dbo.alertasEstado_setEstadoActualCircuito @Circuito = @Circuito, @estadoCircuito = @estadoCircuito
					print 'alertasEstado_estado - ' + @Circuito + ' estado actualizado'
				end
		end

		print 'alertasEstado_estado - ' + @Circuito + ' Fin'

	RETURN @estadoCircuito

END