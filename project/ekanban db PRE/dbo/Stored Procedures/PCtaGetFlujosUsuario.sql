﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PCtaGetFlujosUsuario] 
	-- Add the parameters for the stored procedure here
	@username varchar(255) = '',
	@idUbi varchar(255) = '',
	@codUser int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT * FROM usuario_ubicacion_flujo WHERE IdUsuario = @codUser)
	BEGIN
		if @idUbi is not null and @idUbi <> ''
		begin
			--print 'por ubicacion';
			SELECT F.IdFlujo as ID_FLUJO, UU.IdUbicacion as idubicacion, f.Descripcion, TP.IdPregunta, TP.Obligatorio, TP.Orden, NULL AS RESPUESTA, TP.TipoRespuesta,
				TP.ToolTip, TP.TextoPregunta, TP.ValorPorDefecto, CASE 
							 WHEN tp.SentenciaSQL IS NOT NULL AND TP.SentenciaSQL <> '' THEN NULL
							 ELSE TP.OrigenDatos
						  END AS LISTAVALORES, NULL AS MATRICULA, NULL AS FASE, IconoURL as ICONO_URL, NULL as TIENE_RESPUESTABYTES
			from TFlujo_Usuario tu inner join TFlujo F on tu.IdFlujo = f.IdFlujo INNER JOIN usuario_ubicacion UU1 ON UU1.id_usuario = TU.IdUsuario INNER JOIN 
				 usuario_ubicacion_flujo UU ON TU.IdFlujo = UU.IdFlujo AND TU.IdUsuario = UU.IdUsuario AND UU.IDUbicacion = UU1.id_ubicacion
				 INNER JOIN TFlujoPregunta TP ON F.IdFlujo = TP.IdFlujo
			WHERE TU.IdUsuario = @codUser AND F.Activo = 1 and uu1.id_ubicacion = @idUbi and tp.TipoRespuesta='START_FLOW'
			ORDER BY UU.prioridad
		end
		else
		begin
			SELECT F.IdFlujo as ID_FLUJO, UU.IdUbicacion as idubicacion, f.Descripcion, TP.IdPregunta, TP.Obligatorio, TP.Orden, NULL AS RESPUESTA, TP.TipoRespuesta,
				TP.ToolTip, TP.TextoPregunta, TP.ValorPorDefecto, CASE 
							 WHEN tp.SentenciaSQL IS NOT NULL AND TP.SentenciaSQL <> '' THEN NULL
							 ELSE TP.OrigenDatos
						  END AS LISTAVALORES, NULL AS MATRICULA, NULL AS FASE, IconoURL as ICONO_URL, NULL as TIENE_RESPUESTABYTES
			from TFlujo_Usuario tu inner join TFlujo F on tu.IdFlujo = f.IdFlujo INNER JOIN usuario_ubicacion UU1 ON UU1.id_usuario = TU.IdUsuario INNER JOIN 
				 usuario_ubicacion_flujo UU ON TU.IdFlujo = UU.IdFlujo AND TU.IdUsuario = UU.IdUsuario AND UU.IDUbicacion = UU1.id_ubicacion
				 INNER JOIN TFlujoPregunta TP ON F.IdFlujo = TP.IdFlujo
			WHERE TU.IdUsuario = @codUser AND F.Activo = 1 and tp.TipoRespuesta='START_FLOW'
			ORDER BY UU.prioridad
		end
	END
	ELSE
	BEGIN
		select distinct -1 as ID_FLUJO, UU.id_ubicacion as idubicacion, f.Descripcion, ValorPorDefecto as IdPregunta, TP.Obligatorio, TP.Orden, NULL AS RESPUESTA, TP.TipoRespuesta,
				TP.ToolTip, TP.TextoPregunta, TP.ValorPorDefecto, CASE 
							 WHEN tp.SentenciaSQL IS NOT NULL AND TP.SentenciaSQL <> '' THEN NULL
							 ELSE TP.OrigenDatos
						  END AS LISTAVALORES, NULL AS MATRICULA, NULL AS FASE, IconoURL as ICONO_URL, NULL as TIENE_RESPUESTABYTES
		from TFlujo_Usuario tu inner join TFlujo F on tu.IdFlujo = f.IdFlujo INNER JOIN usuario_ubicacion UU ON TU.IdUsuario = UU.id_usuario
			INNER JOIN TFlujoPregunta TP ON F.IdFlujo = TP.IdFlujo
		where IdUsuario = @codUser and f.Activo = 1 and tp.TipoRespuesta='START_FLOW'
		--ORDER BY TU.Prioridad
	END
	--para obtener la lista de valores de cada preguna
	
END
