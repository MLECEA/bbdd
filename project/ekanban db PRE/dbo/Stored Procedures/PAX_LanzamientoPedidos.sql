﻿
-- =============================================
-- Author:		iker
-- Create date: 3-6-2014
-- Description:	Para crear los pedidos AX desde el nuevo sistema eKanban, android e iKtrace
-- =============================================

CREATE PROCEDURE [dbo].[PAX_LanzamientoPedidos]
(
	@CodCircuito varchar(20) = '',
	@NumTarjRojas integer = 0,
	@NumTarjetas integer = 0, 
	@FechEntregaConfirmada datetime,
	@Ubicacion varchar(255) = '',
	@Usuario varchar(255) = ''
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET XACT_ABORT ON;  --- AITOR. 2014-06-24 
						--- Transakzio errorea kentzeko. Horrela rollback-ak transakzio osoa anulatzen du errorea dagoenean.
	BEGIN TRY
		DECLARE @MsgTxt varchar(255)=''
		DECLARE @numTarjRojasAhora integer=0
		DECLARE @Resul int=0
		DECLARE @Tipo varchar(10)=''
		DECLARE @CodCliPro varchar(100)=''
		DECLARE @Art varchar(100)=''
		DECLARE @Cli varchar(100)
		DECLARE @pro varchar(100)
		DECLARE	@return_value int
		DECLARE @AUT VARCHAR (5)=''
		DECLARE @PedPRO bit=0
		DECLARE @PedCLI bit=0
		DECLARE @IdTar varchar(50)
		DECLARE @FeTar datetime
		DECLARE @Consig as bit=0
		DECLARE @ConTipoVen as varchar(2)
		DECLARE @ConDesde as varchar(25)=''
		DECLARE @StdDesde as varchar(25)=''
		DECLARE @Facturar as int
		DECLARE @SalesTipo varchar(10)=''
		DECLARE @NumPed varchar(50)=''
		DECLARE @CampGest varchar(50)=''
		DECLARE @ReaprovAgrupada as bit=0
		DECLARE @ConFactAgrupada as bit=0
		DECLARE @StatusTrat varchar(1)
		DECLARE @StatusPed int=9
		DECLARE @FechEntregaConfirmada1 AS DATE=GETDATE()
		DECLARE @NumPedCli varchar(50)


		
		--Revisamos si el circuito es de tipo cliente o proveedor y sus cógidos de cliente/proveedor
		SELECT @Tipo=dbo.tipo_circuito.nombre, @Pro=dbo.circuito.Proveedor_id, @Cli=dbo.circuito.cliente_id, @Art=referencia_id, @Consig=Cir_Consigna, @SalesTipo=Cir_StdSalesType, @ConTipoVen=Cir_ConSalesType, @STDDesde=rtrim(ltrim(Cir_StdInventLocationId)), @ConDesde=rtrim(ltrim(Cir_ConInventLocationId)), @Facturar=Cir_ConFacturarAut, @Ubicacion=dbo.circuito.ubicacion, @ConFactAgrupada=dbo.circuito.Cir_ConFacturarAgrup , @ReaprovAgrupada=dbo.circuito.Cir_ReaprovAgrup, @NumPedCli=Cir_NumPedCli
		FROM            dbo.circuito INNER JOIN
					dbo.tipo_circuito ON dbo.circuito.tipo_circuito = dbo.tipo_circuito.id
		WHERE        (dbo.circuito.id_circuito = @CodCircuito)
		
		set @Consig = isnull(@Consig, 0)
		set @ConTipoVen = isnull(@ConTipoVen, '')
		set @STDDesde = isnull(@STDDesde, 'EXP.AUXIL')
		set @ConDesde = isnull(@ConDesde, 'EXP.AUXIL')
		set @Facturar=isnull(@Facturar, 0)
		set @ConFactAgrupada=isnull(@ConFactAgrupada, 0)
		set @SalesTipo=isnull(@SalesTipo, '')
		set @NumPedCli=isnull(@NumPedCli,'')

		if @ConDesde = '' set @ConDesde='EXP.AUXIL'
		if @STDDesde = '' set @STDDesde='EXP.AUXIL'
		


		SET @FechEntregaConfirmada1 = CONVERT(date, @FechEntregaConfirmada) ---******************** IKER 11/7/2017

		SET @StatusPed=9 --por defecto para que si es un equipo no oficial, no vaya a crear pedidos. 
		if CONVERT(sysname, SERVERPROPERTY(N'servername'))= 'SRVDMZ' SET @StatusPed=0  -- estado para que entren los pedidos en crear pedidos.

		IF {fn HOUR(@FechEntregaConfirmada)}=0 AND { fn MINUTE(@FechEntregaConfirmada) }=0
		BEGIN
			SET @FechEntregaConfirmada=DATEADD(MINUTE, { fn MINUTE(GETDATE()) }, DATEADD(HOUR, { fn HOUR(GETDATE()) }, @FechEntregaConfirmada))
		END

		
		SET @StatusTrat='T'
		if @Tipo='CLI' SET @CodCliPro=@CLI
		if @Tipo='PRO' SET @CodCliPro=@PRO

		if @Tipo='INT' OR @Tipo='EXT' 
		BEGIN
			SET @CodCliPro=''
			SET @StatusTrat='G'
		END


			
		--paso de control
		Set @Resul=0
		set @MsgTxt='Sin validar usuario'
		
		--SE MIRA SI EL USUARIO TIENE PERMISO PARA LANZAR LOS PEDIDOS
		--SI ES AUTOMÁTICO, SI, SI ES UN USUARIO, SE MIRA SI CORRESPONDE A UN ROL CON PERMISOS.
		IF @Usuario='AUT'  or  @Usuario='ALB' or @Usuario='5MI'  or @Usuario='iker' 
		  -- HORAS FIJAS           No se utiliza         CADA 5MIN
			begin
				SET @AUT=''--'A'  --LO QUITAMOS PARA QUE TANTO LOS PEDIDOS LANZADOS AUTOMÁTICAMENTE COMO LOS LANZADOS DESDE EL ANDROID SE AGRUPEN. YA QUE TENIENDO A EN AUTOMÁTICOS EL PEDIDO ES DIFERENTE Y SON DOS PEDIDOS APARTE.
				set @pedCLI=1
				set @pedPRO=1
			end
		else
			begin
				SELECT        @PedPRO=dbo.sga_rol.rol_PedPRO, @PedCLI=dbo.sga_rol.rol_PedCLI
				FROM            dbo.sga_rol INNER JOIN
										dbo.sga_usuario ON dbo.sga_rol.cod_rol = dbo.sga_usuario.ptr_rol
				WHERE        (dbo.sga_usuario.username = @Usuario)
			end

		IF @Usuario='demogm' and @Ubicacion='U00' set @pedPRO=1


		--si realmente hay permiso para realizar el pedido...
		IF @pedcli=1 or @pedpro=1
			begin
				-- Si la cantidad de tarjetas rojas actual es diferente a la del panel, no hacemos nada
				SELECT      @numTarjRojasAhora=   [dbo].[R] (@CodCircuito) 
				set @numTarjRojasAhora=isnull(@numTarjRojasAhora,0)
			
				if @numTarjRojasAhora <> @NumTarjRojas
					begin
						set @Resul=18  --Código de resultado para el android. Error al generar pedido.
						set @MsgTxt='Se han dado consumos justo en este instante. Vuelva a intentarlo'
						goto fin
					end
			
				-- Seleccionamos los datos, y los marcamos con el código de circuito en campo de GES. 
				-- significa que estamos en tratamiento.

				set @CampGest='AutAX' + '_' + CONVERT(nvarchar(100), @FechEntregaConfirmada, 112) + '_' + CONVERT(nvarchar(100), getdate(), 113)

				if @NumTarjetas=@NumTarjRojas
					--si hay que gestionar todas...
					begin
					UPDATE [EKANBAN].[dbo].[tarjeta]
						SET gestionado =  @CampGest
						, Estado=@StatusTrat, Tar_GestionadoUSU = @Usuario
						, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada1
						WHERE     (gestionado IS NULL OR gestionado = '') AND (id_circuito =  @CodCircuito)
					end
				else	
					begin
					if @NumTarjetas<@NumTarjRojas
						begin
							--SELECT    TOP (@NumTarjetas)     @IdTar =id_lectura
							--FROM            dbo.tarjeta
							--WHERE        (gestionado IS NULL OR
							--				gestionado = '') AND (id_circuito = @CodCircuito)
							--ORDER BY id_lectura

							SELECT    TOP (@NumTarjetas)  @FeTar=fecha_lectura,   @IdTar =id_lectura
							FROM            dbo.tarjeta
							WHERE        (gestionado IS NULL OR
											gestionado = '') AND (id_circuito = @CodCircuito)
							ORDER BY fecha_lectura, id_lectura



							UPDATE [EKANBAN].[dbo].[tarjeta]
								SET gestionado =  @CampGest
								, Estado=@StatusTrat, Tar_GestionadoUSU = @Usuario
								, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada1
								WHERE     (gestionado IS NULL OR gestionado = '') AND (id_circuito =  @CodCircuito)
								 and
								 (fecha_lectura < @FeTar or  --todas las tarjetas anteriores a la última seleccionada.
								 (fecha_lectura = @FeTar and id_lectura<=@IdTar)) --+ las que siendo la misma fecha, (si ha habido ajustes), la id lectura sea igual o menor. 
						end
					else
						begin
							UPDATE [EKANBAN].[dbo].[tarjeta]
								SET gestionado =  @CampGest
								, Estado=@StatusTrat, Tar_GestionadoUSU = @Usuario
								, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada1
								WHERE     (gestionado IS NULL OR gestionado = '') AND (id_circuito =  @CodCircuito) AND ESTADO <>'A_MODIF'
							
							--añadir registros A_MODIF en Tarjetas, tantos como @NumTarjetas-@NumTarjRojas
							--ya que hemos insertardo tarjetas nuevas en el circuito al crear el pedido de más cantidad 
							--que la solicitada según las tarjetas rojas. 
							INSERT INTO dbo.tarjeta (id_lectura,antena,estado,fecha_lectura,gestionado,movil,tipo,cliente_id,id_circuito,referencia_id,ubicacion,sentido, kill_tag, Tar_FecEntSol)
							SELECT        TOP (@NumTarjetas-@NumTarjRojas) 'AJUSTE' + RIGHT(dbo.sga_usuario.cod_usuario, 2) + '_' + CONVERT(nvarchar(30), GETDATE(), 131) AS Expr1, NULL AS ANTENA, 'A_MODIF' AS ES, GETDATE() AS Expr2, NULL AS GEST, 'M' AS MOV, 'M' AS tipo, 
									 dbo.circuito.cliente_id, dbo.circuito.id_circuito, dbo.circuito.referencia_id, dbo.circuito.ubicacion, 'S' AS Expr3, 0, @FechEntregaConfirmada1
							FROM            dbo.circuito CROSS JOIN dbo.sga_usuario
							WHERE        (dbo.circuito.id_circuito = @CodCircuito)
						end
					end

				
				--inserción de los datos para la creación del pedido de ventas-compras.
				--tipo de pedido según por defecto del cliente



				set @NumPed = @Ubicacion + CONVERT(nvarchar(100), @FechEntregaConfirmada, 112) + @AUT
				

				
				------------------------------------------------------------
				DECLARE @cnt INT = 0;
				DECLARE @nVeces INT = 1;

				if @Tipo='CLI' OR @Tipo='PRO' 
				begin


					IF @ReaprovAgrupada=0 --Significa que no queremos agrupar los consumos, y cada uno tiene que lanzar una línea de pedido, varias tarjetas
						BEGIN 
							set @nVeces=@NumTarjetas
							set @NumTarjetas=1
						END

					if @Tipo='PRO' and @Consig=1 
					begin
						set @NumPed = CONVERT(nvarchar(100), getdate(), 120) --NO AÑADIMOS MÁS Porque la longitud del pedido es de 20 y no entra.
					end

					if @Tipo='PRO' and @Consig<>1 
					begin
						--set @NumPed = @Ubicacion + CONVERT(nvarchar(100), @FechEntregaConfirmada, 112) --CONVERT(nvarchar(100), getdate(), 103) + ' ' + CONVERT(nvarchar(100), getdate(), 108) --
						--añadir la referencia del artículo o el circuito, para que el número de pedido sea no agrupado
						--*************************
						--iker: goikoa kenduta, para poner articulo y fecha. 17/7/2017
						set @NumPed =left(@Ubicacion + CONVERT(nvarchar(100), @FechEntregaConfirmada, 112)+ @Art,20) --CONVERT(nvarchar(100), getdate(), 103) + ' ' + CONVERT(nvarchar(100), getdate(), 108) --
					end

					if @Tipo='CLI' and @NumPedCli<>''
					BEGIN
					set @NumPed = @NumPedCli   --TODAVÍA ANULADO, HASTA QUE ELI Y JOSU LO VALIDEN. INCIDENICAS EN CREACIÓN DE PEDIDOS??
					END


					WHILE @cnt < @nVeces
						BEGIN
							INSERT INTO [preaxsql].[ORKLB000].[dbo].[TAUX_CREAR_PEDIDO]
							(Status, Journalname, Name, CUSTACCOUNT, PURCHORDERFORMNUM, ITEMID, SALESQTY,  SHIPPINGDATEREQUESTED, uservolc, fechaVolc, Reservaauto, type, SalesType, InventLocationId, FacturarAut, PedMarco, NumFactCli, AlbaranarAut, RegistrarPedCompra)
							SELECT        @StatusPed AS Estado, id_circuito AS Circ, descripcion AS Des, @CodCliPro AS CliPro, @NumPed AS NPed, referencia_id AS Ref, 
									cantidad_piezas * @NumTarjetas AS Cte, @FechEntregaConfirmada AS Fecha, @Usuario, GETDATE() AS Gaur, reserva_auto, @Tipo, @SalesTipo,@StdDesde,0, Cir_PedMarco,'',ISNULL(Cir_AlbaranarAut, 0) , ISNULL(Cir_RegistrarPedCompra, 0) 
							
							--@ConDesde
							
							FROM            dbo.circuito AS TCIRCUITO_1
							WHERE        (id_circuito = @CodCircuito)

							SET @cnt = @cnt + 1;
						END;

				end

				if @Tipo='INT' 
				begin
					set @NumPed = @CodCircuito -- @Ubicacion + CONVERT(nvarchar(100), @FechEntregaConfirmada, 112) + @AUT
				

					IF @ReaprovAgrupada=0 --Significa que no queremos agrupar los consumos, y cada uno tiene que lanzar una línea de pedido, varias tarjetas
						BEGIN 
							set @nVeces=@NumTarjetas
							set @NumTarjetas=1

						END

					WHILE @cnt < @nVeces
						BEGIN
						--@StatusPed    @CodCliPro
							INSERT INTO [preaxsql].[ORKLB000].[dbo].[TAUX_CREAR_PEDIDO]
							(Status, Journalname, Name, CUSTACCOUNT, PURCHORDERFORMNUM, ITEMID, SALESQTY,  SHIPPINGDATEREQUESTED, uservolc, fechaVolc, Reservaauto, type, SalesType, InventLocationId, FacturarAut, PedMarco, NumFactCli, AlbaranarAut, RegistrarPedCompra)
							SELECT       @StatusPed AS Estado, id_circuito AS Circ, descripcion AS Des, '000000' AS CliPro, @NumPed AS NPed, referencia_id AS Ref, 
									cantidad_piezas * @NumTarjetas AS Cte, @FechEntregaConfirmada AS Fecha, @Usuario, GETDATE() AS Gaur, reserva_auto, @Tipo, @SalesTipo,@StdDesde,0, Cir_PedMarco,'',ISNULL(Cir_AlbaranarAut, 0) , ISNULL(Cir_RegistrarPedCompra, 0) 
						--@ConDesde
							FROM            dbo.circuito AS TCIRCUITO_1
							WHERE        (id_circuito = @CodCircuito)

					

							SET @cnt = @cnt + 1;
						END;

				end
				------------------------------------------------------------

				if @Consig=1 and @Tipo='CLI' --en proveedores, no lanzamos la parte de pedido venta/compra, ya que se debe lanzar a final de día. Para que no lance en GARAY
				begin

					if @ConFactAgrupada=0 
					begin
						DECLARE @LectID VARCHAR(255)
						DECLARE @ALB VARCHAR(255)
						DECLARE @ALBLIN VARCHAR(255)
						DECLARE @NBase_Cursor varchar(255)

						DECLARE  CDatos1 CURSOR FOR
							SELECT        id_lectura
							FROM            dbo.tarjeta
							WHERE        (gestionado = @CampGest)

						OPEN CDatos1 

						FETCH NEXT FROM CDatos1
						INTO @NBase_Cursor

																																
						WHILE @@fetch_status = 0 
						BEGIN
						SET @LectID = @NBase_Cursor


						SET @ALB=''
						SET @ALBLIN=''
						
						IF SUBSTRING(@LECTID, 7, 3)='_AL'
						BEGIN
								SET @ALB=SUBSTRING(@LECTID, 8, 10)
								SET @ALBLIN =SUBSTRING(@LECTID, 19, 1)

								--PARA MARCAR LA LÍNEA DEL ALBARÁN COMO FACTURADO EN AX
								UPDATE preaxsql.SQLAXBAK.dbo.CustPackingSlipTrans
								SET  INK_FACTURADOCONSIGNA = 1
								WHERE   (DATAAREAID = N'ORK') AND (PACKINGSLIPID = @ALB) AND (LINENUM = @ALBLIN)


						END

						--******************************
						--en los de consigna, coge el número de albarán para copreci. hay que coger el que aparezca en campo IDPEDIDO, pero cuanto HECTOR lo actualice.
						--como va a afectar a los pedidos de compra?? hay que coger también la cantidad real.
						SELECT        @NumPed=SUBSTRING(id_lectura, 8, 10) 
						FROM            dbo.tarjeta
						WHERE        (id_lectura = @LectID)



						set @NumPed=isnull(@NumPed, @Ubicacion + CONVERT(nvarchar(100), @FechEntregaConfirmada, 112) + @AUT )

						INSERT INTO [preaxsql].[ORKLB000].[dbo].[TAUX_CREAR_PEDIDO]
							(Status, Journalname, Name, CUSTACCOUNT, PURCHORDERFORMNUM, ITEMID, SALESQTY,  SHIPPINGDATEREQUESTED, uservolc, fechaVolc, Reservaauto, type, SalesType, InventLocationId, FacturarAut, PedMarco, NumFactCli, AlbaranarAut, RegistrarPedCompra)
							SELECT        @StatusPed AS Estado, TCIRCUITO_1.id_circuito AS Circ, TCIRCUITO_1.descripcion AS Des, @CodCliPro AS CliPro, @NumPed AS NumPed, TCIRCUITO_1.referencia_id AS Ref, 
										 TCIRCUITO_1.cantidad_piezas AS Cte, @FechEntregaConfirmada AS Fecha, @Usuario AS Expr1, GETDATE() AS Gaur, TCIRCUITO_1.reserva_auto, @Tipo AS Expr2, @ConTipoVen AS Expr3, 
										 @ConDesde AS Expr4, @Facturar AS Expr5, TCIRCUITO_1.Cir_PedMarco, TCIRCUITO_1.Cir_ConNumFactCli, 0 AS Expr6, ISNULL(Cir_RegistrarPedCompra, 0) 
								FROM            dbo.circuito AS TCIRCUITO_1 INNER JOIN
														 dbo.tarjeta ON TCIRCUITO_1.id_circuito = dbo.tarjeta.id_circuito
								WHERE        (TCIRCUITO_1.id_circuito = @CodCircuito) AND (dbo.tarjeta.gestionado = @CampGest) AND (dbo.tarjeta.id_lectura = @LectID)
					
					
							/**SELECT       0 AS Estado, id_circuito AS Circ, descripcion AS Des, @CodCliPro AS CliPro, @NumPed AS NumPed, referencia_id AS Ref, 
									cantidad_piezas * @NumTarjetas AS Cte, @FechEntregaConfirmada AS Fecha, @Usuario, GETDATE() AS Gaur, reserva_auto, @Tipo, @ConTipoVen, @ConDesde, @Facturar, Cir_PedMarco,Cir_ConNumFactCli,0
							FROM            dbo.circuito AS TCIRCUITO_1
							WHERE        (id_circuito = @CodCircuito)
							**/

							FETCH NEXT FROM CDatos1
							INTO @NBase_Cursor
					END
						
						close CDatos1
						deallocate CDatos1
					end
					else
					begin

						-- entran los circuitos de cosigna que hay que facturar de forma agrupada

						set @NumPed=@Ubicacion + CONVERT(nvarchar(100), getdate(), 112) + @AUT


						INSERT INTO [preaxsql].[ORKLB000].[dbo].[TAUX_CREAR_PEDIDO]
								(Status, Journalname, Name, CUSTACCOUNT, PURCHORDERFORMNUM, ITEMID, SALESQTY,  SHIPPINGDATEREQUESTED, uservolc, fechaVolc, Reservaauto, type, SalesType, InventLocationId, FacturarAut, PedMarco, NumFactCli, AlbaranarAut)
								SELECT        @StatusPed AS Estado, TCIRCUITO_1.id_circuito AS Circ, TCIRCUITO_1.descripcion AS Des, @CodCliPro AS CliPro, @NumPed AS NumPed, TCIRCUITO_1.referencia_id AS Ref, 
											 TCIRCUITO_1.cantidad_piezas * @NumTarjetas AS Cte, @FechEntregaConfirmada AS Fecha, @Usuario AS Expr1, GETDATE() AS Gaur, TCIRCUITO_1.reserva_auto, @Tipo AS Expr2, @ConTipoVen AS Expr3, 
											 @ConDesde AS Expr4, @Facturar AS Expr5, TCIRCUITO_1.Cir_PedMarco, TCIRCUITO_1.Cir_ConNumFactCli, 0 AS Expr6
									FROM            dbo.circuito AS TCIRCUITO_1 
									WHERE        (TCIRCUITO_1.id_circuito = @CodCircuito) 
				
					end

				end	
				
				
				
				
				
				Set @Resul=0
				set @MsgTxt='se han insertado los datos'

				SET XACT_ABORT OFF;  --- AITOR. 2014-06-24 
				--- Transakzio errorea kentzeko. Horrela rollback-ak transakzio osoa anulatzen du errorea dagoenean.
			end

		fin:
		select  @Resul as Result, @MsgTxt as Msg, @IdTar
	END TRY
	BEGIN CATCH
		EXEC dbo.common_sendMailError @usuario = @Usuario
	END CATCH
END

