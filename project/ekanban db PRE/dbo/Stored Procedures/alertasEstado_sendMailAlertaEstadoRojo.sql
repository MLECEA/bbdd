﻿/*******************************************************************************************
 *
 * sendMailAlertaEstadoRojo
 *
 * Envía un mail de alerta indicando que el estado del circuito a pasado a rojo
 *
 * @Des es el destinatario/s , si son más de uno deberá ir separado por ";" (TO)
 * @Cc son las direcciones de correo a las que se envía copia, si son varios separados por ";" (CC)
 * @Cco son las direcciones de correo a las que se envía copia oculta, si son varias separadas por ";" (CCO)
 * @Tem es el título o asunto del correo electrónico
 * @Cue es el contenido del mensaje
 *
 ********************************************************************************************/
CREATE PROCEDURE [dbo].[alertasEstado_sendMailAlertaEstadoRojo]
	( @Circuito as nvarchar(50),
	 @estadoAnteriorCircuito as INT,
	 @estadoNuevoCircuito as INT,
	 @idReferencia as VARCHAR(255),
	 @ubicacion as VARCHAR(255),
	 @descripcion as VARCHAR(255),
	 @numeroTarjetas as INT,
	 @transitoActivado as BIT,
	 @CtdVerdes as INT, 
	 @CtdAmarillas as INT, 
	 @CtdRojas as INT, 
	 @CtdAmarillasVerdes as INT,
	 @CtdSobreStock as INT)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		DECLARE @subject as VARCHAR(255)
		DECLARE @to as VARCHAR(1000)
		DECLARE @cc as VARCHAR(1000) = null
		DECLARE @body as VARCHAR(5000)
		DECLARE @mensaje as VARCHAR(100) = ''

		select @subject = mailRojoAsunto, @to = mailRojoPara, @cc = mailRojoCc from alertasEstado_configuracionMail
		where cliente_id = (select cliente_id from circuito where id_circuito = @Circuito)
		
		if @@ROWCOUNT > 0 and (@to is not null or @cc is not null)
			begin

				SET @body = '<html>' +
					'<p>eKanban notifica que la siguiente referencia ha pasado del estado ' + @estadoAnteriorCircuito +' a estado ' + @estadoNuevoCircuito + '</p>' +
					'<p>Referencia: ' + @idReferencia + '</p>' +
					'<p>Ubicaci&oacute;n: ' + @ubicacion + '</p>' + 
					'<p>Descripci&oacute;n: ' + @descripcion + '</p>' + 
					'<p>Cantidad de tarjetas eKanban: ' + @numeroTarjetas + '</p>' +
					'<p>&nbsp;</p>' +
					'<h2>' + @estadoNuevoCircuito + '</h2>' +
					'<p>Cantidad de rojas: {R}</p>  thistoricoverdes.ctdrojas ' +
					'<p>Cantidad de amarillas: ' + @CtdAmarillas + '</p> thistoricoverdes.ctdamarillas ';
				if (@transitoActivado = 1)
					BEGIN
						SET @body = @body + '<p>Cantidad de amarillas-verdes: ' + @CtdAmarillasVerdes + '</p>' + 
					END

				SET @body = @body +	'<p>Cantidad de verdes: ' + @CtdVerdes + '</p>' +
					'<p>Cantidad de sobre stock: ' + @CtdSobreStock + '</p>'+
					'<html>'

				SET @to = RTRIM(@to)

				EXEC dbo.common_sendMail  @subject = @subject, @to = @to, @cc = @cc, @body = body	  
			end		 
		else
			begin				
				SET @mensaje = 'No se ha especificado ni to ni cc, o no existe configuracion de envío de la alerta para el circuito ' + @Circuito
				print 'alertasEstado_sendMailAlertaEstadoRojo - ' + @mensaje
				RAISERROR(@mensaje, 19, 1) WITH LOG
			end
	END TRY
	BEGIN CATCH
		EXEC dbo.common_sendMailError 
	END CATCH 
END