﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PAX_EnviarFlujo] 
	-- Add the parameters for the stored procedure here
	@IdFlujo as int = 0,
	@IdMatricula  as int = NULL,
	@IdUsuario as varchar(100) = NULL,
	@IdUbicacion as varchar(255) = NULL,
	@IdSubUbicacion as varchar(255) = NULL,
	@IdFase as int = NULL,
	@Pos as int = 0,
	@idPregunta1 varchar(100) = NULL,
	@Respuesta1 varchar(255) = NULL,
	@Respuesta1_bytes varbinary(max) = NULL,
	@idPregunta2 varchar(100) = NULL,
	@Respuesta2 varchar(255) = NULL,
	@Respuesta2_bytes varbinary(max) = NULL,
	@idPregunta3 varchar(100) = NULL,
	@Respuesta3 varchar(255) = NULL,
	@Respuesta3_bytes varbinary(max) = NULL,
	@idPregunta4 varchar(100) = NULL,
	@Respuesta4 varchar(255) = NULL,
	@Respuesta4_bytes varbinary(max) = NULL,
	@idPregunta5 varchar(100) = NULL,
	@Respuesta5 varchar(255) = NULL,
	@Respuesta5_bytes varbinary(max) = NULL,
	@idPregunta6 varchar(100) = NULL,
	@Respuesta6 varchar(255) = NULL,
	@Respuesta6_bytes varbinary(max) = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @IDPREGUNTA VARCHAR(100);
	DECLARE @TXTPREGUNTA VARCHAR(255);
	DECLARE @TIPORESPUESTA VARCHAR(255);
	DECLARE @SENTENCIASQL VARCHAR(255);
	DECLARE @OBLIGATORIO VARCHAR(1);
	DECLARE @ORIGENDATOS VARCHAR(255);
	DECLARE @ORDEN VARCHAR(10);
	DECLARE @SQL VARCHAR(500);
	DECLARE @MATRICULA INT;
	DECLARE @FASE INT;
	DECLARE @RESPUESTA VARCHAR(255);
	DECLARE @CANCELADO BIT;
	DECLARE @TOOLTIP VARCHAR(255);
	DECLARE @VALORPORDEFECTO VARCHAR(255);
	declare @TIENE_RESPUESTA_BYTES BIT = 0;
	
	DECLARE @PREGUNTAS TABLE (MATRICULA INT, IDFLUJO INT, descripcionFlujo VARCHAR(255), IDPREGUNTA VARCHAR(100), TXTPREGUNTA VARCHAR(255), TOOLTIP VARCHAR(255), TIPORESPUESTA VARCHAR(100), OBLIGATORIO BIT, LISTAVALORES VARCHAR(255),
			ORDEN INT, FASE INT, RESPUESTA VARCHAR(255), RESUL INT, RESULMSG VARCHAR(255), VALORPORDEFECTO VARCHAR(255), TIENE_RESPUESTABYTES BIT);
	DECLARE @TRESULTADO TABLE (valoresPosibles varchar(1000));
    -- Insert statements for procedure here
    
    declare @descFlujo varchar(255);
    select @descFlujo = Descripcion from TFlujo where IdFlujo= @IdFlujo;
	
	--Si idMatricula = NULL -> Indica que el usuario va a crear una matricula nueva -> insert into matricula
	IF @IdMatricula IS NULL OR @IdMatricula <= 0
	BEGIN
		if exists(select * from TFlujo where IdFlujo = @IdFlujo)
		begin
		INSERT INTO Matricula (IdFlujo, IdUsuario,  FechaInicio,Ubicacion, SubUbicacion ) VALUES (@IdFlujo, @IdUsuario, GETDATE(), @IdUbicacion, @IdSubUbicacion);
		SET @MATRICULA = SCOPE_IDENTITY()  ;
		PRINT  @MATRICULA;
		SET @IdMatricula = @MATRICULA;
		end
		else set @IdMatricula = NULL;
	END		
	ELSE
	BEGIN
		SELECT @CANCELADO = CANCELADO FROM Matricula WHERE IdMatricula = @IdMatricula;
		IF @CANCELADO = 1 
		BEGIN
			INSERT INTO  @PREGUNTAS (MATRICULA, IDPREGUNTA, TXTPREGUNTA, TOOLTIP, TIPORESPUESTA, OBLIGATORIO, LISTAVALORES, ORDEN, FASE, RESPUESTA, RESUL, RESULMSG,VALORPORDEFECTO, TIENE_RESPUESTABYTES) VALUES 
				(NULL, NULL , NULL , NULL ,NULL , NULL,  NULL, NULL, NULL , NULL, 6, 'Flujo cancelado', NULL, 0 );
			GOTO FIN;
		END
		IF @Pos = 1 --solo guardamos si va a pasar al siguiente paso
		BEGIN
			--GUARDAR LAS RESPUETAS
			IF @idPregunta1 IS NOT NULL AND  @idPregunta1 <> '' 
			BEGIN
				if exists(select * from MatriculaRespuesta where IdMatricula = @IdMatricula and IdPregunta = @idPregunta1)
					update MatriculaRespuesta set Respuesta = @Respuesta1, RespuestaBytes = @Respuesta1_bytes where IdMatricula = @IdMatricula and IdPregunta = @idPregunta1
				else
					INSERT INTO MATRICULARESPUESTA (IDMATRICULA, IDPREGUNTA, RESPUESTA, FASE, RespuestaBytes) VALUES (@IdMatricula,  @idPregunta1, @Respuesta1, @IdFase, @Respuesta1_bytes);
			END
			IF @idPregunta2 IS NOT NULL AND  @idPregunta2 <> '' 
			BEGIN
				if exists(select * from MatriculaRespuesta where IdMatricula = @IdMatricula and IdPregunta = @idPregunta2)
					update MatriculaRespuesta set Respuesta = @Respuesta2, RespuestaBytes = @Respuesta2_bytes where IdMatricula = @IdMatricula and IdPregunta = @idPregunta2
				else
					INSERT INTO MATRICULARESPUESTA (IDMATRICULA, IDPREGUNTA, RESPUESTA, FASE, RespuestaBytes) VALUES (@IdMatricula,  @idPregunta2, @Respuesta2, @IdFase, @Respuesta2_bytes);
			END
			IF @idPregunta3 IS NOT NULL AND  @idPregunta3 <> '' 
			BEGIN
				if exists(select * from MatriculaRespuesta where IdMatricula = @IdMatricula and IdPregunta = @idPregunta3)
					update MatriculaRespuesta set Respuesta = @Respuesta3, RespuestaBytes = @Respuesta3_bytes where IdMatricula = @IdMatricula and IdPregunta = @idPregunta3
				else
					INSERT INTO MATRICULARESPUESTA (IDMATRICULA, IDPREGUNTA, RESPUESTA, FASE, RespuestaBytes) VALUES (@IdMatricula,  @idPregunta3, @Respuesta3, @IdFase, @Respuesta3_bytes);
			END
			IF @idPregunta4 IS NOT NULL AND  @idPregunta4 <> '' 
			BEGIN
				if exists(select * from MatriculaRespuesta where IdMatricula = @IdMatricula and IdPregunta = @idPregunta4)
					update MatriculaRespuesta set Respuesta = @Respuesta4, RespuestaBytes = @Respuesta4_bytes where IdMatricula = @IdMatricula and IdPregunta = @idPregunta4
				else
					INSERT INTO MATRICULARESPUESTA (IDMATRICULA, IDPREGUNTA, RESPUESTA, FASE, RespuestaBytes) VALUES (@IdMatricula,  @idPregunta4, @Respuesta4, @IdFase, @Respuesta4_bytes);
			END
			IF @idPregunta5 IS NOT NULL AND  @idPregunta5 <> '' 
			BEGIN
				if exists(select * from MatriculaRespuesta where IdMatricula = @IdMatricula and IdPregunta = @idPregunta5)
					update MatriculaRespuesta set Respuesta = @Respuesta5, RespuestaBytes = @Respuesta5_bytes where IdMatricula = @IdMatricula and IdPregunta = @idPregunta5
				else
					INSERT INTO MATRICULARESPUESTA (IDMATRICULA, IDPREGUNTA, RESPUESTA, FASE, RespuestaBytes) VALUES (@IdMatricula,  @idPregunta5, @Respuesta5, @IdFase, @Respuesta5_bytes);
			END
			IF @idPregunta6 IS NOT NULL AND  @idPregunta6 <> '' 
			BEGIN
				if exists(select * from MatriculaRespuesta where IdMatricula = @IdMatricula and IdPregunta = @idPregunta6)
					update MatriculaRespuesta set Respuesta = @Respuesta6, RespuestaBytes = @Respuesta6_bytes where IdMatricula = @IdMatricula and IdPregunta = @idPregunta6
				else
					INSERT INTO MATRICULARESPUESTA (IDMATRICULA, IDPREGUNTA, RESPUESTA, FASE, RespuestaBytes) VALUES (@IdMatricula,  @idPregunta6, @Respuesta6, @IdFase, @Respuesta6_bytes);
			END
		END
	END
	--si el ultima pregunta saltar al final
	declare @ultimaPregunta as bit = 0;
	select @ultimaPregunta = UltimaPregunta from TFlujoPregunta where Fase = @IdFase and IdPregunta = @idPregunta1 and IdFlujo = @IdFlujo;
	if @ultimaPregunta <> 0 goto ultimaPregunta
	PRINT 'PREGUNTAS';
	if @Pos = 0 --POSICION ACTUAL
	begin
		--PRINT @IDFASE;
		IF @IdFase IS NULL or @IdFase = -1
		BEGIN
			PRINT 'FASE INICIAL';
			DECLARE PREGUNTA_CURSOR CURSOR LOCAL FOR 
				SELECT TOP(1) IdPregunta, TextoPregunta, TipoRespuesta, OrigenDatos, SentenciaSQL, Obligatorio, Orden, Fase, tooltip, ValorPorDefecto
					FROM TFlujoPregunta
					WHERE IdFlujo = @IdFlujo and Activo = 1 and TipoRespuesta <> 'START_FLOW' and TipoRespuesta <> 'FLOW_END'
					GROUP BY Fase, IdPregunta, TextoPregunta, TipoRespuesta, OrigenDatos, SentenciaSQL, Obligatorio, Orden, TOOLTIP, ValorPorDefecto
					ORDER BY FASE ASC, ORDEN ASC
		END
		ELSE
		BEGIN
			DECLARE PREGUNTA_CURSOR CURSOR LOCAL FOR 
				SELECT IdPregunta, TextoPregunta, TipoRespuesta, OrigenDatos, SentenciaSQL, Obligatorio, Orden, fase, tooltip,ValorPorDefecto
					FROM TFlujoPregunta
					WHERE IdFlujo = @IdFlujo and Activo = 1 and Fase = @IdFase
					GROUP BY Fase, IdPregunta, TextoPregunta, TipoRespuesta, OrigenDatos, SentenciaSQL, Obligatorio, Orden, TOOLTIP,ValorPorDefecto
					ORDER BY ORDEN
		END
	end
	
	if @Pos = 1 --EL SIGUIENTE PASO
	begin
		if @IdFlujo is not null and @idPregunta1 is not null
		begin
			declare @condiciona bit = 0;
			declare @condicion varchar(max);
			select @condiciona = CondicionaSigPregunta, @condicion = Condicion from TFlujoPregunta where IdFlujo=@IdFlujo and IdPregunta=@idPregunta1
			if @condiciona = 1
			begin
				print 'hay una condicion';
				set @IDPREGUNTA = dbo.GetFlujoSiguientePregunta(@IdFlujo, @respuesta1, @condicion, 'SIG_PREGUNTA');
				if @IDPREGUNTA <> -1
					DECLARE PREGUNTA_CURSOR CURSOR LOCAL FOR
					SELECT IdPregunta, TextoPregunta, TipoRespuesta, OrigenDatos, SentenciaSQL, Obligatorio, Orden, Fase, tooltip,ValorPorDefecto
					FROM TFlujoPregunta
						WHERE IdFlujo = @IdFlujo and Activo = 1 and IdPregunta = @IDPREGUNTA --and Fase > @IdFase
						GROUP BY Fase,IdPregunta, TextoPregunta, TipoRespuesta, OrigenDatos, SentenciaSQL, Obligatorio, Orden, TOOLTIP,ValorPorDefecto
						ORDER BY Fase, orden
				else
					begin
						print 'idPregunta=1';
						goto ultimaPregunta;
					end
					
			end
		else
			begin
				print 'no hay una condicion';	
				print 'idflujo=' +  CONVERT(varchar(10), @idflujo)
				print 'idPregunta=' +  CONVERT(varchar(10), @IDPREGUNTA)
				print 'idfase=' +  CONVERT(varchar(10), @idfase)
				
				print 'a obtneer el cursor';
				
				DECLARE PREGUNTA_CURSOR CURSOR LOCAL FOR 
				SELECT IdPregunta, TextoPregunta, TipoRespuesta, OrigenDatos, SentenciaSQL, Obligatorio, Orden, Fase, tooltip,ValorPorDefecto
					FROM TFlujoPregunta
					WHERE IdFlujo = @IdFlujo and Activo = 1 and Fase > @IdFase and IdPregunta = (select top(1) IdPregunta FROM TFlujoPregunta WHERE IdFlujo=@IdFlujo
						 AND IdPregunta= (@IDPREGUNTA1 +1) and Fase > @idFase order by Fase asc)
					GROUP BY Fase,IdPregunta, TextoPregunta, TipoRespuesta, OrigenDatos, SentenciaSQL, Obligatorio, Orden, TOOLTIP,ValorPorDefecto
					ORDER BY Fase, orden
				end
		end
		
	end
	if @Pos = -1 --EL ANTERIOR PASO
	begin
		DECLARE PREGUNTA_CURSOR CURSOR LOCAL FOR 
			SELECT  IdPregunta, TextoPregunta, TipoRespuesta, OrigenDatos, SentenciaSQL, Obligatorio, Orden, Fase, tooltip,ValorPorDefecto
				FROM TFlujoPregunta
				WHERE IdFlujo = @IdFlujo and Activo = 1 and Fase = (select top(1) Fase FROM TFlujoPregunta WHERE IdFlujo=@IdFlujo AND Fase < @IdFase order by Fase DESC)
				GROUP BY Fase,IdPregunta, TextoPregunta, TipoRespuesta, OrigenDatos, SentenciaSQL, Obligatorio, Orden, TOOLTIP,ValorPorDefecto
				ORDER BY Fase, orden
		--si voy a la fase anterior elimino las respuestas de esta fase.
		DELETE FROM MATRICULARESPUESTA WHERE IDMATRICULA = @IdMatricula AND FASE= @IdFase;
	end
	
	OPEN PREGUNTA_CURSOR;
	FETCH NEXT FROM PREGUNTA_CURSOR INTO @IDPREGUNTA, @TXTPREGUNTA, @TIPORESPUESTA, @ORIGENDATOS, @SENTENCIASQL, @OBLIGATORIO, @ORDEN, @Fase, @TOOLTIP, @VALORPORDEFECTO;  
	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		PRINT 'WHILEAN';
		print @idpregunta;
		-- Concatenate and display the current values in the variables.  
	   IF @SENTENCIASQL IS NOT NULL AND @SENTENCIASQL <> ''
	   BEGIN
			PRINT @SENTENCIASQL;
			
			INSERT INTO @TRESULTADO exec (@SENTENCIASQL);
			--select @ORIGENDATOS = valoresPosibles from @TRESULTADO;
			select @ORIGENDATOS =  COALESCE(@ORIGENDATOS + '|', '') + valoresPosibles FROM @TRESULTADO
			--PRINT @ORIGENDATOS;
	   END
		SET @RESPUESTA = NULL;
	    
		SELECT @RESPUESTA = RESPUESTA, @TIENE_RESPUESTA_BYTES = CASE WHEN RespuestaBytes IS NULL THEN 0 ELSE 1 END  FROM MATRICULARESPUESTA WHERE IDMATRICULA=@IdMatricula AND IDPREGUNTA=@IDPREGUNTA;
	   
		INSERT INTO  @PREGUNTAS (MATRICULA,IDFLUJO, descripcionFlujo, IDPREGUNTA, TXTPREGUNTA, TOOLTIP, TIPORESPUESTA, OBLIGATORIO, LISTAVALORES, ORDEN, FASE,
			 RESPUESTA, RESUL, RESULMSG, VALORPORDEFECTO, TIENE_RESPUESTABYTES) VALUES 
			 (@IdMatricula, @IdFlujo, @descFlujo, @IDPREGUNTA , @TXTPREGUNTA , @TOOLTIP, @TIPORESPUESTA ,@OBLIGATORIO ,  
			 @ORIGENDATOS, @ORDEN, @FASE , @RESPUESTA, 0, 'OK', @VALORPORDEFECTO, @TIENE_RESPUESTA_BYTES );
		select * from @PREGUNTAS;
	   -- This is executed as long as the previous fetch succeeds.  
	   FETCH NEXT FROM PREGUNTA_CURSOR  
	   INTO @IDPREGUNTA, @TXTPREGUNTA, @TIPORESPUESTA, @ORIGENDATOS, @SENTENCIASQL, @OBLIGATORIO,  @ORDEN,@FASE, @TOOLTIP, @VALORPORDEFECTO; 
	END  
	
	
	
	FIN:
	
	CLOSE PREGUNTA_CURSOR;
	
	DEALLOCATE PREGUNTA_CURSOR;
	
	if exists(SELECT * FROM @PREGUNTAS)
	begin
		--print 'ok'
		SELECT * FROM @PREGUNTAS
	end
	else
	BEGIN
		SELECT @IdMatricula as MATRICULA, @IdFlujo AS IDFLUJO, NULL AS descripcionFlujo,	''  AS IDPREGUNTA , NULL AS TXTPREGUNTA , NULL AS TOOLTIP, 'FLOW_END' AS TIPORESPUESTA , NULL AS OBLIGATORIO,  NULL AS LISTAVALORES,
				 NULL AS ORDEN, -2 AS FASE , NULL AS RESPUESTA, 0 AS RESUL, 'NO HAY MAS PREGUNTAS' AS RESULMSG, null as VALORPORDEFECTO, NULL AS TIENE_RESPUESTABYTES; 
	end
	
	ultimaPregunta:
	SELECT @IdMatricula as MATRICULA, @IdFlujo AS IDFLUJO, NULL AS descripcionFlujo,	'' AS IDPREGUNTA , NULL AS TXTPREGUNTA , NULL AS TOOLTIP,  'FLOW_END' AS TIPORESPUESTA , NULL AS OBLIGATORIO,  NULL AS LISTAVALORES,
				 NULL AS ORDEN, -2 AS FASE , NULL AS RESPUESTA, 0 AS RESUL, 'NO HAY MAS PREGUNTAS' AS RESULMSG, null as VALORPORDEFECTO, NULL AS TIENE_RESPUESTABYTES; 
				 
	
END
