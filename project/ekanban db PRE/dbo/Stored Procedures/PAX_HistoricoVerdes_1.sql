﻿
-- =============================================
-- Author:		IMR
-- Create date: 4-10-2018
-- Description:	Procedimiento para actualizar el histórico con los valores de las tarjetas
-- =============================================
CREATE PROCEDURE [dbo].[PAX_HistoricoVerdes]
(
@Circuito varchar(255)='', @Origen AS varchar(255)='', @Usuario AS varchar(255)=''
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
	BEGIN TRANSACTION;
	
		DECLARE @V as int 
		DECLARE @AE as int
		DECLARE @A as int
		DECLARE @R as int
		DECLARE @SobreStock as int
		DECLARE @UltLec as datetime
		DECLARE @OrigenActualizacion as varchar(255)

		SET @V = dbo.V(@Circuito)
		SET @A = dbo.A(@Circuito)
		SET @R = dbo.R(@Circuito)
		SET @AE = dbo.AE(@Circuito)
		SET @SobreStock=dbo.SS(@Circuito)
		SET @OrigenActualizacion = @Origen
				
		PRINT @Circuito
		
		IF NOT EXISTS (
					   /*SELECT IdCircuito, CtdVerdes, CtdAmarillas, CtdRojas, CtdAmarillasVerdes,
		                      CtdSobreStock
		               FROM THistoricoVerdes
		               WHERE IdCircuito = @Circuito AND CtdVerdes = @V AND CtdRojas = @R AND 
		               CtdAmarillasVerdes = @AE AND CtdSobreStock = @SobreStock */
		               SELECT MAX(fecha) as fecha, IdCircuito, CtdVerdes, CtdAmarillas, CtdRojas, CtdAmarillasVerdes,
		                      CtdSobreStock
		               FROM THistoricoVerdes
		               WHERE IdCircuito = @Circuito AND CtdVerdes = @V AND CtdRojas = @R AND 
								CtdAmarillasVerdes = @AE AND CtdSobreStock = @SobreStock AND
								fecha in (select max(fecha) as fecha from THistoricoVerdes where IdCircuito = @Circuito)
		               GROUP BY IdCircuito, CtdVerdes, CtdAmarillas, CtdRojas, CtdAmarillasVerdes,
		                      CtdSobreStock
		               )
			BEGIN
				INSERT INTO ekanban.dbo.THistoricoVerdes (IdCircuito, Fecha, CtdVerdes, CtdAmarillas, CtdRojas, CtdAmarillasVerdes,
															CtdSobreStock,tag, origen, Usuario)
				values (@Circuito, getdate(), @V,@A,@R, @AE, @SobreStock, null, @OrigenActualizacion, @Usuario)
			END
			
		----20181217 INCLUSION ALERTAS ESTADO
		exec dbo.alertasEstado_estado @Circuito = @Circuito--, @Usuario = @Usuario
		
	COMMIT TRANSACTION;
	
END