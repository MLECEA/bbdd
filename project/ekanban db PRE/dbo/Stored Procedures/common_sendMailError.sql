﻿/*******************************************************************************************
 *
 * sendMailError
 *
 * Envía un mail de error
 *
 ********************************************************************************************/
CREATE PROCEDURE [dbo].[common_sendMailError]
	( --@subject as VARCHAR(255),
	  --@to as VARCHAR(1000),
	  --@cc as VARCHAR(1000) = null,
	  --@body as VARCHAR(5000),
	  @usuario as VARCHAR(250) = null
	)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		DECLARE @subject as VARCHAR(250) 
		DECLARE @to as VARCHAR(250) 
		DECLARE @body as VARCHAR(5000)
					
		SET @subject = N'PREPRODUCCION ' + 'Error eKanban'
			
		SET @to = 'iratxem@cyc.es;mlecea@orkli.es' --aquí se pondría el mail al que se quiere que lleguen los mensajes de preproducción

		SET @body = N'Error' + CHAR(10) + CHAR(13) +  
					CAST(sysdatetime() AS nvarchar(max)) + ' '  + ISNULL(@usuario, '') + CHAR(10) + CHAR(13) + 
					'Error Number:  ' + CAST(ERROR_NUMBER() AS nvarchar(max)) + CHAR(10) + CHAR(13) + 
					'Error Severity:  ' + CAST(ERROR_SEVERITY() AS nvarchar(max)) + CHAR(10) + CHAR(13) + 
					'Error State:  ' + CAST(ERROR_STATE() AS nvarchar(max)) + CHAR(10) + CHAR(13) + 
					'Error Procedure:  ' + CAST(ERROR_PROCEDURE() AS nvarchar(max))	+ CHAR(10) + CHAR(13) + 
					'Error Line:  ' + CAST(ERROR_LINE() AS nvarchar(max)) + CHAR(10) + CHAR(13) + 
					'Error Message: ' + ERROR_MESSAGE();

		EXEC dbo.common_sendMail @subject = @subject, @to = @to, @body = @body, @usuario = @usuario
			
	END TRY
	BEGIN CATCH
		select @usuario as ERROR_USER,
			 ERROR_NUMBER() as ERROR_NUMBER,
			 ERROR_STATE() as ERROR_STATE,
			 ERROR_SEVERITY() as ERROR_SEVERITY,
			 ERROR_LINE() as ERROR_LINE,
			 ERROR_PROCEDURE() as ERROR_PROCEDURE,
			 ERROR_MESSAGE() as ERROR_MESSAGE,
			 GETDATE() as date
	END CATCH
END