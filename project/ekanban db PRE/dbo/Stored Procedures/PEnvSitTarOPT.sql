﻿
CREATE PROCEDURE [dbo].[PEnvSitTarOPT] 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	 DECLARE @tableHTML  NVARCHAR(MAX) ;

	SET @tableHTML =
    N'<H1>eKanban Tarjetas Optimas</H1>' +
    N'<table border="1">' +
    N'<tr><th>Eguna</th>' +
	N'<th>Kodea</th>' +
    N'<th>Izena</th>' +
	N'<th>Kokapena</th>' +
	N'<th>Deskribapena</th>' +
    N'<th>Artikulua</th>' +
	N'<th>Bezeroaren Erref</th>' +
	N'<th>Txartelak</th>' +
	N'<th>OPTIMOAK</th>' +
	N'<th>Diferentzia</th>' +
		N'<th>Pzak/Kanban</th>' +
	N'</tr>' +
    CAST ( ( SELECT "td/@align" =  'center', td = dbo.TRegistroOptHis.id_Reg,   ''--convert(varchar(50),  dbo.TRegistroOptHis.id_Fec , 120)
		,td = dbo.circuito.cliente_id,   ''
		,td =   dbo.circuito.cliente_nombre,   ''
		,td =   dbo.circuito.ubicacion,   ''
		,td =  dbo.ubicacion.descripcion,   ''
		,td =  dbo.circuito.referencia_id,   ''
		,td =    dbo.circuito.ref_cliente,   ''
		,"td/@align" =  'center', td =   dbo.TRegistroOptHis.num_kanbanAct,   ''
		,"td/@align" =  'center', td =   dbo.TRegistroOptHis.num_kanbanOpt,   ''
		,"td/@align" =  'center',  "td/@bgcolor" =  case when ((dbo.TRegistroOptHis.num_kanbanOpt*100/dbo.TRegistroOptHis.num_kanbanAct)>50 and (dbo.TRegistroOptHis.num_kanbanOpt*100/dbo.TRegistroOptHis.num_kanbanAct)<120) then 'green' ELSE 'yellow' END, td = dbo.TRegistroOptHis.num_kanbanDif,  '' --case when [Dis_EnError]=0 then 'OK' ELSE 'ERR' END--   
		,"td/@align" =  'right', td =   dbo.TRegistroOptHis.cantidad_piezas,   ''
    --  ,td = convert(varchar(50), [ultima_conexion], 120),   ''
	--  ,td = 'https://' + [ip] + ':10000',   ''
    --  ,"td/@align" =  'center', td = [version], '',td=convert(varchar(50), [fecha_inicio], 120)
		FROM            dbo.circuito INNER JOIN
		dbo.TRegistroOptHis ON dbo.circuito.id_circuito = dbo.TRegistroOptHis.id_circuito INNER JOIN
		dbo.ubicacion ON dbo.circuito.ubicacion = dbo.ubicacion.id_ubicacion
		WHERE        (dbo.circuito.activo = 1) AND (dbo.TRegistroOptHis.id_Vig = 1) and  (NOT (dbo.circuito.cliente_id LIKE '0000%'))

		ORDER BY dbo.circuito.cliente_id, dbo.circuito.ubicacion,  num_kanban DESC,  dbo.circuito.referencia_id

	  FOR XML PATH('tr'), TYPE 
		) AS NVARCHAR(MAX) ) +
		N'</table>' ;


	EXEC msdb.dbo.sp_send_dbmail 
	  @profile_name='posta',
	  @recipients='aarandia@orkli.es; eunanue@orkli.es',
	  @subject='eKanban Tarjetas Optimas',
	  @body = @tableHTML,
	  @body_format = 'HTML'
	  --@body='Datos de los porticos RFID:',
	  --@query = 'SELECT id_dispositivo, descripcion, ultima_conexion, Dis_EnError, ip, version FROM  ekanban.dbo.dispositivo' ,
	  --@query_result_header=0,
	  --@attach_query_result_as_file = 0
	  --@file_attachments='C:\temp\ik.txt'





END


