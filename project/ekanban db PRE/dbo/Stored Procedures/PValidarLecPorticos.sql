﻿

CREATE PROCEDURE [dbo].[PValidarLecPorticos] 
--DE MOMENTO SE LANZA CADA VEZ QUE HAY ACTUALIZACIÓN DE DISPOSITIVOS, COMO TRIGUER DE SEGUIMINETO DE ERRORES, AL FINAL DEL CODIGO
--ES DEMASIADO, Y SE PODRÍA PONER EN ACTUALIZACIÓN DE TARJETA, PERO NO GARANTIZARÍAMOS QUE SE BORRE AL TIEMPO, SI ES QUE NO HAY MÁS LECTURAS. 

AS

BEGIN

	--DECLARE	@return_value int
	DECLARE @Resul as varchar(100)
	DECLARE @Emaitza as int
	DECLARE @TConsLecPor as int
	DECLARE @NMinLecPor as int
	declare @Lecturas as int
	DECLARE @HayLecMin as varchar(100)
	DECLARE @Cli as varchar(100)
	DECLARE @Art as varchar(100)
	DECLARE @Ubi as varchar(100)

	set @Emaitza=1  --por defecto como error
	set @HayLecMin=''

	--Recuperamos los parámetros para considerar o no válido las lecturas de palet por el portico
	SELECT        @TConsLecPor=valor
	FROM            dbo.sga_parametros
	WHERE        (nombre = 'TMaxEntreLecturasPor')

	SELECT        @NMinLecPor=valor
	FROM            dbo.sga_parametros
	WHERE        (nombre = 'NMinLecturasPor')

	set @TConsLecPor=isnull(@TConsLecPor,0)
	set @NMinLecPor=isnull(@NMinLecPor,0)

	--REVISAMOS QUE TENEMOS CONEXIÓN DURANTE LOS ÚLTIMOS X SEGUNDOS, PARA RECIBIR LECTURAS, SI ES QUE NO. YA PROBAREMOS MÁS ADELANTE.
	--DE MOMENTO SIN IMPLANTAR, YA QUE NO SABEMOS SI VA A SER DE UN ARCO O DE OTRO. CONSIDERAMOS QUE SI HA LLEGADO LECTURA INICIAL, DEBERÍA DE 
	--HABER TENIDO CONNEXIÓN. SI VUELVE A ENVIAR, YA REACTIVAREMOS. 
	
	--PARA ACTUALIZAR A GESTIONADO, AQUELLAS LECTURAS DE CIRCUITOS A NIVEL DE PALET, QUE TENGAN MENOS LECTURAS AL DEFINIDO Y QUE HAYA PASADO YA EL TIEMPO MAXIMO
	--ENTRE LECTURAS.


	select @HayLecMin=id_lectura, @Lecturas=dbo.tarjeta.num_lecturas, @cli=dbo.circuito.cliente_nombre, @Art=dbo.circuito.referencia_id, @NMinLecPor=dbo.circuito.Cir_LecMinParaCons, @Ubi=dbo.circuito.ubicacion
	from 	dbo.tarjeta INNER JOIN  dbo.circuito 
	ON dbo.tarjeta.id_circuito = dbo.circuito.id_circuito AND dbo.tarjeta.num_lecturas < dbo.circuito.Cir_LecMinParaCons
	WHERE        (dbo.tarjeta.tipo = 'P') AND (dbo.circuito.tipo_contenedor = 2) AND (dbo.tarjeta.estado = 'P') AND (dbo.tarjeta.gestionado IS NULL OR
                         dbo.tarjeta.gestionado = '') AND (DATEADD(ss, @TConsLecPor, dbo.tarjeta.ultima_lectura) < GETDATE())
	

	set @HayLecMin= isnull(@HayLecMin,'')
	set @Lecturas= isnull(@Lecturas,0)
	set @NMinLecPor=isnull(@NMinLecPor,1)


	declare @cad varchar(200)
	set @cad = 'Se han realizado ' + CONVERT(CHAR(3),@Lecturas) + 'lecturas de '+ @HayLecMin + ' en ' + @cli + ' (' + @Ubi + '), y el mínimo debe ser de ' + CONVERT(CHAR(3),@NMinLecPor)  + '. '
	SET @cad= @cad + ' Revisar si realmente se debe considerar o no la lectura. Artículo ' + @Art + '.'

	if @HayLecMin<>''  
	begin 
		EXEC	[dbo].[PEnvCorreo]
		@Des = N'mmujika@orkli.es; dallodoli@orkli.it; alert@ekanban.es', 
		@Tem = N'Faltan Lecturas Mínimas',
		@Cue = @cad
	end 



	--PARA ANULAR LECTURAS QUE NO HAYA CTD MÍNIMA EN TIEMPO MÁXIMO
	update   dbo.tarjeta 
	SET gestionado = 'GesAUT_NumLecMin', estado='T'
	from
	dbo.tarjeta INNER JOIN  dbo.circuito ON dbo.tarjeta.id_circuito = dbo.circuito.id_circuito
	WHERE      id_lectura=@HayLecMin AND sentido='S'
	


	--PARA RECUPERAR LECTURAS VIEJAS, QUE SE ANULARON PORQUE HAYA LECTURA NUEVA
	update   dbo.tarjeta 
	SET gestionado = '', estado='P', dbo.tarjeta.num_lecturas=1
	from
	dbo.tarjeta
	WHERE        (gestionado = 'GesAUT_NumLecMin') AND (DATEADD(ss, @TConsLecPor, ultima_lectura) > GETDATE()) -- (estado = 'T') AND
	
END

