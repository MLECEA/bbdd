﻿CREATE VIEW dbo.VTarGestionadas
AS
SELECT        TOP (100) PERCENT dbo.tarjeta.cliente_id, dbo.tarjeta.ubicacion, dbo.ubicacion.descripcion, dbo.tarjeta.referencia_id, dbo.circuito.ref_cliente, dbo.tarjeta.sentido, dbo.tarjeta.fecha_lectura, dbo.tarjeta.id_lectura, 
                         dbo.circuito.cantidad_piezas, dbo.tarjeta.estado, dbo.tarjeta.gestionado, dbo.tarjeta.Tar_GestionadoUSU, dbo.tarjeta.Tar_GestionadoFEC, DATEPART(yy, dbo.tarjeta.Tar_GestionadoFEC) AS año, DATEPART(ww, 
                         dbo.tarjeta.Tar_GestionadoFEC) AS sem, DATEPART(dw, dbo.tarjeta.Tar_GestionadoFEC) AS diaSem, SUBSTRING(CONVERT(nvarchar(50), dbo.tarjeta.Tar_GestionadoFEC, 120), 1, 16) AS FecGest
FROM            dbo.tarjeta INNER JOIN
                         dbo.circuito ON dbo.tarjeta.cliente_id = dbo.circuito.cliente_id AND dbo.tarjeta.id_circuito = dbo.circuito.id_circuito AND dbo.tarjeta.referencia_id = dbo.circuito.referencia_id AND 
                         dbo.tarjeta.ubicacion = dbo.circuito.ubicacion INNER JOIN
                         dbo.ubicacion ON dbo.circuito.ubicacion = dbo.ubicacion.id_ubicacion
WHERE        (dbo.tarjeta.Tar_GestionadoUSU <> N'""')
ORDER BY dbo.tarjeta.cliente_id, dbo.tarjeta.ubicacion, dbo.tarjeta.referencia_id, CONVERT(nvarchar(50), dbo.tarjeta.Tar_GestionadoFEC, 120) DESC, dbo.tarjeta.fecha_lectura DESC

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[28] 4[39] 2[5] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tarjeta"
            Begin Extent = 
               Top = 2
               Left = 79
               Bottom = 192
               Right = 272
            End
            DisplayFlags = 280
            TopColumn = 11
         End
         Begin Table = "circuito"
            Begin Extent = 
               Top = 6
               Left = 477
               Bottom = 196
               Right = 653
            End
            DisplayFlags = 280
            TopColumn = 9
         End
         Begin Table = "ubicacion"
            Begin Extent = 
               Top = 4
               Left = 718
               Bottom = 134
               Right = 888
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 19
         Width = 284
         Width = 1500
         Width = 735
         Width = 1200
         Width = 1290
         Width = 1155
         Width = 705
         Width = 2580
         Width = 1350
         Width = 525
         Width = 495
         Width = 1530
         Width = 1500
         Width = 2280
         Width = 750
         Width = 450
         Width = 810
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 6075
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VTarGestionadas';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VTarGestionadas';

