﻿CREATE TABLE [dbo].[tipo_contenedor] (
    [id]          NUMERIC (19)  NOT NULL,
    [descripcion] VARCHAR (255) NULL,
    [nombre]      VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

