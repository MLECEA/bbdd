﻿CREATE TABLE [dbo].[alertasEstado_alertasEnviadas] (
    [circuitoId]     VARCHAR (255) NOT NULL,
    [referenciaId]   VARCHAR (255) NOT NULL,
    [estadoAnterior] VARCHAR (255) NOT NULL,
    [estadoNuevo]    VARCHAR (255) NOT NULL,
    [ubicacion]      VARCHAR (255) NULL,
    [proveedorId]    VARCHAR (255) NULL,
    [clienteId]      VARCHAR (255) NULL,
    [fecha]          DATETIME      NOT NULL, 
    CONSTRAINT [PK_alertasEstado_alertasEnviadas] PRIMARY KEY ([circuitoId], [referenciaId],[fecha])
);

