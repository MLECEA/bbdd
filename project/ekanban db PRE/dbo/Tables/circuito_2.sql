﻿CREATE TABLE [dbo].[circuito] (
    [cliente_id]              VARCHAR (255) NOT NULL,
    [id_circuito]             VARCHAR (255) NOT NULL,
    [referencia_id]           VARCHAR (255) NOT NULL,
    [activo]                  BIT           NOT NULL,
    [cantidad_piezas]         INT           NOT NULL,
    [cliente_nombre]          VARCHAR (255) NULL,
    [descripcion]             VARCHAR (255) NULL,
    [entrega_dias]            INT           NOT NULL,
    [entrega_horas]           INT           NOT NULL,
    [mail]                    VARCHAR (255) NULL,
    [num_kanban]              INT           NOT NULL,
    [ref_cliente]             VARCHAR (255) NULL,
    [reserva_auto]            BIT           NOT NULL,
    [ubicacion]               VARCHAR (255) NOT NULL,
    [tipo_circuito]           NUMERIC (19)  NOT NULL,
    [tipo_contenedor]         NUMERIC (19)  NOT NULL,
    [Cir_UbiAnt]              VARCHAR (3)   NULL,
    [Cir_LanPedAut]           BIT           NULL,
    [Cir_LunM]                INT           NULL,
    [Cir_LunT]                INT           NULL,
    [Cir_MarM]                INT           NULL,
    [Cir_MarT]                INT           NULL,
    [Cir_MieM]                INT           NULL,
    [Cir_MieT]                INT           NULL,
    [Cir_JueM]                INT           NULL,
    [Cir_JueT]                INT           NULL,
    [Cir_VieM]                INT           NULL,
    [Cir_VieT]                INT           NULL,
    [Proveedor_id]            NCHAR (20)    NULL,
    [Cir_HoraM]               TIME (7)      NULL,
    [Cir_HoraT]               TIME (7)      NULL,
    [Cir_SabM]                INT           NULL,
    [Cir_SabT]                INT           NULL,
    [Cir_DomM]                INT           NULL,
    [Cir_DomT]                INT           NULL,
    [Cir_RAnt]                INT           NULL,
    [Cir_RFec]                DATETIME      NULL,
    [Cir_Consigna]            BIT           NULL,
    [Cir_ConSalesType]        NCHAR (2)     NULL,
    [Cir_ConInventLocationId] NCHAR (25)    NULL,
    [Cir_ConFacturarAut]      INT           NULL,
    [Cir_ConNumFactCli]       NCHAR (50)    NULL,
    [Cir_StdSalesType]        NCHAR (2)     NULL,
    [Cir_StdInventLocationId] NCHAR (25)    NULL,
    [Cir_PedMarco]            NCHAR (20)    NULL,
    [Cir_AlbaranarAut]        INT           NULL,
    [Cir_IntLun]              BIT           NULL,
    [Cir_IntMar]              BIT           NULL,
    [Cir_IntMie]              BIT           NULL,
    [Cir_IntJue]              BIT           NULL,
    [Cir_IntVie]              BIT           NULL,
    [Cir_IntSab]              BIT           NULL,
    [Cir_IntDom]              BIT           NULL,
    [Cir_LanPed5MI]           BIT           NULL,
    [Cir_ConFacturarAgrup]    BIT           NULL,
    [Cir_RegistrarPedCompra]  BIT           NULL,
    [Cir_LecMinParaCons]      INT           CONSTRAINT [DF_circuito_Cir_LecMinParaCons] DEFAULT ((1)) NOT NULL,
    [num_kanbanOPT]           INT           NULL,
    [Cir_ReaprovAgrup]        BIT           NULL,
    [Cir_Empresa]             NCHAR (3)     NULL,
    [Cir_DupConsCir]          VARCHAR (255) NULL,
    [Cir_NumPedCli]           NCHAR (50)    NULL,
    [TransitoActivado]        BIT           NULL,
    [ref_IntegracionERP]      VARCHAR (255) NULL,
    CONSTRAINT [PK__circuito__BE13CAC00D7A0286] PRIMARY KEY CLUSTERED ([cliente_id] ASC, [id_circuito] ASC, [referencia_id] ASC, [ubicacion] ASC),
    CONSTRAINT [FK_3vn6jckmib1of620by1jg35a6] FOREIGN KEY ([tipo_circuito]) REFERENCES [dbo].[tipo_circuito] ([id]),
    CONSTRAINT [FK_gpat2w8x7tswwv6tmkaq9u1po] FOREIGN KEY ([tipo_contenedor]) REFERENCES [dbo].[tipo_contenedor] ([id]),
    CONSTRAINT [FK_iejhi9irkh7tx1ji2y8t5k47k] FOREIGN KEY ([ubicacion]) REFERENCES [dbo].[ubicacion] ([id_ubicacion])
);


GO
CREATE NONCLUSTERED INDEX [IdCircuito]
    ON [dbo].[circuito]([id_circuito] ASC);


GO
CREATE NONCLUSTERED INDEX [activoTipoCircuito]
    ON [dbo].[circuito]([tipo_circuito] ASC, [activo] ASC)
    INCLUDE([cliente_id], [id_circuito], [referencia_id], [num_kanban], [ref_cliente], [ubicacion]);


GO
CREATE NONCLUSTERED INDEX [Ubicacion]
    ON [dbo].[circuito]([ubicacion] ASC);


GO
CREATE NONCLUSTERED INDEX [ActivoUbicacion]
    ON [dbo].[circuito]([ubicacion] ASC, [activo] ASC);


GO
CREATE NONCLUSTERED INDEX [UbicClienteRef]
    ON [dbo].[circuito]([cliente_id] ASC, [referencia_id] ASC, [ubicacion] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Cantidad mínima a considerar para que una lectura se considere como consumo', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'circuito', @level2type = N'COLUMN', @level2name = N'Cir_LecMinParaCons';

