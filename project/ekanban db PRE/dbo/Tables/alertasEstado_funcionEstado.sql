﻿CREATE TABLE [dbo].[alertasEstado_funcionEstado] (
    [clienteId]                  VARCHAR (255) NOT NULL,
    [tipoCircuito]               NUMERIC (19)  NOT NULL,
    [funcionAlertasCambioEstado] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_alertasEstado_funcionEstado] PRIMARY KEY CLUSTERED ([tipoCircuito] ASC, [clienteId] ASC)
);

