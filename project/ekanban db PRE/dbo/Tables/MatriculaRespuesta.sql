﻿CREATE TABLE [dbo].[MatriculaRespuesta] (
    [IdMatricula]    INT             NOT NULL,
    [IdPregunta]     INT             NOT NULL,
    [Respuesta]      VARCHAR (255)   NULL,
    [Fase]           INT             NULL,
    [RespuestaBytes] VARBINARY (MAX) NULL,
    CONSTRAINT [PK_MatriculaRespuesta] PRIMARY KEY CLUSTERED ([IdMatricula] ASC, [IdPregunta] ASC)
);



