﻿CREATE TABLE [dbo].[circuitosEspeciales] (
    [circuitoId]     VARCHAR (255) NOT NULL,
    [circuitoHijoId] VARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([circuitoId] ASC, [circuitoHijoId] ASC)
);


