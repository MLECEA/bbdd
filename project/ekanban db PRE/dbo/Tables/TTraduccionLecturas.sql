﻿CREATE TABLE [dbo].[TTraduccionLecturas] (
    [lectura]    NCHAR (255) NULL,
    [fecha]      DATETIME    NULL,
    [ubicacion]  NCHAR (100) NULL,
    [articulo]   NCHAR (100) NULL,
    [lote]       NCHAR (100) NULL,
    [numserie]   NCHAR (100) NULL,
    [cantidad]   NCHAR (100) NULL,
    [usuario]    NCHAR (100) NULL,
    [modificado] NCHAR (100) DEFAULT (sysdatetime()) NULL
);




GO
CREATE UNIQUE CLUSTERED INDEX [IDX_TTraduccionLecturas_lectura]
    ON [dbo].[TTraduccionLecturas]([lectura] ASC);

