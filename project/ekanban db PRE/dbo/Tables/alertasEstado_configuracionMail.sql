﻿CREATE TABLE [dbo].[alertasEstado_configuracionMail] (
    [cliente_id]         VARCHAR (255) NOT NULL,
    [mailAmarilloAsunto] VARCHAR (255) NOT NULL,
    [mailAmarilloPara]   VARCHAR (255) NOT NULL,
    [mailAmarilloCC]     VARCHAR (255) NULL,
    [mailRojoAsunto]     VARCHAR (255) NOT NULL,
    [mailRojoPara]       VARCHAR (255) NOT NULL,
    [mailRojoCC]         VARCHAR (255) NULL,
    [mailVerdeAsunto] VARCHAR(255) NULL, 
    [mailVerdePara] VARCHAR(255) NULL, 
    [mailVerdeCC] VARCHAR(255) NULL, 
    PRIMARY KEY CLUSTERED ([cliente_id] ASC)
);



