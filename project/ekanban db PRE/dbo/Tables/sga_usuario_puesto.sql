﻿CREATE TABLE [dbo].[sga_usuario_puesto] (
    [ptr_usuario] NUMERIC (19) NOT NULL,
    [ptr_puesto]  NUMERIC (19) NULL,
    CONSTRAINT [FKEB3B4B51F04FDB86] FOREIGN KEY ([ptr_usuario]) REFERENCES [dbo].[sga_usuario] ([cod_usuario])
);

