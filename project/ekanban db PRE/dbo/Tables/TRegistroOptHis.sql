﻿CREATE TABLE [dbo].[TRegistroOptHis] (
    [id_Reg]          VARCHAR (50)  NOT NULL,
    [id_Fec]          DATETIME      NOT NULL,
    [id_Vig]          INT           NOT NULL,
    [id_circuito]     VARCHAR (255) NULL,
    [cliente_id]      VARCHAR (255) NULL,
    [ubicacion]       VARCHAR (255) NULL,
    [referencia_id]   VARCHAR (255) NULL,
    [num_kanbanAct]   INT           NULL,
    [num_kanbanOpt]   INT           NULL,
    [num_kanbanDif]   INT           NULL,
    [MedDia]          INT           NULL,
    [cantidad_piezas] INT           NULL,
    [MedDiaTarKanban] INT           NULL,
    [NumEnvios]       INT           NULL,
    [DemoraDias]      INT           NULL,
    [DiasTrans]       INT           NULL,
    [HorasTrans]      INT           NULL
);

