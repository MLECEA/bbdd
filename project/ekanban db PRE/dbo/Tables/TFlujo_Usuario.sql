﻿CREATE TABLE [dbo].[TFlujo_Usuario] (
    [IdFlujo]     INT           NOT NULL,
    [IdUsuario]   NUMERIC (19)  NOT NULL,
    [Prioridad]   INT           NULL,
    [Descripcion] VARCHAR (255) NULL,
    CONSTRAINT [PK_TFlujo_Usuario] PRIMARY KEY CLUSTERED ([IdFlujo] ASC, [IdUsuario] ASC),
    CONSTRAINT [FK_TFlujoUsuario_IdFlujo] FOREIGN KEY ([IdFlujo]) REFERENCES [dbo].[TFlujo] ([IdFlujo])
);



