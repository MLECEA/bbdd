﻿CREATE TABLE [dbo].[sga_rol_privilegio] (
    [ptr_rol]        NUMERIC (19)  NOT NULL,
    [cod_privilegio] VARCHAR (255) NULL,
    CONSTRAINT [FKAF59FABC40F68DC8] FOREIGN KEY ([ptr_rol]) REFERENCES [dbo].[sga_rol] ([cod_rol])
);

