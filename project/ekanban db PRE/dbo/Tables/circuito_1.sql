﻿CREATE TABLE [dbo].[circuito] (
    [cliente_id]              VARCHAR (255) NOT NULL,
    [id_circuito]             VARCHAR (255) NOT NULL,
    [referencia_id]           VARCHAR (255) NOT NULL,
    [activo]                  BIT           NOT NULL,
    [cantidad_piezas]         INT           NOT NULL,
    [cliente_nombre]          VARCHAR (255) NULL,
    [descripcion]             VARCHAR (255) NULL,
    [entrega_dias]            INT           NOT NULL,
    [entrega_horas]           INT           NOT NULL,
    [mail]                    VARCHAR (255) NULL,
    [num_kanban]              INT           NOT NULL,
    [ref_cliente]             VARCHAR (255) NULL,
    [reserva_auto]            BIT           NOT NULL,
    [ubicacion]               VARCHAR (255) NOT NULL,
    [tipo_circuito]           NUMERIC (19)  NOT NULL,
    [tipo_contenedor]         NUMERIC (19)  NOT NULL,
    [Cir_UbiAnt]              VARCHAR (3)   NULL,
    [Cir_LanPedAut]           BIT           NULL,
    [Cir_LunM]                INT           NULL,
    [Cir_LunT]                INT           NULL,
    [Cir_MarM]                INT           NULL,
    [Cir_MarT]                INT           NULL,
    [Cir_MieM]                INT           NULL,
    [Cir_MieT]                INT           NULL,
    [Cir_JueM]                INT           NULL,
    [Cir_JueT]                INT           NULL,
    [Cir_VieM]                INT           NULL,
    [Cir_VieT]                INT           NULL,
    [Proveedor_id]            NCHAR (20)    NULL,
    [Cir_HoraM]               TIME (7)      NULL,
    [Cir_HoraT]               TIME (7)      NULL,
    [Cir_SabM]                INT           NULL,
    [Cir_SabT]                INT           NULL,
    [Cir_DomM]                INT           NULL,
    [Cir_DomT]                INT           NULL,
    [Cir_RAnt]                INT           NULL,
    [Cir_RFec]                DATETIME      NULL,
    [Cir_Consigna]            BIT           NULL,
    [Cir_ConSalesType]        NCHAR (2)     NULL,
    [Cir_ConInventLocationId] NCHAR (25)    NULL,
    [Cir_ConFacturarAut]      INT           NULL,
    [Cir_ConNumFactCli]       NCHAR (50)    NULL,
    [Cir_StdSalesType]        NCHAR (2)     NULL,
    [Cir_StdInventLocationId] NCHAR (25)    NULL,
    [Cir_PedMarco]            NCHAR (20)    NULL,
    [Cir_AlbaranarAut]        INT           NULL,
    [Cir_IntLun]              BIT           NULL,
    [Cir_IntMar]              BIT           NULL,
    [Cir_IntMie]              BIT           NULL,
    [Cir_IntJue]              BIT           NULL,
    [Cir_IntVie]              BIT           NULL,
    [Cir_IntSab]              BIT           NULL,
    [Cir_IntDom]              BIT           NULL,
    [Cir_LanPed5MI]           BIT           NULL,
    [Cir_ConFacturarAgrup]    BIT           NULL,
    [Cir_RegistrarPedCompra]  BIT           NULL,
    [Cir_LecMinParaCons]      INT            NOT NULL,
    [num_kanbanOPT]           INT           NULL,
    [Cir_ReaprovAgrup]        BIT           NULL,
    [Cir_Empresa]             NCHAR (3)     NULL,
    [Cir_DupConsCir]          VARCHAR (255) NULL,
    [Cir_NumPedCli]           NCHAR (50)    NULL,
    [TransitoActivado]        BIT           NULL,
    [ref_IntegracionERP]      VARCHAR (255) NULL,
    ,
    ,
    ,
    
);




GO



GO



GO



GO



GO



GO


