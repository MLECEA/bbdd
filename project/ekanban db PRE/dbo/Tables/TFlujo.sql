﻿CREATE TABLE [dbo].[TFlujo] (
    [IdFlujo]     INT           NOT NULL,
    [Descripcion] VARCHAR (255) NULL,
    [Activo]      BIT           NULL,
    [IconoURL]    VARCHAR (255) NULL,
    CONSTRAINT [PK_TFlujo] PRIMARY KEY CLUSTERED ([IdFlujo] ASC)
);

