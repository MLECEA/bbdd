﻿CREATE TABLE [dbo].[ubicacion] (
    [id_ubicacion]     VARCHAR (255) NOT NULL,
    [activo]           TINYINT       NOT NULL,
    [descripcion]      VARCHAR (255) NULL,
    [localizacion]     VARCHAR (255) NULL,
    [Ubi_DesLoc]       VARCHAR (255) NULL,
    [Ubi_MosPenFac]    BIT           NULL,
    [Ubi_MsgPenFac]    NCHAR (150)   NULL,
    [Ubi_RegTrazAX]    BIT           NULL,
    [Ubi_Dataareaid]   NCHAR (10)    NULL,
    [Ubi_Modo]         NCHAR (4)     CONSTRAINT [DF_ubicacion_Ubi_Modo] DEFAULT (N'EVEN') NULL,
    [Ubi_NotEtiVacias] BIT           CONSTRAINT [DF_ubicacion_Ubi_NotEtiVacias] DEFAULT ((1)) NULL,
    [ubi_des_loc]      VARCHAR (255) NULL,
    [TipoEtiqueta]     VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([id_ubicacion] ASC)
);

