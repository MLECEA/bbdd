﻿CREATE TABLE [dbo].[p_leer_ficha] (
    [artcli]           VARCHAR (255) NOT NULL,
    [art]              VARCHAR (255) NULL,
    [ctdcaja]          INT           NULL,
    [descrip]          VARCHAR (255) NULL,
    [des]              VARCHAR (255) NULL,
    [eng]              VARCHAR (255) NULL,
    [fecprod]          VARCHAR (255) NULL,
    [lin]              VARCHAR (255) NULL,
    [ofab]             VARCHAR (255) NULL,
    [tctdcaja]         VARCHAR (255) NULL,
    [tdes]             VARCHAR (255) NULL,
    [teng]             VARCHAR (255) NULL,
    [articulo_cliente] VARCHAR (255) NOT NULL,
    [articulo]         VARCHAR (255) NULL,
    [deseng]           VARCHAR (255) NULL,
    [fprod]            VARCHAR (255) NULL,
    [linea]            VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([artcli] ASC)
);

