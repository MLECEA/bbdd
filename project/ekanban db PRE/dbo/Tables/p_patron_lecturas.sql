﻿CREATE TABLE [dbo].[p_patron_lecturas] (
    [grab] INT           NOT NULL,
    [ctd]  VARCHAR (255) NULL,
    [cli]  VARCHAR (255) NULL,
    [ser]  VARCHAR (255) NULL,
    [lot]  VARCHAR (255) NULL,
    [pro]  DATETIME      NULL,
    [art]  VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([grab] ASC)
);

