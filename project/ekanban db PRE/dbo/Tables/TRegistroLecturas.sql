﻿CREATE TABLE [dbo].[TRegistroLecturas] (
    [Reg_Fec] DATETIME    NULL,
    [Reg_Dis] NCHAR (100) NULL,
    [Reg_Usu] NCHAR (100) NULL,
    [Reg_Ubi] NCHAR (100) NULL,
    [Reg_Pan] NCHAR (100) NULL,
    [Reg_Txt] NCHAR (255) NULL,
    [Reg_Cir] NCHAR (100) NULL,
    [Reg_Cli] NCHAR (100) NULL,
    [Reg_Art] NCHAR (100) NULL,
    [Reg_Lot] NCHAR (100) NULL,
    [Reg_Ser] NCHAR (100) NULL,
    [Reg_Ant] NCHAR (50)  NULL,
    [Reg_Sen] NCHAR (5)   NULL,
    [Reg_Tip] INT         NULL,
    [Reg_Ctd] NCHAR (50)  NULL,
    [Reg_Emp] NCHAR (10)  NULL
);






GO
CREATE NONCLUSTERED INDEX [Index]
    ON [dbo].[TRegistroLecturas]([Reg_Lot] ASC, [Reg_Ser] ASC);


GO
CREATE NONCLUSTERED INDEX [Fecha]
    ON [dbo].[TRegistroLecturas]([Reg_Fec] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_TRegistroLecturas_LecturaFecha]
    ON [dbo].[TRegistroLecturas]([Reg_Txt] ASC, [Reg_Fec] ASC);


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TRG_ActTraduccionLecturas]
   ON  [dbo].[TRegistroLecturas]
   AFTER  INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRY
		print '[TRG_ActTraduccionLecturas] inicio'
		DECLARE @lectura AS NCHAR(255)
		DECLARE @fecha AS DATETIME
		DECLARE @ubicacion AS NCHAR(100)
		DECLARE @articulo AS NCHAR(100)
		DECLARE @lote AS NCHAR(100)
		DECLARE @serie AS NCHAR(100)
		DECLARE @cantidad AS NCHAR(50)
		DECLARE @usuario AS NCHAR(100)
		
		select @lectura = i.Reg_Txt, @fecha = i.reg_fec, @ubicacion = i.Reg_Ubi, @articulo = i.reg_art,
		 @lote = i.reg_lot, @serie = i.reg_ser, @cantidad = i.reg_ctd, @usuario = i.reg_usu
		FROM INSERTED i
		
		IF @@ROWCOUNT > 0 
			BEGIN
				update dbo.TTraduccionLecturas
							set ubicacion = @ubicacion, articulo = @articulo, lote = @lote, numserie = @serie,
								cantidad = @cantidad, usuario = @usuario, fecha = @fecha, modificado = SYSDATETIME()
							from TTraduccionLecturas
							where lectura = @lectura
				IF @@ROWCOUNT = 0 
					-- si no se ha actualizado, entonces se inserta
					BEGIN
						insert into dbo.TTraduccionLecturas
						(lectura, fecha, ubicacion, articulo, lote, numserie, cantidad, usuario, modificado)
						select i.reg_txt, i.reg_fec, i.reg_ubi, i.reg_art, i.reg_lot, i.reg_ser, i.reg_ctd, i.reg_usu, SYSDATETIME()
						FROM inserted i	
					END					
			END
		print '[TRG_ActTraduccionLecturas] fin'
	END TRY
	BEGIN CATCH
		EXEC dbo.common_sendMailError -- sin parametro de usuario
	END CATCH
END