﻿CREATE TABLE [dbo].[TRegistroWeb] (
    [RegWeb_Date]          DATETIME    NULL,
    [RegWeb_Name]          NCHAR (200) NULL,
    [RegWeb_Company]       NCHAR (200) NULL,
    [RegWeb_Website]       NCHAR (200) NULL,
    [RegWeb_Tel]           NCHAR (200) NULL,
    [RegWeb_Email]         NCHAR (200) NULL,
    [RegWeb_Contactado]    DATETIME    NULL,
    [RegWeb_ContactadoPor] NCHAR (100) NULL,
    [RegWeb_Observaciones] NCHAR (900) NULL
);

