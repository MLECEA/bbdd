﻿CREATE TABLE [dbo].[sga_rol] (
    [cod_rol]            NUMERIC (19)  IDENTITY (1, 1) NOT NULL,
    [activo]             TINYINT       NOT NULL,
    [descripcion]        VARCHAR (255) NOT NULL,
    [nombre]             VARCHAR (255) NOT NULL,
    [optimistic_lock]    NUMERIC (19)  NOT NULL,
    [superusuario]       TINYINT       NULL,
    [anonimo]            INT           NULL,
    [vista_datos_reales] INT           NULL,
    [rol_PedPRO]         BIT           NULL,
    [rol_PedCLI]         BIT           NULL,
    [rol_RecMaiPar]      BIT           NULL,
    PRIMARY KEY CLUSTERED ([cod_rol] ASC)
);

