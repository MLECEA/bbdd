USE [EKANBAN]
GO
/****** Object:  StoredProcedure [dbo].[common_sendMail]    Script Date: 11/14/2018 13:05:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*******************************************************************************************
 *
 * sendMail
 *
 * Envía un mail 
 *
 * @to es el destinatario/s , si son más de uno deberá ir separado por ";" (TO)
 * @cc son las direcciones de correo a las que se envía copia, si son varios separados por ";" (CC)
 * @cco son las direcciones de correo a las que se envía copia oculta, si son varias separadas por ";" (CCO)
 * @subject es el título o asunto del correo electrónico
 * @body es el contenido del mensaje
 *
 ********************************************************************************************/
CREATE PROCEDURE [dbo].[common_sendMail]
	( @subject as VARCHAR(255),
	  @to as VARCHAR(1000),
	  @cc as VARCHAR(1000) = null,
	  @body as VARCHAR(5000),
	  @usuario as VARCHAR(5000) = null
	)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		if (@to is not null)
			begin
				SET @to = RTRIM(@to)
			end
		else
			begin
				SET	 @to = ISNULL(@to,'')
			end

		if (@cc is not null)
			begin
				SET	 @cc = RTRIM(@cc)
			end
		else
			begin
				SET	 @cc = ISNULL(@cc,'')
			end
			
			--set @to = '' --borrar

		if (@to = '' and @cc = '')
			begin
				print N'common_sendMail - No se ha especificado ni to ni cc'
				RAISERROR(N'No se ha especificado ni to ni cc', 19, 1) WITH LOG
			end
		else
			begin

				if (1 = 1) --ejecutamos en el caso de que sea preproduccion
					begin
						SET @subject = N'PREPRODUCCION ' + ISNULL(@subject,'')
				
						SET @body = N'Para: ' + @to + CHAR(13) +CHAR(10) + 
										'Cc: ' + @cc + CHAR(13) + CHAR(10) + 
										@body
					
						print N'common_sendMail - Body del mail ' + @body

						SET @to = 'iratxem@cyc.es;mlecea@orkli.es' --aquí se pondría el mail al que se quiere que lleguen los mensajes de preproducción
						SET @cc = Null
					end
				
				EXEC msdb.dbo.sp_send_dbmail 
					@profile_name='posta',
					@recipients = @to,
					@copy_recipients = @cc, 
					@subject=@subject,
					@body=@body
				
			end
	END TRY
	BEGIN CATCH
		select @usuario as ERROR_USER,
			 ERROR_NUMBER() as ERROR_NUMBER,
			 ERROR_STATE() as ERROR_STATE,
			 ERROR_SEVERITY() as ERROR_SEVERITY,
			 ERROR_LINE() as ERROR_LINE,
			 ERROR_PROCEDURE() as ERROR_PROCEDURE,
			 ERROR_MESSAGE() as ERROR_MESSAGE,
			 GETDATE() as date
	END CATCH
END