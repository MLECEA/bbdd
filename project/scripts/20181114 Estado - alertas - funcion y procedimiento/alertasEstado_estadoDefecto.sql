USE [EKANBAN]
GO
/****** Object:  UserDefinedFunction [dbo].[alertasEstado_estadoDefecto]    Script Date: 11/13/2018 13:34:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[alertasEstado_estadoDefecto]
(@Circuito as nvarchar(50) )
RETURNS int
AS
BEGIN
	DECLARE @T AS INT
	DECLARE @V AS int 
	declare @AE as int
	declare @A as int
	declare @R as int
	DECLARE @SS as int
	DECLARE @Stat as int


	declare @Tipo as int
	--1 proveedor, 3 cliente
	begin
		SELECT @Tipo=tipo_circuito
		FROM dbo.circuito
		WHERE id_circuito = @Circuito
	end
	set @Tipo=isnull(@Tipo, 1) -- si no estuviera definido, lo consideramos como proveedor


	SET @T=DBO.T(@Circuito)
	SET @R=DBO.R(@Circuito)
	SET @AE=DBO.AE(@Circuito)
	SET @A=DBO.A(@Circuito)
	SET @SS=DBO.SS(@Circuito)
	
	SET @V=@T-@AE-@A-@R+@SS

	set @Stat=1 --verde por defecto

	if @tipo=3 --cliente
	begin
		if ((@R+@A > @T/2) and @SS=0 ) OR @V=0 
			begin
				set @Stat=2 --amarillo
				if @AE=0 or @r>=@t
				BEGIN
				set @Stat=3 -- ROJO
				END
			end
	end
	else
	begin
		--no existe AE por ser proveedor, externos, etc...
		if @V < @T/2
			begin
				set @Stat=2 --amarillo
				if @A=0 
				BEGIN
					set @Stat=3 -- ROJO
				END
			end
	end

	RETURN @Stat

END