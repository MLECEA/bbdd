USE [EKANBAN]
GO

/****** Object:  Table [dbo].[alertasEstado_configuracionMail]    Script Date: 11/13/2018 13:39:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[alertasEstado_configuracionMail](
	[cliente_id] [varchar](255) NOT NULL,
	[mailAmarilloAsunto] [varchar](255) NOT NULL,
	[mailAmarilloPara] [varchar](255) NOT NULL,
	[mailAmarilloCC] [varchar](255) NULL,
	[mailRojoAsunto] [varchar](255) NOT NULL,
	[mailRojoPara] [varchar](255) NOT NULL,
	[mailRojoCC] [varchar](255) NULL,
	[mailVerdeAsunto] [varchar](255) NULL,
	[mailVerdePara] [varchar](255) NULL,
	[mailVerdeCC] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[cliente_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


