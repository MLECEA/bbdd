﻿/*
Script de implementación para EKANBAN

Una herramienta generó este código.
Los cambios realizados en este archivo podrían generar un comportamiento incorrecto y se perderán si
se vuelve a generar el código.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "EKANBAN"
:setvar DefaultFilePrefix "EKANBAN"
:setvar DefaultDataPath "E:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\"
:setvar DefaultLogPath "E:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\"

GO
:on error exit
GO
/*
Detectar el modo SQLCMD y deshabilitar la ejecución del script si no se admite el modo SQLCMD.
Para volver a habilitar el script después de habilitar el modo SQLCMD, ejecute lo siguiente:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'El modo SQLCMD debe estar habilitado para ejecutar correctamente este script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO

IF (SELECT OBJECT_ID('tempdb..#tmpErrors')) IS NOT NULL DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
GO
BEGIN TRANSACTION

--------------------------------------------


GO
PRINT N'Modificando [dbo].[THistoricoVerdes]...';


GO
ALTER TABLE [dbo].[THistoricoVerdes]
    ADD [origen]  VARCHAR (255) NULL,
        [usuario] VARCHAR (255) NULL;


GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END




GO
PRINT N'Creando [dbo].[PAX_HistoricoVerdes]...';


GO

-- =============================================
-- Author:		IMR
-- Create date: 4-10-2018
-- Description:	Procedimiento para actualizar el histórico con los valores de las tarjetas
-- =============================================
CREATE PROCEDURE [dbo].[PAX_HistoricoVerdes]
(
@Circuito varchar(255)='', @Origen AS varchar(255)='', @Usuario AS varchar(255)=''
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
	BEGIN TRANSACTION;
	
		DECLARE @V as int 
		DECLARE @AE as int
		DECLARE @A as int
		DECLARE @R as int
		DECLARE @SobreStock as int
		DECLARE @UltLec as datetime
		DECLARE @OrigenActualizacion as varchar(255)

		SET @V = dbo.V(@Circuito)
		SET @A = dbo.A(@Circuito)
		SET @R = dbo.R(@Circuito)
		SET @AE = dbo.AE(@Circuito)
		SET @SobreStock=dbo.SS(@Circuito)
		SET @OrigenActualizacion = @Origen
				
		PRINT @Circuito
		
		IF NOT EXISTS (
					   /*SELECT IdCircuito, CtdVerdes, CtdAmarillas, CtdRojas, CtdAmarillasVerdes,
		                      CtdSobreStock
		               FROM THistoricoVerdes
		               WHERE IdCircuito = @Circuito AND CtdVerdes = @V AND CtdRojas = @R AND 
		               CtdAmarillasVerdes = @AE AND CtdSobreStock = @SobreStock */
		               SELECT MAX(fecha) as fecha, IdCircuito, CtdVerdes, CtdAmarillas, CtdRojas, CtdAmarillasVerdes,
		                      CtdSobreStock
		               FROM THistoricoVerdes
		               WHERE IdCircuito = @Circuito AND CtdVerdes = @V AND CtdRojas = @R AND 
								CtdAmarillasVerdes = @AE AND CtdSobreStock = @SobreStock AND
								fecha in (select max(fecha) as fecha from THistoricoVerdes where IdCircuito = @Circuito)
		               GROUP BY IdCircuito, CtdVerdes, CtdAmarillas, CtdRojas, CtdAmarillasVerdes,
		                      CtdSobreStock
		               )
			BEGIN
				INSERT INTO ekanban.dbo.THistoricoVerdes (IdCircuito, Fecha, CtdVerdes, CtdAmarillas, CtdRojas, CtdAmarillasVerdes,
															CtdSobreStock,tag, origen, Usuario)
				values (@Circuito, getdate(), @V,@A,@R, @AE, @SobreStock, null, @OrigenActualizacion, @Usuario)
			END
		
	COMMIT TRANSACTION;
	
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAccAjustar]...';


GO


ALTER PROCEDURE [dbo].[PAccAjustar]
(
	@CodCircuito varchar(20) = '',
	@NumTarjVerdes integer = 0,
	@Usu varchar(255) = '',
	@Dis varchar(4)=''
)
	
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @NumTarjRojas as int
	DECLARE @NumTarjetas AS INT
	DECLARE @Verdes as int
	DECLARE @SobreStock as int

	DECLARE @CampGest varchar(50)=''

	DECLARE @Tipo varchar(10)=''
	DECLARE @StatusTrat varchar(1)
	DECLARE @IdTar varchar(50)
	DECLARE @Usuario varchar(255) = ''
	DECLARE @FechEntregaConfirmada AS DATETIME

	DECLARE @MsgTxt varchar(255)=''
	DECLARE @Resul int=0

	SET @Usuario=@Usu

	SELECT @Tipo=dbo.tipo_circuito.nombre
	FROM            dbo.circuito INNER JOIN
				dbo.tipo_circuito ON dbo.circuito.tipo_circuito = dbo.tipo_circuito.id
	WHERE        (dbo.circuito.id_circuito = @CodCircuito)

	SET @StatusTrat='T'
	if @Tipo='INT' OR @Tipo='EXT' 
	BEGIN
		SET @StatusTrat='G'
	END

	set @NumTarjRojas=dbo.r(@CodCircuito)
	set @Verdes=dbo.v(@CodCircuito)
	set @SobreStock=dbo.SS(@CodCircuito)
	SET @FechEntregaConfirmada=GETDATE()
	
	if @NumTarjVerdes<0 set @NumTarjVerdes=0
	
	SET @NumTarjetas=@NumTarjVerdes-@Verdes


	set @CampGest='AutAX' + '_' + CONVERT(nvarchar(100), @FechEntregaConfirmada, 112) + '_' + CONVERT(nvarchar(100), getdate(), 113)


	IF @NumTarjetas>0 --Hay más stock real, verdes que los que indica el circuito. Si hay rojas, quitar. sino crear amodif
	BEGIN
		print 'numTarjetas > 0 ';
		IF @NumTarjRojas>=@NumTarjetas--=@NumTarjetas
		BEGIN
			SELECT    TOP (@NumTarjetas)     @IdTar =id_lectura
					FROM            dbo.tarjeta
					WHERE        (gestionado IS NULL OR
									gestionado = '') AND (id_circuito = @CodCircuito)
					ORDER BY id_lectura


			UPDATE [EKANBAN].[dbo].[tarjeta]
				SET gestionado =  @CampGest
				, Estado='T'    -- @StatusTrat de lo contrario, en circuitos internos y externos, pueden considerarse como amarillas, tipo G
				, Tar_GestionadoUSU = @Usuario
				, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada
				WHERE     (gestionado IS NULL OR gestionado = '') AND (id_circuito =  @CodCircuito) and id_lectura<=@IdTar
		END
		ELSE
		BEGIN
					UPDATE [EKANBAN].[dbo].[tarjeta]
					SET gestionado =  @CampGest
					, Estado='T'    -- @StatusTrat de lo contrario, en circuitos internos y externos, pueden considerarse como amarillas, tipo G
					, Tar_GestionadoUSU = @Usuario
					, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada
					WHERE     (gestionado IS NULL OR gestionado = '') AND (id_circuito =  @CodCircuito) AND ESTADO <>'A_MODIF'
					

					INSERT INTO dbo.tarjeta (id_lectura,antena,estado,fecha_lectura,gestionado,movil,tipo,cliente_id,id_circuito,referencia_id,ubicacion,sentido, kill_tag, Tar_FecEntSol)
					SELECT        TOP (@NumTarjetas-@NumTarjRojas) 'AJUSTE' + RIGHT(dbo.sga_usuario.cod_usuario, 2) + '_' + CONVERT(nvarchar(30), GETDATE(), 131) AS Expr1, NULL AS ANTENA, 'A_MODIF' AS ES, GETDATE() AS Expr2, NULL AS GEST, 'M' AS MOV, 'M' AS tipo, 
							 dbo.circuito.cliente_id, dbo.circuito.id_circuito, dbo.circuito.referencia_id, dbo.circuito.ubicacion, 'S' AS Expr3, 0, @FechEntregaConfirmada
					FROM            dbo.circuito CROSS JOIN dbo.sga_usuario
					WHERE        (dbo.circuito.id_circuito = @CodCircuito) ORDER BY id_circuito
		END
	END
	ELSE
	--HAY QUE AÑADIR ROJAS (PRIMERO QUITAR AMODIF)
	BEGIN
		print ' opcion 2';
		set @NumTarjetas=-@NumTarjetas

		IF @SobreStock>=@NumTarjetas
		BEGIN

			SELECT    TOP (@NumTarjetas)     @IdTar =id_lectura
			FROM            dbo.tarjeta
			WHERE        (estado = 'A_MODIF') AND (sentido = 'S') AND (gestionado IS NULL OR
			gestionado = '') AND (id_circuito = @CodCircuito)
			ORDER BY id_lectura


			UPDATE [EKANBAN].[dbo].[tarjeta]
				SET gestionado =  @CampGest
				, Estado='T'    -- @StatusTrat de lo contrario, en circuitos internos y externos, pueden considerarse como amarillas, tipo G
				, Tar_GestionadoUSU = @Usuario
				, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada
				WHERE        (estado = 'A_MODIF') AND (sentido = 'S') AND (gestionado IS NULL OR
				gestionado = '') AND (id_circuito = @CodCircuito) and id_lectura<=@IdTar
		END

		ELSE
		BEGIN

					UPDATE [EKANBAN].[dbo].[tarjeta]
					SET gestionado =  @CampGest
					, Estado='T'    -- @StatusTrat de lo contrario, en circuitos internos y externos, pueden considerarse como amarillas, tipo G, 
				    , Tar_GestionadoUSU = @Usuario
					, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada
					WHERE        (estado = 'A_MODIF') AND (sentido = 'S') AND (gestionado IS NULL OR
					gestionado = '') AND (id_circuito = @CodCircuito)
					

					INSERT INTO dbo.tarjeta (id_lectura,antena,estado,fecha_lectura,gestionado,movil,tipo,cliente_id,id_circuito,referencia_id,ubicacion,sentido, kill_tag, Tar_FecEntSol)
					SELECT        TOP (@NumTarjetas-@SobreStock) 'AJUSTE' + RIGHT(dbo.sga_usuario.cod_usuario, 2) + '_' + CONVERT(nvarchar(30), GETDATE(), 131) AS Expr1, NULL AS ANTENA, 'A' AS ES, GETDATE() AS Expr2, NULL AS GEST, 'M' AS MOV, 'M' AS tipo, 
							 dbo.circuito.cliente_id, dbo.circuito.id_circuito, dbo.circuito.referencia_id, dbo.circuito.ubicacion, 'S' AS Expr3, 0, @FechEntregaConfirmada
					FROM            dbo.circuito CROSS JOIN dbo.sga_usuario
					WHERE        (dbo.circuito.id_circuito = @CodCircuito) ORDER BY id_circuito
		END




	END

	--Set @Resul=1
	--set @MsgTxt='Sin validar usuario'
	
	--Set @Resul=0
	--set @MsgTxt='se han insertado los datos'

	--set @Resul=18  --Código de resultado para el android. Error al generar pedido.
	--set @MsgTxt='Se han dado consumos justo en este instante. Vuelva a intentarlo'

	Set @Resul=0
	set @MsgTxt='se han insertado los datos'

	select  @Resul as Result, @MsgTxt as Msg
	
	-----20181008 INCLUSION DE TABLA HISTORICOVERDES	
	exec dbo.PAX_HistoricoVerdes @Circuito = @CodCircuito, @Origen = 'AjusteStock', @Usuario = @Usu
	-----20181008 FIN INCLUSION DE TABLA HISTORICOVERDES
	
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END



GO
PRINT N'Modificando [dbo].[PAX_PatronLec]...';


GO


-- =============================================
-- Author:		IKER
-- Create date: 13/5/2014
-- Description:	Procedimiento para conseguir datos, según patrón de lectura del código de barras

-- Este procedimiento se ejecuta siempre que hay una lectura de un codigo de barras en los dispositivos ANDROID (Consumo y Consulta)
-- y cuando hay una lectura del portico. 
-- El objetivo es conseguir la OF y la SERIE de la información de la etiqueta/QR/RFID, etc. 
-- El resultado da, OF, SERIE y GRAB. El campo grab es la que se utiliza para garantizar que no se repite la lectura de la misma etiqueta. 
-- Si el resultado tiene OF no válido (Que no sea null, vacio o 000000) el proceso siguiente, coge el Articulo y cliente desde el resultado del procedimiento. 
-- Si el resultado tiene OF válido, va a VAX_EKAN_OF y coge como resultado el artículo y cliente. 

-- una vez que tiene, ARTICULO, CLIENTE Y USUARIO (el de la aplicación), consulta en la tabla de circuitos y obtiene el código circuito para seguir el proceso. 
-- =============================================
ALTER PROCEDURE [dbo].[PAX_PatronLec]
(
@Txt varchar(255)='', @Usu AS varchar(255)='', @Ubi AS varchar(255)='', @Pant AS int=0 , @Disp as varchar(255)='', @Ant as varchar(50)='', @Sen as varchar(5)='', @OrigenLectura as varchar(25)='PatronLectura'
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @TP AS INT=0  --TIPO DE PATRON APLICADO. SI ES 0 EN RESULTADO, NO HA ENTRADO EN NINGUNO DEFINIDO. 
	DECLARE @L AS VARCHAR(25)=''
	DECLARE @L1 AS INT
	DECLARE @L2 AS INT
	DECLARE @S AS VARCHAR(100)=''
	DECLARE @S1 AS INT
	DECLARE @S2 AS INT
	DECLARE @A AS VARCHAR(25)=''
	DECLARE @A1 AS INT
	DECLARE @A2 AS INT
	DECLARE @AR AS VARCHAR(25)=''
	DECLARE @Cli AS VARCHAR(25)=''
	DECLARE @UbiTip AS VARCHAR(25)=''
	DECLARE @V AS VARCHAR(25)=''
	DECLARE @CliOF AS VARCHAR(25)=''
	DECLARE @CliUBI AS VARCHAR(25)=''
	DECLARE @I AS INT
	DECLARE @CIR AS VARCHAR(100)=''
	DECLARE @CTD AS VARCHAR(100)=''
	DECLARE @PED AS VARCHAR(100)=''
	DECLARE @NIVACC AS INT
	DECLARE @ACCION AS VARCHAR(10)=''
	DECLARE @CTDACCCION AS INT
	DECLARE @TipCont AS INT
	DECLARE @position int
	DECLARE @string char(500)
	DECLARE @1P AS VARCHAR(100)=''
	DECLARE @16K AS VARCHAR(100)=''
	DECLARE @2L AS VARCHAR(100)=''
	DECLARE @1J AS VARCHAR(100)=''
	DECLARE @5J AS VARCHAR(100)=''
	DECLARE @6J AS VARCHAR(100)=''
	DECLARE @7Q58 AS VARCHAR(100)=''
	DECLARE @7QGT AS VARCHAR(100)=''
	DECLARE @16D AS VARCHAR(100)=''
	DECLARE @2P AS VARCHAR(100)=''
	DECLARE @ALB AS VARCHAR(100)=''
	DECLARE @ALBLIN AS VARCHAR(100)=''
	DECLARE @UbiCap AS VARCHAR(25)=''
	DECLARE @LotCap AS VARCHAR(50)=''
	DECLARE @Emp AS VARCHAR(3)=''
	DECLARE @URLPlano AS VARCHAR(50)=''
	--SI LA UBICACIÓN ACTUAL, TIENE CHECK DE REGISTRO DE TRAZABILIDAD. 
	DECLARE @RegTraz bit=0
	DECLARE @EMPR NCHAR(3)=''
	declare @cade varchar(200)
	declare @TIPOETIQUETA VARCHAR(50) = NULL;
	DECLARE @TIPO AS VARCHAR(50) = NULL;
	DECLARE @INDICE AS INT = 0;
	DECLARE @QRDATA VARCHAR(255) = NULL;

	SET NOCOUNT ON;
	SET XACT_ABORT ON;  --- iker. 2016-05-09 crear pedidos-en sortu ahal izateko. iker.
	--SET TRANSACTION ISOLATION LEVEL SNAPSHOT
	begin transaction
	--*************************************************************************************************************************************************
	--Si la ubicación tiene predefinida un tipo concreto de etiqueta tratamos esa
	 SELECT @TIPOETIQUETA = TipoEtiqueta FROM ubicacion WHERE id_ubicacion = @UBI;
	 PRINT @TIPOETIQUETA;
	 IF @TIPOETIQUETA <> ''
	 BEGIN
		PRINT 'TIPO ETIQUETA PERSONALIZADA';
		DECLARE TIPO CURSOR FOR select * from dbo.fnSplitString (@TIPOETIQUETA,':');
		OPEN TIPO
		FETCH NEXT FROM TIPO INTO @TIPO
		WHILE @@fetch_status = 0
		BEGIN
			IF @INDICE = 0 SET @TP = @TIPO;
			else set @qrData = @tipo;
			SET @INDICE = @INDICE + 1;
			FETCH NEXT FROM TIPO INTO @TIPO
		END
		print @tp;
		DECLARE @VARIABLE VARCHAR(255);
		DECLARE @VALOR VARCHAR(255);
		declare @var varchar(255);
		declare @posVariable int = -1;
		DECLARE DATA CURSOR FOR select * from dbo.fnSplitString (@QRDATA,'|');
		OPEN DATA
		FETCH NEXT FROM DATA INTO @var
		WHILE @@fetch_status = 0
		BEGIN
			select @posVariable=  CHARINDEX('=', @var); 
			set @VARIABLE = SUBSTRING(@var, 0, @posVariable);
			set @VALOR = SUBSTRING(@var, @posVariable + 1, len(@var));
			print @variable +  @valor;
			IF @VARIABLE='CTD'  BEGIN PRINT @VALOR;SET @CTD = DBO.GS1Data(@TXT, @VALOR); END
			IF @VARIABLE='LOTE' SET @L = DBO.GS1Data(@TXT, @VALOR);
			IF @VARIABLE='REF' SET @A = DBO.GS1Data(@TXT, @VALOR);
			IF @VARIABLE='PROV' SET @V = DBO.GS1Data(@TXT, @VALOR);
			IF @VARIABLE='BULTO' SET @S = DBO.GS1Data(@TXT, @VALOR);
			FETCH NEXT FROM DATA INTO @VAR
		END
		print 'lote';
		print @L;
		PRINT 'ERREFERENTZIA=';
		PRINT @A;
		deallocate  tipo;
		deallocate  data;
		
		set @CIR=''

		--SELECT @S=DBO.GS1Data(@txt,'S')
		set @L=isnull(@L,'')
		IF @L='' SET @L='0' 
		PRINT @CTD;
		SET @CTD= CONVERT(NUMERIC(9,1),@CTD)
		PRINT 'CANTIDAD OK';
		--revisamos a qué empresa corresponde la ubicación
		SELECT       @RegTraz=Ubi_RegTrazAX, @EMPR=Ubi_Dataareaid
		FROM            dbo.ubicacion
		WHERE        (id_ubicacion = @Ubi)

		--PARA QUE EL DISPOSITIVO MOVIL NO DE ERROR, ASIGNAMOS CIRCUITOS, LOTES ETC.
		--Cogemos el cod cliente de la ubiación
		SET @A=ISNULL(@A,'')
		SET @CIR=''
		--En caso de que ese artículo no tenga circuito, cogemos el de trazabilidad, para que podamos dar consumos. 
		PRINT 'REFERENCIA_ID=';
		PRINT @A;
		SELECT        @CIR=id_circuito, @Cli=cliente_id
		FROM            dbo.circuito
		WHERE        (referencia_id = @A) AND (ubicacion =  @Ubi) AND activo=1

		set @ant=@V

		SET @CIR=ISNULL(@CIR,'')

		--SI NO TIENE CIRCUITO EL ARTÍCULO, COGEMOS EL DE TRAZABILIDAD, SI EXISTE. SINO ERROR.
		IF @CIR='' or @A in ('20100034','20100060','L-11158','L-11180','L-11170','20100551','20100017','20100013') 
		--hasta el 2/9/2017 20100463 ere sartuta. hori kendu. iker.

		BEGIN
			SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
			WHERE        (ubicacion = @Ubi) AND (activo = 1) and referencia_id='TRAZABILITATEA'
		END


		SET @S=RTRIM(@S) + '_' + @L
		SET @L='000000'
		
		if (not exists(select * from tarjetaMetadata where id_lectura=@L + '_' + @S and sentido=@Sen))
			insert into tarjetaMetadata (id_lectura, sentido, txtLectura, tipo,fechaPrimeraLectura ,fechaUltimaLectura, PatronLectura) values (@L + '_' + @S, @Sen, @Txt, @tp, GETDATE(), GETDATE(), @TIPOETIQUETA);
		else update tarjetaMetadata set fechaUltimaLectura = GETDATE(), PatronLectura=@TIPOETIQUETA where id_lectura = @L + '_' + @S and sentido = @Sen
		GOTO REGISTROLECTURAS;
	 END
	
	
	--*************************************************************************************************************************************************
	-- Cuando no se inserta ningún dato manualmente en la referencia
	if @txt='P'
	BEGIN
		SET @TP=1
		SET @L='000000'
		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación y que sean activos
		SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi AND (activo = 1) ORDER BY num_kanban
	END

	--*************************************************************************************************************************************************
	--Si es un artículo el que se lee. Automaticamente le añade una P al inicio
	if @txt<>'P' AND left(@txt,1)='P' AND left(@txt,2)<>'P*' AND left(@txt,4)<>'PROV' and  (CHARINDEX('_', @txt, 1) = 0)
	begin
		--P08296603
		SET @TP=2
		SET @L ='000000'
		SET @Cli ='00000000'

		SET @AR =SUBSTRING(@txt, 2, LEN(@TXT))

		SELECT      @Cli= localizacion
		FROM        dbo.ubicacion
		WHERE       id_ubicacion = @Ubi

		--en vez de esta consulta, mejor la de lista de precios, o ref cruzadas, para garantizar que es una del cliente.
		SELECT   @A = ITEMID
		FROM     PREAXSQL.SQLAXBAK.dbo.INVENTTABLE AS INVENTTABLE
		WHERE    (DATAAREAID = N'ORK') AND (ITEMID = @AR)

		IF @A = ''
		BEGIN
			SELECT     @A= CUSTVENDEXTERNALITEM.ITEMID
			FROM       PREAXSQL.SQLAXBAK.dbo.CUSTVENDEXTERNALITEM AS CUSTVENDEXTERNALITEM INNER JOIN
					   PREAXSQL.SQLAXBAK.dbo.INVENTTABLE AS INVENTTABLE ON CUSTVENDEXTERNALITEM.DATAAREAID = INVENTTABLE.DATAAREAID AND CUSTVENDEXTERNALITEM.ITEMID = INVENTTABLE.ITEMID
			WHERE        (CUSTVENDEXTERNALITEM.DATAAREAID = N'ork') AND (CUSTVENDEXTERNALITEM.EXTERNALITEMID = @AR) AND (CUSTVENDEXTERNALITEM.CUSTVENDRELATION = @Cli)
			ORDER BY CUSTVENDEXTERNALITEM.EXTERNALITEMID
		END
	end 



	--*************************************************************************************************************************************************
	-- cuando se lee una consulta con * para realizar operaciones. 
	--  *A2-609:T12
	--  *A2-609:N5
	if left(@txt,2)='P*' and left(@txt,3)<>'P*U'
	begin
		SET @TP=3
		IF CHARINDEX(':', @TXT,1) > 0
			BEGIN
				SET @L='000000'	
				SET @A1 = 3
				SET @A2 =CHARINDEX(':', @TXT,1)
				IF @A2>0 
				begin
					SET @A =SUBSTRING(@txt, @A1, @A2-@A1)
					sET @ACCION =SUBSTRING(@txt, @A2+1, LEN(@TXT)-(@A2))
					set @CTDACCCION= SUBSTRING(@ACCION, 2, len(@ACCION)-1)
					set @ACCION=SUBSTRING(@ACCION, 1, 1)
				end
		
				--CIRCUITO? SI EXISTE
				SELECT       @CIR= id_circuito, @Cli=cliente_id
				FROM            dbo.circuito
				WHERE        ubicacion = @Ubi AND referencia_id = @A
		
				--si no existe, cogemos el cliente de la ubicación
				IF @CIR=''
					BEGIN
						SELECT        @Cli=localizacion
						FROM            dbo.ubicacion
						WHERE        id_ubicacion = @Ubi
					END
		
				--Cogemos el rol del usuario
				SELECT        @NIVACC= ptr_rol
				FROM            dbo.sga_usuario
				WHERE        USERNAME = @Usu
		
				-- Con permiso, para crear y no hay circuito
				IF @NIVACC=7 AND @ACCION='N' AND @CIR=''
				BEGIN
		
					-- CREAR CIRCUITO
					INSERT INTO [dbo].[circuito]
					   ([cliente_id],[id_circuito],[referencia_id],[activo],[cantidad_piezas],[cliente_nombre],[descripcion],[entrega_dias]
					   ,[entrega_horas],[mail],[num_kanban],[ref_cliente],[reserva_auto],[ubicacion],[tipo_circuito],[tipo_contenedor])
					VALUES
					   (@Cli, @Ubi+@A, @A, 1, 1, '', '', 0,0,'', @CTDACCCION, '', 1, @Ubi, 1,1)

					   SET @S='NuevoCir'
				END
	
				-- Con permiso, para modif y  hay circuito
				IF @NIVACC=7 AND @ACCION='T' AND @CIR<>''
				BEGIN
					SET @S='ActNumKan'
					UPDATE [dbo].[circuito]
					SET [num_kanban] = @CTDACCCION
					WHERE [id_circuito]= @CIR
				END

				IF @NIVACC=7 AND @ACCION='Q' AND @CIR<>''
				BEGIN
					SET @S='ActQPzas'
					UPDATE [dbo].[circuito]
					SET [cantidad_piezas] = @CTDACCCION
					WHERE [id_circuito]= @CIR
				END
			END
			else
			begin
				DECLARE	@return_value int
				if left(@txt,4)='P*er' 
				BEGIN
					EXECUTE	@return_value = EKANBAN.[dbo].[PEnvUsers]
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END

				if left(@txt,4)='P*rf' 
				BEGIN
					EXECUTE	@return_value = EKANBAN.[dbo].[PEnvSitArcRFID]
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END

				if left(@txt,4)='P*eg' 
				BEGIN
					EXECUTE	@return_value = EKANBAN.[dbo].[PEnvSitPanel]
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END
				
				if left(@txt,4)='P*01' 
				BEGIN
					UPDATE [dbo].[sga_parametros]
					SET [descripcion] = SUBSTRING(@txt,5, LEN(@TXT))
					WHERE [nombre]='RFIDTEST0001'
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END
				if left(@txt,4)='P*02' 
				BEGIN
					UPDATE [dbo].[sga_parametros]
					SET [descripcion] = SUBSTRING(@txt,5, LEN(@TXT))
					WHERE [nombre]='RFIDTEST0002'
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END

				if left(@txt,4)='P*03' 
				BEGIN
					UPDATE [dbo].[sga_parametros]
					SET [descripcion] = SUBSTRING(@txt,5, LEN(@TXT))
					WHERE [nombre]='RFIDTEST0003'
					SET @L='000000'
					--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
					SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
				END
				
			end

	end


	----*************************************************************************************************************************************************
	----PARA PROBAR EL EKANBAN, BORRANDO LAS ENTRADAS AL CIRCUITO
	----/**
	--if left(@txt,2)='P*' and (left(@txt,5)='P*U00') --OR left(@txt,5)='P*U07'
	--begin
	--	begin
	--		DELETE FROM [dbo].[tarjeta]
	--		WHERE        (ubicacion = SUBSTRING(@TXT,3,3))

	--	end
	--	SET @L='000000'	
	--	--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
	--	SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito WHERE ubicacion = @Ubi ORDER BY num_kanban
	--end
	----**/
	----*************************************************************************************************************************************************



	-- si es tipo de SMSTO
	if left(@txt,5)='SMSTO'
	begin
		SET @TP=4
		--LOS MENSAJES NORMALES:     (QUE NO SEAN DE FORMACIÓN NI DE TIPO ORK. QUE NO EXISTA UORK O FORMACIÓN)
		IF CHARINDEX('UORK_', @TXT,1)=0 and CHARINDEX('FORMACION', @TXT,1)=0 
		BEGIN
			--SMSTO:+34696469617:H123456_179993_20900-31
			SET @L1 =CHARINDEX('H', @TXT,15)+1
			SET @L2 =CHARINDEX('_', @TXT,@L1)
			SET @L =SUBSTRING(@txt, @L1, @L2-@L1)

			SET @S1 =CHARINDEX('_', @TXT,@L2)+1
			SET @S2 =CHARINDEX('_', @TXT,@S1)
			SET @S =SUBSTRING(@txt, @S1, @S2-@S1)

			SET @A1 =CHARINDEX('_', @TXT,@S2)+1
			SET @A2 =LEN(@TXT)
			SET @A =SUBSTRING(@txt, @A1, @A2-@A1+1)

			IF @L ='000000' SET @Cli ='00000000'

 
			-- para el error de reetiquetados de COPRECI
			--SI MIRAMOS 

			
			SELECT       @CliOF=CUSTACCOUNT
			FROM            dbo.VAX_EKAN_OF
			WHERE        (PRODID = @L)


			SELECT      @CliUBI= localizacion
			FROM        dbo.ubicacion
			WHERE       id_ubicacion = @Ubi

			IF @CliUBI<>@CliOF 
				BEGIN
					SET @Cli =@CliUBI
					SET @L ='000000'
				END

		END
		
		--IF CHARINDEX('FORMACION', @TXT,1)>0 
		--BEGIN
		----SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
		----SMSTO:+34696469617:U0PFORMACION4K00
		--	SET @A ='20900-1'
		--	SET @L ='000000'
		--	SET @Cli ='00000000'
		--	SET @S = CONVERT(varchar(100), GETDATE(), 121) 
		--END

			
		IF CHARINDEX('UORK_', @TXT,1)>0 
		BEGIN
			--Etiquetas internas de SKINTER
			--SMSTO:+34629552542:UORK_A2-609_S69901
			SET @A1 =CHARINDEX('UORK_', @TXT,15)+5
			SET @A2 =CHARINDEX('_', @TXT,@A1)
			SET @A =SUBSTRING(@txt, @A1, @A2-@A1)

			SET @S1 =CHARINDEX('_S', @TXT,@A2)+2
			SET @S2 =LEN(@TXT)+1
			SET @S =SUBSTRING(@txt, @S1, @S2-@S1)
			SET @L ='000000'
			SET @Cli ='00000000'
		END
	
	END

--*************************************************************************************************************************************************

-- si es tipo de http://www.ekanban.es/qr/157238252841
	-- si es tipo de http		
	if left(@txt,5)='http:' --and @Ubi<>'U41'
	begin
		SET @TP=5
		SET @Emp='ORK'
		--http://www.orkli.com/qr/123456088082
		SET @L1 =CHARINDEX('QR/', @TXT,15)+3
		SET @L =SUBSTRING(@txt, @L1, 6)
		SET @S =SUBSTRING(@txt, @L1+6, 6)
		SET @Emp=SUBSTRING(@txt, @L1+12, 3)-- no siempre lo tiene. 

		--Si no dispone de dato en empresa, cogemos ORK por defecto.
		IF @Emp<>'BRA' SET @Emp='ORK'
		
		--SET @Emp='BRA'


		SELECT       @CliOF=CUSTACCOUNT, @A=ITEMID
		FROM            dbo.VAX_EKAN_OF
		WHERE        (PRODID = @L) AND (DATAAREAID = @Emp)

		SELECT      @CliUBI= localizacion
		FROM        dbo.ubicacion
		WHERE       id_ubicacion = @Ubi

		IF (@CliUBI<>@CliOF) or (@Emp='BRA')   -- EN CASO DE QUE SEA LA EMPRESA BRASIL, COMO EL APP, NO REVISA LAS OF, SEGÚN EMPRESA, LE PASAMOS EL CLIENTE CORRESPONDIENTE Y LOTE 000000, PARA QUE NO COJA EN BASE 
			BEGIN
				SET @Cli =@CliUBI
				SET @S= @L + @S + @Emp  -- @Emp jarria el 25/5/2017, para evitar de que consumos de defendi, diga que existen por haber consumido apis
				SET @L ='000000'

				
				SET @AR=''
				--PARA RECUPERAR LOS MAL ETIQUETADSO   http://www.ekanban.es/ekanban/qr/1287251230/1851-1200
				--1230/1851-1200
				SELECT        @AR=referencia_id
				FROM            dbo.circuito
				WHERE        (referencia_id = @A) AND (ubicacion = @Ubi)

				SET @AR=ISNULL(@AR,'')

				IF @AR=''
				BEGIN
					set @A=SUBSTRING (@A, LEN(@A)-3, 4)
					IF SUBSTRING (@A, LEN(@A)-3, 4)='1000' SET @A='12301851K1000'
					IF SUBSTRING (@A, LEN(@A)-3, 4)='1001' SET @A='12301851K1000' --berria 2/8/2016, IES
					IF SUBSTRING (@A, LEN(@A)-3, 4)='1200' SET @A='12301851K1200'
					IF SUBSTRING (@A, LEN(@A)-3, 4)='1201' SET @A='12301851K1200'--berria 2/8/2016, IES
				END

			END
	END


	
-- si es tipo de http://www.ekanban.es/qr/157238252841   para mueller
	-- si es tipo de http		, long 37
	if left(@txt,5)='http:'  and @Ubi='U41' and CHARINDEX('BRA', @TXT,1)=0 
	begin
		SET @TP=55
		--http://www.orkli.com/qr/123456088082
		--http://www.ekanban.es/ekanban/qr/196020BR123018511201 
		SET @L1 =CHARINDEX('QR/', @TXT,15)+3
		SET @S =SUBSTRING(@txt, @L1, 6)
		SET @A =SUBSTRING(@txt, @L1+6, 50)
		set @L='000000'


			--SELECT       @CliOF=CUSTACCOUNT, @A=ITEMID
			--FROM            dbo.VAX_EKAN_OF
			--WHERE        (PRODID = @L)


			SET @AR=''
			--PARA RECUPERAR LOS MAL ETIQUETADSO   http://www.ekanban.es/ekanban/qr/1287251230/1851-1200
			--1230/1851-1200
			SELECT        @AR=referencia_id
			FROM            dbo.circuito
			WHERE        (referencia_id = @A) AND (ubicacion = @Ubi)

			SET @AR=ISNULL(@AR,'')

			IF @AR=''
			BEGIN
				set @A=SUBSTRING (@A, LEN(@A)-3, 4)
				IF SUBSTRING (@A, LEN(@A)-3, 4)='1000' SET @A='12301851K1000'
				IF SUBSTRING (@A, LEN(@A)-3, 4)='1001' SET @A='12301851K1000' --berria 2/8/2016, IES
				IF SUBSTRING (@A, LEN(@A)-3, 4)='1200' SET @A='12301851K1200'
				IF SUBSTRING (@A, LEN(@A)-3, 4)='1201' SET @A='12301851K1200'--berria 2/8/2016, IES

			END




			SELECT      @CliUBI= localizacion
			FROM        dbo.ubicacion
			WHERE       id_ubicacion = @Ubi

			IF @CliUBI<>@CliOF 
				BEGIN
					SET @Cli =@CliUBI
					SET @L ='000000'
				END
	end 


-------------------------------------------------------------------------------------------
	
	--borrado el 30/11/2015. quitar el 31/12/2015 del todo
	-------------------------------------------------------------------------------------------
	
	--ETIQUETA "ESTAMPA" CON SOLO PROVEEDOR Y ARTÍCULO (opcional la cantidad)
		--PROV_000388_20100374_151.1

		--SI ES EL TIPO VIEJO DE ETIQUETAS DE ESTAMPA. A BORRAR ESTA PARTE.
	if (SUBSTRING(@txt, 1, 4)='PROV' AND CHARINDEX('PROVV', @txt, 1) = 0)				--PROV_V
	begin
		SET @TP=6
		IF CHARINDEX('_', @txt, 13)>0
			BEGIN
			SET @A=SUBSTRING(@txt, 13, CHARINDEX('_', @txt, 13)-13)
			SET @CTD=SUBSTRING(@txt, CHARINDEX('_', @txt, 13)+1, LEN(@TXT)-CHARINDEX('_', @txt, 13))
			END
		ELSE
			BEGIN
			SET @A=SUBSTRING(@txt, 13, LEN(@TXT))
			SET @CTD='0'
			END

		SET @L ='000000'
		SET @Cli ='00000000'
		
		--SET @S = right(@A,3) + '_' + @CTD + '_' + CONVERT(varchar(100), GETDATE(), 103) +' ' + left(CONVERT(varchar(100), GETDATE(), 114) ,5) 
		SET @S = SUBSTRING(@txt, 6, LEN(@TXT))+ CONVERT(varchar(100), GETDATE(), 103) +' ' + left(CONVERT(varchar(100), GETDATE(), 114) ,5) 
		
	end 
	--	A BORRAR HASTA ESTA PARTE

	
--*************************************************************************************************************************************************

	-- ETIQUETAS DE TRAZABILIDAD INTERNA, SIN CONSIDERAR EL FORMATO PROV_ QUE ES EL TIPO 6 E INICIO CON INTE O PROV Y SEPARADOR ;
	if ((SUBSTRING(@txt, 1, 4)='PROV' or SUBSTRING(@txt, 1, 4)='INTE') AND 
		SUBSTRING(@txt, 1, 5)<>'PROV_' AND CHARINDEX(';', @txt, 1) > 0 )
	
		begin
		SET @TP=7
		
		--PROV;V000388;P20100301;Q1010;TT032580351;S14288      se utiliza en estampa
		--INTE;SORKA77013  se utiliza para etiquetado interior decoletajes, lapeados, etc. 

		DECLARE @N AS INT=0 
		DECLARE @N1 AS INT=0
		DECLARE @C AS char(40)=''
		DECLARE @ID AS Char(30)=''
		DECLARE @P AS Char(30)=''
		DECLARE @VCAP AS Char(30)=''

		SET @CTD='0'
		SET @L ='000000'
		SET @Cli ='00000000'
		
		SET @N=CHARINDEX(';', @txt, 1)+1
		
		SET @TXT=REPLACE(@TXT,CHAR(4),'') --EOT


		SET @EMPR='ORK'
		
		--QUE EMPRESA ES ESTA UBICACIÓN?
		SELECT    @EMPR=Ubi_Dataareaid 
		FROM     dbo.ubicacion
		WHERE   (id_ubicacion = @Ubi)

		--PROV;V000388;P20100301;Q1010;TT032580351;S14288   
		WHILE @N<LEN(@TXT)
			BEGIN
				SET @ID=''
				SET @N1=CHARINDEX(';', @txt, @N)
				IF @N>0 AND @N1=0 SET @N1=LEN(@TXT)+1
				SET @C =SUBSTRING(@TXT, @N, @N1-@N)

				SET @ID=SUBSTRING(@C, 1, 1)
			
				if @ID='P' set @A=SUBSTRING(@C, 2, len(@c)) 
				if @ID='Q' set @CTD=SUBSTRING(@C, 2, len(@c)) 
				if @ID='H' OR @ID='T' set @L=SUBSTRING(@C, 2, len(@c)) 
				if @ID='S' set @S=SUBSTRING(@C, 2, len(@c))						-- PARA LAS ETIQUETAS INTERNAS, COGEMOS LA SERIE
				if @ID='V' set @V=SUBSTRING(@C, 2, len(@c)) 

				SET @N=@N1+1
			END

		--INTE;SORKA77013  ETIQUETADO INTERIOR DE DECOLETAJE, LAPEADO
		IF SUBSTRING(@txt, 1, 4)='INTE' 
			BEGIN
				--COGEMOS LOS DATOS REGISTRADOS AL IMPRIMIR LA ETIQUETA
				SELECT TOP 1 @A=Referencia, @L=NLote, @CTD=Cantidad, @V=Proveedor         --CAMPOS DISPONIBLES Referencia, NSerie, NLote, Cantidad, Proveedor, Fecha, Tipo
				from [SRVSPS2007\SQL2005x32].[ORKLB000].dbo.VEtiquetas 
				where nserie = @S
				ORDER BY FECHA DESC
			END
		
		
		set @LotCap=@S
		-- SI ES DE CAPTOR
		IF SUBSTRING(@txt, 1, 9)='INTE;SCAP' 
		BEGIN
				SELECT   @Usu=SUBSTRING(descripcion,1,10)--limitamos a 10 la descripcion, bestela error
				FROM     dbo.ubicacion
				WHERE   (id_ubicacion = @Ubi)

				--EL USUARIO DEBE SER EL NOMBRE DEL RECURSO DEL AX
				-- 9/3/2017 no estamos insertando THI1 como recurso, pq el apartado anterior estaba anulado. lo activo otra vez. iker.  INTE;SCAP115049  
				--iñigo y blanca han venido. hay foto etiquta en mov ies



				set @S=SUBSTRING(@S, 4, LEN(@S))
				
				SELECT        @L=Lote, @A=Referencia, @CTD=Cantidad--, Compañia
				FROM            SRVCAPTORDB.Captor3.dbo.VCAPTOR_Lotes AS A
				WHERE        ( A.Compañia = '01' AND A.Lote=@S)

				set @LotCap=@S
				set @S=@ubi

				SET @V='000000' -- CONSIDERAMOS QUE SI ES INTERNA Y DE CAPTOR EL PROVEEDOR ES ORKLI
		END

		

		set @A=isnull(@A,'')
		SET @CTD= CONVERT(NUMERIC(9,1),@CTD)
		set @Cli='00000000'
		SET @V=ISNULL(@V,'000000')
		IF @V='' SET @V='000000'

		EXEC PREAXSQL.ORKLB000.[dbo].[PAUX_EtiTrazabValores]  --este procemiento se ha creado por no poder grabar directamente en AX, por tema permisos. iker. 15/4/2016
				@empresa = @EMPR,
				@linea = @Usu, -- @Ubi, 
				@P = @A,
				@Q = @CTD ,
				@V = @V,
				@serie = @S,
				@H = @L
		
		
			SET @VCAP='PROV:' + @V -- + ';TRAT:16' + ';POS:8'    --CUIDADO EN APLICAR ESTO, YA QUE LUEGO NO ENCUENTRA EL CIRCUITO DE ESTE PROVEEDOR. CAMBIAR NOMBRE DE VARIALBE.


		EXEC	@return_value = PREAXSQL.[ORKLB000].[dbo].[Captor_StartConsumption]
			@Empresa = N'01',
			@Maquina = @Usu,
			@NLote =@LotCap,
			@Referencia = @A,
			@Cantidad = @CTD,
			@DatosAdicionales = N''		--DEBERÍA DE SER EL SIGUIENTE PARA AÑADIR EL PROVEEDOR. DE MOMENTO NADA. @DatosAdicionales = @VCAP

		
		
		--MIRAMOS SI LA REF Y PROVEEDOR DISPONE DE CIRCUITO O NO 
		SET @CIR=''
		
		SELECT @CIR= id_circuito
		FROM  dbo.circuito
		WHERE (referencia_id = @A) AND (Proveedor_id = @V) AND (ubicacion = @Ubi) AND    (activo = 1)

		SET @CIR=isnull(@CIR,'')	--SI NO EXISTE MANTENEMOS EN ''

		IF @CIR=''
		BEGIN
			SET @A=''  --ANULAMOS LA REFERENCIA PARA EVITAR QUE SE PUEDA DAR CONSUMO DE UNA REFERENCIA QUE SEA DE OTRO PROVEEDOR QUE TENGA KANBAN
			SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
			WHERE        (ubicacion = @Ubi) AND (activo = 1) and referencia_id='TRAZABILITATEA'
			set @A=isnull(@A,'')
		END


		SET @S=@L + '/' + @S +@CIR
		SET @L ='000000'

	end

	
--*************************************************************************************************************************************************

	if left(@txt,5)='[)>{R'
	begin
		SET @TP=8
		SET @txt='[)>{RS}06{GS}2L1{GS}V001700{GS}1PXXX{GS}16K004330{GS}Q1{GS}PF-4002-4{GS}1JUNXXXXXXXXXXXX{GS}7Q1058{GS}7Q16GT{GS}T147/99{GS}6DD10/9/99{GS}S2{GS}2P4{RS}{EOT}'



		SET @L ='000000'
		SET @Cli ='00000000'
		SET @S = CONVERT(varchar(100), GETDATE(), 121) 
		SET @A='Prueba GTL'
	end


--*************************************************************************************************************************************************


	--ETIQUETA "MALA" CON SOLO ARTÍCULO DE SKINTER
	--A2-609
	if SUBSTRING(@txt, 1, 1)='A' AND CHARINDEX('-', @txt, 1) =3
	begin
		SET @TP=9
		SET @L ='000000'
		SET @Cli ='00000000'
		SET @S = CONVERT(varchar(100), GETDATE(), 121) 
		SET @A=@Txt
	end 

	
--*************************************************************************************************************************************************


	--ETIQUETA EAN 13
	--8477637477
	if LEN(@TXT)=13 AND (SUBSTRING(@txt, 1, 2)='84' OR SUBSTRING(@txt, 1, 2)='44')
	begin
		SET @TP=10
		SET @L ='000000'
		SET @Cli ='00000000'
		SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
		SET @A=@Txt
	end 

	
--*************************************************************************************************************************************************

	--LECTURAS RFID DE LOS PORTICOS
	--114208092289
	if LEN(@TXT)=12 AND left(@txt,1)<>'P' AND LEFT(@TXT,2)<>'P*' AND LEFT(@TXT,3)<>'CAE' AND LEFT(@TXT,2)<>'AL' AND LEFT(@TXT,4)<>'RFID'
	begin
		SET @TP=11

		SET @L =SUBSTRING(@txt, 1, 6)
		SET @S =SUBSTRING(@txt, 7, 6)


		SELECT        @A= ITEMID, @CLI=CUSTACCOUNT
		FROM            dbo.VAX_EKAN_OF
		WHERE        (PRODID = @L)
	
			
		SELECT       @CIR= id_circuito,  @TipCont = tipo_contenedor,  @CLI=cliente_id
		FROM            dbo.circuito
		WHERE        (referencia_id = @A) AND (ubicacion = @Ubi) --AND (cliente_id = @CLI)   --al haber cambiado al hijo de SABAF, 01, 


		set @TipCont=isnull(@TipCont,0)
		--QUE OCURRE SI EL CLI ES EL HIJO, Y ES A NIVEL DE PALET?, QUE AL NO EXISTIR EL CIRCUITO DE ESE CLIENTE, NO GENERAR CONSUMO
		--XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

		-- EN CASO DE QUE SEA A NIVEL DE PALET
		-- DESCARTAMOS NUM SERIE Y LE PASAMOS 000000 
		IF @TipCont=2 AND @L<>'000000'
		BEGIN 
			IF @Ubi='UWOR'
				BEGIN
				--si es en la ubicación BOSCH, LE AÑADIMOS EL NÚMERO DE PALET AL SERIE
			
				set @S=@L + @S --SUBSTRING(@S,1,2) + '0000'--MACHACAMOS EL NÚMERO DE SERIE CON EL LOTE
				set @L='000000' --PARA QUE COJA LOS ARTÍCULOS QUE SE INDICAN EN ESTE RESULTADO

				END
			ELSE
				BEGIN 
				--LECTURAS DE SABAF
			
				set @S=@L --MACHACAMOS EL NÚMERO DE SERIE CON EL LOTE
				set @L='000000' --PARA QUE COJA LOS ARTÍCULOS QUE SE INDICAN EN ESTE RESULTADO


				END
		
		END
		ELSE
		BEGIN
		IF @TipCont=1 
			begin

				set @S=@L + @S--MACHACAMOS EL NÚMERO DE SERIE CON EL LOTE
					set @L='000000' --PARA QUE COJA LOS ARTÍCULOS QUE SE INDICAN EN ESTE RESULTADO

			end


		END
				
			--*****************************
			--PARA COGER SÓLO LAS ANTENAS QUE QUEREMOS
			DECLARE @ActAnt0 bit
			DECLARE @ActAnt1 bit

			SELECT        @ActAnt0=dbo.dispositivo.Dis_ActAnt0, @ActAnt1=dbo.dispositivo.Dis_ActAnt1
			FROM            dbo.dispositivo INNER JOIN
			dbo.dispositivo_ubicacion ON dbo.dispositivo.id_dispositivo = dbo.dispositivo_ubicacion.id_dispositivo
			WHERE        (dbo.dispositivo_ubicacion.id_ubicacion = @Ubi)

			if (@Ant='Ant0' and @ActAnt0=0) or (@Ant='Ant1' and @ActAnt1=0)--SI ES LA ANTENA X Y NO HAY QUE CONSIDERARLO, BORRA EL ART PARA NO CONSIDERARLO
			begin
				set @A='' --lo borramos para que no habiendo lote y sin referencia no considere nada
				set @S='000000'
				set @L='000000'
			end
			--*****************************
			
			if @Ubi='BIDI' set @S=@S + 'DI'

			if @Ubi='BIDI' AND @Sen='S'
			BEGIN
			
				DELETE FROM [dbo].[tarjeta]
				WHERE (id_lectura = @L + '_' + @S) AND (ubicacion = 'BIDI')	AND (sentido = 'E')	
			END
			
			if @Ubi='BIDI' AND @Sen='E'
			BEGIN
			
				DELETE FROM [dbo].[tarjeta]
				WHERE (id_lectura = @L + '_' + @S) AND (ubicacion = 'BIDI')	AND (sentido = 'S')	
			END

			

	end

	
	--LECTURAS RFID DE LOS PORTICOS CON CÓDIGO HEXADECIMAL
	--114208092289
	if (LEN(@TXT)=22 OR LEN(@TXT)=24 ) AND left(@txt,1)<>'P' AND LEFT(@TXT,2)<>'P*' AND LEFT(@TXT,3)<>'CAE' AND LEFT(@TXT,2)<>'AL' AND LEFT(@TXT,4)<>'RFID' AND LEFT(@TXT,4)<>'PROV'
	begin
		SET @TP=110



		declare @sql nvarchar(MAX), @ch varchar(200)
		--select @v = '313931323434323637393338'
		select @sql = 'SELECT @ch = convert(varchar, 0x' + @TXT + ')'
		EXEC sp_executesql @sql, N'@ch varchar(30) OUTPUT', @ch OUTPUT
		--SELECT @ch AS ConvertedToASCII
		SET @txt=@ch
		
		SET @L =SUBSTRING(@txt, 1, 6)
		SET @S =SUBSTRING(@txt, 7, 6)

		SELECT        @A= ITEMID, @CLI=CUSTACCOUNT
		FROM            dbo.VAX_EKAN_OF
		WHERE        (PRODID = @L)
	
			
		SELECT       @CIR= id_circuito,  @TipCont = tipo_contenedor,  @CLI=cliente_id
		FROM            dbo.circuito
		WHERE        (referencia_id = @A) AND (ubicacion = @Ubi) --AND (cliente_id = @CLI)   --al haber cambiado al hijo de SABAF, 01, 



		--QUE OCURRE SI EL CLI ES EL HIJO, Y ES A NIVEL DE PALET?, QUE AL NO EXISTIR EL CIRCUITO DE ESE CLIENTE, NO GENERAR CONSUMO
		--XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

		-- EN CASO DE QUE SEA A NIVEL DE PALET
		-- DESCARTAMOS NUM SERIE Y LE PASAMOS 000000 
		IF @TipCont=2 AND @L<>'000000'
		BEGIN 

			--SET @S ='000000'

			set @S=@L
			--al haber cambiado en sabaf a hijos
			set @L='000000'
			


		END
		IF @TipCont<>2 AND @L<>'000000'
		BEGIN 

			--SET @S ='000000'

			--set @S=@L
			--al haber cambiado en sabaf a hijos
			set @L='000000'
			


		END

					
			if @Ubi='BIDI' set @S=@S + 'DI'

			if @Ubi='BIDI' AND @Sen='S'
			BEGIN
			
				DELETE FROM [dbo].[tarjeta]
				WHERE (id_lectura = @L + '_' + @S) AND (ubicacion = 'BIDI')	AND (sentido = 'E')	
			END
			
			if @Ubi='BIDI' AND @Sen='E'
			BEGIN
			
				DELETE FROM [dbo].[tarjeta]
				WHERE (id_lectura = @L + '_' + @S) AND (ubicacion = 'BIDI')	AND (sentido = 'S')	
			END


	end

	
--*************************************************************************************************************************************************
	
	--control de stocks DISPONIBILIDAD
	if LEFT(@TXT,3)='CAE' --AND LEN(@TXT)=12 left(@txt,1)<>'P' AND LEFT(@TXT,2)<>'P*' AND 
	BEGIN
		SET @TP=12
		begin 
			set @A=substring(@TXT,1,6)
			
			
			SELECT      @UbiTip=Ubi_Modo   --EVEN  DISP
			FROM        dbo.ubicacion
			WHERE       id_ubicacion = @Ubi


			SET @L='000000'
			SET @Cli ='00000000'
			SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
			SET @S = @Ubi + '_' + @TXT 


			IF @Sen='E' AND @UbiTip='DISP'
			BEGIN
				DELETE FROM [dbo].[tarjeta]
				WHERE
				id_lectura=@L + '_' + @S AND SENTIDO='S'
			END





		end
	END

	
--*************************************************************************************************************************************************

	--LECTURA DE ALBARANES (CONSIGNA)
	--AL00109472_1_134340    
	if  LEFT(@TXT,2)='AL'
	begin
			DECLARE @CLI1 AS VARCHAR(25)=''
			

			SET @TP=13
			
			SET @ALB = SUBSTRING(@txt, 1, CHARINDEX('_', @txt, 1)-1)
			SET @ALBLIN = SUBSTRING(@txt,  CHARINDEX('_', @txt, 1)+1,1)
			SET @L = SUBSTRING(@txt,  CHARINDEX('_', @txt,  CHARINDEX('_', @txt, 1)+1)+1, len(@txt))
			
			--000500 ES UN LOTE FICTICIO DEL AX
			SET @L=ISNULL(@L,'000500')
			IF @L='' SET @L='000500'


			SET @S = @txt 

			--Cogemos el artículo según el lote de fabricación. 
			SELECT        @A= ITEMID, @CLI=CUSTACCOUNT
			FROM            dbo.VAX_EKAN_OF
			WHERE        (PRODID = @L)


			--CANTIDADES SEGÚN EL ALBARAN??????????????????????????????????????????????????????????


			--Cogemos el cliente, según el artículo y ubicación donde estamos leyendo.
			SELECT       @CIR= id_circuito,  @TipCont = tipo_contenedor, @CLI1=cliente_id 
			FROM            dbo.circuito
			WHERE        (referencia_id = @A) AND (ubicacion = @Ubi) AND   (activo = 1)

			IF @CLI<>@CLI1
			begin
				set @CLI=@CLI1
				SET @L='000000'
			end
			



	end

	--Si es sólo lote de la etiqueta, cogemos el lote y apartir de este dato, consultará en vista de OFs
	if left(@txt,1)='H' AND LEN(@TXT)=7
	begin
		--H113688
		SET @TP=15
		SET @L =SUBSTRING(@txt, 2, 6)
		SET @S ='000000'
	end 
	
--*************************************************************************************************************************************************

	IF  LEFT(@TXT,4)='RFID'
	BEGIN
		SET @TP=16
		SET @L ='000000'--SUBSTRING(@txt, 1, 8)
		SET @S =SUBSTRING(@txt, 1, 8)
		set @cade = @TXT + ' en Ubi: ' + @Ubi + ' ' --    + CONVERT(CHAR(3),@NMinLecPor)  + '. '
		declare @cade1 as varchar(50)

		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
		SELECT @A=REFERENCIA_ID, @Cli=cliente_id FROM dbo.circuito WHERE id_circuito = @TXT
		SET @L='000000'
		SET @S = CONVERT(varchar(100), GETDATE(), 121) 



		
			--*****************************
			--PARA COGER SÓLO LAS ANTENAS QUE QUEREMOS
			--DECLARE @ActAnt0 bit
			--DECLARE @ActAnt1 bit

			SELECT        @ActAnt0=dbo.dispositivo.Dis_ActAnt0, @ActAnt1=dbo.dispositivo.Dis_ActAnt1
			FROM            dbo.dispositivo INNER JOIN
			dbo.dispositivo_ubicacion ON dbo.dispositivo.id_dispositivo = dbo.dispositivo_ubicacion.id_dispositivo
			WHERE        (dbo.dispositivo_ubicacion.id_ubicacion = @Ubi)

			if (@Ant='Ant0' and @ActAnt0=0) or (@Ant='Ant1' and @ActAnt1=0)--SI ES LA ANTENA X Y NO HAY QUE CONSIDERARLO, BORRA EL ART PARA NO CONSIDERARLO
			begin
				set @A='' --lo borramos para que no habiendo lote y sin referencia no considere nada
			end
			--*****************************
			




		SELECT        @cade1=mail
		FROM            dbo.circuito
		WHERE        (id_circuito = @TXT)


		set @cade1=isnull(@cade1,'')

		--SELECT @cade1= [descripcion] + '; iker@orkli.es'
		--FROM [dbo].[sga_parametros]
		--where [nombre]=@TXT
		IF @cade1<>''
		BEGIN
			EXEC	[dbo].[PEnvCorreo]
			@Des = @cade1,
			@Tem = N'Lectura de Etiqueta TEST RFID',
			@Cue =@cade
		END

	END

	
--*************************************************************************************************************************************************

	--ELABORACIÓN, SEGUIMINETO TRAZABILIDAD THIELENHAUS....
	
	IF  LEFT(@TXT,2)='SO' AND @TP=0 --COINCIDE CON AL OPCIÓN Y TODAVÍA NO HA ENTRADO EN NINGUNA OPCIÓN ANTERIOR. 
	BEGIN
		SET @TP=17

			SET @S=SUBSTRING(@TXT, 2, LEN(@TXT)+1)
		EXEC	PREAXSQL.ORKLB000.[dbo].[PAUX_EtiTrazab]
		@serie =@S,--'ORKA50136',--
		@linea =@Ubi


		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
		SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
		WHERE        (ubicacion = @Ubi) AND (activo = 1)
		ORDER BY num_kanban


		SET @S=@L + '/' + @S
		SET @L ='000000'
	END


	--V001857PE-20262Q4093H126511K154572   la etiqueta de vilardell
	--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

	--PTE


	--ETIQUETA GS1, SEGÚN NUEVA NORMA. ENERO 2016
	--[)>06V000782PA5-437Q450H2/5357S
	
	if left(@txt,3)='[)>'
		BEGIN
		SET @TP=18
			print @txt;
			SET @L=''
			SET @A=''
			set @CIR=''

			SELECT @A=DBO.GS1Data(@txt,'P')
			SELECT @CTD=DBO.GS1Data(@txt,'Q')
			SELECT @L=DBO.GS1Data(@txt,'H')
			IF @L='' SELECT @L=DBO.GS1Data(@txt,'T')
			SELECT @S=DBO.GS1Data(@txt,'S')
			SELECT @V=DBO.GS1Data(@txt,'V')

			--SELECT @16K=DBO.GS1Data(@txt,'16K')
			--SELECT @2L=DBO.GS1Data(@txt,'2L')
			--SELECT @1J=DBO.GS1Data(@txt,'1J')
			--SELECT @5J=DBO.GS1Data(@txt,'5J')
			--SELECT @6J=DBO.GS1Data(@txt,'6J')
			--SELECT @7Q58=DBO.GS1Data(@txt,'7Q')
			--SELECT @16D=DBO.GS1Data(@txt,'16D')
			--SELECT @2P=DBO.GS1Data(@txt,'2P')
			print @L;
			set @L=isnull(@L,'')
			IF @L='' SET @L='0' 
			


			--SET @CTD=REPLACE(@CTD,'.','') gs-n jarriak
			--SET @CTD=REPLACE(@CTD,',','.')gs-n jarriak
			SET @CTD= CONVERT(NUMERIC(9,1),@CTD)

			--revisamos a qué empresa corresponde la ubicación
			SELECT       @RegTraz=Ubi_RegTrazAX, @EMPR=Ubi_Dataareaid
			FROM            dbo.ubicacion
			WHERE        (id_ubicacion = @Ubi)
			
			--if @RegTraz=1
			--begin

			--	--REGISTRAMOS DATOS EN EL AX A TRAVÉS DEL PROCEDMINETO DEL SERVIDOR DEL AX.
			--	EXEC PREAXSQL.ORKLB000.[dbo].[PAUX_EtiTrazabValores]  --este procemiento se ha creado por no poder grabar directamente en AX, por tema permisos. iker. 15/4/2016
			--		@empresa = @EMPR,
			--		@linea = @Usu, -- @Ubi,
			--		@P = @A,
			--		@Q = @CTD ,
			--		@V = @V,
			--		@serie = @S,
			--		@H = @L
		


			--	----REGISTRAMOS LOS DATOS EN CAPTOR
			--	--como da error si el lote no existe...


			--	SET @V='PROV:' + @V
			--	set @LotCap=@L + '_' + @S

			--	EXEC	@return_value =PREAXSQL.[ORKLB000].[dbo].[Captor_StartConsumption]
			--			@Empresa = N'01',
			--			@Maquina = @Usu,
			--			@NLote =@LotCap,
			--			@Referencia = @A,
			--			@Cantidad =@CTD,
			--			@DatosAdicionales = @V

			--END

			--PARA QUE EL DISPOSITIVO MOVIL NO DE ERROR, ASIGNAMOS CIRCUITOS, LOTES ETC.
			--Cogemos el cod cliente de la ubiación
			SET @A=ISNULL(@A,'')
			SET @CIR=''
			--En caso de que ese artículo no tenga circuito, cogemos el de trazabilidad, para que podamos dar consumos. 


			if  @ubi='U45' OR @ubi='U49' OR @ubi='U51' OR @ubi='U52' OR @ubi='U53' 

				BEGIN 
				SELECT        @CIR=id_circuito, @Cli=cliente_id
				FROM            dbo.circuito
				WHERE        (referencia_id = @A) AND (ubicacion =  @Ubi)  AND activo=1 and Proveedor_id=@V
								
				END
			ELSE
				BEGIN
				
				SELECT        @CIR=id_circuito, @Cli=cliente_id
				FROM            dbo.circuito
				WHERE        (referencia_id = @A) AND (ubicacion =  @Ubi) AND activo=1
				END
			--PRINT 'CLIENTE = ' + @CLI;
			--PRINT 'CIRCUITO =' + @CIR;

			set @ant=@V




			SET @CIR=ISNULL(@CIR,'')

			--SI NO TIENE CIRCUITO EL ARTÍCULO, COGEMOS EL DE TRAZABILIDAD, SI EXISTE. SINO ERROR.
			IF @CIR='' or @A in ('20100034','20100060','L-11158','L-11180','L-11170','20100551','20100017','20100013') 
			--hasta el 2/9/2017 20100463 ere sartuta. hori kendu. iker.

			BEGIN
				SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
				WHERE        (ubicacion = @Ubi) AND (activo = 1) and referencia_id='TRAZABILITATEA'
			END


			SET @S=RTRIM(@S) + '_' + @L
			SET @L='000000'

		END
		---------------*******************************************************************

	IF  LEFT(@TXT,3)='UTI' AND @TP=0 and (@Ubi='UUTI' or @Ubi='UUTIDEK') --COINCIDE CON AL OPCIÓN Y TODAVÍA NO HA ENTRADO EN NINGUNA OPCIÓN ANTERIOR. 
	--UTI_P1210-1115-1-10_Q4_V000535
	BEGIN
		SET @TP=19
		
		DECLARE @Usuario nvarchar(50),
		@Referencia nvarchar(20),
		@ReferenciaDescripcion nvarchar(60),
		@Proveedor nvarchar(20),
		@Cantidad int,
		@Ubicacion nvarchar(600),
		@FechaSolicitud nvarchar(10),
		@FechaPrevista nvarchar(10),
		@Precio decimal(14,6),
		@Url nvarchar(1),
		@Resultado int
		
		DECLARE @SelectedValue int=0
		DECLARE @SelectedValueTXT VARCHAR(50)



		SET @A = SUBSTRING(@txt, CHARINDEX('_P', @txt, 1)+2,CHARINDEX('_Q', @txt, 1)-CHARINDEX('_P', @txt, 1)-2)
		SET @CTD = SUBSTRING(@txt, CHARINDEX('_Q', @txt, 1)+2,CHARINDEX('_V', @txt, 1)-CHARINDEX('_Q', @txt, 1)-2)
		SET @Cli = SUBSTRING(@txt, CHARINDEX('_V', @txt, 1)+2,CHARINDEX('_U', @txt, 1)-CHARINDEX('_V', @txt, 1)-2)
		SET @Ubicacion = SUBSTRING(@txt, CHARINDEX('_U', @txt, 1)+2,LEN(@TXT))

		SET @Cli =isnull(@Cli,'')
		SET @Ubicacion =isnull(@Ubicacion,'')

		SET @Cli = SUBSTRIng(@Cli ,1,6)


		set @URLPlano='1'--'http://srvsps2007/Procesos/UtilesPDF/' + @A + '.pdf'

		SET XACT_ABORT ON  
	   
		SELECT @Usuario=[Usu_Dominio]
		FROM [dbo].[sga_usuario]
		where [username]=@Usu

		SET @Usuario =isnull(@Usuario,'orkliord\gm')
		
		DECLARE @UbicacionDes nchar(100)
		
		SELECT @UbicacionDes=[descripcion]
		FROM [EKANBAN].[dbo].[ubicacion]
		WHERE   [id_ubicacion]=@Ubi
		
		SET @UbicacionDes =isnull(@UbicacionDes,'')


		SET @Referencia = @A  --N'1331-0111-0-11'
		SET @ReferenciaDescripcion = ''
		SET @Proveedor = @Cli
		SET @Cantidad = @CTD
		SET @Ubicacion = 'Sekzioa: ' + rtrim(LTRIM( @UbicacionDes)) + ' (' + @Ubicacion + ') Erreferentziak LASER bidez markatu.' 
		SET @FechaSolicitud = null --para que coja el de hoy
		SET @FechaPrevista = null --si es nulo, añade 21 días. 
		SET @Precio = 1 --no se utiliza para nada
		SET @Url = 1 --1 edo 0





		SELECT        @Referencia= ITEMID
		FROM             [PREaxsql].[SQLAXBAK].dbo.INVENTTABLE
		WHERE        (DATAAREAID = N'ork') AND (ITEMID = @A)

		set @Referencia=isnull(@Referencia,'') 

		if @Referencia=''
		begin
			SELECT     @Referencia=   ITEMID
			FROM             [PREaxsql].[SQLAXBAK].dbo.CUSTVENDEXTERNALITEM
			WHERE        (DATAAREAID = N'ORK') AND (EXTERNALITEMID = @A) AND (CUSTVENDRELATION = @Proveedor)
		end

		set @Referencia=isnull(@Referencia,'') 


		


		--EXEC [PREaxsql].[ORKLB000].[dbo].[TSolicitudesCompra_WS_SEL]
		--	'orkliord\aauzmendi',
		--	'1331-0111-0-11',
		--	'',
		--	'000535 MEKALAN',
		--	1,
		--	'UUTI',
		--	null,
		--	null,
		--	1,
		--	1,
		--   @Resultado OUTPUT

		EXEC [PREaxsql].[ORKLB000].[dbo].[TSolicitudesCompra_WS_SEL]
			@Usuario,
			@Referencia,
			@ReferenciaDescripcion,
			@Proveedor,
			@Cantidad,
			@Ubicacion,
			@FechaSolicitud,
			@FechaPrevista,
			@Precio,
			@Url,
		   @Resultado OUTPUT

		 SET @SelectedValueTXT=CASE @Resultado 
					WHEN 0 THEN 'Error dentro del servicio. Tendréis que comunicármelo para que revise que a poder pasar.'
					WHEN 1 THEN 'El pedido se ha generado. Otra cosa que se haya quedado parado, se haya un producido un problema al volcar al AX o haya todo correctamente. Estas situaciones ya se revisaran dentro de la aplicación.'
					WHEN 2 THEN 'Fecha solicitud incorrecta.  La fecha tiene un formato incorrecto o es anterior al día de hoy.'
					WHEN 3 THEN 'Fecha prevista incorrecta.  La fecha tiene un formato incorrecto'
					WHEN 4 THEN 'Fecha Prevista menor que la Fecha Solicitud.'
					WHEN 5 THEN 'Usuario incorrecto. El usuario que se pasa (usuario de red) es incorrecto o no tiene permisos para Compras.'
					WHEN 6 THEN 'Empresa incorrecta. La empresa del usuario no es correcta (No debería pasar que se valida por si acaso).'
					WHEN 7 THEN 'Sección incorrecta. Se valida que la sección del usuario es correcta (No debería pasar que se valida por si acaso).'
					WHEN 8 THEN 'Almacén incorrecto. Se valida que el almacén que se obtiene en base a los datos de la sección del usuario es correcto en base a la empresa en que estamos tratando (No debería pasar que se valida por si acaso).'
					WHEN 9 THEN 'Referencia incorrecta. El código de la referencia no existe en la empresa que estamos tratando.'
					WHEN 10 THEN 'Proveedor incorrecto. El código de proveedor introducido no existe para la empresa que estamos tratando.'
					WHEN 11 THEN 'Precio no encontrado. No hay en el AX un precio definido para ese artículo y proveedor.'
				END
		


		--REGISTRAMOS EL PEDIDO EN CREAR PEDIDOS, PERO YA TRATADO CON ESTADO 99. PARA SEGUIMIENTO
		INSERT INTO [PREaxsql].[ORKLB000].[dbo].[TAUX_CREAR_PEDIDO]
		(Status, Journalname, Name, CUSTACCOUNT, PURCHORDERFORMNUM, ITEMID, SALESQTY,  SHIPPINGDATEREQUESTED, uservolc, fechaVolc, Reservaauto, type, SalesType, InventLocationId, FacturarAut, PedMarco, NumFactCli, AlbaranarAut, RegistrarPedCompra, DocumentoAdjunto,  Observaciones)
		VALUES
		('99',    @Referencia,        'UTILES', substring(@Cli,1,6),  @Usu, @A,   @CTD,      GETDATE()+7,           @Usu,   GETDATE(),   '0',    'PRO', '3',                       '', 0, '','', 0,0,@SelectedValue,  @Ubicacion + @SelectedValueTXT)
		
		IF @Resultado=1--1
		BEGIN 
		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
			SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
			WHERE        (ubicacion = @Ubi) AND (activo = 1)
			ORDER BY num_kanban
		END

		SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
		SET @L ='000000'

	END

	---------------*******************************************************************

	IF   @Ubi='URFIdD' --COINCIDE CON AL OPCIÓN Y TODAVÍA NO HA ENTRADO EN NINGUNA OPCIÓN ANTERIOR. 
	--pruebas de EMBEBLUE
	BEGIN
		SET @TP=20

		--SET @A = SUBSTRING(@txt, CHARINDEX('_P', @txt, 1)+2,CHARINDEX('_Q', @txt, 1)-CHARINDEX('_P', @txt, 1)-2)
		--SET @CTD = SUBSTRING(@txt, CHARINDEX('_Q', @txt, 1)+2,CHARINDEX('_V', @txt, 1)-CHARINDEX('_Q', @txt, 1)-2)
		--SET @Cli = SUBSTRING(@txt, CHARINDEX('_V', @txt, 1)+2,LEN(@TXT))
			
		
		SET @cade=@Usu + ': ' + @Txt
		--SELECT @cade1= [descripcion] + '; iker@orkli.es'
		--FROM [dbo].[sga_parametros]
		--where [nombre]=@TXT
		--IF @cade1<>''
		--BEGIN
		--	EXEC	[dbo].[PEnvCorreo]
		--	@Des = 'test_910@embeblue.com; auxiliar@orkli.es; iker@orkli.es',
		--	@Tem = N'Lectura de Etiqueta RFID EMBEBLUE',
		--	@Cue =@cade
		--END

		--Cogemos el artículo con más tarjetas kanban/mas comun de la ubicación
		SELECT @A=referencia_id, @Cli=cliente_id FROM dbo.circuito 
		WHERE        (ubicacion = @Ubi) AND (activo = 1)
		ORDER BY num_kanban


		SET @S = CONVERT(nvarchar(30), GETDATE(), 121) 
		SET @L ='000000'

	END
	
		
--*************************************************************************************************************************************************


	if @txt like '%@%' 
	begin
		SET @TP=14
		DECLARE @Notif bit=0

		SELECT       @Notif= Ubi_NotEtiVacias
		FROM            dbo.ubicacion
		WHERE        (id_ubicacion = @Ubi)



		set @cade = 'Ubi: ' + @Ubi + ', Texto: ' + @txt +' ' --    + CONVERT(CHAR(3),@NMinLecPor)  + '. '

		if @Notif=1 --@Ubi<>'UCAE'
			begin
			EXEC	[dbo].[PEnvCorreo]
					@Des = N'alert@ekanban.es;',
					@Tem = N'Lectura de Etiquetas Vacías por RFID',
					@Cue = @cade
			end
	end



	
--*************************************************************************************************************************************************
REGISTROLECTURAS:

print 'tregistroLecturas';

INSERT INTO [dbo].[TRegistroLecturas] 
           ([Reg_Fec]
           ,[Reg_Dis]
           ,[Reg_Usu]
           ,[Reg_Ubi]
           ,[Reg_Pan]
           ,[Reg_Txt]
           ,[Reg_Cir]
           ,[Reg_Cli]
           ,[Reg_Art]
           ,[Reg_Lot]
           ,[Reg_Ser]
		   ,[Reg_Ant]
		   ,[Reg_Sen]
		   ,[Reg_Tip]
		   ,[Reg_Ctd]) 
     VALUES
           (getdate()
           ,@Disp
           ,@Usu
		   ,@Ubi
           ,@Pant
           ,@Txt
           ,@CIR
           ,@Cli
           ,@A
           ,@L
           ,@s
		   ,@Ant
		   ,@Sen
		   ,@TP
		   ,@CTD)


	
	--select @L1,  @L2, @L,  @S1,  @S2, @S, @A1,  @A2, @A
	select @L AS LOT,  @S AS SER, @A AS ART, 0 as CTD, '000000' AS PRO, @Cli as CLI,  @L + '_' + @S AS GRAB, @CTD AS CTDLEIDA, @PED AS IDPED
	
	-----20181005 INCLUSION DE TABLA HISTORICOVERDES	
	exec dbo.PAX_HistoricoVerdes @Circuito = @CIR, @Origen = @OrigenLectura, @Usuario = @Usu
	-----20181005 FIN INCLUSION DE TABLA HISTORICOVERDES

	commit;
	print getDate();
	SET XACT_ABORT OFF;  --- iker. 2016-05-09 crear pedidos-en sortu ahal izateko. iker.
		



END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAccAjustarStock]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PAccAjustarStock]
	-- Add the parameters for the stored procedure here
	@CodCircuito varchar(20) = '',
	@NumTarjVerdes integer = 0,
	@Usu varchar(255) = '',
	@Dis varchar(4)=''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @codRespuesta int; declare @resultado varchar(255);
	
	declare @Tresultado table (Resul  varchar(255), MsgTxt  varchar(255));
	
	declare @result table (art varchar(255), artcli varchar(255), t int, r  int, a  int, ae  int, v  int, ss  int,
				 estado int, circuito varchar(255), ultLec datetime, Cont  int, Circ  int, opt int, transito_Activado bit, ctd_Kan int);	
	
    -- Insert statements for procedure here
	INSERT INTO @Tresultado EXEC PAccAjustar @CodCircuito, @NumTarjVerdes, @Usu, @Dis;
	
	select @codRespuesta= Resul, @resultado = MsgTxt from @Tresultado;
	--if @codRespuesta = 0
	--begin
	insert into @result exec PAX_SitTarjetas @codCircuito, @Usu;
	select *, @codRespuesta as Result, @resultado as Msg from @result;
	--end	
	--else
	--begin
		--select * from @Tresultado;
	--end
	
	-----20181008 INCLUSION DE TABLA HISTORICOVERDES	
	exec dbo.PAX_HistoricoVerdes @Circuito = @CodCircuito, @Origen = 'AjusteStock', @Usuario = @Usu
	-----20181008 FIN INCLUSION DE TABLA HISTORICOVERDES
	
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAX_CtaLeerFicha]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PAX_CtaLeerFicha] 
	-- Add the parameters for the stored procedure here
	@codQR varchar(255) = '',
	@ref varchar(255)= '',
	@usuarioID varchar(255) = '',
	@ubicacionID varchar(255) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @REFMODIFICADA VARCHAR(255);
	DECLARE @OF VARCHAR(255);
	DECLARE @NUMSERIE VARCHAR(255);
	DECLARE @REFERENCIA VARCHAR(255);
	DECLARE @CANTIDAD INT;
	DECLARE @PROVEEDOR VARCHAR(255)
	DECLARE @CLIENTE VARCHAR(255);
	DECLARE @TEXTO VARCHAR(255);
	DECLARE @CTDLEIDA VARCHAR(255);
	DECLARE @IDPED VARCHAR(255);
	DECLARE @REFID VARCHAR(255) = NULL;
	DECLARE @CLIENTEID VARCHAR(255) = NULL;
	declare @circuitoID varchar(255);
	
	-----20181008 INCLUSION DE TABLA HISTORICOVERDES
	DECLARE @Origen as varchar(25);
	
	SET @Origen = 'LecturaQR'
	-----20181008 FIN DE TABLA HISTORICOVERDES
	
	
	DECLARE @TRESULT TABLE (OFAB VARCHAR(255), ARTICULO VARCHAR(255), ARTICULO_CLIENTE VARCHAR(255), DESCRIP VARCHAR(255), FPROD DATE,
		LINEA VARCHAR(255), ENG VARCHAR(255), DESENG VARCHAR(255), CTDCAJA VARCHAR(255), TENG VARCHAR(255), TDES VARCHAR(255), TCTDCAJA VARCHAR(255));
    -- Insert statements for procedure here
   declare @TTarjeta table (LOT varchar(255), SER varchar(255), ART varchar(255), CTD int, 
		PRO varchar(255), CLI varchar(255), GRAB varchar(255), CTDLEIDA varchar(255), IDPED varchar(255));
    if @codQR IS NULL OR @codQR = ''
		BEGIN
			--referencia metida manualmente
			SET @REFMODIFICADA = 'P' + 	@referencia;
			insert into @TTarjeta
				EXEC	[dbo].[PAX_PatronLec] @REFMODIFICADA, @usuarioID, @ubicacionID, 1, 'M', ' ', ' ', @Origen
				-----20181008 ORIGEN INCLUIDO PARA LAE TABLA HISTORICOVERDES
			select TOP(1) @OF = lot, @NUMSERIE = SER, @REFERENCIA=ART, @CANTIDAD = CTD, @PROVEEDOR = PRO, @CLIENTE=CLI, @TEXTO = GRAB
			, @CTDLEIDA = CTDLEIDA,  @IDPED = IDPED from @TTarjeta
			IF @REFERENCIA IS NOT NULL AND @REFERENCIA <> ''  
				BEGIN
					print @REFERENCIA;
					select @circuitoID = id_circuito from circuito where ubicacion = @ubicacionID and activo = 1 and referencia_id = @REFERENCIA
					INSERT INTO @TRESULT
						EXEC [dbo].PAX_CtaEspecifaciones null, @REFERENCIA, @usuarioID, @ubicacionID
					print 'ok';
					--return 0;
				END
			else
				begin
					--referencia en  blanco
					--select @REFERENCIA = referencia_id, @circuitoID = id_circuito from circuito where ubicacion =  @ubicacionID and activo = 1;
					--set @REFERENCIA = '';
					--set @circuitoID = '';
					if @ref is null or @ref = ''
					begin
						select @REFERENCIA = referencia_id, @circuitoID = id_circuito from circuito where ubicacion =  @ubicacionID and activo = 1;
					end
					else
					begin
						set @REFERENCIA = @ref;
						IF @circuitoID = '' OR @circuitoID IS NULL
						BEGIN
							select @circuitoID = id_circuito from circuito where ubicacion =  @ubicacionID and activo = 1 AND referencia_id= @REF;
						END
						IF @circuitoID = '' OR @circuitoID IS NULL
						BEGIN
							-- LA REFERENCIA INTRODUCIDA ES LA REF DEL CLIENTE
							select @circuitoID = id_circuito from circuito where ubicacion =  @ubicacionID and activo = 1 AND ref_cliente= @REF;
						END
					end
					INSERT INTO @TRESULT
						EXEC [dbo].PAX_CtaEspecifaciones null, @REFERENCIA, @usuarioID, @ubicacionID
					--return 11;
				end
		END
    ELSE
		BEGIN
			--FICHA DESDE EL CODIGO QR
			--print 'Lectura con QR'
			insert into @TTarjeta
				EXEC	[dbo].[PAX_PatronLec] @codQR, @usuarioID, @ubicacionID, 1, 'M', ' ', ' ', @Origen
			select TOP(1) @OF = lot, @NUMSERIE = SER, @REFERENCIA=ART, @CANTIDAD = CTD, @PROVEEDOR = PRO, @CLIENTE=CLI, @TEXTO = GRAB
			, @CTDLEIDA = CTDLEIDA,  @IDPED = IDPED from @TTarjeta
			select @circuitoID = id_circuito from circuito where ubicacion = @ubicacionID and activo = 1 and referencia_id = @REFERENCIA
			print @TEXTO
			IF @TEXTO <> '_'
			BEGIN
				IF @OF <> '000000'
				BEGIN
					--PRINT 'OF NO ES 000000'
					SELECT @REFID = ITEMID, @CLIENTEID = CUSTACCOUNT FROM VAX_EKAN_OF WHERE PRODID= @OF
					IF @REFID IS NULL AND @CLIENTEID IS NULL
					BEGIN
						RETURN 6 --orden de fabricación inexisistente
					END
					
				END	
				else
				begin
					-- OF es 000000
					--PRINT 'OF ES 000000'
					--print @REFERENCIA 
					INSERT INTO @TRESULT
					EXEC [dbo].PAX_CtaEspecifaciones null, @REFERENCIA, @usuarioID, @ubicacionID
				end
				PRINT @REFID;
				PRINT @CLIENTEID;
				IF @REFID IS NOT NULL AND @CLIENTEID IS NOT NULL
				BEGIN
					INSERT INTO @TRESULT
					EXEC [dbo].PAX_CtaEspecifaciones @OF, @REFERENCIA, @usuarioID, @ubicacionID	
				END
			END
			ELSE
			BEGIN
				RETURN 6;
			END
		END
		PRINT @circuitoID;
		SELECT *, @circuitoID as circuito FROM @TRESULT;
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAccAnularLectura]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

ALTER PROCEDURE [dbo].[PAccAnularLectura]
	-- Add the parameters for the stored procedure here
	@codQR varchar(255) = '',
	@idUbi varchar(255) = '',
	@idUsu varchar(255) = '',
	@Pant int = 0,
	@Disp varchar(255) = 'M',
	@Ant varchar(50) = ' ',
	@Sen varchar(5) = ' '
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @OF VARCHAR(255);
	DECLARE @NUMSERIE VARCHAR(255);
	DECLARE @REFERENCIA VARCHAR(255);
	DECLARE @CANTIDAD INT;
	DECLARE @PROVEEDOR VARCHAR(255)
	DECLARE @CLIENTE VARCHAR(255);
	DECLARE @TEXTO VARCHAR(255);
	DECLARE @CTDLEIDA VARCHAR(255);
	DECLARE @IDPED VARCHAR(255);
	DECLARE @RESULT AS INT = -1;
	DECLARE @MsgTxt AS VARCHAR(255) = 'OK';
	declare @idCircuito as varchar(255);
	-----20181008 INCLUSION DE TABLA HISTORICOVERDES
	DECLARE @Origen as varchar(25);
	
	SET @Origen = 'LecturaAnulada';
	-----20181008 FIN DE TABLA HISTORICOVERDES

 ----   -- Ejecutamos el proc PAX_PatronLec: con este procedimiento obtenemos el registro de la lectura
	declare @TTarjeta table (LOT varchar(255), SER varchar(255), ART varchar(255), CTD int, 
		PRO varchar(255), CLI varchar(255), GRAB varchar(255), CTDLEIDA varchar(255), IDPED varchar(255));

	insert into @TTarjeta
		EXEC	[dbo].[PAX_PatronLec] @codQR, @idUsu, @idUbi, @Pant, @Disp, @Ant, @Sen, @Origen
		-----20181008 INCLUSION ORIGEN PARA LA TABLA HISTORICOVERDES 
	
	select TOP(1) @OF = lot, @NUMSERIE = SER, @REFERENCIA=ART, @CANTIDAD = CTD, @PROVEEDOR = PRO, @CLIENTE=CLI, @TEXTO = GRAB
		, @CTDLEIDA = CTDLEIDA,  @IDPED = IDPED from @TTarjeta
	print @TEXTO;
	IF @TEXTO <> '_'
	BEGIN
		IF EXISTS (SELECT *
					FROM tarjeta T
					WHERE T.id_lectura = @TEXTO AND  T.sentido = 'S')
			BEGIN
				print 'eliminar tarjeta';
				select @idCircuito = id_circuito from tarjeta WHERE id_lectura = @TEXTO AND sentido = 'S'; 
				DELETE FROM tarjeta WHERE id_lectura = @TEXTO AND sentido = 'S';
				SET @RESULT = 0;
				
			END
		
	END
	ELSE
	BEGIN
		print '@TEXTO = _ ';
	END 
	
	-----20181005 INCLUSION DE TABLA HISTORICOVERDES	
	exec dbo.PAX_HistoricoVerdes @Circuito = @IDCIRCUITO, @Origen = @Origen, @Usuario = @idUsu
	-----20181005 FIN INCLUSION DE TABLA HISTORICOVERDES
	--SELECT @RESULT AS RESULTADO
	select  @idCircuito as idCircuito, @RESULT as Result, @MsgTxt as Msg		
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END


GO
PRINT N'Modificando [dbo].[PAccRegLectura]...';


GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PAccRegLectura]
	-- Add the parameters for the stored procedure here
	@tagID as varchar(255),
	@imei as varchar(255),
	@idUbi as varchar(255),
	@idUsu as varchar(255),
	@antena as varchar(255) = '',
	@numSerie as varchar(255) = '',
	@sentido as varchar(255) = '',
	@tipoLectura as varchar(5) = '',
	@subUbicacion as varchar(255) = NULL,
	@Pant as int = 0
AS
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	 
	--BEGIN TRY
	DECLARE @OF VARCHAR(255);
	DECLARE @NUMEROSERIE VARCHAR(255);
	DECLARE @REFERENCIA VARCHAR(255);
	DECLARE @CANTIDAD INT;
	DECLARE @PROVEEDOR VARCHAR(255)
	DECLARE @CLIENTE VARCHAR(255);
	DECLARE @TEXTO VARCHAR(255);
	DECLARE @CTDLEIDA VARCHAR(255);
	DECLARE @IDPED VARCHAR(255);
	DECLARE @IDREFERENCIA VARCHAR(255) = '';
	DECLARE @IDCLIENTE VARCHAR(255)='';
	DECLARE @IDCIRCUITO VARCHAR(255) = '';
	DECLARE @IDLECTURA VARCHAR(255) = '';
	DECLARE @RESULT AS INT = -1;
	DECLARE @MsgTxt AS VARCHAR(255) = 'OK';
	DECLARE @SENTIDOLECTURA VARCHAR(5) = '';
	DECLARE @TIPOAVISO INT = 0;
	DECLARE @MSGAVISO VARCHAR(255)='';
	
	
	
	--SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
	BEGIN TRANSACTION;
								
	if @subUbicacion = ''
	begin
		set @subUbicacion = NULL;
	end
	
     --LECTURAS DE MOVIL
	IF @tipoLectura = 'M'
	BEGIN
		IF @tagID <> '' AND @idUbi <> '' AND @idUsu <> ''
		BEGIN	
			-- Ejecutamos el proc PAX_PatronLec: con este procedimiento obtenemos el registro de la lectura
			--SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
			begin  tran t1;
			
				-----20181008 INCLUSION DE TABLA HISTORICOVERDES
				DECLARE @Origen as varchar(25);
	
				SET @Origen = 'RegistrarLectura'
				-----20181008 FIN DE TABLA HISTORICOVERDES
	
			declare @TTarjeta table (LOT varchar(255), SER varchar(255), ART varchar(255), CTD int, 
				PRO varchar(255), CLI varchar(255), GRAB varchar(255), CTDLEIDA varchar(255), IDPED varchar(255));
			
			insert into @TTarjeta
				EXEC	[dbo].[PAX_PatronLec] @tagID, @idUsu, @idUbi, @Pant, 'M', ' ', ' ', @Origen
				-----20181008 INCLUSION DE ORIGEN PARA LA TABLA HISTORICOVERDES
			select TOP(1) @OF = lot, @NUMEROSERIE = SER, @REFERENCIA=ART, @CANTIDAD = CTD, @PROVEEDOR = PRO, @CLIENTE=CLI, @TEXTO = GRAB
				, @CTDLEIDA = CTDLEIDA,  @IDPED = IDPED from @TTarjeta 
			COMMIT TRANSACTION t1;
			
			print @texto;
			
			IF @OF ='000000'
				BEGIN 
					print 'of desde patronLec';
					SET @IDREFERENCIA = @REFERENCIA;
					SET @IDCLIENTE = @CLIENTE;
				END
			ELSE
				BEGIN
					SELECT TOP(1) @IDREFERENCIA = V_OF.ITEMID, @IDCLIENTE = V_OF.CUSTACCOUNT  FROM [dbo].VAX_EKAN_OF  AS V_OF WITH (NOLOCK) WHERE V_OF.PRODID = @OF
				END
						
						
			
			IF @TEXTO <> '_' --
			BEGIN
				IF NOT EXISTS(SELECT 1 
					FROM [dbo].tarjeta T  WITH (NOLOCK)
					WHERE T.id_lectura = @TEXTO) -- NO EXISTE UNA TARJETA CON ESA INFORMACION
				BEGIN
					--IF @OF ='000000'
					--	BEGIN 
					--		print 'of desde patronLec';
					--		SET @IDREFERENCIA = @REFERENCIA;
					--		SET @IDCLIENTE = @CLIENTE;
					--	END
					--ELSE
					--	BEGIN
					--		SELECT TOP(1) @IDREFERENCIA = V_OF.ITEMID, @IDCLIENTE = V_OF.CUSTACCOUNT  FROM [dbo].VAX_EKAN_OF  AS V_OF WITH (NOLOCK) WHERE V_OF.PRODID = @OF
					--	END
					PRINT 'REFERENCIA OF:' + @IDREFERENCIA + ' IDCLIENTE:' + @IDCLIENTE;
					IF @IDREFERENCIA is not null AND @IDCLIENTE is not null
					BEGIN
						print 'barruan';
						print @idcliente;
						print @idReferencia;
						--Buscar circuito por ubicacion, ref y cliente
						--SELECT * FROM ubicacion u WHERE id_ubicacion = @idUbi 
						select @IDCIRCUITO = id_circuito FROM [dbo].Circuito c WITH (NOLOCK) WHERE c.ubicacion = @idUbi AND c.cliente_id =@IDCLIENTE AND c.referencia_id =@IDREFERENCIA AND c.activo = 1
						PRINT 'CIRCUITO:' + @IDCIRCUITO;
						IF @IDCIRCUITO <> ''
							BEGIN
								--mirar si hay registros tipo ajuste pendientes de ser modificados
								SELECT @IDLECTURA = T.id_lectura FROM [dbo].Tarjeta t WITH (NOLOCK)  WHERE t.id_circuito=@IDCIRCUITO AND t.estado='A_MODIF' AND t.gestionado IS NULL ORDER BY t.fecha_lectura ASC
								IF @IDLECTURA = '' -- no existe ningun registro de tipo ajuste pendiente
								BEGIN
									--print 'insert into tarjeta'
									
									INSERT INTO [dbo].tarjeta WITH (ROWLOCK) (id_lectura, sentido, estado, fecha_lectura, id_circuito, movil, num_lecturas, ultima_lectura, kill_tag, antena, tipo, referencia_id, cliente_id, ubicacion, sububicacion) VALUES 
											(@TEXTO, 'S', 'P', GETDATE(), @IDCIRCUITO, @imei, 1, GETDATE(), 0, NULL, 'M', @IDREFERENCIA, @IDCLIENTE, @idUbi, @subUbicacion );
									SET @RESULT=  0;
									set @MsgTxt = 'Lectura correctamente registrada';
									
									set @TIPOAVISO = 1;
								END
								ELSE
								BEGIN
									--MODIFICAR REGISTRO @IDLECTURA
									
									UPDATE [dbo].tarjeta WITH (ROWLOCK)  SET gestionado='Ajuste' where id_lectura = @IDLECTURA
									INSERT INTO [dbo].tarjeta WITH (ROWLOCK)	 (id_lectura, sentido, estado, fecha_lectura, gestionado, id_circuito, movil, tipo, kill_tag, referencia_id, cliente_id, ubicacion, sububicacion) VALUES 
										(@TEXTO, 'S', 'A', GETDATE(), 'Ajuste', @IDCIRCUITO, @imei, 'M', 0, @IDREFERENCIA, @IDCLIENTE, @idUbi, @subUbicacion );
									SET @RESULT=  1;
									set @TIPOAVISO = 3;
									set @MsgTxt = 'Referencia leida, se ha incrementado el nº de lecturas';
									
								END 
							END
							else
							begin
		
								set  @MsgTxt = 'La referencia leída no pertenece a la ubicación seleccionada';
								
								set @TIPOAVISO = 2;
								SET @RESULT=  5;
								
							end  
					END
					
				END
				ELSE
				BEGIN
					select @IDCIRCUITO = id_circuito FROM [dbo].Circuito c WITH (NOLOCK) WHERE c.ubicacion = @idUbi AND c.cliente_id =@IDCLIENTE AND c.referencia_id =@IDREFERENCIA AND c.activo = 1
					IF @IDCIRCUITO <> '' begin
						--la etiqueta ya ha sido leida
						UPDATE [dbo].tarjeta  WITH (ROWLOCK) SET num_lecturas = num_lecturas + 1, fecha_lectura = GETDATE() WHERE TARJETA.id_lectura = @TEXTO
						SET @RESULT=  8;
						set @MsgTxt = 'Etiqueta leida, cantidad de lecturas actualizada';
						set @TIPOAVISO = 3;
					end
					else
						begin

						set  @MsgTxt = 'La referencia leída no pertenece a la ubicación seleccionada';
						set @TIPOAVISO = 2;
						SET @RESULT=  5;
					end
				END
			END	
			ELSE
			BEGIN
				set @MsgTxt = 'No existe ninguna OF para la etiqueta';
				set @TIPOAVISO = 4;
				SET @RESULT=  6;
			END 
		END
	END
	select  @IDCIRCUITO as circuito, @REFERENCIA as referencia, @RESULT as Result, @MsgTxt as Msg, @TIPOAVISO as Tipo_Aviso, @MsgTxt as Mensaje_Aviso;
	----select  'PRECOZ4' as circuito, '20900-51' as referencia, 0 as Result, 'OK' as Msg, '' as Tipo_Aviso, '' as Mensaje_Aviso;
	COMMIT TRANSACTION
----END TRY
----BEGIN CATCH 
----	ROLLBACK TRANSACTION
----	--select  '' as circuito, '' as referencia, -1 as Result, 'deadlock' as Msg, '2' as Tipo_Aviso, 'Error-Intente otra vez' as Mensaje_Aviso; 
----	IF ERROR_NUMBER() = 1205 -- Deadlock Error Number
----	BEGIN
----		WAITFOR DELAY '00:00:00.500' -- Wait for 5 ms
----		GOTO RETRY -- Go to Label RETRY
----	END
----	ELSE
----		select  '' as circuito, '' as referencia, -1 as Result, 'deadlock' as Msg, '2' as Tipo_Aviso, 'Error-Intente otra vez' as Mensaje_Aviso; 
----END CATCH

	-----20181005 INCLUSION DE TABLA HISTORICOVERDES	
	exec dbo.PAX_HistoricoVerdes @Circuito = @IDCIRCUITO, @Origen = 'RegistrarLectura', @Usuario = @idUsu
	-----20181005 FIN INCLUSION DE TABLA HISTORICOVERDES
	SET XACT_ABORT OFF;
END
GO
IF @@ERROR <> 0
   AND @@TRANCOUNT > 0
    BEGIN
        ROLLBACK;
    END

IF @@TRANCOUNT = 0
    BEGIN
        INSERT  INTO #tmpErrors (Error)
        VALUES                 (1);
        BEGIN TRANSACTION;
    END




GO

IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT N'La parte de transacción de la actualización de la base de datos se realizó correctamente.'
COMMIT TRANSACTION
END
ELSE PRINT N'Error de la parte de transacción de la actualización de la base de datos.'
GO
DROP TABLE #tmpErrors
GO
PRINT N'Comprobando los datos existentes con las restricciones recién creadas';


GO
USE [$(DatabaseName)];



GO
PRINT N'Actualización completada.';


GO
