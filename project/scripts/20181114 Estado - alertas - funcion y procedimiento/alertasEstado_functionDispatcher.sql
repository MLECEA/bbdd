USE [EKANBAN]
GO
/****** Object:  UserDefinedFunction [dbo].[alertasEstado_functionDispatcher]    Script Date: 11/13/2018 13:35:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[alertasEstado_functionDispatcher](
	@funcion SYSNAME,
	@parametro as nvarchar(50)
	)
RETURNS INT
AS
BEGIN
  DECLARE @r INT;
  IF OBJECT_ID(@funcion, N'FN') IS NULL  --handling non-existing function
    RETURN 401;
  EXEC @r = @funcion @Circuito = @parametro; 
  RETURN @r;
END;