USE [EKANBAN]
GO

/****** Object:  Table [dbo].[alertasEstado_funcionEstado]    Script Date: 11/14/2018 13:00:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[alertasEstado_funcionEstado](
	[clienteId] [varchar](255) NOT NULL,
	[tipoCircuito] [numeric](19, 0) NOT NULL,
	[funcionAlertasCambioEstado] [varchar](255) NOT NULL,
 CONSTRAINT [PK_alertasEstado_funcionEstado] PRIMARY KEY CLUSTERED 
(
	[tipoCircuito] ASC,
	[clienteId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO