USE [EKANBAN]
GO
/****** Object:  StoredProcedure [dbo].[alertasEstado_estado]    Script Date: 11/14/2018 13:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[alertasEstado_estado]
(@Circuito as nvarchar(50) )
AS
BEGIN
	BEGIN TRY
		DECLARE @estadoCircuito as INT
		DECLARE @estadoActualCircuito as INT
		DECLARE @Tipo as int

		print 'alertasEstado_estado - ' + @Circuito + ' Inicio'

		--1 proveedor, 3 cliente
		begin
			SELECT @Tipo=tipo_circuito
			FROM dbo.circuito
			WHERE id_circuito = @Circuito
		end
		set @Tipo=isnull(@Tipo, 1) -- si no estuviera definido, lo consideramos como proveedor

		--!!! forzamos a tipocliente !!!!! esto habrá que borrarlo
		--set @Tipo=3

		--set @Stat=1 --verde por defecto
		set @estadoCircuito = 0
		

		if @tipo=3 --cliente
			begin
				print 'alertasEstado_estado - ' + @Circuito + ' es cliente'

				-- obtenemos el estado actual del circuito
				EXEC @estadoCircuito = alertasEstado_calcularEstadoCircuito @Circuito
				Print N'alertasEstado_estado - Estado CALCULADO ' + CONVERT(varchar, @estadoCircuito)
				
				-- obtenemos el estado anterior del circuito
				EXEC @estadoActualCircuito = alertasEstado_getEstadoCircuito @Circuito
				print N'alertasEstado_estado - Estado ANTERIOR ' + CONVERT(varchar, @estadoActualCircuito)

				if (@estadoActualCircuito is null or @estadoCircuito <> @estadoActualCircuito)
					begin
						-- ha cambiado de estado
						print N'alertasEstado_estado - El estado ha cambiado'

						if (@estadoCircuito = 3)
							-- rojo
							begin
								print N'alertasEstado_estado - El estado ha cambiado a rojo'
								-- ha pasado a estado rojo enviar mail alerta
								--insert into _pruebas_mensajes (mensaje) values ('' + @Circuito + ' es rojo')
								EXEC alertasEstado_sendMailAlertaEstadoRojo @Circuito = @Circuito
								--insert into _pruebas_mensajes (mensaje) values ('' + @Circuito + ' mail enviado')
								
							end
						else 
							begin
								if (@estadoCircuito = 2)
									-- amarillo
									begin
										print N'alertasEstado_estado - alertasEstado_estado - El estado ha cambiado a amarillo'
										-- ha pasado a estado amarillo enviar mail alerta
										EXEC alertasEstado_sendMailAlertaEstadoAmarillo @Circuito = @Circuito
									end
								else 
									begin
										if (@estadoCircuito = 1)
											-- verde
											begin
												print N'alertasEstado_estado - El estado ha cambiado a verde'
												-- ha pasado a estado verde enviar mail alerta
												EXEC alertasEstado_sendMailAlertaEstadoVerde @Circuito = @Circuito
											end
									end
							end

						-- actualizamos el estado
						print 'alertasEstado_estado - ' + @Circuito + ' vamos a actualizar el estado'
						EXEC dbo.alertasEstado_setEstadoActualCircuito @Circuito = @Circuito, @estadoCircuito = @estadoCircuito
						print 'alertasEstado_estado - ' + @Circuito + ' estado actualizado'
					end
			end

		print 'alertasEstado_estado - ' + @Circuito + ' Fin'
	END TRY
	BEGIN CATCH
		EXEC dbo.common_sendMailError 
	END CATCH

	RETURN @estadoCircuito

END