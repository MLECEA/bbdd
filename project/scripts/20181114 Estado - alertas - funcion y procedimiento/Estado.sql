USE [EKANBAN]
GO
/****** Object:  UserDefinedFunction [dbo].[Estado]    Script Date: 11/14/2018 12:59:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[Estado]
(@Circuito as nvarchar(50) )
RETURNS int
AS
BEGIN
	DECLARE @estadoCircuito as INT
	
	EXEC @estadoCircuito = alertasEstado_calcularEstadoCircuito @Circuito

	RETURN @estadoCircuito
END