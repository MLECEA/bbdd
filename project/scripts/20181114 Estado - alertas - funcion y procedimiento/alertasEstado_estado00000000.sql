USE [EKANBAN]
GO
/****** Object:  UserDefinedFunction [dbo].[alertasEstado_estado00000000]    Script Date: 11/13/2018 13:34:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[alertasEstado_estado00000000]
(@Circuito as nvarchar(50) )
RETURNS int
AS
BEGIN
	DECLARE @T AS INT
	DECLARE @V AS int 
	declare @AE as int
	declare @A as int
	declare @R as int
	DECLARE @SS as int
	DECLARE @Stat as int

	declare @Tipo as int
	
	SET @T=DBO.T(@Circuito)
	SET @R=DBO.R(@Circuito)
	SET @AE=DBO.AE(@Circuito)
	SET @A=DBO.A(@Circuito)
	SET @SS=DBO.SS(@Circuito)
	
	SET @V=@T-@AE-@A-@R+@SS

	set @Stat=1 --verde por defecto
	
	if ((@R+@A > @T/2)  OR @V=0 )
		begin
			set @Stat=2 --amarillo
			if @R > 80 --20 --23
				BEGIN
					set @Stat=3 -- ROJO
				END
		end

	RETURN @Stat

END