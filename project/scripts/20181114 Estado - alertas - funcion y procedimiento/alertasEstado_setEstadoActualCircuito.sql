USE [EKANBAN]
GO
/****** Object:  StoredProcedure [dbo].[alertasEstado_setEstadoActualCircuito]    Script Date: 11/13/2018 13:29:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*****************************************************************************************
 *
 * setEstadoActualCircuito
 *
 * Establece el  estado actual de un circuito 
 *
 * Posibles valores: 'A' , 'R', 'V'
 *
 ******************************************************************************************/

CREATE PROCEDURE [dbo].[alertasEstado_setEstadoActualCircuito]
	( @Circuito as nvarchar(50),
	  @estadoCircuito as VARCHAR(2)  )
AS
BEGIN
	SET NOCOUNT ON;
	-- guardar el valor del estado en la tabla que corresponda para el circuito indicado
	update THistoricoVerdes
	set estado = @estadoCircuito 
	where IdCircuito = @Circuito
	and Fecha = (select MAX(fecha) from THistoricoVerdes where IdCircuito =  @Circuito)

	print 'guardar estado ' + @Circuito + ' ' + @estadoCircuito
END
