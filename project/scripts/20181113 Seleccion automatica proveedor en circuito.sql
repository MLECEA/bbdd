-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PAccRegLectura]
	-- Add the parameters for the stored procedure here
	@tagID as varchar(255),
	@imei as varchar(255),
	@idUbi as varchar(255),
	@idUsu as varchar(255),
	@antena as varchar(255) = '',
	@numSerie as varchar(255) = '',
	@sentido as varchar(255) = '',
	@tipoLectura as varchar(5) = '',
	@subUbicacion as varchar(255) = NULL,
	@Pant as int = 0
AS
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRY 
		--BEGIN TRY
		DECLARE @OF VARCHAR(255);
		DECLARE @NUMEROSERIE VARCHAR(255);
		DECLARE @REFERENCIA VARCHAR(255);
		DECLARE @CANTIDAD INT;
		DECLARE @PROVEEDOR VARCHAR(255)
		DECLARE @CLIENTE VARCHAR(255);
		DECLARE @TEXTO VARCHAR(255);
		DECLARE @CTDLEIDA VARCHAR(255);
		DECLARE @IDPED VARCHAR(255);
		DECLARE @IDREFERENCIA VARCHAR(255) = '';
		DECLARE @IDCLIENTE VARCHAR(255)='';
		DECLARE @IDCIRCUITO VARCHAR(255) = '';
		DECLARE @IDLECTURA VARCHAR(255) = '';
		DECLARE @RESULT AS INT = -1;
		DECLARE @MsgTxt AS VARCHAR(255) = 'OK';
		DECLARE @SENTIDOLECTURA VARCHAR(5) = '';
		DECLARE @TIPOAVISO INT = 0;
		DECLARE @MSGAVISO VARCHAR(255)='';
		
		
		
		--SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
		BEGIN TRANSACTION;
									
		if @subUbicacion = ''
		begin
			set @subUbicacion = NULL;
		end
		
		 --LECTURAS DE MOVIL
		IF @tipoLectura = 'M'
		BEGIN
			IF @tagID <> '' AND @idUbi <> '' AND @idUsu <> ''
			BEGIN	
				-- Ejecutamos el proc PAX_PatronLec: con este procedimiento obtenemos el registro de la lectura
				--SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
				begin  tran t1;
				
					-----20181008 INCLUSION DE TABLA HISTORICOVERDES
					DECLARE @Origen as varchar(25);
		
					SET @Origen = 'RegistrarLectura'
					-----20181008 FIN DE TABLA HISTORICOVERDES
		
				declare @TTarjeta table (LOT varchar(255), SER varchar(255), ART varchar(255), CTD int, 
					PRO varchar(255), CLI varchar(255), GRAB varchar(255), CTDLEIDA varchar(255), IDPED varchar(255));
				
				insert into @TTarjeta
					EXEC	[dbo].[PAX_PatronLec] @tagID, @idUsu, @idUbi, @Pant, 'M', ' ', ' ', @Origen
					-----20181008 INCLUSION DE ORIGEN PARA LA TABLA HISTORICOVERDES
				select TOP(1) @OF = lot, @NUMEROSERIE = SER, @REFERENCIA=ART, @CANTIDAD = CTD, @PROVEEDOR = PRO, @CLIENTE=CLI, @TEXTO = GRAB
					, @CTDLEIDA = CTDLEIDA,  @IDPED = IDPED from @TTarjeta 
				COMMIT TRANSACTION t1;
				
				print @texto;
				
				IF @OF ='000000'
					BEGIN 
						print 'of desde patronLec';
						SET @IDREFERENCIA = @REFERENCIA;
						SET @IDCLIENTE = @CLIENTE;
					END
				ELSE
					BEGIN
						SELECT TOP(1) @IDREFERENCIA = V_OF.ITEMID, @IDCLIENTE = V_OF.CUSTACCOUNT  FROM [dbo].VAX_EKAN_OF  AS V_OF WITH (NOLOCK) WHERE V_OF.PRODID = @OF
					END
							
							
				
				IF @TEXTO <> '_' --
				BEGIN
					IF NOT EXISTS(SELECT 1 
						FROM [dbo].tarjeta T  WITH (NOLOCK)
						WHERE T.id_lectura = @TEXTO) -- NO EXISTE UNA TARJETA CON ESA INFORMACION
					BEGIN
						--IF @OF ='000000'
						--	BEGIN 
						--		print 'of desde patronLec';
						--		SET @IDREFERENCIA = @REFERENCIA;
						--		SET @IDCLIENTE = @CLIENTE;
						--	END
						--ELSE
						--	BEGIN
						--		SELECT TOP(1) @IDREFERENCIA = V_OF.ITEMID, @IDCLIENTE = V_OF.CUSTACCOUNT  FROM [dbo].VAX_EKAN_OF  AS V_OF WITH (NOLOCK) WHERE V_OF.PRODID = @OF
						--	END
						PRINT 'REFERENCIA OF:' + @IDREFERENCIA + ' IDCLIENTE:' + @IDCLIENTE;
						IF @IDREFERENCIA is not null AND @IDCLIENTE is not null
						BEGIN
							print 'barruan';
							print @idcliente;
							print @idReferencia;
							--Realizamos inicialmente la búsqueda teniendo en cuenta el proveedor, 
							-- si no lo encuentra, realizará la búsqueda sin especificar elproveedor
							select @IDCIRCUITO = id_circuito 
							FROM [dbo].Circuito c WITH (NOLOCK) 
							WHERE c.ubicacion = @idUbi 
								AND c.cliente_id =@IDCLIENTE 
								AND c.referencia_id =@IDREFERENCIA 
								AND c.activo = 1
								AND c.proveedor_id = @PROVEEDOR
							if  @@ROWCOUNT = 0  
								begin
									--Buscar circuito por ubicacion, ref y cliente (sin especificar el proveedor)
									--SELECT * FROM ubicacion u WHERE id_ubicacion = @idUbi 
									select @IDCIRCUITO = id_circuito 
									FROM [dbo].Circuito c WITH (NOLOCK) 
									WHERE c.ubicacion = @idUbi 
										AND c.cliente_id =@IDCLIENTE 
										AND c.referencia_id =@IDREFERENCIA 
										AND c.activo = 1
								end
							PRINT 'CIRCUITO:' + @IDCIRCUITO;
							IF @IDCIRCUITO <> ''
								BEGIN
									--mirar si hay registros tipo ajuste pendientes de ser modificados
									SELECT @IDLECTURA = T.id_lectura FROM [dbo].Tarjeta t WITH (NOLOCK)  WHERE t.id_circuito=@IDCIRCUITO AND t.estado='A_MODIF' AND t.gestionado IS NULL ORDER BY t.fecha_lectura ASC
									IF @IDLECTURA = '' -- no existe ningun registro de tipo ajuste pendiente
									BEGIN
										--print 'insert into tarjeta'
										
										INSERT INTO [dbo].tarjeta WITH (ROWLOCK) (id_lectura, sentido, estado, fecha_lectura, id_circuito, movil, num_lecturas, ultima_lectura, kill_tag, antena, tipo, referencia_id, cliente_id, ubicacion, sububicacion) VALUES 
												(@TEXTO, 'S', 'P', GETDATE(), @IDCIRCUITO, @imei, 1, GETDATE(), 0, NULL, 'M', @IDREFERENCIA, @IDCLIENTE, @idUbi, @subUbicacion );
										SET @RESULT=  0;
										set @MsgTxt = 'Lectura correctamente registrada';
										
										set @TIPOAVISO = 1;
									END
									ELSE
									BEGIN
										--MODIFICAR REGISTRO @IDLECTURA
										
										UPDATE [dbo].tarjeta WITH (ROWLOCK)  SET gestionado='Ajuste' where id_lectura = @IDLECTURA
										INSERT INTO [dbo].tarjeta WITH (ROWLOCK)	 (id_lectura, sentido, estado, fecha_lectura, gestionado, id_circuito, movil, tipo, kill_tag, referencia_id, cliente_id, ubicacion, sububicacion) VALUES 
											(@TEXTO, 'S', 'A', GETDATE(), 'Ajuste', @IDCIRCUITO, @imei, 'M', 0, @IDREFERENCIA, @IDCLIENTE, @idUbi, @subUbicacion );
										SET @RESULT=  1;
										set @TIPOAVISO = 3;
										set @MsgTxt = 'Referencia leida, se ha incrementado el nº de lecturas';
										
									END 
								END
								else
								begin
			
									set  @MsgTxt = 'La referencia leída no pertenece a la ubicación seleccionada';
									
									set @TIPOAVISO = 2;
									SET @RESULT=  5;
									
								end  
						END
						
					END
					ELSE
					BEGIN
						select @IDCIRCUITO = id_circuito FROM [dbo].Circuito c WITH (NOLOCK) WHERE c.ubicacion = @idUbi AND c.cliente_id =@IDCLIENTE AND c.referencia_id =@IDREFERENCIA AND c.activo = 1
						IF @IDCIRCUITO <> '' begin
							--la etiqueta ya ha sido leida
							UPDATE [dbo].tarjeta  WITH (ROWLOCK) SET num_lecturas = num_lecturas + 1, fecha_lectura = GETDATE() WHERE TARJETA.id_lectura = @TEXTO
							SET @RESULT=  8;
							set @MsgTxt = 'Etiqueta leida, cantidad de lecturas actualizada';
							set @TIPOAVISO = 3;
						end
						else
							begin

							set  @MsgTxt = 'La referencia leída no pertenece a la ubicación seleccionada';
							set @TIPOAVISO = 2;
							SET @RESULT=  5;
						end
					END
				END	
				ELSE
				BEGIN
					set @MsgTxt = 'No existe ninguna OF para la etiqueta';
					set @TIPOAVISO = 4;
					SET @RESULT=  6;
				END 
			END
		END
		select  @IDCIRCUITO as circuito, @REFERENCIA as referencia, @RESULT as Result, @MsgTxt as Msg, @TIPOAVISO as Tipo_Aviso, @MsgTxt as Mensaje_Aviso;
		----select  'PRECOZ4' as circuito, '20900-51' as referencia, 0 as Result, 'OK' as Msg, '' as Tipo_Aviso, '' as Mensaje_Aviso;
		COMMIT TRANSACTION
	----END TRY
	----BEGIN CATCH 
	----	ROLLBACK TRANSACTION
	----	--select  '' as circuito, '' as referencia, -1 as Result, 'deadlock' as Msg, '2' as Tipo_Aviso, 'Error-Intente otra vez' as Mensaje_Aviso; 
	----	IF ERROR_NUMBER() = 1205 -- Deadlock Error Number
	----	BEGIN
	----		WAITFOR DELAY '00:00:00.500' -- Wait for 5 ms
	----		GOTO RETRY -- Go to Label RETRY
	----	END
	----	ELSE
	----		select  '' as circuito, '' as referencia, -1 as Result, 'deadlock' as Msg, '2' as Tipo_Aviso, 'Error-Intente otra vez' as Mensaje_Aviso; 
	----END CATCH

		-----20181005 INCLUSION DE TABLA HISTORICOVERDES	
		exec dbo.PAX_HistoricoVerdes @Circuito = @IDCIRCUITO, @Origen = 'RegistrarLectura', @Usuario = @idUsu
		-----20181005 FIN INCLUSION DE TABLA HISTORICOVERDES
		SET XACT_ABORT OFF;
	END TRY
	BEGIN CATCH
		EXEC dbo.common_sendMailError @usuario = @idUsu
	END CATCH
END
