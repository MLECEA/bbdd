

ALTER PROCEDURE [dbo].[PAccAjustar]
(
	@CodCircuito varchar(20) = '',
	@NumTarjVerdes integer = 0,
	@Usu varchar(255) = '',
	@Dis varchar(4)=''
)
	
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Usuario varchar(255) = ''

	BEGIN TRY

		DECLARE @NumTarjRojas as int
		DECLARE @NumTarjetas AS INT
		DECLARE @Verdes as int
		DECLARE @SobreStock as int

		DECLARE @CampGest varchar(50)=''

		DECLARE @Tipo varchar(10)=''
		DECLARE @StatusTrat varchar(1)
		DECLARE @IdTar varchar(50)
		--DECLARE @Usuario varchar(255) = '' -- he sacado esta línea fuera del try para que la vea el catch
		DECLARE @FechEntregaConfirmada AS DATETIME

		DECLARE @MsgTxt varchar(255)=''
		DECLARE @Resul int=0

		SET @Usuario=@Usu

		SELECT @Tipo=dbo.tipo_circuito.nombre
		FROM            dbo.circuito INNER JOIN
					dbo.tipo_circuito ON dbo.circuito.tipo_circuito = dbo.tipo_circuito.id
		WHERE        (dbo.circuito.id_circuito = @CodCircuito)

		SET @StatusTrat='T'
		if @Tipo='INT' OR @Tipo='EXT' 
		BEGIN
			SET @StatusTrat='G'
		END

		set @NumTarjRojas=dbo.r(@CodCircuito)
		set @Verdes=dbo.v(@CodCircuito)
		set @SobreStock=dbo.SS(@CodCircuito)
		SET @FechEntregaConfirmada=GETDATE()
	
		if @NumTarjVerdes<0 set @NumTarjVerdes=0
	
		SET @NumTarjetas=@NumTarjVerdes-@Verdes


		set @CampGest='AutAX' + '_' + CONVERT(nvarchar(100), @FechEntregaConfirmada, 112) + '_' + CONVERT(nvarchar(100), getdate(), 113)


		IF @NumTarjetas>0 --Hay más stock real, verdes que los que indica el circuito. Si hay rojas, quitar. sino crear amodif
		BEGIN
			print 'numTarjetas > 0 ';
			IF @NumTarjRojas>=@NumTarjetas--=@NumTarjetas
			BEGIN
				SELECT    TOP (@NumTarjetas)     @IdTar =id_lectura
						FROM            dbo.tarjeta
						WHERE        (gestionado IS NULL OR
										gestionado = '') AND (id_circuito = @CodCircuito)
						ORDER BY id_lectura


				UPDATE [EKANBAN].[dbo].[tarjeta]
					SET gestionado =  @CampGest
					, Estado='T'    -- @StatusTrat de lo contrario, en circuitos internos y externos, pueden considerarse como amarillas, tipo G
					, Tar_GestionadoUSU = @Usuario
					, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada
					WHERE     (gestionado IS NULL OR gestionado = '') AND (id_circuito =  @CodCircuito) and id_lectura<=@IdTar
			END
			ELSE
			BEGIN
						UPDATE [EKANBAN].[dbo].[tarjeta]
						SET gestionado =  @CampGest
						, Estado='T'    -- @StatusTrat de lo contrario, en circuitos internos y externos, pueden considerarse como amarillas, tipo G
						, Tar_GestionadoUSU = @Usuario
						, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada
						WHERE     (gestionado IS NULL OR gestionado = '') AND (id_circuito =  @CodCircuito) AND ESTADO <>'A_MODIF'
					

						INSERT INTO dbo.tarjeta (id_lectura,antena,estado,fecha_lectura,gestionado,movil,tipo,cliente_id,id_circuito,referencia_id,ubicacion,sentido, kill_tag, Tar_FecEntSol)
						SELECT        TOP (@NumTarjetas-@NumTarjRojas) 'AJUSTE' + RIGHT(dbo.sga_usuario.cod_usuario, 2) + '_' + CONVERT(nvarchar(30), GETDATE(), 131) AS Expr1, NULL AS ANTENA, 'A_MODIF' AS ES, GETDATE() AS Expr2, NULL AS GEST, 'M' AS MOV, 'M' AS tipo, 
								 dbo.circuito.cliente_id, dbo.circuito.id_circuito, dbo.circuito.referencia_id, dbo.circuito.ubicacion, 'S' AS Expr3, 0, @FechEntregaConfirmada
						FROM            dbo.circuito CROSS JOIN dbo.sga_usuario
						WHERE        (dbo.circuito.id_circuito = @CodCircuito) ORDER BY id_circuito
			END
		END
		ELSE
		--HAY QUE AÑADIR ROJAS (PRIMERO QUITAR AMODIF)
		BEGIN
			print ' opcion 2';
			set @NumTarjetas=-@NumTarjetas

			IF @SobreStock>=@NumTarjetas
			BEGIN

				SELECT    TOP (@NumTarjetas)     @IdTar =id_lectura
				FROM            dbo.tarjeta
				WHERE        (estado = 'A_MODIF') AND (sentido = 'S') AND (gestionado IS NULL OR
				gestionado = '') AND (id_circuito = @CodCircuito)
				ORDER BY id_lectura


				UPDATE [EKANBAN].[dbo].[tarjeta]
					SET gestionado =  @CampGest
					, Estado='T'    -- @StatusTrat de lo contrario, en circuitos internos y externos, pueden considerarse como amarillas, tipo G
					, Tar_GestionadoUSU = @Usuario
					, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada
					WHERE        (estado = 'A_MODIF') AND (sentido = 'S') AND (gestionado IS NULL OR
					gestionado = '') AND (id_circuito = @CodCircuito) and id_lectura<=@IdTar
			END

			ELSE
			BEGIN

						UPDATE [EKANBAN].[dbo].[tarjeta]
						SET gestionado =  @CampGest
						, Estado='T'    -- @StatusTrat de lo contrario, en circuitos internos y externos, pueden considerarse como amarillas, tipo G, 
						, Tar_GestionadoUSU = @Usuario
						, Tar_GestionadoFEC = GETDATE(), Tar_FecEntSol=@FechEntregaConfirmada
						WHERE        (estado = 'A_MODIF') AND (sentido = 'S') AND (gestionado IS NULL OR
						gestionado = '') AND (id_circuito = @CodCircuito)
					

						INSERT INTO dbo.tarjeta (id_lectura,antena,estado,fecha_lectura,gestionado,movil,tipo,cliente_id,id_circuito,referencia_id,ubicacion,sentido, kill_tag, Tar_FecEntSol)
						SELECT        TOP (@NumTarjetas-@SobreStock) 'AJUSTE' + RIGHT(dbo.sga_usuario.cod_usuario, 2) + '_' + CONVERT(nvarchar(30), GETDATE(), 131) AS Expr1, NULL AS ANTENA, 'A' AS ES, GETDATE() AS Expr2, NULL AS GEST, 'M' AS MOV, 'M' AS tipo, 
								 dbo.circuito.cliente_id, dbo.circuito.id_circuito, dbo.circuito.referencia_id, dbo.circuito.ubicacion, 'S' AS Expr3, 0, @FechEntregaConfirmada
						FROM            dbo.circuito CROSS JOIN dbo.sga_usuario
						WHERE        (dbo.circuito.id_circuito = @CodCircuito) ORDER BY id_circuito
			END




		END

		--Set @Resul=1
		--set @MsgTxt='Sin validar usuario'
	
		--Set @Resul=0
		--set @MsgTxt='se han insertado los datos'

		--set @Resul=18  --Código de resultado para el android. Error al generar pedido.
		--set @MsgTxt='Se han dado consumos justo en este instante. Vuelva a intentarlo'

		Set @Resul=0
		set @MsgTxt='se han insertado los datos'

		select  @Resul as Result, @MsgTxt as Msg
	
		-----20181008 INCLUSION DE TABLA HISTORICOVERDES	
		exec dbo.PAX_HistoricoVerdes @Circuito = @CodCircuito, @Origen = 'AjusteStock', @Usuario = @Usu
		-----20181008 FIN INCLUSION DE TABLA HISTORICOVERDES
	END TRY
	BEGIN CATCH
		EXEC dbo.common_sendMailError @usuario = @Usuario
	END CATCH
END


