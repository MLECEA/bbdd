


ALTER PROCEDURE [dbo].[PAccRegConsumo]
(
	@CodCircuito varchar(20) = '',
	@NumTarj integer = 0,
	@Usu varchar(255) = '',
	@Dis varchar(4)='', 
	@TxT varchar(50)=''
)
	
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO dbo.tarjeta (id_lectura,antena,estado,fecha_lectura,gestionado,movil,tipo,cliente_id,id_circuito,referencia_id,ubicacion,sentido, kill_tag, Tar_FecEntSol)
		SELECT        TOP (@NumTarj) 'CONSUM' + RIGHT(dbo.sga_usuario.cod_usuario, 2) + '_' + CONVERT(nvarchar(30), GETDATE(), 131) AS Expr1, NULL AS ANTENA, 'P' AS ES, GETDATE() AS Expr2, NULL AS GEST, 'M' AS MOV, 'M' AS tipo, 
					dbo.circuito.cliente_id, dbo.circuito.id_circuito, dbo.circuito.referencia_id, dbo.circuito.ubicacion, 'S' AS Expr3, 0, GETDATE()
		FROM            dbo.circuito CROSS JOIN dbo.sga_usuario
		WHERE        (dbo.circuito.id_circuito = @CodCircuito)
		
	END TRY
	BEGIN CATCH
		EXEC dbo.common_sendMailError @usuario = @Usu
	END CATCH

END