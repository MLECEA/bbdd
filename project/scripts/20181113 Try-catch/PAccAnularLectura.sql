-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

ALTER PROCEDURE [dbo].[PAccAnularLectura]
	-- Add the parameters for the stored procedure here
	@codQR varchar(255) = '',
	@idUbi varchar(255) = '',
	@idUsu varchar(255) = '',
	@Pant int = 0,
	@Disp varchar(255) = 'M',
	@Ant varchar(50) = ' ',
	@Sen varchar(5) = ' '
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	BEGIN TRY
	
		DECLARE @OF VARCHAR(255);
		DECLARE @NUMSERIE VARCHAR(255);
		DECLARE @REFERENCIA VARCHAR(255);
		DECLARE @CANTIDAD INT;
		DECLARE @PROVEEDOR VARCHAR(255)
		DECLARE @CLIENTE VARCHAR(255);
		DECLARE @TEXTO VARCHAR(255);
		DECLARE @CTDLEIDA VARCHAR(255);
		DECLARE @IDPED VARCHAR(255);
		DECLARE @RESULT AS INT = -1;
		DECLARE @MsgTxt AS VARCHAR(255) = 'OK';
		declare @idCircuito as varchar(255);
		-----20181008 INCLUSION DE TABLA HISTORICOVERDES
		DECLARE @Origen as varchar(25);
		
		SET @Origen = 'LecturaAnulada';
		-----20181008 FIN DE TABLA HISTORICOVERDES

	 ----   -- Ejecutamos el proc PAX_PatronLec: con este procedimiento obtenemos el registro de la lectura
		declare @TTarjeta table (LOT varchar(255), SER varchar(255), ART varchar(255), CTD int, 
			PRO varchar(255), CLI varchar(255), GRAB varchar(255), CTDLEIDA varchar(255), IDPED varchar(255));

		insert into @TTarjeta
			EXEC	[dbo].[PAX_PatronLec] @codQR, @idUsu, @idUbi, @Pant, @Disp, @Ant, @Sen, @Origen
			-----20181008 INCLUSION ORIGEN PARA LA TABLA HISTORICOVERDES 
		
		select TOP(1) @OF = lot, @NUMSERIE = SER, @REFERENCIA=ART, @CANTIDAD = CTD, @PROVEEDOR = PRO, @CLIENTE=CLI, @TEXTO = GRAB
			, @CTDLEIDA = CTDLEIDA,  @IDPED = IDPED from @TTarjeta
		print @TEXTO;
		IF @TEXTO <> '_'
		BEGIN
			IF EXISTS (SELECT *
						FROM tarjeta T
						WHERE T.id_lectura = @TEXTO AND  T.sentido = 'S')
				BEGIN
					print 'eliminar tarjeta';
					select @idCircuito = id_circuito from tarjeta WHERE id_lectura = @TEXTO AND sentido = 'S'; 
					DELETE FROM tarjeta WHERE id_lectura = @TEXTO AND sentido = 'S';
					SET @RESULT = 0;
					
				END
			
		END
		ELSE
		BEGIN
			print '@TEXTO = _ ';
		END 
		
		-----20181005 INCLUSION DE TABLA HISTORICOVERDES	
		exec dbo.PAX_HistoricoVerdes @Circuito = @IDCIRCUITO, @Origen = @Origen, @Usuario = @idUsu
		-----20181005 FIN INCLUSION DE TABLA HISTORICOVERDES
		--SELECT @RESULT AS RESULTADO
		select  @idCircuito as idCircuito, @RESULT as Result, @MsgTxt as Msg	
	END TRY
	BEGIN CATCH
		EXEC dbo.common_sendMailError @usuario = @idUsu
	END CATCH
END
