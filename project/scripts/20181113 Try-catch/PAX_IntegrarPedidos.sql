
ALTER PROCEDURE [dbo].[PAX_IntegrarPedidos] 

AS

BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;  --- iker. 20151020
						--- Transakzio errorea kentzeko. Horrela rollback-ak transakzio osoa anulatzen du errorea dagoenean.
	BEGIN TRY						
		--DECLARE	@return_value int
		DECLARE @Resul as varchar(100)
		DECLARE @Emaitza as int
		DECLARE @EmaitzaErr as int
		DECLARE @Des as varchar(300)
		DECLARE @JournalNumMIN as varchar(100)
		DECLARE @ErrIntegracion as varchar(100)

		set @Emaitza=1  --por defecto como error
		set @EmaitzaErr=1

		SELECT        @JournalNumMIN= MIN(JournalNum) 
		FROM            PREAXSQL.ORKLB000.dbo.TAUX_CREAR_PEDIDO AS TAUX_CREAR_PEDIDO_1
		WHERE        (Status = 0)

		--si no hay registro en 0
		if ISNULL(@JournalNumMIN,'')='' set @JournalNumMIN='99999'
			

		--******************************************************************************************************
			EXEC	PREAXSQL.[ORKLB000].[dbo].[PAX_IntegrarPedKanban]
			--ws alojado en wspro1, y realizado por iniker. Cyc no ha participado
		--******************************************************************************************************
			



		--Para ver si ha subido ok o no, vamos a mirar si hay algún registro todavía en 0. No debería de estar. 
		SELECT     @Resul=JournalNum
		FROM       PREAXSQL.ORKLB000.dbo.TAUX_CREAR_PEDIDO AS TAUX_CREAR_PEDIDO_1
		WHERE      (Status = 0)

		if ISNULL(@Resul,'')='' set @Emaitza=0
		
		if @Emaitza=1
		begin
			EXEC [dbo].[PEnvCorreo]
			@Des = N'alert@ekanban.es',--; loli@orkli.es; aarrondo@orkli.es',
			@Tem = N'Fallo en la Integración de Pedidos al AX',
			@Cue = N'No se han integrado los pedidos en el AX. Se debe de revisar el proceso de integración. En caso de tener problemas, ejecutar la integarción manual desde el AX: Menú Clientes, Periódico : INTEGRACIÓN PEDIDOS KANBAN'
		end
			
		SELECT @Emaitza AS RESULT
		-- SELECT 1 AS RESULT en caso de que esté mal. lo considera el websercice como error
		-- de momento como en el AX no tenemos feedback, dejamos como fijo. Como si fuera ok. IES, 10-7-2014



		--PARA MIRAR SI EN LA INTEGRACIÓN HAY ALGÚN REGISTRO CON ERROR DE INTEGRACIÓN DE PEDIDOS

		
		SELECT       @ErrIntegracion=JournalNum
		FROM            PREAXSQL.ORKLB000.dbo.TAUX_CREAR_PEDIDO AS TAUX_CREAR_PEDIDO_1
		WHERE        (Status = 2) AND (JournalNum >= @JournalNumMIN)

		if ISNULL(@ErrIntegracion,'')='' set @EmaitzaErr=0
		


		if @EmaitzaErr=1
		begin
			EXEC [dbo].[PEnvCorreo]
			@Des = N'alert@ekanban.es',--; loli@orkli.es',
			@Tem = N'Error en Integrar un Pedido al AX',
			@Cue = N'No se han integrado todos los pedidos en el AX. Revisar el listado de Crear Pedidos y mirar el tipo de error'
		end

		SET XACT_ABORT OFF;  --- iker. 20151020
			--- Transakzio errorea kentzeko. Horrela rollback-ak transakzio osoa anulatzen du errorea dagoenean.
	END TRY
	BEGIN CATCH
		EXEC dbo.common_sendMailError -- No incluimos el valor de usuario
	END CATCH
END
