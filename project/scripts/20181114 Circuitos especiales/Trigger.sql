USE [EKANBAN]
GO
/****** Object:  Trigger [dbo].[ActGesEntradas]    Script Date: 11/14/2018 13:08:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER TRIGGER [dbo].[ActGesEntradas]
   ON  [dbo].[tarjeta]
   AFTER  INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRY


		UPDATE [dbo].[tarjeta]
			SET [gestionado] = 'Entrada', estado='E', sentido='E' 
			WHERE 
			sentido='E'  and gestionado is null
			AND ubicacion 
			NOT IN 
			(SELECT        id_ubicacion
			FROM            dbo.ubicacion AS ubicacion_1
			WHERE        (Ubi_Modo = N'DISP'))




		--UPDATE [dbo].[tarjeta]
		--	SET  estado='P', sentido='S' 
		--	WHERE 
		--	sentido='E'  and gestionado is null
		--	AND ubicacion 
		--	IN 
	 --       (SELECT        id_ubicacion
	 --       FROM            dbo.ubicacion AS ubicacion_1
	 --       WHERE        (Ubi_Modo = N'DISP'))










		UPDATE [dbo].[tarjeta]
			SET [gestionado] = 'Inventario', estado='I'
			WHERE sentido='I'  and gestionado is null
			
			
			
			
			
		--DECLARE @idLectura AS VARCHAR(255)	
			
		--select 	@idLectura = inserted.id_lectura  from inserted
		
		--insert into _pruebas_mensajes (mensaje) values ('idLectura ' + @idLectura)
		
		--select 	@idLectura = inserted.id_circuito  from inserted
		
		--insert into _pruebas_mensajes (mensaje) values ('id_circuito ' + @idLectura)
	
		
		-- Incluir el tratamiento de Circuitos Especiales
		insert into dbo.tarjeta 
			(id_lectura, antena, estado, fecha_lectura, gestionado, movil, tipo, cliente_id, id_circuito, referencia_id, 
			 ubicacion, num_lecturas, ultima_lectura, sentido, kill_tag,kill_enviado, lote, Tar_Facturado)
		select     inserted.id_lectura + '--' + especiales.circuitoHijoId as id_lectura, 
						   inserted.antena, 
						   inserted.estado, 
						   CURRENT_TIMESTAMP as fecha_lectura, 
						   inserted.gestionado, 
						   inserted.movil, 
						   inserted.tipo, 
						   circuito.cliente_id, 
						   especiales.circuitoHijoId as id_circuito, 
						   circuito.referencia_id as referencia_id, 
						   circuito.ubicacion, 
						   inserted.num_lecturas, 
						   CURRENT_TIMESTAMP as ultima_lectura, 
						   'S' as sentido, 
						   inserted.kill_tag, 
						   inserted.kill_enviado, 
						   inserted.lote,
						   0 as Tar_Facturado
					FROM inserted
					JOIN circuitosEspeciales especiales
						ON especiales.circuitoId = inserted.id_circuito
					JOIN dbo.circuito circuito 
						ON especiales.circuitoHijoId = circuito.id_circuito
					where estado in ('A', 'P')
					
					--join ((select * from dbo.tarjeta inserted2
					--		where inserted2.id_circuito = 'RFIDTEST0001'
					--		and inserted2.fecha_lectura = (select MAX(inserted4.fecha_lectura) from dbo.tarjeta inserted4
					--		where inserted4.id_circuito = 'RFIDTEST0001'))) as inserted
					--ON especiales.circuitoId = inserted.id_circuito

	END TRY
	BEGIN CATCH
		EXEC dbo.common_sendMailError -- sin parametro de usuario
	END CATCH
END
