﻿CREATE VIEW dbo.VEKA_PERMISOS_PROV
AS
SELECT        TOP (100) PERCENT dbo.sga_usuario.username, dbo.sga_usuario.cod_usuario, dbo.usuario_proveedor.id_proveedor, dbo.circuito.id_circuito, dbo.circuito.referencia_id, dbo.circuito.descripcion, 
                         dbo.circuito.ref_cliente
FROM            dbo.sga_usuario INNER JOIN
                         dbo.usuario_proveedor ON dbo.sga_usuario.cod_usuario = dbo.usuario_proveedor.id_usuario INNER JOIN
                         dbo.usuario_ubicacion ON dbo.sga_usuario.cod_usuario = dbo.usuario_ubicacion.id_usuario INNER JOIN
                         dbo.circuito ON dbo.usuario_ubicacion.id_ubicacion = dbo.circuito.ubicacion AND dbo.usuario_proveedor.id_proveedor = dbo.circuito.Proveedor_id
ORDER BY dbo.sga_usuario.username, dbo.usuario_proveedor.id_proveedor

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[6] 2[3] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "sga_usuario"
            Begin Extent = 
               Top = 0
               Left = 29
               Bottom = 335
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "usuario_proveedor"
            Begin Extent = 
               Top = 186
               Left = 303
               Bottom = 442
               Right = 507
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "usuario_ubicacion"
            Begin Extent = 
               Top = 0
               Left = 362
               Bottom = 288
               Right = 532
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "circuito"
            Begin Extent = 
               Top = 17
               Left = 691
               Bottom = 256
               Right = 1192
            End
            DisplayFlags = 280
            TopColumn = 5
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 4635
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VEKA_PERMISOS_PROV';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VEKA_PERMISOS_PROV';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VEKA_PERMISOS_PROV';

