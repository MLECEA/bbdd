﻿CREATE VIEW dbo.VAX_EKANSTOCKCONSIGPROV
AS
SELECT        TOP (100) PERCENT dbo.sga_usuario.username, dbo.circuito.Proveedor_id, dbo.circuito.ubicacion, dbo.circuito.id_circuito, dbo.circuito.cliente_id, PURCHTABLE_1.ORDERACCOUNT, PURCHLINE_1.ITEMID, 
                         PURCHLINE_1.NAME, PURCHTABLE_1.PURCHID, PURCHLINE_1.LINENUM, PURCHTABLE_1.DELIVERYDATE, PURCHLINE_1.PURCHQTY AS PEDIDO, PURCHLINE_1.INK_ENTRADASCONSIGNA AS ENVIADO, 
                         PURCHLINE_1.PURCHQTY - PURCHLINE_1.INK_ENTRADASCONSIGNA AS PTE, PURCHLINE_1.REMAINPURCHPHYSICAL AS STOCKCONSIGNA
FROM            SRVAXSQL.SQLAX.dbo.PURCHLINE AS PURCHLINE_1 INNER JOIN
                         SRVAXSQL.SQLAX.dbo.PURCHTABLE AS PURCHTABLE_1 ON PURCHLINE_1.PURCHID = PURCHTABLE_1.PURCHID AND PURCHLINE_1.DATAAREAID = PURCHTABLE_1.DATAAREAID INNER JOIN
                         dbo.circuito ON PURCHTABLE_1.ORDERACCOUNT = dbo.circuito.Proveedor_id AND PURCHLINE_1.ITEMID = dbo.circuito.referencia_id INNER JOIN
                         dbo.usuario_proveedor ON dbo.circuito.Proveedor_id = dbo.usuario_proveedor.id_proveedor INNER JOIN
                         dbo.sga_usuario ON dbo.usuario_proveedor.id_usuario = dbo.sga_usuario.cod_usuario
WHERE        (PURCHLINE_1.DATAAREAID = N'ork') AND (PURCHLINE_1.PURCHSTATUS = 1) AND (PURCHLINE_1.PURCHASETYPE = 5) AND (YEAR(PURCHLINE_1.DELIVERYDATE) > 2014) AND (PURCHTABLE_1.VENDORREF LIKE N'%') AND 
                         (PURCHLINE_1.PURCHQTY > 0) AND (dbo.circuito.activo = 1) AND (PURCHLINE_1.REMAINPURCHPHYSICAL > 0)
ORDER BY PURCHLINE_1.ITEMID, PURCHTABLE_1.PURCHID, PURCHLINE_1.LINENUM

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[12] 4[49] 2[15] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "PURCHLINE_1"
            Begin Extent = 
               Top = 60
               Left = 731
               Bottom = 190
               Right = 1006
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PURCHTABLE_1"
            Begin Extent = 
               Top = 29
               Left = 354
               Bottom = 159
               Right = 666
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "circuito"
            Begin Extent = 
               Top = 12
               Left = 31
               Bottom = 384
               Right = 251
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "usuario_proveedor"
            Begin Extent = 
               Top = 209
               Left = 547
               Bottom = 339
               Right = 717
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sga_usuario"
            Begin Extent = 
               Top = 237
               Left = 1108
               Bottom = 367
               Right = 1299
            End
            DisplayFlags = 280
            TopColumn = 4
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 17
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
        ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VAX_EKANSTOCKCONSIGPROV';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N' Width = 2445
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 10185
         Alias = 2970
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VAX_EKANSTOCKCONSIGPROV';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VAX_EKANSTOCKCONSIGPROV';

