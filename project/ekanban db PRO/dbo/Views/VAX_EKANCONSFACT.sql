﻿CREATE VIEW dbo.VAX_EKANCONSFACT
AS
SELECT        TOP (100) PERCENT dbo.sga_usuario.username, dbo.circuito.Proveedor_id, dbo.circuito.id_circuito, dbo.circuito.ubicacion, dbo.circuito.cliente_id, dbo.circuito.ref_cliente, InventTrans_1.ITEMID, 
                         InventTrans_1.DATEPHYSICAL, InventTrans_1.PACKINGSLIPID, InventTrans_1.QTY, InventTrans_1.CUSTVENDAC, InventTrans_1.COSTAMOUNTPHYSICAL, 
                         InventTrans_1.COSTAMOUNTPHYSICAL / InventTrans_1.QTY AS Precio, InventTrans_1.TRANSREFID
FROM            SRVAXSQL.SQLAX.dbo.InventTrans AS InventTrans_1 INNER JOIN
                         dbo.circuito ON InventTrans_1.ITEMID = dbo.circuito.referencia_id AND InventTrans_1.CUSTVENDAC = dbo.circuito.Proveedor_id INNER JOIN
                         dbo.usuario_proveedor ON dbo.circuito.Proveedor_id = dbo.usuario_proveedor.id_proveedor INNER JOIN
                         dbo.sga_usuario ON dbo.usuario_proveedor.id_usuario = dbo.sga_usuario.cod_usuario
WHERE        (InventTrans_1.PACKINGSLIPID LIKE N'EK%') AND (InventTrans_1.STATUSRECEIPT = 2) AND (InventTrans_1.TRANSTYPE = 3) AND (dbo.circuito.activo = 1)
ORDER BY dbo.circuito.ubicacion, dbo.circuito.id_circuito

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[36] 4[31] 2[3] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "InventTrans_1"
            Begin Extent = 
               Top = 0
               Left = 644
               Bottom = 445
               Right = 949
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "circuito"
            Begin Extent = 
               Top = 18
               Left = 46
               Bottom = 296
               Right = 266
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "usuario_proveedor"
            Begin Extent = 
               Top = 342
               Left = 397
               Bottom = 472
               Right = 567
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sga_usuario"
            Begin Extent = 
               Top = 311
               Left = 1201
               Bottom = 537
               Right = 1392
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 18
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1005
         Width = 1005
         Width = 1005
         Width = 1605
         Width = 1005
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VAX_EKANCONSFACT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'= 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VAX_EKANCONSFACT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'VAX_EKANCONSFACT';

