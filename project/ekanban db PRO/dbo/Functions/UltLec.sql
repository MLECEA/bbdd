﻿

CREATE FUNCTION [dbo].[UltLec]
(@Circuito as nvarchar(50) )
RETURNS DATETIME
AS
BEGIN
	DECLARE @ULd as DATETIME
	DECLARE @UL as DATETIME
	
	SELECT      @ULd=   MAX(ISNULL(ultima_lectura, fecha_lectura))  --ISNULL(MAX(ultima_lectura), MAX(fecha_lectura))  
	FROM            dbo.tarjeta
	--WHERE        (estado <> 'A_MODIF') AND (sentido = 'S') AND (id_circuito = @Circuito) -- AND (gestionado IS NULL OR gestionado = '')
	WHERE        (estado <> 'A_MODIF') AND (sentido = 'S') AND (id_circuito = @Circuito) AND (ISNULL(gestionado, '') <> 'GesAUT_NumLecMin')--para que no coja los de ctd mínima de lecturas

	set @ULd=isnull(@ULd, 0)
	
	set @UL=@ULd 
	
	--IF SUBSTRING(convert(nvarchar(100), @ULD, 120), 1,4)='1900' SET @UL=''

	RETURN @UL

END



