﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetFlujoSiguientePregunta]
(
	-- Add the parameters for the function here
	@IdFlujo int,
	@Respuesta varchar(255),
	@condicion varchar(255),
	@tipoCondicion varchar(255)
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	declare @idPregunta int = -1;
	declare @cond varchar(255);
	if (@condicion is not null and @condicion != '')
	begin
		if (@tipoCondicion = 'SIG_PREGUNTA')
		begin
			--obtener el array con todas las condiciones 
			 DECLARE condiciones CURSOR  FOR select * FROM dbo.SplitString(@condicion, ';');
			 Open condiciones fetch condiciones into @cond;
			 declare @condCumplida bit = 0;
			 while(@@fetch_status=0 and @condCumplida = 0)
				begin
					if @cond <> '' and not @cond is null begin
						declare @cond1 varchar(255) = substring(@cond, 1, charindex('then', @cond) - 1);
						declare @sec varchar(255) = substring(@cond, charindex('then', @cond), len(@cond));
						declare @tipoCond varchar(10) = substring(@cond, charindex('respuesta', @cond1) + 9, charindex('[', @cond1) -  (charindex('respuesta', @cond1) + 9));
						DECLARE @VALOR VARCHAR(255) = substring(@cond1, charindex('[', @cond1) + 1, (charindex(']', @cond1)-1) - (charindex('[', @cond1) ));
						declare @idSigPregunta varchar(10) = substring(@sec, charindex('idPregunta=', @sec) + 11, len(@sec));
						
						if ltrim(rtrim(@tipoCond)) = '='
						begin
							if @Respuesta = @VALOR begin set @idPregunta = @idSigPregunta; set @condCumplida = 1 end
						end
						if ltrim(rtrim(@tipoCond)) = '<>'
						begin
							if @Respuesta <> @VALOR begin set @idPregunta = @idSigPregunta; ; set @condCumplida = 1 end
						end
						if ltrim(rtrim(@tipoCond)) = '<'
						begin
							if @Respuesta < cast(@VALOR as int) begin set @idPregunta = @idSigPregunta; ; set @condCumplida = 1 end
						end
						if ltrim(rtrim(@tipoCond)) = '>'
						begin
							if @Respuesta > cast(@VALOR as int) begin set @idPregunta = @idSigPregunta; ; set @condCumplida = 1 end
						end
					end
					
					fetch condiciones into @cond;
				end
			-- Ir tratando cada condición - si se cumple uno terminar la ejecución
			
		end
		--if (@tipoCondicion = 'RESP_PREGUNTA')
		--begin
			
		--end
	end
	CLOSE condiciones;
	
	DEALLOCATE condiciones;
	
	
	RETURN @idPregunta;

END
