﻿CREATE FUNCTION [dbo].[RoundMult] (@Valor numeric(18,2),@Multiplo int)
RETURNS nvarchar(50)
WITH EXECUTE AS CALLER
AS
BEGIN

declare @Residuo numeric(18,0)
DECLARE @ValASumar numeric(18,0)

set @Residuo=@Valor%@Multiplo

IF @Residuo=0
 BEGIN
  return @Valor
 END
 ELSE
  set @ValASumar= @Multiplo-@Residuo
  return @Valor+@ValASumar
 END