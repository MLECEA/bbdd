﻿CREATE TABLE [dbo].[p_pedidos_pendientes] (
    [linenum]                 FLOAT (53)    NOT NULL,
    [salesid]                 VARCHAR (255) NOT NULL,
    [remainsalesphysical]     FLOAT (53)    NULL,
    [qtyordered]              FLOAT (53)    NULL,
    [shippingdatereconfirmed] DATETIME      NULL,
    [shippingdaterequsted]    DATETIME      NULL,
    [co]                      VARCHAR (255) NULL,
    [externalitemid]          VARCHAR (255) NULL,
    [itemid]                  VARCHAR (255) NULL,
    [namealias]               VARCHAR (255) NULL,
    [purchorderformnum]       VARCHAR (255) NULL,
    [rownumber]               INT           NULL,
    [tipo]                    INT           NULL,
    [shippingdaterequested]   DATETIME      NULL,
    [t]                       INT           NULL,
    [color]                   INT           NULL,
    PRIMARY KEY CLUSTERED ([linenum] ASC, [salesid] ASC)
);

