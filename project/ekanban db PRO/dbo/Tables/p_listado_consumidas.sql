﻿CREATE TABLE [dbo].[p_listado_consumidas] (
    [fecha_lectura] DATETIME      NOT NULL,
    [lote]          VARCHAR (255) NOT NULL,
    [estado]        VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([fecha_lectura] ASC, [lote] ASC)
);

