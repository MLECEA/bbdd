﻿CREATE TABLE [dbo].[version_app] (
    [num_version]    VARCHAR (255) NOT NULL,
    [descripcion]    VARCHAR (255) NULL,
    [fecha_creacion] DATETIME      NULL,
    [isAppNueva]     BIT           NULL,
    [URLDescarga]    VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([num_version] ASC)
);

