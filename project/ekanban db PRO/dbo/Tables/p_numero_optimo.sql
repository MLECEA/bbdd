﻿CREATE TABLE [dbo].[p_numero_optimo] (
    [art]          VARCHAR (255) NOT NULL,
    [ctdkan]       INT           NULL,
    [cli]          VARCHAR (255) NULL,
    [cons_tar_dia] FLOAT (53)    NULL,
    [transd]       INT           NULL,
    [transh]       INT           NULL,
    [meddia]       FLOAT (53)    NULL,
    [act]          INT           NULL,
    [opt]          INT           NULL,
    [constardia]   FLOAT (53)    NULL,
    [ubi]          INT           NULL,
    PRIMARY KEY CLUSTERED ([art] ASC)
);

