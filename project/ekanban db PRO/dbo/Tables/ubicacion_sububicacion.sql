﻿CREATE TABLE [dbo].[ubicacion_sububicacion] (
    [id_ubicacion]    VARCHAR (255) NOT NULL,
    [id_subUbicacion] VARCHAR (255) NOT NULL,
    [activo]          TINYINT       CONSTRAINT [DF_ubicacion_sububicacion_activo] DEFAULT ((1)) NOT NULL,
    [orden]           INT           NULL,
    CONSTRAINT [PK_ubicacion_sububicacion] PRIMARY KEY CLUSTERED ([id_ubicacion] ASC, [id_subUbicacion] ASC),
    CONSTRAINT [FK_ubicacion_sububicacion_subUbicacion] FOREIGN KEY ([id_subUbicacion]) REFERENCES [dbo].[subUbicacion] ([id_SubUbicacion]),
    CONSTRAINT [FK_ubicacion_sububicacion_ubicacion] FOREIGN KEY ([id_ubicacion]) REFERENCES [dbo].[ubicacion] ([id_ubicacion])
);

