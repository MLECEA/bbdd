﻿CREATE TABLE [dbo].[TPedido] (
    [PedidoID]             VARCHAR (255)   NOT NULL,
    [FechaCreacion]        DATETIME        NULL,
    [FechaEntregaEstimada] DATETIME        NOT NULL,
    [Cantidad]             NUMERIC (18, 5) NULL,
    [CircuitoID]           VARCHAR (255)   NOT NULL,
    [AlbaranNum]           VARCHAR (100)   NULL,
    CONSTRAINT [PK_TPedido] PRIMARY KEY CLUSTERED ([PedidoID] ASC, [FechaEntregaEstimada] ASC, [CircuitoID] ASC)
);

