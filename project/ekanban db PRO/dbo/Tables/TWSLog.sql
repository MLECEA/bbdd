﻿CREATE TABLE [dbo].[TWSLog] (
    [TransactionID]                    INT           IDENTITY (1, 1) NOT NULL,
    [ConsumptionsReceptionConfirmed]   BIT           NULL,
    [ConsumptionsReceptionDate]        DATETIME      NULL,
    [ConsumptionsIntegrationConfirmed] BIT           NULL,
    [ConsumptionsIntegrationDate]      DATETIME      NULL,
    [OrdersIntegrationConfirmed]       BIT           NULL,
    [OrdersIntegrationDate]            DATETIME      NULL,
    [TransactionStartDate]             DATETIME      NULL,
    [TransactionUser]                  VARCHAR (255) NULL,
    [TransactionResult]                VARCHAR (255) NULL,
    [TransactionType]                  VARCHAR (255) NULL,
    CONSTRAINT [PK_TWSLog] PRIMARY KEY CLUSTERED ([TransactionID] ASC)
);

