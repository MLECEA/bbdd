﻿CREATE TABLE [dbo].[TrazabilidadManualLecturas] (
    [Lote]            VARCHAR (255) NULL,
    [CtdEtiquetas]    INT           NULL,
    [Ubicacion]       VARCHAR (100) NULL,
    [EtiquetaInicial] VARCHAR (255) NULL,
    [EtiquetaFinal]   VARCHAR (255) NULL
);


GO
GRANT ALTER
    ON OBJECT::[dbo].[TrazabilidadManualLecturas] TO [rfid]
    AS [dbo];


GO
GRANT CONTROL
    ON OBJECT::[dbo].[TrazabilidadManualLecturas] TO [rfid]
    AS [dbo];


GO
GRANT DELETE
    ON OBJECT::[dbo].[TrazabilidadManualLecturas] TO [rfid]
    AS [dbo];


GO
GRANT INSERT
    ON OBJECT::[dbo].[TrazabilidadManualLecturas] TO [rfid]
    AS [dbo];


GO
GRANT REFERENCES
    ON OBJECT::[dbo].[TrazabilidadManualLecturas] TO [rfid]
    AS [dbo];


GO
GRANT SELECT
    ON OBJECT::[dbo].[TrazabilidadManualLecturas] TO [rfid]
    AS [dbo];


GO
GRANT TAKE OWNERSHIP
    ON OBJECT::[dbo].[TrazabilidadManualLecturas] TO [rfid]
    AS [dbo];


GO
GRANT UPDATE
    ON OBJECT::[dbo].[TrazabilidadManualLecturas] TO [rfid]
    AS [dbo];


GO
GRANT VIEW DEFINITION
    ON OBJECT::[dbo].[TrazabilidadManualLecturas] TO [rfid]
    AS [dbo];


GO
GRANT VIEW CHANGE TRACKING
    ON OBJECT::[dbo].[TrazabilidadManualLecturas] TO [rfid]
    AS [dbo];

