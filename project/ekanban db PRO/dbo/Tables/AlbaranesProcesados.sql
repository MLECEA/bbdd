﻿CREATE TABLE [dbo].[AlbaranesProcesados] (
    [Albaran]    VARCHAR (100) NOT NULL,
    [Fecha]      DATETIME      NOT NULL,
    [Referencia] VARCHAR (100) NULL,
    [RecID]      BIGINT        NULL
);

