﻿CREATE TABLE [dbo].[TAccionesReports] (
    [AccRep_Idd]    NCHAR (9)   NOT NULL,
    [AccRep_Act]    TINYINT     NULL,
    [AccRep_Des]    NCHAR (100) NULL,
    [AccRep_Tip]    NCHAR (3)   NULL,
    [AccRep_LogURL] NCHAR (150) NULL,
    [AccRep_NVar]   INT         NULL,
    [AccRep_V1Txt]  NCHAR (100) NULL,
    [AccRep_V1Def]  NCHAR (100) NULL,
    [AccRep_V2Txt]  NCHAR (100) NULL,
    [AccRep_V2Def]  NCHAR (100) NULL,
    [AccRep_V3Txt]  NCHAR (100) NULL,
    [AccRep_V3Def]  NCHAR (100) NULL,
    [AccRep_CCir]   BIT         NULL,
    [AccRep_Ser]    NCHAR (100) NULL,
    [AccRep_Rep]    NCHAR (100) NULL,
    [AccRep_QRVar]  INT         NULL,
    CONSTRAINT [PK_TAccionesReports] PRIMARY KEY CLUSTERED ([AccRep_Idd] ASC)
);

