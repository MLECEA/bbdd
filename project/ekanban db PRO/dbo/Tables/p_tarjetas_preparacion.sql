﻿CREATE TABLE [dbo].[p_tarjetas_preparacion] (
    [ped]  VARCHAR (255) NOT NULL,
    [lin]  VARCHAR (255) NOT NULL,
    [ctd]  FLOAT (53)    NULL,
    [fec]  DATETIME      NULL,
    [tipo] INT           NULL,
    PRIMARY KEY CLUSTERED ([ped] ASC, [lin] ASC)
);

