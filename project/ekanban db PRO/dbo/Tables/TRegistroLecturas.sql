﻿CREATE TABLE [dbo].[TRegistroLecturas] (
    [Reg_Fec] DATETIME    NULL,
    [Reg_Dis] NCHAR (100) NULL,
    [Reg_Usu] NCHAR (100) NULL,
    [Reg_Ubi] NCHAR (100) NULL,
    [Reg_Pan] NCHAR (100) NULL,
    [Reg_Txt] NCHAR (255) NULL,
    [Reg_Cir] NCHAR (100) NULL,
    [Reg_Cli] NCHAR (100) NULL,
    [Reg_Art] NCHAR (100) NULL,
    [Reg_Lot] NCHAR (100) NULL,
    [Reg_Ser] NCHAR (100) NULL,
    [Reg_Ant] NCHAR (50)  NULL,
    [Reg_Sen] NCHAR (5)   NULL,
    [Reg_Tip] INT         NULL,
    [Reg_Ctd] NCHAR (50)  NULL,
    [Reg_Emp] NCHAR (10)  NULL
);

