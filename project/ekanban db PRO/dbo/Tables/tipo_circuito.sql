﻿CREATE TABLE [dbo].[tipo_circuito] (
    [id]          NUMERIC (19)  NOT NULL,
    [descripcion] VARCHAR (255) NULL,
    [nombre]      VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);

