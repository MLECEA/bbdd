﻿CREATE TABLE [dbo].[p_clientes] (
    [cod]         INT           NOT NULL,
    [nom]         VARCHAR (255) NULL,
    [cod_empresa] VARCHAR (255) NULL,
    [idioma]      VARCHAR (255) NULL,
    [codempresa]  VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([cod] ASC)
);

