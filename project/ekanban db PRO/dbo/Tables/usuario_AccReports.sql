﻿CREATE TABLE [dbo].[usuario_AccReports] (
    [id]         NUMERIC (19) IDENTITY (1, 1) NOT NULL,
    [id_usuario] NUMERIC (19) NOT NULL,
    [id_accrep]  NCHAR (9)    NOT NULL,
    [prioridad]  INT          NULL
);

