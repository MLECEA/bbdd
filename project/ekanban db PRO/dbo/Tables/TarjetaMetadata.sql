﻿CREATE TABLE [dbo].[TarjetaMetadata] (
    [id_lectura]          VARCHAR (255) NOT NULL,
    [sentido]             VARCHAR (10)  NOT NULL,
    [txtLectura]          VARCHAR (255) NULL,
    [tipo]                INT           NULL,
    [fechaUltimaLectura]  DATETIME      NULL,
    [fechaPrimeraLectura] DATETIME      NULL,
    [PatronLectura]       VARCHAR (255) NULL,
    CONSTRAINT [PK_TarjetaMetadata] PRIMARY KEY CLUSTERED ([id_lectura] ASC, [sentido] ASC)
);

