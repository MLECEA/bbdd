﻿CREATE TABLE [dbo].[usuario_ubicacion] (
    [id]           NUMERIC (19)  IDENTITY (1, 1) NOT NULL,
    [id_ubicacion] VARCHAR (255) NOT NULL,
    [id_usuario]   NUMERIC (19)  NOT NULL,
    [prioridad]    INT           NULL,
    [descripcion]  VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK5577C8CE315BB8B3] FOREIGN KEY ([id_usuario]) REFERENCES [dbo].[sga_usuario] ([cod_usuario]),
    CONSTRAINT [FK5577C8CE8CC191D5] FOREIGN KEY ([id_ubicacion]) REFERENCES [dbo].[ubicacion] ([id_ubicacion])
);

