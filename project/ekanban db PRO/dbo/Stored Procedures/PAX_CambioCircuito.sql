﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PAX_CambioCircuito]
	-- Add the parameters for the stored procedure here
	@ubi varchar(255) = '',
	@circuito  varchar(255) = '',
	@usuario varchar(255) = '',
	@nextPrev int = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
	BEGIN TRANSACTION;
	
	declare @ubicacion varchar(255);
	select @ubicacion = ubicacion from circuito where id_circuito = @circuito and activo = 1;
	declare @circuitoID varchar(255);
	
	declare @result table (art varchar(255), artcli varchar(255), t int, r  int, a  int, ae  int, v  int, ss  int,
				 estado int, circuito varchar(255), ultLec datetime, Cont  int, Circ  int, opt int, transito_Activado bit, ctd_Kan int);
	declare @circuitos table (idCircuito varchar(255), referenciaId varchar(255), refCliente varchar(255), estado varchar(255));
	if @nextPrev = -1
	begin
		SELECT top(1) @circuitoID = id_circuito FROM Circuito c WHERE id_circuito < @circuito AND ubicacion = @ubicacion AND activo=1
			ORDER BY id_circuito DESC
		if @circuitoID = '' or @circuitoID is null
		begin
			print '-1';
			insert into @circuitos exec PAX_SitPanelUbi @ubi, @usuario, 0;
			select top(1) @circuitoID = idCircuito from @circuitos order by idCircuito desc;
			print @circuitoID;
		end
	end
	else if @nextPrev = 0
	begin
		if @circuito = '' or @circuito is null
		begin
			--obtener los circuitos de la ubicacion y devolver los datos
			insert into @circuitos exec PAX_SitPanelUbi @ubi, @usuario, 0;
			select top(1) @circuitoID = idCircuito from @circuitos order by idCircuito DESC;
		end
		else
		begin
			set @circuitoID = @circuito;
		end
		
	end
	else if @nextPrev = 1
	begin
		SELECT top(1) @circuitoID = id_circuito FROM Circuito c WHERE id_circuito > @circuito AND ubicacion =@ubicacion AND activo=1
			ORDER BY id_circuito ASC
		if @circuitoID = '' or @circuitoID is null
		begin
			print '1';
			insert into @circuitos exec PAX_SitPanelUbi @ubi, @usuario, 0;
			select top(1) @circuitoID = idCircuito from @circuitos order by idCircuito asc;
			print @circuitoID;
		end
	end
	--print 'circuito siguiente:' + @circuitoID;
	insert into @result 
			exec PAX_SitTarjetas @circuitoID, @usuario
	select * from @result;
	commit;
END



