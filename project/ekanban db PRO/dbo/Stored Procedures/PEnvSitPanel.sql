﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PEnvSitPanel] 
	-- Add the parameters for the stored procedure here
	--@Des as varchar(40)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	 DECLARE @tableHTML  NVARCHAR(MAX) ;

	SET @tableHTML =
    N'<H1>eKanban Panela</H1>' +
    N'<table border="1">' +
    N'<tr><th>Bezeroa</th>' +
	N'<th>Kokap</th>' +
    N'<th>Erreferentzia</th>' +
	N'<th>Bez. Erref.</th>' +
	N'<th>Guztira</th>' +
    N'<th>Konsum</th>' +
	N'<th>Eskaer</th>' +
	N'<th>Bidean</th>' +
	N'<th>Bertan</th>' +
	N'<th>Gehieg</th>' +
	N'<th>AzkenKonsumoa</th>' +
	N'</tr>' +
    CAST ( ( SELECT "td/@align" =  'center', td = cliente_id,   ''
      ,"td/@align" =  'center', td = ubicacion,   ''
      ,"td/@align" =  'center',  td =  referencia_id,   '' --"td/@bgcolor" =  case when [Dis_EnError]=0 then 'green' ELSE 'red' END,
      ,td = ref_cliente,   ''
	  ,"td/@align" =  'center', td = T,   ''
	  ,"td/@align" =  'center', "td/@bgcolor" ='red', td = R,   ''
	  ,"td/@align" =  'center', "td/@bgcolor" ='yellow', td = A,   ''
	  ,"td/@align" =  'center', "td/@bgcolor" ='olive', td = AE,   ''
	  ,"td/@align" =  'center', "td/@bgcolor" ='green', td = V,   ''
      ,"td/@align" =  'center', td = SS,   ''
	  ,td = ultlec
	  FROM            dbo.VAX_SitTarjetas
--WHERE        (cliente_id = '01900200')
ORDER BY cliente_id, ubicacion, T DESC
	  FOR XML PATH('tr'), TYPE 
		) AS NVARCHAR(MAX) ) +
		N'</table>' ;

		/**
		SELECT        TOP (100) PERCENT cliente_id, ubicacion, referencia_id, ref_cliente, T, R, A, AE, V, SS
FROM            dbo.VAX_SitTarjetas
WHERE        (cliente_id = '01900200')
ORDER BY ubicacion, T DESC

		**/




	EXEC msdb.dbo.sp_send_dbmail 
	  @profile_name='posta',
	  @recipients='info@ekanban.es', --;aarandia@orkli.es
	  @subject='eKanban Panela',
	  @body = @tableHTML,
	  @body_format = 'HTML'
	  --@body='Datos de los porticos RFID:',
	  --@query = 'SELECT id_dispositivo, descripcion, ultima_conexion, Dis_EnError, ip, version FROM  ekanban.dbo.dispositivo' ,
	  --@query_result_header=0,
	  --@attach_query_result_as_file = 0
	  --@file_attachments='C:\temp\ik.txt'


	  /**







	  DECLARE @tableHTML  NVARCHAR(MAX) ;

SET @tableHTML =
    N'<H1>Work Order Report</H1>' +
    N'<table border="1">' +
    N'<tr><th>Work Order ID</th><th>Product ID</th>' +
    N'<th>Name</th><th>Order Qty</th><th>Due Date</th>' +
    N'<th>Expected Revenue</th></tr>' +
    CAST ( ( SELECT td = [id_dispositivo],   ''
      ,td = [activo],   ''
      ,td = [imei],   ''
      ,td = [num_serie],   ''
      ,td = [descripcion],   ''
      ,td = [Dis_UltAccFec]
  FROM [dbo].[dispositivo]
  FOR XML PATH('tr'), TYPE 
    ) AS NVARCHAR(MAX) ) +
    N'</table>' ;

EXEC msdb.dbo.sp_send_dbmail 
	@profile_name='posta',
	@recipients='iker@orkli.es',
    @subject = 'Work Order List',
    @body = @tableHTML,
    @body_format = 'HTML'











	  SELECT        TOP (100) PERCENT dbo.dispositivo.id_dispositivo, dbo.dispositivo.descripcion, dbo.dispositivo.ultima_conexion, dbo.dispositivo.Dis_EnError, dbo.dispositivo.ip, dbo.dispositivo.version, 
                         dbo.TRegistroEstPort.RegPor_Des, dbo.TRegistroEstPort.RegPor_Has, dbo.TRegistroEstPort.RegPor_Err, DATEDIFF(mi, dbo.TRegistroEstPort.RegPor_Des, dbo.TRegistroEstPort.RegPor_Has) AS Expr1
FROM            dbo.dispositivo INNER JOIN
                         dbo.TRegistroEstPort ON dbo.dispositivo.id_dispositivo = dbo.TRegistroEstPort.RegPor_Dis
WHERE        (dbo.dispositivo.id_dispositivo = '4') AND (dbo.TRegistroEstPort.RegPor_Err = 1) AND (DATEDIFF(mi, dbo.TRegistroEstPort.RegPor_Des, dbo.TRegistroEstPort.RegPor_Has) > 10)
ORDER BY dbo.TRegistroEstPort.RegPor_Has DESC
	  **/



END


