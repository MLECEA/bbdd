﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PAX_LanzarPedidoProcesoCompleto] 
	-- Add the parameters for the stored procedure here
	@CodCircuito varchar(20) = '',
	@cantidadTarjetas integer = 0, 
	@FechEntregaConfirmada datetime,
	@Ubicacion varchar(255) = '',
	@Usuario varchar(255) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @RESULTADO INT = -1;
	DECLARE @MSG AS VARCHAR(255);
	declare @result table (art varchar(255), artcli varchar(255), t int, r  int, a  int, ae  int, v  int, ss  int,
				 estado int, circuito varchar(255), ultLec datetime, Cont  int, Circ  int, opt int, transito_Activado bit, ctd_Kan int);				 
	declare @result2 table (Result int , Msg varchar(255), idTar varchar(255));
    -- Insert statements for procedure here
    
    declare @cantRojas int = dbo.R(@CodCircuito);
    insert into @result2
	exec PAX_LanzamientoPedidos @CodCircuito, @cantRojas, @cantidadTarjetas, @FechEntregaConfirmada, @Ubicacion, @Usuario;
	
	SELECT @RESULTADO = RESULT, @MSG = Msg FROM @result2;
	if @RESULTADO = 0 
	begin
		exec PAX_IntegrarPedidos;
	end
	
	insert into @result exec PAX_SitTarjetas @codCircuito, @Usuario;
	select *, @RESULTADO as RESULT, @MSG AS MSG from @result;
		
END
