﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PCtaGetRegistroLectura] 
	-- Add the parameters for the stored procedure here
	@idCircuito varchar(255) = ''
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT        TOP (1) *
		FROM            dbo.TRegistroLecturas
		WHERE        Ltrim(RTrim(Reg_Lot)) + '_' +  rtrim(Ltrim(Reg_Ser)) = @idCircuito
		ORDER BY Reg_Fec
		
END

