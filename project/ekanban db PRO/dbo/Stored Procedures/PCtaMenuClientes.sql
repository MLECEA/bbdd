﻿


CREATE PROCEDURE [dbo].[PCtaMenuClientes] 
(@USU varchar(255)='', @Dis int=0)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT        derivedtbl_1.localizacion as Cod, custtable_1.NAME as Nom, DATAAREAID as CodEmpresa, LanguageId as Idioma
FROM            (SELECT DISTINCT dbo.ubicacion.localizacion
                          FROM            dbo.usuario_ubicacion INNER JOIN
                                                    dbo.sga_usuario ON dbo.usuario_ubicacion.id_usuario = dbo.sga_usuario.cod_usuario INNER JOIN
                                                    dbo.ubicacion ON dbo.usuario_ubicacion.id_ubicacion = dbo.ubicacion.id_ubicacion
                          WHERE        (dbo.sga_usuario.username = @USU) AND (dbo.ubicacion.activo = 1) AND (dbo.sga_usuario.activo = 1) --AND (dbo.ubicacion.localizacion <> '00000000')
                          UNION
                          SELECT DISTINCT dbo.usuario_cliente.id_cliente
                          FROM            dbo.usuario_cliente INNER JOIN
                                                   dbo.sga_usuario AS sga_usuario_1 ON dbo.usuario_cliente.id_usuario = sga_usuario_1.cod_usuario
                          WHERE        (sga_usuario_1.username = @USU)) AS derivedtbl_1 INNER JOIN
                         SRVAXSQL.SQLAX.dbo.custtable AS custtable_1 ON derivedtbl_1.localizacion = custtable_1.ACCOUNTNUM
WHERE        (custtable_1.DATAAREAID = N'ork')



END



