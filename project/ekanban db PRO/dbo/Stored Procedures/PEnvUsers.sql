﻿


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PEnvUsers] 
	-- Add the parameters for the stored procedure here
	--@Des as varchar(40)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	 DECLARE @tableHTML  NVARCHAR(MAX) ;

	SET @tableHTML =
    N'<H1>eKanban Erabiltzaileak</H1>' +
    N'<table border="1">' +
    N'<tr><th>Izena</th>' +
	N'<th>Abizena</th>' +
    N'<th>Erabiltzailea</th>' +
	N'<th>Pasahitza</th>' +
	N'<th>Erabiltzaile moeta</th>' +
	N'</tr>' +
    CAST ( ( SELECT "td/@align" =  'center', td = sga_usuario.nombre,   ''
      ,"td/@align" =  'center', td = sga_usuario.apellidos,   ''
      ,"td/@align" =  'center',  td =  sga_usuario.username,   '' --"td/@bgcolor" =  case when [Dis_EnError]=0 then 'green' ELSE 'red' END,
      ,td = [sga_usuario].[password],   ''
	  ,"td/@align" =  'center', td = sga_rol.descripcion
		FROM            dbo.sga_usuario AS sga_usuario INNER JOIN
		dbo.sga_rol AS sga_rol ON sga_usuario.ptr_rol = sga_rol.cod_rol
		ORDER BY sga_usuario.nombre, sga_usuario.apellidos
	  FOR XML PATH('tr'), TYPE 
		) AS NVARCHAR(MAX) ) +
		N'</table>' ;

		/**
SELECT        TOP (100) PERCENT sga_usuario.nombre, sga_usuario.apellidos, sga_usuario.username, sga_usuario.password, sga_rol.descripcion
FROM            dbo.sga_usuario AS sga_usuario INNER JOIN
                         dbo.sga_rol AS sga_rol ON sga_usuario.ptr_rol = sga_rol.cod_rol
ORDER BY sga_usuario.nombre, sga_usuario.apellidos

		**/




	EXEC msdb.dbo.sp_send_dbmail 
	  @profile_name='posta',
	  @recipients='iker@orkli.es', --;aarandia@orkli.es
	  @subject='eKanban Erabiltzaileak',
	  @body = @tableHTML,
	  @body_format = 'HTML'
	  --@body='Datos de los porticos RFID:',
	  --@query = 'SELECT id_dispositivo, descripcion, ultima_conexion, Dis_EnError, ip, version FROM  ekanban.dbo.dispositivo' ,
	  --@query_result_header=0,
	  --@attach_query_result_as_file = 0
	  --@file_attachments='C:\temp\ik.txt'



END



