﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PAX_GetCircuitoByIntegrationRef] 
	-- Add the parameters for the stored procedure here
	@integrationRef varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select * from circuito where ref_IntegracionERP = @integrationRef and activo = 1;
	
END
