﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PCta_GuardarHistoricoTarjetasVerdes] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @idCircuito varchar(255);
declare @verdes int;
declare @amarillas int;
declare @rojas int;
declare @amarillasVerdes int;
declare @ss int;
DECLARE cursorCircuitos CURSOR FOR
select c.id_circuito 
from circuito c
where c.activo = 1

OPEN cursorCircuitos
FETCH NEXT FROM cursorCircuitos INTO @idCircuito

WHILE (@@FETCH_STATUS = 0)
    BEGIN
       -- PRINT @A 
        set @verdes = dbo.V(@idCircuito);
        set @amarillas = dbo.A(@idCircuito);
        set @rojas = dbo.R(@idCircuito);
        set @amarillasVerdes = dbo.AE(@idCircuito);
        set @ss = dbo.SS(@idCircuito);
        print @idCircuito + '-' + CONVERT(varchar(255), @verdes);
        insert into THistoricoVerdes (idCircuito, fecha, ctdVerdes, ctdAmarillas, CtdRojas, CtdAmarillasverdes, ctdsobrestock) values (@idCircuito, GETDATE(), @verdes,
				@amarillas, @rojas, @amarillasVerdes, @ss);
        FETCH NEXT FROM cursorCircuitos INTO @idCircuito
    END

CLOSE cursorCircuitos
DEALLOCATE cursorCircuitos
END
