﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PCtaGetAllConsumptionsByUser] 
	-- Add the parameters for the stored procedure here
	@usuario varchar(255), @pwd varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT t.id_lectura, t.fecha_lectura, t.num_lecturas, t.sentido, c.id_circuito, c.cliente_id, c.ubicacion, c.referencia_id as referencia_id, c.ref_cliente
	--FROM ((SELECT DISTINCT UU.id_ubicacion FROM  sga_usuario U INNER JOIN usuario_ubicacion UU ON U.cod_usuario = UU.id_usuario 
	--	WHERE U.username = @usuario and U.password =@pwd
	--) AS T1
	--	INNER JOIN circuito C ON C.ubicacion = T1.id_ubicacion) INNER JOIN tarjeta T ON C.id_circuito = T.id_circuito 
	--	 inner join ubicacion ub on T1.id_ubicacion = ub.id_ubicacion
	--WHERE C.activo = 1 AND (T.estado = 'A' OR T.estado = 'P') AND (T.gestionado IS NULL OR T.gestionado ='')
	--	and c.descripcion <> 'TRAZABILIDAD'
	--	and t.sentido ='S' 
	--	and ub.activo = 1
	--ORDER BY t.ubicacion, t.referencia_id
	
	declare @ULT_INTEGRACION as datetime;
	select @ULT_INTEGRACION = UltimaIntegracionERP from sga_usuario where username=@usuario and [password]=@pwd;
	if @ULT_INTEGRACION = NULL OR @ULT_INTEGRACION IS NULL set @ULT_INTEGRACION = '1900-01-01'
	
	SELECT distinct t.id_lectura, t.fecha_lectura, t.num_lecturas, t.sentido, c.id_circuito, c.cliente_id, c.ubicacion, c.referencia_id as referencia_id, c.ref_cliente
		, tr.Reg_Ctd as ctdRegistroLecturas , tr.Reg_Tip as tipo, ub.TipoEtiqueta as Patron, tr.Reg_Txt as LOTE
	FROM ((SELECT DISTINCT UU.id_ubicacion FROM  sga_usuario U INNER JOIN usuario_ubicacion UU ON U.cod_usuario = UU.id_usuario 
		WHERE U.username = @usuario and U.password =@pwd
	) AS T1
		INNER JOIN circuito C ON C.ubicacion = T1.id_ubicacion) INNER JOIN tarjeta T ON C.id_circuito = T.id_circuito 
		 inner join ubicacion ub on T1.id_ubicacion = ub.id_ubicacion
		 left outer join TRegistroLecturas tr on  Ltrim(RTrim(tr.Reg_Lot)) + '_' +  rtrim(Ltrim(tr.Reg_Ser)) = t.id_lectura
	WHERE C.activo = 1 AND (T.estado = 'A' OR T.estado = 'P') AND (T.gestionado IS NULL OR T.gestionado ='')
		and c.descripcion <> 'TRAZABILIDAD'
		and t.sentido ='S' and t.fecha_lectura > @ULT_INTEGRACION
		and ub.activo = 1 AND C.Proveedor_id IN (SELECT P1.id_proveedor FROM usuario_proveedor P1 INNER JOIN sga_usuario U1
			ON P1.id_usuario = U1.cod_usuario WHERE U1.username=@usuario AND U1.[password] = @pwd) 
			
	update sga_usuario set UltimaIntegracionERP=GETDATE() where username=@usuario and [password] = @pwd;
	--ORDER BY t.ubicacion, t.referencia_id
END
