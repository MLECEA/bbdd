﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PAX_CancelarFlujo]
	-- Add the parameters for the stored procedure here
	@idMatricula int = 0,
	@IdUsu varchar(100) = NULL,
	@cancelado bit = 1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Matricula SET Cancelado = @cancelado WHERE IdMatricula = @idMatricula
	
	select 0 as Resul, 'Flujo cancelado' as ResultMsg;
END
