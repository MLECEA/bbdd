﻿



CREATE PROCEDURE [dbo].[PCtaConsumidas] 
(
@Circuito varchar(255)='', @Usu AS varchar(255)=''
)
AS
BEGIN
	SET NOCOUNT ON;

SELECT   TOP (100) PERCENT dbo.tarjeta.estado, dbo.tarjeta.id_lectura AS lote, ISNULL(dbo.tarjeta.ultima_lectura, dbo.tarjeta.fecha_lectura) AS fecha_lectura
FROM            dbo.circuito INNER JOIN
				dbo.tarjeta ON dbo.circuito.id_circuito = dbo.tarjeta.id_circuito
	WHERE        (dbo.tarjeta.estado = 'A' OR
				dbo.tarjeta.estado = 'P') AND (dbo.tarjeta.sentido = 'S') AND (dbo.circuito.id_circuito = @Circuito) AND (dbo.tarjeta.gestionado IS NULL OR
				dbo.tarjeta.gestionado = '') AND (dbo.circuito.activo = 1)

ORDER BY dbo.tarjeta.fecha_lectura DESC

END

