﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PAX_CtaLeerFicha] 
	-- Add the parameters for the stored procedure here
	@codQR varchar(255) = '',
	@ref varchar(255)= '',
	@usuarioID varchar(255) = '',
	@ubicacionID varchar(255) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @REFMODIFICADA VARCHAR(255);
	DECLARE @OF VARCHAR(255);
	DECLARE @NUMSERIE VARCHAR(255);
	DECLARE @REFERENCIA VARCHAR(255);
	DECLARE @CANTIDAD INT;
	DECLARE @PROVEEDOR VARCHAR(255)
	DECLARE @CLIENTE VARCHAR(255);
	DECLARE @TEXTO VARCHAR(255);
	DECLARE @CTDLEIDA VARCHAR(255);
	DECLARE @IDPED VARCHAR(255);
	DECLARE @REFID VARCHAR(255) = NULL;
	DECLARE @CLIENTEID VARCHAR(255) = NULL;
	declare @circuitoID varchar(255);
	
	-----20181008 INCLUSION DE TABLA HISTORICOVERDES
	DECLARE @Origen as varchar(25);
	
	SET @Origen = 'LecturaQR'
	-----20181008 FIN DE TABLA HISTORICOVERDES
	
	
	DECLARE @TRESULT TABLE (OFAB VARCHAR(255), ARTICULO VARCHAR(255), ARTICULO_CLIENTE VARCHAR(255), DESCRIP VARCHAR(255), FPROD DATE,
		LINEA VARCHAR(255), ENG VARCHAR(255), DESENG VARCHAR(255), CTDCAJA VARCHAR(255), TENG VARCHAR(255), TDES VARCHAR(255), TCTDCAJA VARCHAR(255));
    -- Insert statements for procedure here
   declare @TTarjeta table (LOT varchar(255), SER varchar(255), ART varchar(255), CTD int, 
		PRO varchar(255), CLI varchar(255), GRAB varchar(255), CTDLEIDA varchar(255), IDPED varchar(255));
    if @codQR IS NULL OR @codQR = ''
		BEGIN
			--referencia metida manualmente
			SET @REFMODIFICADA = 'P' + 	@referencia;
			insert into @TTarjeta
				EXEC	[dbo].[PAX_PatronLec] @REFMODIFICADA, @usuarioID, @ubicacionID, 1, 'M', ' ', ' ', @Origen
				-----20181008 ORIGEN INCLUIDO PARA LAE TABLA HISTORICOVERDES
			select TOP(1) @OF = lot, @NUMSERIE = SER, @REFERENCIA=ART, @CANTIDAD = CTD, @PROVEEDOR = PRO, @CLIENTE=CLI, @TEXTO = GRAB
			, @CTDLEIDA = CTDLEIDA,  @IDPED = IDPED from @TTarjeta
			IF @REFERENCIA IS NOT NULL AND @REFERENCIA <> ''  
				BEGIN
					print @REFERENCIA;
					select @circuitoID = id_circuito from circuito where ubicacion = @ubicacionID and activo = 1 and referencia_id = @REFERENCIA
					INSERT INTO @TRESULT
						EXEC [dbo].PAX_CtaEspecifaciones null, @REFERENCIA, @usuarioID, @ubicacionID
					print 'ok';
					--return 0;
				END
			else
				begin
					--referencia en  blanco
					--select @REFERENCIA = referencia_id, @circuitoID = id_circuito from circuito where ubicacion =  @ubicacionID and activo = 1;
					--set @REFERENCIA = '';
					--set @circuitoID = '';
					if @ref is null or @ref = ''
					begin
						select @REFERENCIA = referencia_id, @circuitoID = id_circuito from circuito where ubicacion =  @ubicacionID and activo = 1;
					end
					else
					begin
						set @REFERENCIA = @ref;
						IF @circuitoID = '' OR @circuitoID IS NULL
						BEGIN
							select @circuitoID = id_circuito from circuito where ubicacion =  @ubicacionID and activo = 1 AND referencia_id= @REF;
						END
						IF @circuitoID = '' OR @circuitoID IS NULL
						BEGIN
							-- LA REFERENCIA INTRODUCIDA ES LA REF DEL CLIENTE
							select @circuitoID = id_circuito from circuito where ubicacion =  @ubicacionID and activo = 1 AND ref_cliente= @REF;
						END
					end
					INSERT INTO @TRESULT
						EXEC [dbo].PAX_CtaEspecifaciones null, @REFERENCIA, @usuarioID, @ubicacionID
					--return 11;
				end
		END
    ELSE
		BEGIN
			--FICHA DESDE EL CODIGO QR
			--print 'Lectura con QR'
			insert into @TTarjeta
				EXEC	[dbo].[PAX_PatronLec] @codQR, @usuarioID, @ubicacionID, 1, 'M', ' ', ' ', @Origen
			select TOP(1) @OF = lot, @NUMSERIE = SER, @REFERENCIA=ART, @CANTIDAD = CTD, @PROVEEDOR = PRO, @CLIENTE=CLI, @TEXTO = GRAB
			, @CTDLEIDA = CTDLEIDA,  @IDPED = IDPED from @TTarjeta
			select @circuitoID = id_circuito from circuito where ubicacion = @ubicacionID and activo = 1 and referencia_id = @REFERENCIA
			print @TEXTO
			IF @TEXTO <> '_'
			BEGIN
				IF @OF <> '000000'
				BEGIN
					--PRINT 'OF NO ES 000000'
					SELECT @REFID = ITEMID, @CLIENTEID = CUSTACCOUNT FROM VAX_EKAN_OF WHERE PRODID= @OF
					IF @REFID IS NULL AND @CLIENTEID IS NULL
					BEGIN
						RETURN 6 --orden de fabricación inexisistente
					END
					
				END	
				else
				begin
					-- OF es 000000
					--PRINT 'OF ES 000000'
					--print @REFERENCIA 
					INSERT INTO @TRESULT
					EXEC [dbo].PAX_CtaEspecifaciones null, @REFERENCIA, @usuarioID, @ubicacionID
				end
				PRINT @REFID;
				PRINT @CLIENTEID;
				IF @REFID IS NOT NULL AND @CLIENTEID IS NOT NULL
				BEGIN
					INSERT INTO @TRESULT
					EXEC [dbo].PAX_CtaEspecifaciones @OF, @REFERENCIA, @usuarioID, @ubicacionID	
				END
			END
			ELSE
			BEGIN
				RETURN 6;
			END
		END
		PRINT @circuitoID;
		SELECT *, @circuitoID as circuito FROM @TRESULT;
END

