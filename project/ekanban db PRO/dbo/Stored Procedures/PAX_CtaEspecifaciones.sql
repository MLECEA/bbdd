﻿
-- =============================================
-- Author:		IKER
-- Create date: 6-5-2014
-- Description:	Procedimiento para obtener datos del artículo
-- =============================================
CREATE PROCEDURE [dbo].[PAX_CtaEspecifaciones]
(
@OF varchar(255)='', @Articulo AS varchar(255)='',  @Usur AS varchar(255)='',  @Ubi AS varchar(255)=''
)
AS
BEGIN
	SET NOCOUNT ON;
	declare @Fam as varchar(25)=''
	declare @ArtCli as Varchar (100)='Artcli'
	DECLARE @Descri as varchar (255)
	DECLARE @Cliente AS varchar(25)--='00000000'
	declare @FecProd as date =''
	Declare @Lin as varchar (25)=''
	declare @Eng as varchar (25)=''
	declare @Des as varchar (25)=''
	declare @CtdCaja as varchar (25)=''
	declare @TEng as varchar(25)=''
	declare @TDes as varchar(25)=''
	declare @TCtdCaja as varchar(25)='Qty/Box'
	declare @Dispos as varchar(25)=''
	DECLARE @UltCon as datetime
	DECLARE @IP as varchar(49)=''
	DECLARE @Err as bit
		

	-- SI DISPONEMOS DE LA OF. CONSULTAMOS EN LA TABLA DE OFs EL ARTÍCULO Y CLIENTE.
	
	if @OF>'' 
	
		begin
			SET @Articulo=''
			SELECT @Articulo=ITEMID, @Cliente=CUSTACCOUNT, @FecProd=ECTD_FEC, @Lin=WRKCTRID
				FROM dbo.VAX_EKAN_OF
				WHERE PRODID = @OF

			SET @Articulo=isnull(@Articulo,'')

			SELECT @ArtCli=CASE WHEN EXTERNALITEMID = '' THEN INVENTTABLE.ITEMID ELSE EXTERNALITEMID END, 
				@Descri=INVENTTABLE.ITEMNAME, @Fam=INVENTTABLE.ItemGroupId, @CtdCaja=CONVERT(int, INVENTTABLE.TAXPACKAGINGQTY)
			FROM SRVAXSQL.SQLAX.dbo.CUSTVENDEXTERNALITEM AS CUSTVENDEXTERNALITEM RIGHT OUTER JOIN
				SRVAXSQL.SQLAX.dbo.INVENTTABLE AS INVENTTABLE ON CUSTVENDEXTERNALITEM.DATAAREAID = INVENTTABLE.DATAAREAID AND CUSTVENDEXTERNALITEM.ITEMID = INVENTTABLE.ITEMID
			WHERE CUSTVENDEXTERNALITEM.CUSTVENDRELATION = @Cliente AND INVENTTABLE.DATAAREAID = N'ORK' AND INVENTTABLE.ITEMID = @Articulo
		end 
	
	else
		--	NO disponemos de OF sino sólo del artículo a consultar.
		begin		
				SELECT @Descri=ITEMNAME, @CtdCaja=CONVERT(int, TAXPACKAGINGQTY), @Fam=ItemGroupId 
				FROM  SRVAXSQL.SQLAX.dbo.INVENTTABLE AS INVENTTABLE
				WHERE ITEMID = @Articulo AND DATAAREAID = N'ORK'
		
				SELECT        @ArtCli=ref_cliente
				FROM            dbo.circuito
				WHERE        (referencia_id = @Articulo) AND (ubicacion = @Ubi)
				
				SELECT        @Dispos=dbo.dispositivo_ubicacion.id_dispositivo, @UltCon=dbo.dispositivo.ultima_conexion, @ip=dbo.dispositivo.ip, @err=dbo.dispositivo.Dis_EnError
				FROM            dbo.dispositivo_ubicacion INNER JOIN
							dbo.dispositivo ON dbo.dispositivo_ubicacion.id_dispositivo = dbo.dispositivo.id_dispositivo
				WHERE        (dbo.dispositivo_ubicacion.id_ubicacion = @Ubi)

				set @Dispos=isnull(@dispos,'')

				SET @OF=@err---??????????????
				SET @FecProd=@UltCon
				SET @Lin=@Dispos
		end

		

	if SUBSTRING(@fam,1,3)='102'
		begin
			-- datos reales de eng y des de la IDS en SRVSQL cabeceras y ensayos. PTE.
			set @TEng='I. Eng'
			SELECT        @Eng= ATTRIBUTEVALUE
			FROM            SRVAXSQL.SQLAX.dbo.AOITEMATTRIBUTES AS AOITEMATTRIBUTES_1
			WHERE        ITEMID = @Articulo AND DATAAREAID = N'ork' AND ATTRIBUTEID = N'ATR0200001'
			
			set @TDes='I. Des'
			SELECT        @Des= ATTRIBUTEVALUE
			FROM            SRVAXSQL.SQLAX.dbo.AOITEMATTRIBUTES AS AOITEMATTRIBUTES_1
			WHERE        ITEMID = @Articulo AND DATAAREAID = N'ork' AND ATTRIBUTEID = N'ATR0200003'
	
			--if @eng='80' set @eng='110'
			if convert(int, @eng)>60 and convert(int, @eng)<110 set @eng='110'
			if convert(int, @eng)  <60 set @eng='60'
		end
		ELSE
		begin
			set @eng='0'--necesarios, sino da error el android
			set @DES='0' --necesarios, sino da error el android
		end


		
	--IF @Dispos<>''
		--BEGIN
		--	set @eng='2'--@IP
		--	set @DES='0'
			--set @CtdCaja='A'
		--	set @TCtdCaja='C'
	--	END
/**


		--El programa no acepta todavía, valores en eng, des y ctd caja que sean no numéricos. '' falla, ' '  también falla y hay que poner sólo numeros. 
		--pte de la modificación del programa por parte de KOldo. 
		if @CtdCaja='1'
		begin
			set @CtdCaja=''
			set @TCtdCaja=''
		end
		**/
		--set @OF=ISNULL(@OF, '000000')
		--set @Lin=isnull(@Lin,'0')
		--set @Descri=isnull(@Descri, 'xx')
		--set @FecProd=isnull(@FecProd,getdate())
		if @CtdCaja='' set @CtdCaja='0'
		if @TEng='' set @TEng=' ' --beharrezkoa bestela errorea ematen du
		if @TDes='' set @TDes=' ' --beharrezkoa bestela errorea ematen du
		set @Descri=isnull(@Descri,'')
		set @FecProd=isnull(@FecProd,CAST('00 AM' AS datetime))

		select @OF as 'OF', @Articulo AS ART, @ArtCli AS ARTCLI, @Descri AS DESCRIP, @FecProd AS FECPROD, @Lin AS LIN, @Eng as ENG, @Des as DES, @CtdCaja AS CTDCAJA, @TEng as TENG, @TDes as TDES, @TCtdCaja as TCTDCAJA

		EXEC	[dbo].[PRegistro]
		@Reg_Usu = @Usur,
		@Reg_Ubi = @Ubi,
		@Reg_opt = N'Cta Especific',
		@Reg_Art = @Articulo,
		@Reg_cli = @Cliente,
		@Reg_Txt = @OF


END


